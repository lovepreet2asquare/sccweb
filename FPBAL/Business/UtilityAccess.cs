﻿//using GSDSDAL;
using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using FPDAL.Data;
using System.Web;
using System.Configuration;
using FPModels.Models;
using System.Web.Mvc;
using System.Globalization;

namespace FPBAL.Business
{
    public class UtilityAccess : ActionFilterAttribute
    {
        public static string BaseUrl = ConfigurationManager.AppSettings["baseurl"].ToString();
        public enum DropdownType
        {
            All = 0,
            Required = 1,
            NoRequired = 2,
        }
        public static CultureInfo enUS = new CultureInfo("en-US");
        public static string[] formatStrings = new string[] {
             "MM-dd-yyyy","MM-dd-yyyy hh:mm", "MM-dd-yyyy hh:mm:ss","MM-dd-yyyy hh:mm:ss tt", "MM-dd-yyyy hh:mm tt",
             "MM/dd/yyyy","MM/dd/yyyy hh:mm", "MM/dd/yyyy hh:mm:ss","MM/dd/yyyy hh:mm:ss tt", "MM/dd/yyyy hh:mm tt",
             "yyyy-MM-dd","yyyy-MM-dd hh:mm", "yyyy-MM-dd hh:mm:ss","yyyy-MM-dd hh:mm:ss tt", "yyyy-MM-dd hh:mm tt",
             "yyyy/MM/dd","yyyy/MM/dd hh:mm", "yyyy/MM/dd hh:mm:ss","yyyy/MM/dd hh:mm:ss tt", "yyyy/MM/dd hh:mm tt",
             "MM/dd/yyyy", "MM/dd/yyyy HH:mm", "MM/dd/yyyy HH:mm:ss", "MM/dd/yyyy HH:mm:ss tt", "MM/dd/yyyy HH:mm tt",
             "dd/MMM/yyyy", "dd/MMM/yyyy HH:mm", "dd/MMM/yyyy HH:mm:ss", "dd/MMM/yyyy HH:mm:ss tt", "dd/MMM/yyyy HH:mm tt",
             "MMM-dd-yyyy", "MMM-dd-yyyy HH:mm", "MMM-dd-yyyy HH:mm:ss", "MMM-dd-yyyy HH:mm:ss tt", "MMM-dd-yyyy HH:mm tt",
             "MMM/dd/yyyy", "MMM/dd/yyyy HH:mm", "MMM/dd/yyyy HH:mm:ss", "MMM/dd/yyyy HH:mm:ss tt", "MMM/dd/yyyy HH:mm tt", };

        public static List<SelectListItem> MonthList(int Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }
                //int months = 12;
                //int currentMonth = DateTime.Now.Month;
                //string Year = string.Empty;
                //if (yearFrom <= yearTo)
                //{
                //    for (yearFrom = 2000; yearFrom <= yearTo; yearFrom++)
                //    {
                //        Year = Convert.ToString(yearFrom);
                //        _items.Add(new SelectListItem { Value = Year, Text = Year });
                //    }
                //}

                _items.Add(new SelectListItem { Value = "January", Text = "January" });
                _items.Add(new SelectListItem { Value = "February", Text = "February" });
                _items.Add(new SelectListItem { Value = "March", Text = "March" });
                _items.Add(new SelectListItem { Value = "April", Text = "April" });
                _items.Add(new SelectListItem { Value = "May", Text = "May" });
                _items.Add(new SelectListItem { Value = "June", Text = "June" });
                _items.Add(new SelectListItem { Value = "July", Text = "July" });
                _items.Add(new SelectListItem { Value = "August", Text = "August" });
                _items.Add(new SelectListItem { Value = "September", Text = "September" });
                _items.Add(new SelectListItem { Value = "October", Text = "October" });
                _items.Add(new SelectListItem { Value = "November", Text = "November" });
                _items.Add(new SelectListItem { Value = "December", Text = "December" });
                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "MonthList");
                return _items;
            }
        }
        public static List<SelectListItem> YearList(int Type)
        {
            DataTable dt = new DataTable();
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }
                int yearFrom = 2000;
                int yearTo = DateTime.Now.Year;
                string Year = string.Empty;
                if (yearFrom <= yearTo)
                {
                    for (yearFrom = 2000; yearFrom <= yearTo; yearFrom++)
                    {
                        Year = Convert.ToString(yearFrom);
                        _items.Add(new SelectListItem { Value = Year, Text = Year });
                    }
                }


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "YearList");
                return _items;
            }
        }
        public static List<SelectListItem> RenderList(DataTable dt, Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]) });
                    }
                }


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }


        public static List<SelectListItem> RenderCountryList(DataTable dt, Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]) });
                    }
                }


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }


        public static List<SelectListItem> RenderPaymentMethodList(DataTable dt, Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = UtilityAccess.Encrypt(Convert.ToString(row["Value"])), Text = Convert.ToString(row["Text"]) });
                    }
                }


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> RenderList1(DataTable dt, Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]) });
                    }
                }


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> RenderStatusList(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                _items.Add(new SelectListItem { Value = "Published", Text = "Schedule" });
                _items.Add(new SelectListItem { Value = "Reject", Text = "Reject" });


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> RenderIndividualList(DataTable dt, Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]), Selected = Convert.ToBoolean(row["IsSelected"]) });
                    }
                }


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> RenderIsRecurringList(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                _items.Add(new SelectListItem { Value = "false", Text = "No" });
                _items.Add(new SelectListItem { Value = "true", Text = "Yes" });
                


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }
        public static List<SelectListItem> RenderMonthList(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                _items.Add(new SelectListItem { Value = "01", Text = "01" });
                _items.Add(new SelectListItem { Value = "02", Text = "02" });
                _items.Add(new SelectListItem { Value = "03", Text = "03" });
                _items.Add(new SelectListItem { Value = "04", Text = "04" });
                _items.Add(new SelectListItem { Value = "05", Text = "05" });
                _items.Add(new SelectListItem { Value = "06", Text = "06" });
                _items.Add(new SelectListItem { Value = "07", Text = "07" });
                _items.Add(new SelectListItem { Value = "08", Text = "08" });
                _items.Add(new SelectListItem { Value = "09", Text = "09" });
                _items.Add(new SelectListItem { Value = "10", Text = "10" });
                _items.Add(new SelectListItem { Value = "11", Text = "11" });
                _items.Add(new SelectListItem { Value = "12", Text = "12" });
              return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }
        public static List<SelectListItem> RenderYearList(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                _items.Add(new SelectListItem { Value = "20", Text = "20" });
                _items.Add(new SelectListItem { Value = "21", Text = "21" });
                _items.Add(new SelectListItem { Value = "22", Text = "22" });
                _items.Add(new SelectListItem { Value = "23", Text = "23" });
                _items.Add(new SelectListItem { Value = "24", Text = "24" });
                _items.Add(new SelectListItem { Value = "25", Text = "25" });
                _items.Add(new SelectListItem { Value = "26", Text = "26" });
                _items.Add(new SelectListItem { Value = "27", Text = "27" });
                _items.Add(new SelectListItem { Value = "28", Text = "28" });
                _items.Add(new SelectListItem { Value = "29", Text = "29" });
                _items.Add(new SelectListItem { Value = "30", Text = "30" });
                _items.Add(new SelectListItem { Value = "31", Text = "31" });
                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }
        public static List<SelectListItem> RenderISDCodeListNew( Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                _items.Add(new SelectListItem { Value = "+1", Text = "+1" });
                _items.Add(new SelectListItem { Value = "+44", Text = "+44" });
                _items.Add(new SelectListItem { Value = "+91", Text = "+91" });


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> RenderISDCodeList(DataTable dt, Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                _items.Add(new SelectListItem { Value = "+1", Text = "+1" });
                _items.Add(new SelectListItem { Value = "+44", Text = "+44" });
                _items.Add(new SelectListItem { Value = "+91", Text = "+91" });


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> RenderCountryList(DataTable dt)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                _items.Add(new SelectListItem { Value = "231", Text = "United States" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]) });
                    }
                }
                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderCountryList");
                return _items;
            }
        }

        public static List<SelectListItem> RenderStateList(DataTable dt)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]) });
                    }
                }
                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderCountryList");
                return _items;
            }
        }
        public static List<SelectListItem> RenderCampaignList(DataTable dt)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]) });
                    }
                }
                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderCampaignList");
                return _items;
            }
        }
        public static List<SelectListItem> StatusListValTextNew(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            DropdownType drpType = (DropdownType)Type;
            switch (drpType)
            {
                case DropdownType.All:
                    _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                    break;
                case DropdownType.Required:
                    _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                    break;
                case DropdownType.NoRequired:
                    _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                    break;
            }

            _items.Add(new SelectListItem { Value = "Active", Text = "Active" });
            _items.Add(new SelectListItem { Value = "Inactive", Text = "Inactive" });

            return _items;
        }

        public static List<SelectListItem> StatusListValTextNew()
        {
            List<SelectListItem> _items = new List<SelectListItem>();


            _items.Add(new SelectListItem { Value = "1", Text = "Active" });
            _items.Add(new SelectListItem { Value = "0", Text = "Inactive" });

            return _items;
        }
        public static List<SelectListItem> ActiveStatusListforvender(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }
                _items.Add(new SelectListItem { Value = "Approved", Text = "Approved" });
                _items.Add(new SelectListItem { Value = "DisApproved", Text = "DisApproved" });
             
                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> ActiveStatusList(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }
                _items.Add(new SelectListItem { Value = "Active", Text = "Active" });
                _items.Add(new SelectListItem { Value = "InActive", Text = "Inactive" });
                _items.Add(new SelectListItem { Value = "Invited", Text = "Invited" });
                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> StatusListValText(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            DropdownType drpType = (DropdownType)Type;
            switch (drpType)
            {
                case DropdownType.All:
                    _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                    break;
                case DropdownType.Required:
                    _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                    break;
                case DropdownType.NoRequired:
                    _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                    break;
            }

            _items.Add(new SelectListItem { Value = "1", Text = "Active" });
            _items.Add(new SelectListItem { Value = "0", Text = "Inactive" });

            return _items;
        }
        public static List<SelectListItem> StatusList(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }
                _items.Add(new SelectListItem { Value = "Published", Text = "Published" });
                _items.Add(new SelectListItem { Value = "Scheduled", Text = "Scheduled" });
                _items.Add(new SelectListItem { Value = "Archived", Text = "Archived" });


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> ActiveStatusListNew(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }
                _items.Add(new SelectListItem { Value = "Active", Text = "Active" });
                _items.Add(new SelectListItem { Value = "InActive", Text = "Inactive" });

                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> DonationStatusList(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }
                _items.Add(new SelectListItem { Value = "Paid", Text = "Paid" });
                _items.Add(new SelectListItem { Value = "Refunded", Text = "Refunded" });
                _items.Add(new SelectListItem { Value = "Unsettled", Text = "Unsettled" });

                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }
        public static List<SelectListItem> ContactStatusList(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }
                _items.Add(new SelectListItem { Value = "New", Text = "New" });
                _items.Add(new SelectListItem { Value = "Contacted", Text = "Contacted" });
                _items.Add(new SelectListItem { Value = "InProcess", Text = "In Process" });


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }
        public static List<SelectListItem> PaymentStatusList(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "2", Text = "--Status--" });
                        break;
                    case DropdownType.NoRequired:
                        //_items.Add(new SelectListItem { Value = "0", Text = "All" });
                        break;
                }
                _items.Add(new SelectListItem { Value = "Paid", Text = "Paid" });
                _items.Add(new SelectListItem { Value = "Skipped", Text = "Skipped" });
                _items.Add(new SelectListItem { Value = "Stopped", Text = "Stopped" });
                _items.Add(new SelectListItem { Value = "Refunded", Text = "Refunded" });
                _items.Add(new SelectListItem { Value = "Scheduled", Text = "Scheduled" });
                _items.Add(new SelectListItem { Value = "Unsettled", Text = "Unsettled" });

                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> ActiveStatusList1(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }
                _items.Add(new SelectListItem { Value = "Active", Text = "Active" });
                _items.Add(new SelectListItem { Value = "InActive", Text = "Inactive" });
                //_items.Add(new SelectListItem { Value = "Invited", Text = "Invited" });
                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> ServiceList1(DataTable dt, Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]) });
                    }
                }


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> PaymentMethodList(Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }
                _items.Add(new SelectListItem { Value = "Cheque", Text = "Cheque" });
                _items.Add(new SelectListItem { Value = "Credit Card", Text = "Credit Card" });
                _items.Add(new SelectListItem { Value = "ACH", Text = "ACH" });
                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }

        public static List<SelectListItem> AppList1(DataTable dt, Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]) });
                    }
                }


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }



        public static List<SelectListItem> CategoryList(DataTable dt, Int32 Type)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                DropdownType drpType = (DropdownType)Type;
                switch (drpType)
                {
                    case DropdownType.All:
                        _items.Add(new SelectListItem { Value = "-1", Text = "All" });
                        break;
                    case DropdownType.Required:
                        _items.Add(new SelectListItem { Value = "", Text = "--Select--" });
                        break;
                    case DropdownType.NoRequired:
                        _items.Add(new SelectListItem { Value = "0", Text = "--Select--" });
                        break;
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]) });
                    }
                }


                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderList");
                return _items;
            }
        }
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
        public static double DateTimeToUnixTimestamp(DateTime dateTime)
        {
            return (dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
        }
        public static String SessionGet(String key)
        {
            try
            {
                String sValue = String.Empty;
                if (HttpContext.Current.Session[key] != null)
                    sValue = Convert.ToString(HttpContext.Current.Session[key]);
                else if (HttpContext.Current.Request.Cookies["FPCookie"][key] != null)
                {
                    HttpContext.Current.Session[key] = HttpContext.Current.Request.Cookies["FPCookie"][key];
                    sValue = HttpContext.Current.Request.Cookies["FPCookie"][key];
                }

                return sValue;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilitiesAccess", "SessionGet");
                return "";
            }
        }
        #region Database_Call
        UtilityData utilityData = new UtilityData();
        public List<SelectListItem> StatesByCountry(Int32 CountryId, Boolean IsAll)
        {
            try
            {
                int i = 0;
                if (IsAll == false)
                    i = 1;
                return RenderList(utilityData.StatesByCountry(CountryId).Tables[0], i);
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "StatesByCountry");
                return null;
            }
        }
        #endregion Database_Call

        public static string Encrypt(string strText)
        {
            try
            {
                Exception exObj;
                string strEncryptedText;
                if (DevelopTech.security.Encrypt(strText, out strEncryptedText, out exObj) == -1)
                {
                    //ExceptionUtility.LogException(exObj, "converter class");
                    ApplicationLogger.LogError(exObj, "Utilities", "Encrypt");
                }
                return strEncryptedText;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "Utilities", "Encrypt");
                // ExceptionUtility.LogException(ex, "converter class");
                return "";
            }
        }

        public static string Decrypt(string strText)
        {
            try
            {
                strText = strText.Replace(" ", "+");
                Exception exObj;
                string strDecryptedText;

                if (DevelopTech.security.Decrypt(strText, out strDecryptedText, out exObj) == -1)
                {
                    //ExceptionUtility.LogException(exObj, "converter class");
                    ApplicationLogger.LogError(exObj, "Utilities", "Decrypt");
                }
                return strDecryptedText;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "Utilities", "Decrypt");
                //ExceptionUtility.LogException(ex, "converter class");
                return "";
            }
        }

        public static Int32 SendEmailForChangepassword(string Email, string FirstName)
        {
            Int32 RetVal = -1;
            try
            {
                //if (model.Email)
                //{
                String url = ConfigurationManager.AppSettings["baseurl"].ToString();
                String _subject = "Email Response";
                String _body = EmailUtility.ReadHtml("user/ContactUs.html");
                if (!String.IsNullOrEmpty(_body))
                {
                    _body = _body.Replace("{firstname}", FirstName)


                        .Replace("{urllink}", url)
                        .Replace("{Email}", Email ?? "");

                    RetVal = EmailUtility.SendEmail("SCC Team", Email, _subject, _body, true);
                }
                //}
                return RetVal;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilitiesAccess", "SendEmailForContactUsApi");
                return -1;
            }
        }

        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }


        public static String FromDate(String date = null)
        {
            try
            {
                var timeZone = "0";
                int hours = 0, minutes = 0;
                string operator_ = "-";
                if (date == null)
                {
                    HttpCookie seenAlertsCookie = HttpContext.Current.Request.Cookies["time_zone"];
                    if (seenAlertsCookie != null)
                    {
                        try
                        {
                            timeZone = seenAlertsCookie.Value == "" ? "0" : seenAlertsCookie.Value;
                        }
                        catch (Exception) { }
                    }
                    if (timeZone != "0")
                    {
                        operator_ = timeZone.Substring(0, 1);
                        timeZone = timeZone.Substring(1);
                        var splitstr = timeZone.Split(':');
                        hours = Convert.ToInt32(splitstr[0]) * (operator_ == "+" ? 1 : -1);
                        minutes = Convert.ToInt32(splitstr[1]) * (operator_ == "+" ? 1 : -1);
                    }
                }
                var dt = UtilityAccess.NullableTryParseDateTime(date) ?? null;
                return (dt != null ? dt.Value.AddHours(hours).AddMinutes(minutes).ToString("MM/dd/yyyy") : DateTime.UtcNow.AddHours(hours).AddMinutes(minutes).ToString("MM/dd/yyyy"));
            }
            catch
            {
                return "";
            }
        }
        public static String ToDate(String date = null)
        {
            try
            {
                var timeZone = "0";
                int hours = 0, minutes = 0;
                string operator_ = "-";
                if (date == null)
                {
                    HttpCookie seenAlertsCookie = HttpContext.Current.Request.Cookies["time_zone"];
                    if (seenAlertsCookie != null)
                    {
                        try
                        {
                            timeZone = seenAlertsCookie.Value == "" ? "0" : seenAlertsCookie.Value;
                        }
                        catch (Exception) { }
                    }
                    if (timeZone != "0")
                    {
                        operator_ = timeZone.Substring(0, 1);
                        timeZone = timeZone.Substring(1);
                        var splitstr = timeZone.Split(':');
                        hours = Convert.ToInt32(splitstr[0]) * (operator_ == "+" ? 1 : -1);
                        minutes = Convert.ToInt32(splitstr[1]) * (operator_ == "+" ? 1 : -1);
                    }
                }
                var dt = UtilityAccess.NullableTryParseDateTime(date) ?? null;
                return (dt != null ? dt.Value.AddHours(hours).AddMinutes(minutes).ToString("MM/dd/yyyy") + " 23:59:59" : DateTime.UtcNow.AddHours(hours).AddMinutes(minutes).ToString("MM/dd/yyyy") + " 23:59:59");
            }
            catch
            {
                return "";
            }
        }
        public static DateTime? NullableTryParseDateTime(string text = null)
        {
            DateTime value;
            return DateTime.TryParseExact(text, formatStrings, enUS, 0, out value) ? value : (DateTime?)null;
        }

        public static List<SelectListItem> DateFilterList = new List<SelectListItem>()
        {
               new SelectListItem() {Text= "--Select--", Value = ""},
            new SelectListItem() {Text= "Daily", Value = "Daily"},
            new SelectListItem() { Text= "Weekly", Value = "Weekly"},
            new SelectListItem() { Text= "Monthly", Value = "Monthly"},
            new SelectListItem() { Text= "Yearly", Value = "Yearly"},
            new SelectListItem() { Text= "Custom", Value = "Custom"}
        };


        public static void GetFromToDate(string Format, out string DateFrom, out string DateTo)
        {
            DateTo = string.Empty;
            DateFrom = string.Empty;
            string FDate = string.Empty;
            DateTime? df = null;
            int hours = 0, minutes = 0;
            try
            {
                if (Format == "Yearly")
                {
                    FDate = DateTime.Now.AddYears(-1).ToString();
                    df = NullableTryParseDateTime(FDate) ?? null;
                    DateFrom = (df != null ? df.Value.AddYears(-1).AddHours(hours).AddMinutes(minutes).ToString("MM/dd/yyyy") + " 00:00" : DateTime.UtcNow.AddYears(-1).AddHours(hours).AddMinutes(minutes).ToString("MM/dd/yyyy") + " 00:00");
                }

                df = NullableTryParseDateTime(FDate) ?? null;
                DateFrom = (df != null ? df.Value.AddHours(hours).AddMinutes(minutes).ToString("MM/dd/yyyy") + " 00:00" : DateTime.UtcNow.AddHours(hours).AddMinutes(minutes).ToString("MM/dd/yyyy") + " 00:00");

                string TDate = DateTime.Now.ToString();
                DateTime? dt = NullableTryParseDateTime(TDate) ?? null;
                DateTo = (dt != null ? dt.Value.AddHours(hours).AddMinutes(minutes).ToString("MM/dd/yyyy") + " 23:59" : DateTime.UtcNow.AddHours(hours).AddMinutes(minutes).ToString("MM/dd/yyyy") + " 23:59");
            }
            catch
            {
                //return "";
            }
        }

        public static List<SelectListItem> RenderZoneList(DataTable dt)
        {
            List<SelectListItem> _items = new List<SelectListItem>();
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        _items.Add(new SelectListItem { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]) });
                    }
                }
                return _items;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityAccess", "RenderZoneList");
                return _items;
            }
        }
        public static double GetTimeZoneInSeconds(DateTime dt, string TimeZoneId)
        {
            //var dt = Convert.ToDateTime("2019-05-03 04:54:38.627");//DateTime.UtcNow;
            //var offset2 = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            //var SystemTimeZones = TimeZoneInfo.GetSystemTimeZones();
            var tz = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId);
            var utcOffset = new DateTimeOffset(dt, TimeSpan.Zero);
            var off_set = utcOffset.ToOffset(tz.GetUtcOffset(utcOffset));

            TimeSpan delta = off_set.Offset;
            double utcSecondOffset = delta.TotalSeconds;

            return utcSecondOffset;
        }

        public static String MaskAccountNo(String ssn)
        {
            try
            {

                string ssn_mask = "";
                if (ssn != null)
                {
                    if (ssn.Length >= 4)
                        ssn_mask = "XXXXXX" + ssn.Substring(ssn.Length - 4);
                }

                return ssn_mask;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "Utilities", "MaskAccountNo");
                return null;
            }
        }

        public static String MaskCardNo(String cn)
        {
            try
            {

                string cn_mask = "";
                if (cn.Length >= 4)
                    cn_mask = "XXXX" + cn.Substring(cn.Length - 4);

                return cn_mask;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "Utilities", "MaskCardNo");
                return null;
            }
        }
        public static String CurrencyFormat(Decimal Amount)
        {
            String _Amount = "0.00";
            if (Amount > 0)
                _Amount = String.Format("{0:n}", Amount);

            //_Amount= Amount.ToString("C", new CultureInfo("en-US"));

            return _Amount;
        }

        public static String NumberFormat(Int64 Number)
        {
            String _Number = "0";
            if (Number > 0)
                _Number = String.Format("{0:n0}", Number);

            return _Number;
        }
    }

    public class Response
    {
        public static String Message(Int32 responseType)
        {
            if (responseType == 0)
                return "We hit a snag, please try again after some time.";
            else if (responseType == 1)
                return "Success";
            else if (responseType == 2)
                return "Retrieved successfully";
            else if (responseType == -5)
                return "Authentication failed";
            else if (responseType == -3)
                return "Email already exists!";
            else if (responseType == -4)
                return "Cannot process!";
            else if (responseType == -1)
                return "Technical error";
            else if (responseType == -2)
                return "No record found";
            else if (responseType == -16)
                return "The old password you have entered is incorrect";
            else if (responseType == 11)
                return "Data saved successfully";
            else if (responseType == 12)
                return "Data updated successfully";
            else if (responseType == 13)
                return "Data deleted successfully";
            else if (responseType == 14)
                return "Payment card set as primary successfully";
            else if (responseType == 15)
                return "Payment processed successfully";
            else if (responseType == 16)
                return "Your password has been changed successfully";
            else if (responseType == 17)
                return "Successfully disabled!!";
            else if (responseType == 18)
                return "Your account has been re-activated.";
            else if (responseType == 19)
                return "We have recieved your account delete request. Your account will be deleted with in 7 days. If you want you can re-activate your account with in 7 days";
            else if (responseType == 20)
                return "Data archived successfully";
            else if (responseType == 21)
                return "We have received your message and would like to thank you for writing to us. we will reply by phone / email as soon as possible.";
            else if (responseType == 23)
                return "Record already exists!";
            else if (responseType == 24)
                return "Data not available, please change your search criteria !";
            else if (responseType == 25)
                return "Your transaction is under process!";
            else if (responseType == 26)
                return "Your refund is successfull!";
            else if (responseType == 27)
                return "The credit card has expired";
            
            //else if (responseType == -2)
            //    return "Duplicate values entered.";
            else
                return "We hit a snag, please try again after some time.";
        }

    }

    
    //public class Timezone
    //{
    //    public  static void TimezoneByCity(string location)
    //    {



    //        string address = "Mohali, Punjab, India";
    //        string requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyCbriqnI83y8JkNtAnwJXuwEnHJHRiGecQ", Uri.EscapeDataString(address));

    //        try
    //        {
    //            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUri);
    //            request.Method = "GET";
    //            String test = String.Empty;
    //            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
    //            {
    //                Stream dataStream = response.GetResponseStream();
    //                StreamReader reader = new StreamReader(dataStream);
    //                test = reader.ReadToEnd();
    //                reader.Close();
    //                dataStream.Close();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            string err = ex.Message;                
    //        }
    //    }
    //}


}


