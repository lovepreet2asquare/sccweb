﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Threading;

namespace FPBAL.Business
{
    public class BusinessAccess:IBusiness
    {
        BusinessData businessData = new BusinessData();

        public BusinessResponse Edit(BusinessModel model)
        {
            Int32 returnResult = 0;
            BusinessResponse response = new BusinessResponse();
            response._businessModel = new BusinessModel();
            int User_Id = model.UserId;
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedUserId))
                {
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                }
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                DataSet ds = businessData.AddorEdit(model, out returnResult);
                if (returnResult > 0)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "Information Submitted Successfully";
                }
                else if (returnResult == -4)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "UniqueId already exists";
                }
                else
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BusinessAccess", "AddorEdit");
                return response;
            }

        }
        public BusinessResponse AddorEdit(BusinessModel model)
        {
            Int32 returnResult = 0;
            BusinessResponse response = new BusinessResponse();
            response._businessModel = new BusinessModel();
            int User_Id = model.UserId;
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedUserId))
                {
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                }
         
                if (!string.IsNullOrEmpty(model.EncryptedBusinessId))
                {
                    model.BusinessId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedBusinessId));
                }
         
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                DataSet ds = businessData.AddorEdit(model, out returnResult);
                if (returnResult >0)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "Information Submitted Successfully";
                }
               else  if (returnResult == -4)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "UniqueId already exists";
                }
                else
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BusinessAccess", "AddorEdit");
                return response;
            }

        }

        public BusinessResponse SelectById(BusinessModel model)
        {
            BusinessResponse businessResponse = new BusinessResponse();
            BusinessModel businessInfo = new BusinessModel();
            businessResponse._businessModel = new BusinessModel();
            businessResponse.ReturnCode = 0;
            businessResponse.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            int userid = 0;
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

            if (model.EncryptedBusinessId == null || model.EncryptedBusinessId == "0")
            {
                model.BusinessId = 0;
            }
            else
            {
                model.BusinessId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedBusinessId));
            }

            if (!string.IsNullOrEmpty(model.EncryptedUserId ))
            { 
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            }
            DataSet ds = businessData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        businessResponse._businessModel = new BusinessModel();
                        businessResponse._businessModel.BusinessName = Convert.ToString(row["BusinessName"] ?? 0);
                        businessResponse._businessModel.BusinessId = Convert.ToInt32(row["BusinessId"] ?? 0);
                        businessResponse._businessModel.UserId = Convert.ToInt32(row["UserId"] ?? 0);
                        businessResponse._businessModel.CategoryName = Convert.ToString(row["CategoryName"] ?? string.Empty);
                        businessResponse._businessModel.CategoryId = Convert.ToString(row["CategoryId"] ?? string.Empty);
                        businessResponse._businessModel.BusinessType = Convert.ToString(row["BusinessType"] ?? 0);
                        businessResponse._businessModel.BusinessDescription = Convert.ToString(row["BusinessDescription"] ?? string.Empty);
                        businessResponse._businessModel.LogoPath = Convert.ToString(row["BusinessLogo"] ?? string.Empty);
                        businessResponse._businessModel.Company = Convert.ToString(row["Company"] ?? string.Empty);
                        businessResponse._businessModel.Address1 = Convert.ToString(row["Address1"] ?? string.Empty);
                        businessResponse._businessModel.Address2 = Convert.ToString(row["Address2"] ?? string.Empty);
                        businessResponse._businessModel.City = Convert.ToString(row["City"] ?? string.Empty);                        
                        businessResponse._businessModel.StateName = Convert.ToString(row["State"] ?? 0);
                        businessResponse._businessModel.StateId = Convert.ToInt32(row["Stateid"] ?? 0);
                        businessResponse._businessModel.CountryId = Convert.ToInt32(row["CountryId"] ?? 0);
                        businessResponse._businessModel.CountryName = Convert.ToString(row["CountryName"] ?? 0);
                        businessResponse._businessModel.ZipCode = Convert.ToString(row["ZipCode"]);
                        businessResponse._businessModel.BusinessPhone = Convert.ToString(row["BusinessPhone"] ?? 0);
                        businessResponse._businessModel.BusinessEmail = Convert.ToString(row["BusinessEmail"] ?? 0);
                        businessResponse._businessModel.BusinessWebsite = Convert.ToString(row["BusinessWebsite"] ?? 0);
                        businessResponse._businessModel.EstablishedYear = Convert.ToInt32(row["EstablishedYear"] ?? string.Empty);
                        //businessResponse._businessModel.Name = Convert.ToString(row["Name"] ?? string.Empty);
                        businessResponse._businessModel.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        businessResponse._businessModel.Phone = Convert.ToString(row["Phone"] ?? string.Empty);
                        businessResponse._businessModel.Email = Convert.ToString(row["Email"] ?? string.Empty);
                        businessResponse._businessModel.ReferredBy = Convert.ToString(row["ReferredBy"] ?? string.Empty);
                        businessResponse._businessModel.StatusId = Convert.ToString(row["Status"] ?? 0);
                        businessResponse._businessModel.Status = Convert.ToString(row["Status"] ?? string.Empty);
                        //businessResponse._businessModel.ISDCode = Convert.ToString(row["ISDCode"] ?? string.Empty);
                        //businessResponse._businessModel.TeleISDCode = Convert.ToString(row["TeleIsdCode"] ?? string.Empty);
                        businessResponse._businessModel.EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"]) ?? string.Empty);
                        businessResponse._businessModel.EncryptedBusinessId = UtilityAccess.Encrypt(Convert.ToString(row["BusinessId"]) ?? string.Empty);
                        
                        businessResponse._businessModel.LogoPath = Convert.ToString(row["BusinessLogo"]) ?? string.Empty;
                        if (businessResponse._businessModel.LogoPath != "")
                        {
                            //directoryResponse._directoryModel.LogoPath = "https://njspba.keplerconnect.com/" + directoryResponse._directoryModel.LogoPath;
                        }

                        businessResponse._businessModel.ModifyDate = Convert.ToString(row["ModifyDate"]) ?? string.Empty;
                        businessResponse._businessModel.CreateDate = Convert.ToString(row["CreateDate"]) ?? string.Empty;
                        businessResponse._businessModel.LoggedUserId = userid;

                    }
                }
                businessResponse._businessModel._CategoryList = UtilityAccess.CategoryList(ds.Tables[1], 1);
                businessResponse._businessModel._StateList = UtilityAccess.RenderList(ds.Tables[2], 1);
                businessResponse._businessModel._CountryList = UtilityAccess.RenderCountryList(ds.Tables[3], 1);
                //businessResponse._businessModel._TitleList = UtilityAccess.RenderList(ds.Tables[3], 1);
                businessResponse._businessModel._StatusList = UtilityAccess.ActiveStatusListforvender(1);
                businessResponse._businessModel._ISDCodeList = UtilityAccess.RenderISDCodeList(ds.Tables[0], 1);
                businessResponse.ReturnCode = returnResult;
                businessResponse.ReturnMessage = Response.Message(returnResult);
                //businessResponse._businessModel = businessInfo;
            }
            return businessResponse;
        }


        public BusinessResponse SelectByIdforrefervendor(BusinessModel model)
        {
            BusinessResponse businessResponse = new BusinessResponse();
            BusinessModel businessInfo = new BusinessModel();
            businessResponse._businessModel = new BusinessModel();
            businessResponse.ReturnCode = 0;
            businessResponse.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            int userid = 0;
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

            if (model.EncryptedBusinessId == null || model.EncryptedBusinessId == "0")
            {
                model.BusinessId = 0;
            }
            else
            {
                model.BusinessId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedBusinessId));
            }

            if (!string.IsNullOrEmpty(model.EncryptedUserId))
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            }
            DataSet ds = businessData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        businessResponse._businessModel = new BusinessModel();
                        businessResponse._businessModel.BusinessName = Convert.ToString(row["BusinessName"] ?? 0);
                        businessResponse._businessModel.BusinessId = Convert.ToInt32(row["BusinessId"] ?? 0);
                        businessResponse._businessModel.UserId = Convert.ToInt32(row["UserId"] ?? 0);
                        businessResponse._businessModel.CategoryName = Convert.ToString(row["CategoryName"] ?? string.Empty);
                        businessResponse._businessModel.CategoryId = Convert.ToString(row["CategoryId"] ?? string.Empty);
                        businessResponse._businessModel.BusinessType = Convert.ToString(row["BusinessType"] ?? 0);
                        businessResponse._businessModel.BusinessDescription = Convert.ToString(row["BusinessDescription"] ?? string.Empty);
                        businessResponse._businessModel.LogoPath = Convert.ToString(row["BusinessLogo"] ?? string.Empty);
                        businessResponse._businessModel.Company = Convert.ToString(row["Company"] ?? string.Empty);
                        businessResponse._businessModel.Address1 = Convert.ToString(row["Address1"] ?? string.Empty);
                        businessResponse._businessModel.City = Convert.ToString(row["City"] ?? string.Empty);
                        businessResponse._businessModel.StateName = Convert.ToString(row["State"] ?? 0);
                        businessResponse._businessModel.StateId = Convert.ToInt32(row["StateId"] ?? 0);
                        businessResponse._businessModel.CountryId = Convert.ToInt32(row["CountryId"] ?? 0);
                        businessResponse._businessModel.CountryName = Convert.ToString(row["CountryId"] ?? 0);
                        businessResponse._businessModel.ZipCode = Convert.ToString(row["ZipCode"]);
                        businessResponse._businessModel.BusinessPhone = Convert.ToString(row["BusinessPhone"] ?? 0);
                        businessResponse._businessModel.BusinessEmail = Convert.ToString(row["BusinessEmail"] ?? 0);
                        businessResponse._businessModel.BusinessWebsite = Convert.ToString(row["BusinessWebsite"] ?? 0);
                        businessResponse._businessModel.EstablishedYear = Convert.ToInt32(row["EstablishedYear"] ?? string.Empty);
                        //businessResponse._businessModel.Name = Convert.ToString(row["Name"] ?? string.Empty);
                        businessResponse._businessModel.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        businessResponse._businessModel.Phone = Convert.ToString(row["Phone"] ?? string.Empty);
                        businessResponse._businessModel.Email = Convert.ToString(row["Email"] ?? string.Empty);
                        businessResponse._businessModel.ReferredBy = Convert.ToString(row["ReferredBy"] ?? string.Empty);
                        businessResponse._businessModel.Status = Convert.ToString(row["Status"] ?? string.Empty);
                        //businessResponse._businessModel.ISDCode = Convert.ToString(row["ISDCode"] ?? string.Empty);
                        //businessResponse._businessModel.TeleISDCode = Convert.ToString(row["TeleIsdCode"] ?? string.Empty);
                        businessResponse._businessModel.EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"]) ?? string.Empty);
                        businessResponse._businessModel.EncryptedBusinessId = UtilityAccess.Encrypt(Convert.ToString(row["BusinessId"]) ?? string.Empty);

                        businessResponse._businessModel.LogoPath = Convert.ToString(row["BusinessLogo"]) ?? string.Empty;
                        if (businessResponse._businessModel.LogoPath != "")
                        {
                            //directoryResponse._directoryModel.LogoPath = "https://njspba.keplerconnect.com/" + directoryResponse._directoryModel.LogoPath;
                        }

                        businessResponse._businessModel.ModifyDate = Convert.ToString(row["ModifyDate"]) ?? string.Empty;
                        businessResponse._businessModel.CreateDate = Convert.ToString(row["CreateDate"]) ?? string.Empty;
                        businessResponse._businessModel.LoggedUserId = userid;

                    }
                }
                businessResponse._businessModel._CategoryList = UtilityAccess.CategoryList(ds.Tables[1], 1);
                businessResponse._businessModel._StateList = UtilityAccess.RenderList(ds.Tables[2], 1);
                businessResponse._businessModel._CountryList = UtilityAccess.RenderCountryList(ds.Tables[3], 1);
                //businessResponse._businessModel._TitleList = UtilityAccess.RenderList(ds.Tables[3], 1);
                businessResponse._businessModel._StatusList = UtilityAccess.ActiveStatusListforvender(1);
                businessResponse._businessModel._ISDCodeList = UtilityAccess.RenderISDCodeList(ds.Tables[0], 1);
                businessResponse.ReturnCode = returnResult;
                businessResponse.ReturnMessage = Response.Message(returnResult);
                //businessResponse._businessModel = businessInfo;
            }
            return businessResponse;
        }
        public BusinessResponse SelectAll(BusinessModel model)
        {
            BusinessResponse businessResponse = new BusinessResponse();
            businessResponse.ReturnCode = 0;
            businessResponse.ReturnMessage = Response.Message(0);
            businessResponse._businessModel = new BusinessModel();
            BusinessLatLngModel businesslatlng = new BusinessLatLngModel();
            BusinessModel businessInfo = new BusinessModel();
            Int32 returnResult = 0;

            try
            {

                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedLoggedId));

                DataSet ds = businessData.SelectAll(model, out returnResult);

                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        businessResponse._businessModel._businessList = BusinessListAll(ds.Tables[0], returnResult);
                        returnResult = 2;

                    }
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                    {
                       

                            int result = 0; //Convert.ToString(ds.Tables[0].Rows[0]["FollowerId"]);
                            foreach (DataRow row in ds.Tables[1].Rows)
                            {
                                result = Convert.ToInt32(row["BusinessId"]);
                                if (result > 0)
                                {
                                businesslatlng = new BusinessLatLngModel();
                                Thread T1 = new Thread(delegate ()
                                    {

                                        GeoAddress geoAddress = new GeoAddress
                                        {
                                            AddressLine1 = Convert.ToString(row["Address1"]),
                                            AddressLine2 = Convert.ToString(row["Address2"]),
                                            StateName = Convert.ToString(row["StateName"]),
                                            ZipCode = Convert.ToString(row["ZipCode"]),
                                            CountryName = Convert.ToString(row["CountryName"]),
                                            CityName = Convert.ToString(row["City"]),
                                        };
                                        GeoTimezone geoTimezone = Timezone.GeoTimezone(geoAddress);
                                        businesslatlng.BusinessId = Convert.ToInt32(row["BusinessId"]);
                                        businesslatlng.Latitude = geoTimezone.latitude.ToString();
                                        businesslatlng.Longitude = geoTimezone.longitude.ToString();

                                        int resultreturn = businessData.BusinessLatLngUpdate(businesslatlng);
                            });
                            T1.IsBackground = true;
                            T1.Start();
                        }
                            }
                        
                        returnResult = 2;
                    }
                }

                else
                {
                    businessResponse.ReturnCode = 0;
                    businessResponse.ReturnMessage = "No Data Found";
                }
                businessResponse._businessModel._StatusList = UtilityAccess.ActiveStatusList(2);
                businessResponse.ReturnCode = returnResult;
                businessResponse.ReturnMessage = Response.Message(returnResult);

                return businessResponse;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "SelectAll");
            }
            return businessResponse;
        }

        private List<BusinessModel> BusinessListAll(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<BusinessModel> data = new List<BusinessModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new BusinessModel
                            {

                                EncryptedBusinessId = UtilityAccess.Encrypt(Convert.ToString(row["BusinessId"])),
                                BusinessName = Convert.ToString(row["BusinessName"]),
                                CategoryName = Convert.ToString(row["CategoryName"] ?? 0),
                                Services = Convert.ToInt32(row["ServiceCount"] ?? 0),
                                
                                EstablishedYear = Convert.ToInt32(row["EstablishedYear"] ?? string.Empty),
                                Name = Convert.ToString(row["Name"] ?? string.Empty),
                               Status = Convert.ToString(row["Status"] ?? string.Empty),



                            });

                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BusinessModel", "BusinessList");
                ReturnResult = -1;
                return null;
            }
        }
        private List<BusinessModel> BusinessList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<BusinessModel> data = new List<BusinessModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new BusinessModel
                            {

                                EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"])),
                                BusinessName = Convert.ToString(row["BusinessName"]),
                                BusinessType = Convert.ToString(row["BusinessType"] ?? 0),
                                BusinessDescription = Convert.ToString(row["BusinessDescription"] ?? string.Empty),
                                LogoPath = Convert.ToString(row["BusinessLogo"] ?? string.Empty),
                                Company = Convert.ToString(row["Company"] ?? string.Empty),
                                Services = Convert.ToInt32(row["Services"] ?? 0),
                                
                                Address1 = Convert.ToString(row["Address1"] ?? string.Empty),
                                Address2 = Convert.ToString(row["Address2"] ?? string.Empty),
                                StateId = Convert.ToInt32(row["StateId"] ?? 0),
                                City = Convert.ToString(row["City"]),
                                ZipCode = Convert.ToString(row["ZipCode"] ?? 0),
                                BusinessPhone = Convert.ToString(row["BusinessPhone"] ?? string.Empty),
                                BusinessEmail = Convert.ToString(row["BusinessEmail"] ?? string.Empty),
                                BusinessWebsite = Convert.ToString(row["BusinessWebsite"] ?? string.Empty),
                                EstablishedYear = Convert.ToInt32(row["EstablishedYear"] ?? string.Empty),
                                //PageCount = Convert.ToInt32(row["Total"] ?? 0),
                                Name = Convert.ToString(row["Name"] ?? string.Empty),
                                Email = Convert.ToString(row["Email"] ?? string.Empty),
                                Title = Convert.ToString(row["Title"] ?? string.Empty),
                                Phone = Convert.ToString(row["Phone"] ?? string.Empty),
                                ReferredBy = Convert.ToString(row["ReferredBy"] ?? string.Empty),
                                Notes = Convert.ToString(row["Notes"] ?? string.Empty),
                                Status = Convert.ToString(row["Status"] ?? string.Empty),

                            });

                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BusinessModel", "BusinessList");
                ReturnResult = -1;
                return null;
            }
        }

        public BusinessResponse Delete(BusinessModel model)
        {
            BusinessResponse serviceResponse = new BusinessResponse();
            BusinessModel userInfo = new BusinessModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            int returnResult = 0;

            if (model != null && model.EncryptedBusinessId != null)
                model.BusinessId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedBusinessId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = businessData.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = "Business Deleted Successfully";
            }

            return serviceResponse;
        }

    }
}
