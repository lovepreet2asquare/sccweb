﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FPDAL.Data;
using FPBAL.Interface;
using FPModels.Models;
using System.Threading;

namespace FPBAL.Business
{
   public class CategoryAccess : ICategory
    {
        CategoryData categoryData = new CategoryData();

        public CategoryResponse SelectAll(CategoryModel categoryModel)
        {
            List<CategoryModel> category = new List<CategoryModel>();


            CategoryResponse serviceResponse = new CategoryResponse();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            CategoryModel categoryModelinfo = new CategoryModel();
            int returnResult = 0;
            if (categoryModel != null && !string.IsNullOrEmpty(categoryModel.EncryptedCategoryId))
                categoryModel.CategoryId = Convert.ToInt32(UtilityAccess.Decrypt(categoryModel.EncryptedCategoryId));

            categoryModel.SessionToken = UtilityAccess.Decrypt(categoryModel.EncryptedSessionToken);
            DataSet ds = categoryData.SelectAll(categoryModel);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        categoryModelinfo = new CategoryModel();
                        categoryModelinfo.CategoryId = Convert.ToInt32(row["CategoryId"] ?? 0);
                        categoryModelinfo.UserId = Convert.ToInt32(row["UserId"] ?? 0);
                        categoryModelinfo.EncryptedCategoryId = UtilityAccess.Encrypt(Convert.ToString(row["CategoryId"] ?? string.Empty));
                        categoryModelinfo.CategoryName = Convert.ToString(row["CategoryName"] ?? string.Empty);
                        categoryModelinfo.CreateDate = Convert.ToString(row["CreateDate"] ?? string.Empty);
                        categoryModelinfo.LogoPath = Convert.ToString(row["ImagePath"] ?? string.Empty);
                        category.Add(categoryModelinfo);
                    }
                }
                categoryModelinfo.categoryModelsList = category;
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.Categorymodel = categoryModelinfo;
            }
            return serviceResponse;
        }

        public CategoryResponse SelectById(CategoryModel model)
        {
            CategoryResponse directoryResponse = new CategoryResponse();
            CategoryModel directoryInfo = new CategoryModel();
            directoryResponse.Categorymodel = new CategoryModel();
            directoryResponse.ReturnCode = 0;
            directoryResponse.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            int userid = 0;
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

            if (model.EncryptedCategoryId == null || model.EncryptedCategoryId == "0")
            {
                model.CategoryId = 0;
            }
            else
            {
                model.CategoryId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedCategoryId));
            }

            userid = model.UserId;
            if (model.EncryptedUserId == null)
            {
                model.UserId = 0;
            }
            else
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            }
            DataSet ds = categoryData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        directoryResponse.Categorymodel = new CategoryModel();
                        directoryResponse.Categorymodel.CategoryId = Convert.ToInt32(row["CategoryId"] ?? 0);
                        directoryResponse.Categorymodel.EncryptedCategoryId = UtilityAccess.Encrypt(Convert.ToString(row["CategoryId"] ?? string.Empty));
                        directoryResponse.Categorymodel.CategoryName = Convert.ToString(row["CategoryName"] ?? 0);
                        directoryResponse.Categorymodel.CreateDate = Convert.ToString(row["CreateDate"] ?? string.Empty);
                        //directoryResponse.Categorymodel.OfferDescription = Convert.ToString(row["OfferDescription"] ?? string.Empty);
                        //directoryResponse.Categorymodel.StatusId = Convert.ToString(row["status"] ?? string.Empty);
                        //directoryResponse.Categorymodel.Status = Convert.ToString(row["status"] ?? string.Empty);
                        directoryResponse.Categorymodel.LogoPath = Convert.ToString(row["ImagePath"] ?? string.Empty);
                        directoryResponse.Categorymodel.UserId = userid;

                    }
                }
                //if (directoryResponse.Categorymodel.StatusId == null)
                //{
                //    directoryResponse.Categorymodel.StatusId = "1";
                //    directoryResponse.Vendorsmodel.Status = "1";

                //}
                //directoryResponse.Vendorsmodel._StatusList = UtilityAccess.StatusListValTextNew();
                //directoryResponse.ReturnCode = returnResult;
                //directoryResponse.ReturnMessage = Response.Message(returnResult);
            }
            return directoryResponse;
        }

        public CategoryResponse AddorEdit(CategoryModel model)
        {
            Int32 returnResult = 0;
            CategoryResponse response = new CategoryResponse();
            response.Categorymodel = new CategoryModel();
            int User_Id = model.UserId;
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedUserId))
                {
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                }
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                DataSet ds = categoryData.AddorEdit(model, out returnResult);
                if (returnResult == -4)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "UniqueId already exists";
                }
                else
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "categoryaccess", "AddorEdit");
                return response;
            }

        }

        public CategoryResponse Delete(CategoryModel model)
        {
            CategoryResponse serviceResponse = new CategoryResponse();
            CategoryModel userInfo = new CategoryModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            int returnResult = 0;

            if (model != null && model.EncryptedCategoryId != null)
                model.CategoryId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedCategoryId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            returnResult = categoryData.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }

            return serviceResponse;
        }
    }

}
