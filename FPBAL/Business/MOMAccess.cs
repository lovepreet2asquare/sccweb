﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class MOMAccess : IMOM
    {
        public MOMResponse GetMinutesOfMeeting(MOMRequest request)
        {
            int returnResult = 0;
            List<MOMModel> MOMList = new List<MOMModel>();
            MOMResponse response = new MOMResponse();
            response.MOMList = MOMList;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);

            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = MOMData.MOMSelect(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["MOMId"]);
                    if (returnResult > 0)
                    {
                        MOMList = MOMDetailList(ds, out returnResult);
                        response.MOMList = MOMList;
                        //returnResult = 2;
                    }
                }

                // response message
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MOMAccess", "MOMSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }

        public NJResponse MOMLogInsert(MOMLogRequest request)
        {
            NJResponse serviceResponse = new NJResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";


            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            returnResult = MOMData.MOMLogInsert(request);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Submitted successfully";
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }

        private List<MOMModel> MOMDetailList(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            try
            {
                List<MOMModel> data = new List<MOMModel>();
                MOMModel model = null;
                List<MOMDetailModel> MOMDetailList = null;
                MOMDetailModel momDetail = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[1].Rows)
                            {
                                model = new MOMModel();
                                MOMDetailList = new List<MOMDetailModel>();
                                model.Year = Convert.ToInt32(row["YearName"]);

                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow row2 in ds.Tables[0].Select("YearName=" + model.Year))
                                    {
                                        momDetail = new MOMDetailModel();
                                        momDetail.MOMId = Convert.ToString(Convert.ToInt32(row2["MOMId"])); //UtilityAccess.Encrypt(Convert.ToString(Convert.ToInt32(row2["MOMId"])));
                                        momDetail.Month = Convert.ToString(row2["MonthsName"]);
                                        //momDetail.DocumentName = Convert.ToString(row2["MonthsName"]);
                                        momDetail.DocumentPath = Convert.ToString(row2["DocumentPath"]);
                                        momDetail.ThumbnailPath = Convert.ToString(row2["ThumbnailPath"]);
                                        MOMDetailList.Add(momDetail);
                                    }
                                }
                                model.MOMDetail = MOMDetailList;
                                data.Add(model);
                                ReturnResult = 2;
                            }
                        }
                    }
                }
                return data;
            }

            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MOMAccess", "MOMList");
                ReturnResult = -1;
                return null;
            }
        }
    }
}
