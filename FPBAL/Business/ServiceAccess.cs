﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Threading;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System.Web.Mvc;   

namespace FPBAL.Business
{
    public class ServiceAccess : IService
    {
        ServiceData serviceData = new ServiceData();

        public ServiceResponse SelectAll(ServiceModel serviceModel)
        {
            List<ServiceModel> service = new List<ServiceModel>();


            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            ServiceModel serviceModelinfo = new ServiceModel();
            int returnResult = 0;
            if ( !string.IsNullOrEmpty(serviceModel.EncryptedServiceId))
                serviceModel.ServiceId = Convert.ToInt32(UtilityAccess.Decrypt(serviceModel.EncryptedServiceId));
            if ( !string.IsNullOrEmpty(serviceModel.EncryptedBusinessId))
                serviceModel.BusinessId = Convert.ToInt32(UtilityAccess.Decrypt(serviceModel.EncryptedBusinessId));
            if (!string.IsNullOrEmpty(serviceModel.ServiceType))
            {
                serviceModel.ServiceType = Convert.ToString(UtilityAccess.Decrypt(serviceModel.ServiceType));
            }

            serviceModel.SessionToken = UtilityAccess.Decrypt(serviceModel.EncryptedSessionToken);
            DataSet ds = serviceData.SelectAll(serviceModel);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        serviceModelinfo = new ServiceModel();
                        serviceModelinfo.ServiceId = Convert.ToInt32(row["ServiceId"] ?? 0);
                        serviceModelinfo.UserId = Convert.ToInt32(row["UserId"] ?? 0);
                        serviceModelinfo.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(row["ServiceId"] ?? string.Empty));
                        serviceModelinfo.ServiceType = Convert.ToString(row["ServiceTypeName"] ?? string.Empty);
                        serviceModelinfo.Status = Convert.ToString(row["Status"] ?? string.Empty);
                        //serviceModelinfo.StatusId = Convert.ToInt32(row["Status"] ?? string.Empty);
                        serviceModelinfo.PeriodFrom = Convert.ToString(row["PeriodFrom"] ?? string.Empty);
                        serviceModelinfo.PeriodTo = Convert.ToString(row["PeriodTo"] ?? string.Empty);
                        //serviceModelinfo.Apps = Convert.ToString(row["AppName"] ?? string.Empty);
                        serviceModelinfo.Likes = Convert.ToString(row["Likes"] ?? string.Empty);
                        serviceModelinfo.Dislike = Convert.ToString(row["Dislike"] ?? string.Empty);
                        serviceModelinfo.Favorite = Convert.ToString(row["Favorite"] ?? string.Empty);
                        serviceModelinfo.Clicks = Convert.ToString(row["Clicks"] ?? string.Empty);
                        serviceModelinfo.Impression = Convert.ToString(row["Impression"] ?? string.Empty);
                        service.Add(serviceModelinfo);
                    }
                }
                serviceModelinfo.EncryptedBusinessId = serviceModel.EncryptedBusinessId;
                serviceModelinfo._ServiceList = service;
                serviceModelinfo._StatusList = UtilityAccess.ActiveStatusList1(1);
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse._ServiceModel = serviceModelinfo;
            }
            return serviceResponse;
        }


        public ServiceResponse AddorEdit(ServiceModel model)
        {
            Int32 returnResult = 0;
            ServiceResponse response = new ServiceResponse();
            response._ServiceModel = new ServiceModel();
            int User_Id = model.UserId;
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedUserId))
                {
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                }
                if (!string.IsNullOrEmpty(model.EncryptedBusinessId))
                {
                    model.BusinessId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedBusinessId));
                }
                if (!string.IsNullOrEmpty(model.EncryptedServiceId))
                {
                    model.ServiceId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedServiceId));
                }

                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                DataSet ds = serviceData.AddorEdit(model, out returnResult);
                if (returnResult >0)
                {
                    
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        response._ServiceModel.ServiceId =  Convert.ToInt32(ds.Tables[0].Rows[0]["ServiceId"]);
                    }

                        if (returnResult == 12)
                    {
                        response.ReturnCode = 12;

                        response.ReturnMessage = "updated Successfully";
                    }
                else if (returnResult == 11)
                    {
                        response.ReturnCode = 11;
                        response.ReturnMessage = "Added Successfully";
                    }
                }
                else if (returnResult == -4)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "UniqueId already exists";
                }
                else
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "serviceaccess", "AddorEdit");
                return response;
            }

        }

        public ServiceResponse ServiceTypeSelectAll(ServiceModel serviceModel)
        {
            List<ServiceModel> service = new List<ServiceModel>();


            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            ServiceModel serviceModelinfo = new ServiceModel();
            int returnResult = 0;
            if (serviceModel != null && !string.IsNullOrEmpty(serviceModel.EncryptedServiceId))
                serviceModel.ServiceId = Convert.ToInt32(UtilityAccess.Decrypt(serviceModel.EncryptedServiceId));


            serviceModel.SessionToken = UtilityAccess.Decrypt(serviceModel.EncryptedSessionToken);
            if (!string.IsNullOrEmpty(serviceModel.EncryptedUserId))
            {
                serviceModel.UserId = Convert.ToInt32(UtilityAccess.Decrypt(serviceModel.EncryptedUserId));
            }
            if (!string.IsNullOrEmpty(serviceModel.ServiceType))
            {
                serviceModel.ServiceType = Convert.ToString(UtilityAccess.Decrypt(serviceModel.ServiceType));
            }
            DataSet ds = serviceData.ServiceTypeSelectAll(serviceModel);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        serviceModelinfo = new ServiceModel();
                        //serviceModelinfo.UserId = Convert.ToInt32(row["UserId"] ?? 0);
                        serviceModelinfo.ServiceList = UtilityAccess.ServiceList1(ds.Tables[0], 1);

                        service.Add(serviceModelinfo);
                    }
                }
                serviceModelinfo.EncryptedBusinessId = serviceModel.EncryptedBusinessId;
                serviceModelinfo._ServiceList = service;
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse._ServiceModel = serviceModelinfo;
            }
            return serviceResponse;
        }

        public ServiceResponse SelectServiceDetailById(ServiceModel model)
        {
            ServiceResponse serviceResponse = new ServiceResponse();
            ServiceModel serviceInfo = new ServiceModel();
            serviceResponse._ServiceModel = new ServiceModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            int userid = 0;
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            if (!string.IsNullOrEmpty(model.EncryptedBusinessId))
            {
                model.BusinessId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedBusinessId));
            }
            if (!string.IsNullOrEmpty(model.EncryptedServiceId))
            {
                model.ServiceId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedServiceId));
            }
            //model.ServiceId = Convert.ToInt32(model.EncryptedServiceId);
            userid = model.UserId;
            if (model.EncryptedUserId == null)
            {
                model.UserId = 0;
            }
            else
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            }
            DataSet ds = serviceData.SelectDetailById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        serviceResponse._ServiceModel = new ServiceModel();
                        serviceResponse._ServiceModel.ServiceType = Convert.ToString(row["ServiceType"] ?? 0);
                        serviceResponse._ServiceModel.PeriodFrom = Convert.ToString(row["PeriodFrom"] ?? string.Empty);
                        serviceResponse._ServiceModel.PeriodTo = Convert.ToString(row["PeriodTo"] ?? string.Empty);
                        serviceResponse._ServiceModel.OffersTitle = Convert.ToString(row["OffersTitle"] ?? 0);
                        serviceResponse._ServiceModel.OffersTerms = Convert.ToString(row["OffersTerms"] ?? string.Empty);
                        serviceResponse._ServiceModel.Status = Convert.ToString(row["status"] ?? string.Empty);
                        serviceResponse._ServiceModel.PaymentMethod = Convert.ToString(row["PaymentMethod"] ?? string.Empty);
                        serviceResponse._ServiceModel.DealsAmounts = Convert.ToDecimal(row["DealsAmounts"] ?? string.Empty);
                        serviceResponse._ServiceModel.PaymentNotes = Convert.ToString(row["PaymentNotes"] ?? string.Empty);
                        serviceResponse._ServiceModel.UserId = userid;

                    }
                }
                serviceResponse._ServiceModel.EncryptedServiceId = model.EncryptedServiceId;
                serviceResponse._ServiceModel._StatusList = UtilityAccess.StatusListValTextNew();
                serviceResponse._ServiceModel._PaymentMethodList = UtilityAccess.PaymentMethodList(0);
                serviceResponse._ServiceModel.AppList = UtilityAccess.AppList1(ds.Tables[1], 1);
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }
            return serviceResponse;
        }
        public ServiceResponse SelectById(ServiceModel model)
        {
            ServiceResponse serviceResponse = new ServiceResponse();
            ServiceModel serviceInfo = new ServiceModel();
            serviceResponse._ServiceModel = new ServiceModel();
            List<DealsModel> DealsList = new List<DealsModel>();
            DealsModel dealmodel = new DealsModel();
            serviceResponse._ServiceModel.DealsList = DealsList;
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            int userid = 0;
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            if (!string.IsNullOrEmpty(model.EncryptedBusinessId))
            {
                model.BusinessId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedBusinessId));
            }
            if (!string.IsNullOrEmpty(model.EncryptedServiceId))
            {
                model.ServiceId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedServiceId));
            }
            
            userid = model.UserId;
            if (model.EncryptedUserId == null)
            {
                model.UserId = 0;
            }
            else
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            }
            DataSet ds = serviceData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        serviceResponse._ServiceModel = new ServiceModel();
                        serviceResponse._ServiceModel.ServiceType = Convert.ToString(row["ServiceType"] ?? 0);
                        serviceResponse._ServiceModel.PeriodFrom = Convert.ToString(row["PeriodFrom"] ?? string.Empty);
                        serviceResponse._ServiceModel.PeriodTo = Convert.ToString(row["PeriodTo"] ?? string.Empty);
                        serviceResponse._ServiceModel.ServiceId = Convert.ToInt32(row["ServiceId"] ?? 0);
                        serviceResponse._ServiceModel.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(row["ServiceId"] ?? string.Empty));
                        serviceResponse._ServiceModel.OffersTitle = Convert.ToString(row["OffersTitle"] ?? 0);
                        serviceResponse._ServiceModel.OffersTerms = Convert.ToString(row["OffersTerms"] ?? string.Empty);
                        serviceResponse._ServiceModel.Status = Convert.ToString(row["status"] ?? string.Empty);
                        //serviceResponse._ServiceModel.StatusId = Convert.ToInt32(row["status"] ?? string.Empty);
                        serviceResponse._ServiceModel.PaymentMethod = Convert.ToString(row["PaymentMethod"] ?? string.Empty);
                        serviceResponse._ServiceModel.DealsAmounts = Convert.ToDecimal(row["DealsAmounts"] ?? string.Empty);
                        serviceResponse._ServiceModel.PaymentNotes = Convert.ToString(row["PaymentNotes"] ?? string.Empty);
                        serviceResponse._ServiceModel.CreateDate = Convert.ToString(row["CreateDate"] ?? string.Empty);

                        serviceResponse._ServiceModel.UserId = userid;

                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        dealmodel = new DealsModel();
                        dealmodel.DealId = UtilityAccess.Encrypt(Convert.ToString(row["DealsId"]));
                        dealmodel.DealTitle =Convert.ToString(row["DealTitle"]);
                        dealmodel.DealCode = Convert.ToString(row["DealCode"]);
                        dealmodel.DealStatus = Convert.ToString(row["DealStatus"]);
                        DealsList.Add(dealmodel);

                    }

                }
                serviceResponse._ServiceModel.DealsList = DealsList;
                serviceResponse._ServiceModel.EncryptedServiceId = model.EncryptedServiceId;
                serviceResponse._ServiceModel._StatusList = UtilityAccess.StatusListValTextNew();
                serviceResponse._ServiceModel._PaymentMethodList = UtilityAccess.PaymentMethodList(0);
                //serviceResponse._ServiceModel.AppList = UtilityAccess.AppList1(ds.Tables[2], 1);
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }
            return serviceResponse;
        }

        public ServiceResponse Delete(ServiceModel model)
        {
            ServiceResponse serviceResponse = new ServiceResponse();
            ServiceModel userInfo = new ServiceModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            int returnResult = 0;

            if (model != null && model.EncryptedServiceId != null)
                model.ServiceId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedServiceId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            returnResult = serviceData.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }

            return serviceResponse;
        }
    }
}
