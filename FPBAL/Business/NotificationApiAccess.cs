﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class NotificationApiAccess : INotificationAPI
    {
        public NotificationAPIResponse GetNotifications(NJRequest request)
        {
            List<NotificationAPIModel> Notifications = new List<NotificationAPIModel>();

            NotificationAPIResponse serviceResponse = new NotificationAPIResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "No record found.";
            serviceResponse.Notifications = Notifications;
            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            DataSet ds = NotificationApiData.GetNotificationSelect(request, out returnResult);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["NotificationId"]);
                if (returnResult > 0)
                {
                    Notifications = NotificationList(ds, out returnResult);
                    if (returnResult == -1)
                    {
                        serviceResponse.ReturnCode = "-1";
                        serviceResponse.ReturnMessage = "Technical error.";
                    }
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "Retrieved successfully.";
                    serviceResponse.Notifications = Notifications;

                }
                else if (returnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";

                }
                else if (returnResult == -5)
                {
                    serviceResponse.ReturnCode = "-5";
                    serviceResponse.ReturnMessage = "Authentication failed.";

                }
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }
            return serviceResponse;
        }

        public NJResponse NotificationRead(NotificationRequest request)
        {
            NJResponse serviceResponse = new NJResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";

            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            returnResult = NotificationApiData.NotificationRead(request);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Submitted successfully";
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }

        private static List<NotificationAPIModel> NotificationList(DataSet ds, out int returnResult)
        {
            List<NotificationAPIModel> Notifications = new List<NotificationAPIModel>();
            NotificationAPIModel model = null;
            returnResult = 0;

            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        returnResult = Convert.ToInt32(row["NotificationId"]);

                        if (returnResult > 0)
                        {
                            model = new NotificationAPIModel();

                            model.NotificationId = returnResult.ToString();
                            model.NotificationType = Convert.ToString(row["NotificationType"]);
                            model.Message = Convert.ToString(row["Message"]);
                            model.DocumentName = Convert.ToString(row["DocumentName"]);
                            model.DocumentPath = Convert.ToString(row["DocumentPath"]);
                            model.CreateDate = Convert.ToString(row["CreateUtcDate"]);
                            model.Content = Convert.ToString(row["Content"]);
                            model.NotificationTypeId = Convert.ToString(row["NotificationTypeId"]);
                            model.IsRead = Convert.ToBoolean(row["IsView"]);
                            Notifications.Add(model);

                        }

                    }
                }

            }

            return Notifications;
        }
        public FPResponse UpdateNotification(UpdateSettingRequest request)
        {
            int returnResult = 0;
            FPResponse response = new FPResponse();
            try
            {
                request.FollowerId = (UtilityAccess.Decrypt(request.FollowerId));
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                int returnvalues = NotificationApiData.UpdateNotification(request);
                if (returnvalues > 0)
                {
                    response.ReturnCode = Convert.ToString(returnvalues);
                    response.ReturnMessage = Response.Message(returnvalues);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationApiAccess", "UpdateNotification");
                return response;
            }
        }
    }
}
