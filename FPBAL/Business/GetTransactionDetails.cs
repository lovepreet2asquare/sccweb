﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;
using FPDAL.Data;
namespace FPBAL.Business
{
    public class GetTransactionDetails
    {

        //public static void GetTransactionStatus()
        //{
        //    string apiLogin = "7Tr9ywT57BL"; //ConfigurationManager.AppSettings["ApiLogin"];
        //    string transactionKey = "8sZD37B5c47H8yXp"; //ConfigurationManager.AppSettings["TransactionKey"];
        //    string transactionId = "40031193032";
        //    PaymentMethodData.GetAuthorizeKeys(out apiLogin, out transactionKey);
        //    //, transactionId
        //    GetTransactionDetails.Run(apiLogin, transactionKey, transactionId);
        //}
        public static ANetApiResponse GetTransactionStatus(String ApiLoginID, String ApiTransactionKey, string transactionId, out string status)
        {
            //Console.WriteLine("Get transaction details sample");
            status = string.Empty;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };

            var request = new getTransactionDetailsRequest();
            request.transId = transactionId;

            // instantiate the controller that will call the service
            var controller = new getTransactionDetailsController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response.transaction == null)
                    return response;
                status = response.transaction.transactionStatus;
                string abc = response.transaction.batch.settlementTimeUTC.ToString();
                //string settl=response
                //Console.WriteLine("Transaction Id: {0}", response.transaction.transId);
                //Console.WriteLine("Transaction type: {0}", response.transaction.transactionType);
                //Console.WriteLine("Transaction status: {0}", response.transaction.transactionStatus);
                //Console.WriteLine("Transaction auth amount: {0}", response.transaction.authAmount);
                //Console.WriteLine("Transaction settle amount: {0}", response.transaction.settleAmount);
            }
            else if (response != null)
            {
                //Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
                //                 response.messages.message[0].text);
            }

            return response;
        }

        public static ANetApiResponse GetTransactionStatusNew(String ApiLoginID, String ApiTransactionKey, string transactionId, out string status, out string SettlementDate)
        {
            //Console.WriteLine("Get transaction details sample");
            status = string.Empty;
            SettlementDate = string.Empty;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };

            var request = new getTransactionDetailsRequest();
            request.transId = transactionId;

            // instantiate the controller that will call the service
            var controller = new getTransactionDetailsController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                SettlementDate = response.transaction.batch.settlementTimeUTC.ToString();
                if (response.transaction == null)
                    return response;
                status = response.transaction.transactionStatus;

                //string settl=response
                //Console.WriteLine("Transaction Id: {0}", response.transaction.transId);
                //Console.WriteLine("Transaction type: {0}", response.transaction.transactionType);
                //Console.WriteLine("Transaction status: {0}", response.transaction.transactionStatus);
                //Console.WriteLine("Transaction auth amount: {0}", response.transaction.authAmount);
                //Console.WriteLine("Transaction settle amount: {0}", response.transaction.settleAmount);
            }
            else if (response != null)
            {
                //Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
                //                 response.messages.message[0].text);

            }

            return response;
        }

        public static void GetTransactions()
        {
            int returnResult = 0;
            string apiLogin = ""; //ConfigurationManager.AppSettings["ApiLogin"];
            string transactionKey = ""; //ConfigurationManager.AppSettings["TransactionKey"];
            PaymentMethodData.GetAuthorizeKeys(out apiLogin, out transactionKey);
            //DataTable dt = new DataTable();
            //dt.Columns.Add("PaymentId", typeof(Int32));
            //dt.Columns.Add("Status", typeof(String));
            //DataRow row = null;
            string PaymentIds = string.Empty, TransactionId = string.Empty, Status = string.Empty;
            string SettlementDates = string.Empty;
            int PaymentId = 0;
            DataSet ds = PaymentMethodData.GetTransactions(out returnResult);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Status = string.Empty;
                    PaymentId = Convert.ToInt32(row["PaymentId"]);
                    TransactionId = Convert.ToString(row["TransactionId"]);

                    ANetApiResponse response = GetTransactionStatus(apiLogin, transactionKey, TransactionId, out Status);
                    if (Status.ToLower() == "settledsuccessfully")
                    {
                        if (!string.IsNullOrEmpty(PaymentIds))
                        {
                            PaymentIds = PaymentIds + "," + PaymentId.ToString();
                        }
                        else
                            PaymentIds = PaymentId.ToString();
                    }
                }
                //row = dt.NewRow();
                //row["PaymentId"] = Convert.ToInt32(ds.Tables[0].Rows[0]["PaymentId"]);
                //row["Status"] = Convert.ToString(ds.Tables[0].Rows[0]["TransactionId"]);
                //dt.Rows.Add(row);
            }
            if (!string.IsNullOrEmpty(PaymentIds))
            {
                returnResult = PaymentMethodData.PaymentTransactionStatusUpdate(PaymentIds);
            }
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("TransactionId", typeof(Int64));
                dt.Columns.Add("PaymentId", typeof(Int64));
                dt.Columns.Add("SettlementDate", typeof(string));

                DataRow dtrow = null;
               
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Status = string.Empty;
                    SettlementDates = string.Empty;
                    PaymentId = Convert.ToInt32(row["PaymentId"]);
                    TransactionId = Convert.ToString(row["TransactionId"]);

                    ANetApiResponse response = GetTransactionStatusNew(apiLogin, transactionKey, TransactionId, out Status, out SettlementDates);
                    if (Status.ToLower() == "settledsuccessfully")
                    {
                        dtrow = dt.NewRow();
                        dtrow["TransactionId"] = TransactionId;
                        dtrow["PaymentId"] = PaymentId;
                        dtrow["SettlementDate"] = SettlementDates;
                        dt.Rows.Add(dtrow);
                      
                    }
                }
                if(dt!=null)
                {
                    returnResult = PaymentMethodData.PaymentTransactionStatusUpdateNew(dt);
                }

                //dbo.FP_GetPaymentTransactions


            }
        }
    }
}
