﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class MemberBenefitAccess : IMemberBenefit
    {
        public MemberBenefitResponse GetMemberBenefits(MemberBenefitRequest request)
        {
            int returnResult = 0;
            List<MemberBenefitModel> MemberBenefits = new List<MemberBenefitModel>();
            MemberBenefitResponse response = new MemberBenefitResponse();
            response.MemberBenefits = MemberBenefits;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);

            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = MemberBenefitData.MemberBenefitSelect(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["MemberBenefitId"]);
                    if (returnResult > 0)
                    {
                        MemberBenefits = MemberBenefitList(ds, out returnResult);
                        response.MemberBenefits = MemberBenefits;
                        //returnResult = 2;
                    }
                }
                // response message
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = Response.Message(-2);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MemberBenefitAccess", "MemberBenefitSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }

        private List<MemberBenefitModel> MemberBenefitList(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            try
            {
                List<MemberBenefitModel> data = new List<MemberBenefitModel>();
                MemberBenefitModel model = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                model = new MemberBenefitModel();
                                model.MemberBenefitId = Convert.ToString(Convert.ToInt32(row2["MemberBenefitId"]));
                                model.EncryptedMemberBenefitId = UtilityAccess.Encrypt(model.MemberBenefitId);
                                model.Title = Convert.ToString(row2["Title"]);
                                model.Descriptiion = Convert.ToString(row2["Description"]);
                                model.AddedBy = Convert.ToString(row2["AddedBy"]);
                                model.AddedByDate = Convert.ToString(row2["AddedByDate"]);
                                model.Sharelink = Convert.ToString(row2["Sharelink"]);

                                data.Add(model);
                                ReturnResult = 2;
                            }
                        }
                    }
                }
                return data;
            }

            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MemberBenefitAccess", "MemberBenefitList");
                ReturnResult = -1;
                return null;
            }
        }
    }
}
