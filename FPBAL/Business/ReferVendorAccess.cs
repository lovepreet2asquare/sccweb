﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System.Data;

namespace FPBAL.Business
{
    public class ReferVendorAccess : IReferVendor
    {
        ReferVendorData businessData = new ReferVendorData();
        public ReferVendorResponse signupSelectAll(ReferVendorModel vendorModel)
        {
            List<ReferVendorModel> category = new List<ReferVendorModel>();


            ReferVendorResponse serviceResponse = new ReferVendorResponse();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            ReferVendorModel categoryModelinfo = new ReferVendorModel();
            int returnResult = 0;
            //if (vendorModel != null && !string.IsNullOrEmpty(vendorModel.EncryptedBusinessId))
            //    vendorModel.BusinessId = Convert.ToInt32(UtilityAccess.Decrypt(vendorModel.EncryptedBusinessId));

            if (vendorModel != null && !string.IsNullOrEmpty(vendorModel.EncryptedUserId))
                vendorModel.UserId = Convert.ToInt32(UtilityAccess.Decrypt(vendorModel.EncryptedUserId));

            vendorModel.SessionToken = UtilityAccess.Decrypt(vendorModel.EncryptedSessionToken);
            DataSet ds = businessData.signupSelectAll(vendorModel);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["BusinessId"]);
                    if (returnResult > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            categoryModelinfo = new ReferVendorModel();
                            categoryModelinfo.BusinessId = Convert.ToInt32(row["BusinessId"] ?? 0);
                            //categoryModelinfo.UserId = Convert.ToInt32(row["UserId"] ?? 0);
                            categoryModelinfo.EncryptedBusinessId = UtilityAccess.Encrypt(Convert.ToString(row["BusinessId"] ?? string.Empty));
                            categoryModelinfo.BusinessName = Convert.ToString(row["BusinessName"] ?? string.Empty);
                            categoryModelinfo.Category = Convert.ToString(row["Category"] ?? string.Empty);
                            categoryModelinfo.PersonName = Convert.ToString(row["PersonName"] ?? string.Empty);
                            categoryModelinfo.ServiceType = Convert.ToString(row["BusinessServiceType"] ?? string.Empty);
                            category.Add(categoryModelinfo);
                        }
                    }
                }
                categoryModelinfo.referModelsList = category;
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.Categorymodel = categoryModelinfo;
            }
            return serviceResponse;
        }
        public ReferVendorResponse SelectAll(ReferVendorModel vendorModel)
        {
            List<ReferVendorModel> category = new List<ReferVendorModel>();


            ReferVendorResponse serviceResponse = new ReferVendorResponse();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            ReferVendorModel categoryModelinfo = new ReferVendorModel();
            int returnResult = 0;
            //if (vendorModel != null && !string.IsNullOrEmpty(vendorModel.EncryptedBusinessId))
            //    vendorModel.BusinessId = Convert.ToInt32(UtilityAccess.Decrypt(vendorModel.EncryptedBusinessId));

            if (vendorModel != null && !string.IsNullOrEmpty(vendorModel.EncryptedUserId))
                vendorModel.UserId = Convert.ToInt32(UtilityAccess.Decrypt(vendorModel.EncryptedUserId));

            vendorModel.SessionToken = UtilityAccess.Decrypt(vendorModel.EncryptedSessionToken);
            DataSet ds = businessData.SelectAll(vendorModel);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["BusinessId"]);
                    if (returnResult > 0)
                    { 
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            categoryModelinfo = new ReferVendorModel();
                            categoryModelinfo.BusinessId = Convert.ToInt32(row["BusinessId"] ?? 0);
                            //categoryModelinfo.UserId = Convert.ToInt32(row["UserId"] ?? 0);
                            categoryModelinfo.EncryptedBusinessId = UtilityAccess.Encrypt(Convert.ToString(row["BusinessId"] ?? string.Empty));
                            categoryModelinfo.BusinessName = Convert.ToString(row["BusinessName"] ?? string.Empty);
                            categoryModelinfo.Category = Convert.ToString(row["Category"] ?? string.Empty);
                            categoryModelinfo.PersonName = Convert.ToString(row["PersonName"] ?? string.Empty);
                            categoryModelinfo.ServiceType = Convert.ToString(row["BusinessServiceType"] ?? string.Empty);
                            category.Add(categoryModelinfo);
                        }
                }
                }
                categoryModelinfo.referModelsList = category;
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.Categorymodel = categoryModelinfo;
            }
            return serviceResponse;
        }
    }
}
