﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FPBAL.Business
{
    public class EventApiAccess : IEvent
    {
        public NJResponse EventAttendeeInsert(EventAttendeeRequest request)
        {
            NJResponse serviceResponse = new NJResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";
            string ConfirmationMsgResponse = string.Empty;
            string firstName = string.Empty;
            string email = string.Empty;
            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            returnResult = EventApiData.EventAttendeeInsert(request, out ConfirmationMsgResponse, out firstName, out email);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Submitted successfully";
                //SEND email to user on confirmation
                if (request.IsAccepted)
                {
                    if (!string.IsNullOrEmpty(email))
                    {
                        if (string.IsNullOrWhiteSpace(ConfirmationMsgResponse))
                            ConfirmationMsgResponse = "Thanks for your confirmation.";
                        //SEND email to user on confirmation
                        SendEventConfirmationEmail(ConfirmationMsgResponse, firstName, email);
                    }
                }

            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }
        public static void SendEventConfirmationEmail(string content, string FirstName, string Email)
        {
            ////
            //string strLink = System.Configuration.ConfigurationManager.AppSettings["baseurl"].ToString();
            string body = string.Empty;
            string strpath = "";
            string strNewsletterPath = "~/Newsletters/EventConfirmation.html";

            if (HttpContext.Current != null)
            {
                strpath = HttpContext.Current.Server.MapPath(strNewsletterPath);
            }
            else
            {
                strpath = System.Web.Hosting.HostingEnvironment.MapPath(strNewsletterPath);
            }
            using (StreamReader reader = new StreamReader(strpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{firstname}", FirstName);
            body = body.Replace("{content}", content);

            Exception exc = null;
            string subject = "Event Confirmation";
            if (EmailUtilityNew.SendEmailNew(Email, "Port Authority", subject, body, out exc, true) == -1)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEventConfirmationEmail");

            }
        }

        public EventDetailResponse GetEventDetail(EventDetailRequest request)
        {
            EventAPIModel eventDetail = new EventAPIModel();

            EventDetailResponse serviceResponse = new EventDetailResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "No record found.";
            serviceResponse.EventDetail = eventDetail;
            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            DataSet ds = EventApiData.EventDetailSelect(request, out returnResult);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["EventId"]);
                if (returnResult > 0)
                {
                    eventDetail = EventDetail(ds, out returnResult);
                    if (returnResult == -1)
                    {
                        serviceResponse.ReturnCode = "-1";
                        serviceResponse.ReturnMessage = "Technical error.";
                    }
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "Retrieved successfully.";
                    serviceResponse.EventDetail = eventDetail;

                }
                else if (returnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";

                }
                else if (returnResult == -5)
                {
                    serviceResponse.ReturnCode = "-5";
                    serviceResponse.ReturnMessage = "Authentication failed.";

                }
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }
            return serviceResponse;
        }
        private static EventAPIModel EventDetail(DataSet ds, out int returnResult)
        {

            EventAPIModel eventModel = new EventAPIModel();
            returnResult = 0;

            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        returnResult = Convert.ToInt32(row["EventId"]);

                        if (returnResult > 0)
                        {

                            eventModel.EventId = returnResult.ToString();
                            eventModel.Title = Convert.ToString(row["Title"]);
                            eventModel.AddressLine1 = Convert.ToString(row["AddressLine1"]);
                            eventModel.AddressLine2 = Convert.ToString(row["AddressLine2"]);
                            eventModel.CityName = Convert.ToString(row["CityName"]);
                            eventModel.EventAgenda = Convert.ToString(row["EventAgenda"]);
                            eventModel.EventDate = Convert.ToString(row["EventDate"]);
                            eventModel.EventImage = Convert.ToString(row["EventImage"]);
                            eventModel.EventURL = Convert.ToString(row["EventURL"]);
                            eventModel.InvitationType = Convert.ToString(row["InvitationType"]);
                            eventModel.StateName = Convert.ToString(row["StateName"]);
                            eventModel.Summary = Convert.ToString(row["Summary"]);
                            eventModel.Zip = Convert.ToString(row["Zip"]);
                            eventModel.IsAttending = Convert.ToBoolean(row["IsAccepted"]);
                            eventModel.AttendeeCount = Convert.ToInt32(row["AttendeeCount"]);
                            eventModel.EventTime = Convert.ToString(row["EventTime"]);
                            eventModel.EventDayLeft = Convert.ToInt32(row["EventDayLeft"]);
                            eventModel.FileType = Convert.ToString(row["FileType"]);
                            eventModel.Fundraiser = "";

                        }

                    }
                }

            }

            return eventModel;
        }
        public EPAboutResponse aboutSelect(EpAboutRequest request)
        {
            Int32 returnResult = 0;
            EPAboutResponse response = new EPAboutResponse();
            response.aboutApiModel = new AboutApiModel();
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                request.FollowerId = request.FollowerId;
                request.SessionToken = request.SessionToken;
                DataSet ds = EventApiData.AboutSelect(request, returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                    if (returnResult > 0)
                    {
                        response.aboutApiModel = AboutList(ds, returnResult);
                        returnResult = 1;
                    }
                    else if (returnResult == 0)
                        returnResult = -2;
                    // response message
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiAccess", "aboutSelect");
                return response;
            }
        }
        private AboutApiModel AboutList(DataSet ds, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                AboutApiModel data = new AboutApiModel();
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                data.ImagePath = Convert.ToString(row2["ImagePath"]);
                                data.Content = Convert.ToString(row2["Content"]);
                                data.UserId = Convert.ToInt32(row2["UserId"]);
                            }
                        }
                    }
                }
                return data;
            }

            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiAccess", "AboutList");
                ReturnResult = -1;
                return null;
            }
        }
        public EventAPIResponse GetEvents(EventAPIRequest request)
        {
            List<EventAPIModel> Events = new List<EventAPIModel>();

            EventAPIResponse serviceResponse = new EventAPIResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "No record found.";
            serviceResponse.Events = Events;
            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            DataSet ds = EventApiData.EventSelect(request, out returnResult);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["EventId"]);
                if (returnResult > 0)
                {
                    Events = EventList(ds, out returnResult);
                    if (returnResult == -1)
                    {
                        serviceResponse.ReturnCode = "-1";
                        serviceResponse.ReturnMessage = "Technical error.";
                    }
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "Retrieved successfully.";
                    serviceResponse.Events = Events;

                }
                else if (returnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";

                }
                else if (returnResult == -5)
                {
                    serviceResponse.ReturnCode = "-5";
                    serviceResponse.ReturnMessage = "Authentication failed.";

                }
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }
            return serviceResponse;
        }


        private static List<EventAPIModel> EventList(DataSet ds, out int returnResult)
        {
            List<EventAPIModel> eventList = new List<EventAPIModel>();
            EventAPIModel eventModel = null;
            returnResult = 0;

            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        returnResult = Convert.ToInt32(row["EventId"]);

                        if (returnResult > 0)
                        {
                            eventModel = new EventAPIModel();

                            eventModel.EventId = returnResult.ToString();
                            eventModel.Title = Convert.ToString(row["Title"]);
                            eventModel.AddressLine1 = Convert.ToString(row["AddressLine1"]);
                            eventModel.AddressLine2 = Convert.ToString(row["AddressLine2"]);
                            eventModel.CityName = Convert.ToString(row["CityName"]);
                            eventModel.EventAgenda = Convert.ToString(row["EventAgenda"]);
                            eventModel.EventDate = Convert.ToString(row["EventDate"]);
                            eventModel.EventImage = Convert.ToString(row["EventImage"]);
                            eventModel.EventURL = Convert.ToString(row["EventURL"]);
                            eventModel.InvitationType = Convert.ToString(row["InvitationType"]);
                            eventModel.StateName = Convert.ToString(row["StateName"]);
                            eventModel.Summary = Convert.ToString(row["Summary"]);
                            eventModel.Zip = Convert.ToString(row["Zip"]);
                            eventModel.IsAttending = Convert.ToBoolean(row["IsAccepted"]);
                            eventModel.AttendeeCount = Convert.ToInt32(row["AttendeeCount"]);
                            eventModel.IsSubmited = Convert.ToBoolean(row["IsSubmited"]);
                            eventModel.EventTime = Convert.ToString(row["EventTime"]);
                            eventModel.EventDayLeft = Convert.ToInt32(row["EventDayLeft"]);
                            eventModel.FileType = Convert.ToString(row["FileType"]);
                            eventModel.Fundraiser = "";
                            eventList.Add(eventModel);
                        }

                    }
                }

            }

            return eventList;
        }

        public FPOfficeLocationResponse OfficeLocationSelect(EpAboutRequest request)
        {
            Int32 returnResult = 0;
            FPOfficeLocationResponse response = new FPOfficeLocationResponse();
            response.officeLocationModel = new List<OfficeLocationModel>();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                request.FollowerId = request.FollowerId;
                request.SessionToken = request.SessionToken;
                DataSet ds = EventApiData.OfficeLocationSelect(request, returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["OfficeId"]);
                    if (returnResult > 0)
                    {
                        response.officeLocationModel = OfficeLocationList(ds, returnResult);
                        returnResult = 2;
                    }

                    // response message
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiAccess", "OfficeLocationSelect");
                return response;
            }
        }

        private List<OfficeLocationModel> OfficeLocationList(DataSet ds, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<OfficeLocationModel> data = new List<OfficeLocationModel>();
                List<OfficeLocationDayModel> dayTitle = null;
                OfficeLocationModel officeDeatilModel = null;
                OfficeLocationDayModel DayTitleModel = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            dayTitle = new List<OfficeLocationDayModel>();
                            officeDeatilModel = new OfficeLocationModel();

                            officeDeatilModel.OfficeId = Convert.ToInt32(row["OfficeId"]);
                            officeDeatilModel.Title = (row["Title"] as string) ?? "";
                            officeDeatilModel.AddressLine1 = (row["AddressLine1"] as string) ?? "";
                            officeDeatilModel.AddressLine2 = (row["AddressLine2"] as string) ?? "";
                            officeDeatilModel.ZipCode = Convert.ToInt32(row["Zip"]);
                            officeDeatilModel.PhoneNumber = (row["PhoneNumber"] as string) ?? "";
                            officeDeatilModel.FaxNumber = (row["FaxNumber"] as string) ?? "";
                            officeDeatilModel.CreateDate = (row["CreateDate"] as string) ?? "";
                            officeDeatilModel.StateName= (row["StateName"] as string) ?? "";
                            officeDeatilModel.CityName= (row["CityName"] as string) ?? "";
                            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                            {
                                foreach (DataRow row2 in ds.Tables[1].Select("OfficeId=" + officeDeatilModel.OfficeId))
                                {
                                    DayTitleModel = new OfficeLocationDayModel();
                                    DayTitleModel.DayTitle = (row2["DayTitle"] as string) ?? "";
                                    DayTitleModel.TimeFrom = (row2["TimeFrom"] as string) ?? "";
                                    DayTitleModel.TimeTo = (row2["TimeTo"] as string) ?? "";
                                    dayTitle.Add(DayTitleModel);
                                }
                            }
                            officeDeatilModel.officeLocationDayList = dayTitle;
                            data.Add(officeDeatilModel);
                        }
                    }

                }
                return data;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiAccess", "OfficeLocationList");
                ReturnResult = -1;
                return null;
            }
        }
    }
}
