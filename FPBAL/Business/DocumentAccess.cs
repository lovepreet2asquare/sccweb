﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class DocumentAccess : IDocument
    {
        public DocumentResponse GetDocuments(DocumentRequest request)
        {
            int returnResult = 0;
            List<DocumentModel> Documents = new List<DocumentModel>();
            DocumentResponse response = new DocumentResponse();
            response.Documents = Documents;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            //DocumentModel model = new DocumentModel();
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = DocumentData.DocumentSelect(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["DocumentId"]);
                    if (returnResult > 0)
                    {
                        Documents = DocumentList(ds, out returnResult);
                        response.Documents = Documents;
                        //returnResult = 2;
                    }
                }

                // response message
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentAccess", "DocumentSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }

        public NJResponse DocumentLogInsert(RestrictedDocLogRequest request)
        {
            NJResponse serviceResponse = new NJResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";


            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            returnResult = DocumentData.DocumentLogInsert(request);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Submitted successfully";
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }
        private List<DocumentModel> DocumentList(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            try
            {
                List<DocumentModel> data = new List<DocumentModel>();
                DocumentModel model = null;
                DocumentDetailModel documentDetailModel = null;
                List<DocumentDetailModel> DocumentDetailList = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[1].Rows)
                            {
                                model = new DocumentModel();
                                DocumentDetailList = new List<DocumentDetailModel>();
                                model.FolderName = Convert.ToString(row["DocumentTitle"]);

                                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow row2 in ds.Tables[0].Select("DocumentTitle='" + model.FolderName + "'"))
                                    {
                                        documentDetailModel = new DocumentDetailModel();
                                        documentDetailModel.DocumentId = Convert.ToString(Convert.ToInt32(row2["DocumentId"]));  //UtilityAccess.Encrypt(Convert.ToString(Convert.ToInt32(row2["DocumentId"])));
                                        documentDetailModel.DocumentName = Convert.ToString(row2["DocumentName"]);
                                        documentDetailModel.DocumentPath = Convert.ToString(row2["DocumentPath"]);
                                        documentDetailModel.ThumbnailPath = Convert.ToString(row2["ThumbnailPath"]);

                                        DocumentDetailList.Add(documentDetailModel);
                                    }
                                }
                                model._DocumentList = DocumentDetailList;
                                data.Add(model);
                                ReturnResult = 2;


                            }
                        }

                    }
                }
                return data;
            }

            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentAccess", "DocumentList");
                ReturnResult = -1;
                return null;
            }
        }
    }
}
