﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace FPBAL.Business
{
    public class BatchAccess : IBatch
    {
        BatchData batchData = new BatchData();
        public BatchResponse BatchSelectAll(BatchModel model)
        {
            BatchResponse batchResponse = new BatchResponse();

            batchResponse._BatchModel = new BatchModel();
            batchResponse.ReturnCode = 0;
            batchResponse.ReturnMessage = Response.Message(0);

            try
            {
                if (!string.IsNullOrEmpty(model.DateFrom))
                    model.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                if (!string.IsNullOrEmpty(model.DateTo))
                    model.DateTo = UtilityAccess.ToDate(model.DateTo);
                model.UserId = UtilityAccess.Decrypt(model.EncryptedUserId);
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                int returnResult = 0;

                DataSet ds = batchData.BatchSelectAll(model, out returnResult);

                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //foreach (DataRow row in ds.Tables[0].Rows)
                        //{
                        batchResponse._BatchModel._BatchList = BatchList(ds.Tables[0], returnResult);
                        //}
                    }
                    if (ds.Tables[1].Rows.Count > 0)
                    {

                        batchResponse._BatchModel._PurposeList = UtilityAccess.RenderList(ds.Tables[1], 0);

                    }
                    batchResponse._BatchModel._PurposeList = batchResponse._BatchModel._PurposeList;
                    batchResponse.ReturnCode = returnResult;
                    batchResponse.ReturnMessage = Response.Message(returnResult);


                }
                else
                {
                    batchResponse.ReturnCode = -2;
                    batchResponse.ReturnMessage = Response.Message(-2);
                }
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventAccess", "EventSelectAll");
            }
            return batchResponse;
        }
        private List<BatchModel> BatchList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<BatchModel> data = new List<BatchModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new BatchModel
                            {
                                BatchId = Convert.ToInt32(row["BatchId"]),
                                BatchNumber = Convert.ToString(row["BatchNumber"]),
                                EncryptedEventId = UtilityAccess.Encrypt(row["BatchId"].ToString()),
                                PurposeText = Convert.ToString(row["PurposeText"]),
                                CreatedBy = Convert.ToString(row["CreatedBy"]),
                                CreateDate = Convert.ToString(row["CreateDate"]),
                                Status = Convert.ToString(row["Status"]),
                                DateRange = Convert.ToString(row["DateRange"]),
                                FileType = Convert.ToString(row["FileType"]),
                                FilePath = Convert.ToString(row["FilePath"])
                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BatchModel", "BatchList");
                ReturnResult = -1;
                return null;
            }
        }

        public BatchResponse Select(BatchModel model)
        {
            Int32 returnResult = 0;
            BatchResponse response = new BatchResponse();

            response._BatchModel = new BatchModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                model.UserId = UtilityAccess.Decrypt(model.EncryptedUserId);
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                DataSet ds = batchData.Select(model, out returnResult);


                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        response._BatchModel.BatchId = Convert.ToInt32(row["BatchId"]);
                        response._BatchModel.BatchNumber = Convert.ToString(row["BatchNumber"]);
                        response._BatchModel.PurposeId = Convert.ToString(row["PurposeId"]);
                        response._BatchModel.PurposeText = Convert.ToString(row["PurposeText"]);
                        response._BatchModel.FileTypeId = Convert.ToString(row["FileTypeId"]);
                        response._BatchModel.FileType = Convert.ToString(row["FileType"]);
                        response._BatchModel.DateFrom = Convert.ToString(row["DateFrom"]);
                        response._BatchModel.DateTo = Convert.ToString(row["DateTo"]);
                        response._BatchModel.TransactionType = Convert.ToString(row["TransactionType"]);
                        response._BatchModel.Status = Convert.ToString(row["Status"]);
                        //response._BatchModel.UserId = Convert.ToString(row["BatchId"]);
                        response._BatchModel.CreateDate = Convert.ToString(row["CreateDate"]);
                        //response._BatchModel.CreateUTCDate = Convert.ToString(row["BatchId"]);
                        response._BatchModel.FilePath = Convert.ToString(row["FilePath"]);
                        response._BatchModel.NoOfPayment = Convert.ToInt32(row["NoOfPayment"]);
                        response._BatchModel.NoOfRefund = Convert.ToInt32(row["NoOfRefund"]);
                        response._BatchModel.CreatedBy = Convert.ToString(row["CreatedBy"]);

                        if (response._BatchModel.TransactionType == "Both")
                        {
                            response._BatchModel.payment = true;
                            response._BatchModel.Refund = true;
                        }
                        else if (response._BatchModel.TransactionType == "Paid")
                        {
                            response._BatchModel.payment = true;
                        }
                        else if (response._BatchModel.TransactionType == "Refunded")
                        {
                            response._BatchModel.Refund = true;
                        }
                    }
                }

                if (ds != null && ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                {
                    response._BatchModel._PurposeList = UtilityAccess.RenderList(ds.Tables[1], 1);
                    response._BatchModel._PurposeList.Add(new SelectListItem() { Text = "Add New", Value = "-2" });
                }
                if (ds != null && ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                {
                    response._BatchModel._FileTypeList = UtilityAccess.RenderList(ds.Tables[2], 1);
                }
                // status list
                response._BatchModel._StatusList = new List<SelectListItem>();
                response._BatchModel._StatusList.Add(new SelectListItem { Text = "Select", Value = "" });
                response._BatchModel._StatusList.Add(new SelectListItem { Text = "Created", Value = "Created" });
                response._BatchModel._StatusList.Add(new SelectListItem { Text = "Downloaded", Value = "Downloaded" });
                response._BatchModel._StatusList.Add(new SelectListItem { Text = "Uploaded", Value = "Uploaded" });

                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignAccess", "SelectAll");
                return response;
            }
        }
        public BatchResponse BatchAdd(BatchModel model)
        {
            Int32 returnResult = 0;
            BatchResponse response = new BatchResponse();

            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = UtilityAccess.Decrypt(model.EncryptedUserId);
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                Int32 UserId = batchData.AddOrEdit(model, out returnResult);
                if (returnResult > 0)
                {
                    response._BatchModel = new BatchModel();
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }


                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AboutUsAccess", "AddOrEdit");
                return response;
            }
        }
        public BatchResponse FileStatusUpdate(BatchModel model)
        {
            Int32 returnResult = 0;
            BatchResponse response = new BatchResponse();

            response._BatchModel = new BatchModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                model.UserId = UtilityAccess.Decrypt(model.EncryptedUserId);
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                batchData.FileStatusUpdate(model, out returnResult);

                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignAccess", "SelectAll");
                return response;
            }
        }

        public BatchResponse SelectBatchTransactions(BatchModel model)
        {
            Int32 returnResult = 0;
            BatchResponse response = new BatchResponse();

            response._BatchModel = new BatchModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                if (!string.IsNullOrEmpty(model.DateFrom))
                    model.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                if (!string.IsNullOrEmpty(model.DateTo))
                    model.DateTo = UtilityAccess.ToDate(model.DateTo);

                model.UserId = UtilityAccess.Decrypt(model.EncryptedUserId);
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                if (model.payment == true && model.Refund == true)
                {
                    model.TransactionType = "Both";
                }
                else if (model.payment == true)
                {
                    model.TransactionType = "Paid";
                }
                else if (model.Refund == true)
                {
                    model.TransactionType = "Refunded";
                }

                DataSet ds = batchData.BatchSelectTransactions(model, out returnResult);


                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    // count number of payments & refunds 
                    model.NoOfRefund = ds.Tables[0].Select("Status = 'Refunded'").Length;
                    model.NoOfPayment = ds.Tables[0].Select("Status = 'Paid'").Length;


                    // create csv file and add entry into database
                    if (model.FileTypeId == "1" && model.Status == "Created")// CSV
                       // model.FilePath = WriteCSVFile(ds.Tables[0]);
                        model.FilePath = WriteCSVFilenew(ds.Tables[0]);

                    //else if (model.FileType == "2")// XLS
                    //    model.FilePath = WriteXLSFile(ds.Tables[0]);

                   
                    // Add batch file
                    batchData.AddOrEdit(model, out returnResult);
                }
                else if(returnResult!=-1)
                    returnResult = 24;

                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignAccess", "SelectAll");
                return response;
            }
        }
        public String WriteCSVFile(DataTable dt)
        {
            try
            {
                // remove extra column fron table before writing file
                dt.Columns.Remove("Status");


                String FilePath = Convert.ToString(DateTimeOffset.UtcNow.ToUnixTimeSeconds());
                String fileLocation = System.Web.HttpContext.Current.Server.MapPath("~/BatchFiles/CSV");


                if (!System.IO.File.Exists(fileLocation))
                    System.IO.Directory.CreateDirectory(fileLocation);


                fileLocation += "/" + FilePath + ".csv";
                // save file path into database
                FilePath = "CSV/" + FilePath + ".csv";

                StreamWriter sw = new StreamWriter(fileLocation, false);
                int iColCount = dt.Columns.Count;
                for (int i = 0; i < iColCount; i++)
                {
                    sw.Write(dt.Columns[i].ToString()
                        .Replace("Reserved1", "Reserved")
                        .Replace("Reserved2", "Reserved")
                        .Replace("Reserved3", "Reserved")
                        .Replace("Reserved4", "Reserved")
                        .Replace("Reserved5", "Reserved")
                        .Replace("Reserved6", "Reserved")
                        );

                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
                // Now write all the rows.
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < iColCount; i++)
                    {
                        if (!Convert.IsDBNull(dr[i]))
                        {
                            sw.Write(dr[i].ToString());
                        }
                        if (i < iColCount - 1)
                        {
                            sw.Write(",");
                        }
                    }
                    sw.Write(sw.NewLine);
                }
                sw.Close();

                return FilePath;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignAccess", "WriteCSVFile");
                return "";
            }
        }
        public String WriteCSVFilenew(DataTable dt)
        {
            try
            {
                // remove extra column fron table before writing file
                dt.Columns.Remove("Status");


                String FilePath = Convert.ToString(DateTimeOffset.UtcNow.ToUnixTimeSeconds());
                String fileLocation = System.Web.HttpContext.Current.Server.MapPath("~/BatchFiles/CSV");


                if (!System.IO.File.Exists(fileLocation))
                    System.IO.Directory.CreateDirectory(fileLocation);


                fileLocation += "/" + FilePath + ".csv";
                // save file path into database
                FilePath = "CSV/" + FilePath + ".csv";
                string strFilePath = fileLocation;
                StreamWriter sw = new StreamWriter(strFilePath, false);
                //headers  
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    sw.Write(dt.Columns[i]);
                    if (i < dt.Columns.Count - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (!Convert.IsDBNull(dr[i]))
                        {
                            string value = dr[i].ToString();
                            if (value.Contains(','))
                            {
                                value = String.Format("\"{0}\"", value);
                                sw.Write(value);
                            }
                            else
                            {
                                sw.Write(dr[i].ToString());
                            }
                        }
                        if (i < dt.Columns.Count - 1)
                        {
                            sw.Write(",");
                        }
                    }
                    sw.Write(sw.NewLine);
                }
                sw.Close();

                return FilePath;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "CampaignAccess", "WriteCSVFile");
                return "";
            }
        }

        //public String WriteXLSFile(DataTable dt)
        //{
        //    try
        //    {

        //        String FilePath = Convert.ToString(DateTimeOffset.UtcNow.ToUnixTimeSeconds());
        //        String fileLocation = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/BatchFiles/XLS");


        //        if (!System.IO.File.Exists(fileLocation))
        //            System.IO.Directory.CreateDirectory(fileLocation);


        //        fileLocation += "/" + FilePath + ".xls";
        //        // save file path into database
        //        FilePath = "XLS/" + FilePath + ".xls";


        //        // write xls
        //        Microsoft.Office.Interop.Excel.Application oXL;
        //        Microsoft.Office.Interop.Excel._Workbook oWB;
        //        Microsoft.Office.Interop.Excel._Worksheet oSheet;
        //        Microsoft.Office.Interop.Excel.Range oRng;



        //        //Start Excel and get Application object.
        //        oXL = new Microsoft.Office.Interop.Excel.Application();
        //        oXL.Visible = true;

        //        //Get a new workbook.
        //        oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
        //        oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;

        //        //Add table headers going cell by cell.
        //        oSheet.Cells[1, 1] = "First Name";
        //        oSheet.Cells[1, 2] = "Last Name";
        //        oSheet.Cells[1, 3] = "Full Name";
        //        oSheet.Cells[1, 4] = "Salary";

        //        //Format A1:D1 as bold, vertical alignment = center.
        //        oSheet.get_Range("A1", "D1").Font.Bold = true;
        //        oSheet.get_Range("A1", "D1").VerticalAlignment =
        //            Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

        //        // Create an array to multiple values at once.
        //        string[,] saNames = new string[5, 2];

        //        saNames[0, 0] = "John";
        //        saNames[0, 1] = "Smith";
        //        saNames[1, 0] = "Tom";

        //        saNames[4, 1] = "Johnson";

        //        //Fill A2:B6 with an array of values (First and Last Names).
        //        oSheet.get_Range("A2", "B6").Value2 = saNames;

        //        //Fill C2:C6 with a relative formula (=A2 & " " & B2).
        //        oRng = oSheet.get_Range("C2", "C6");
        //        oRng.Formula = "=A2 & \" \" & B2";

        //        //Fill D2:D6 with a formula(=RAND()*100000) and apply format.
        //        oRng = oSheet.get_Range("D2", "D6");
        //        oRng.Formula = "=RAND()*100000";
        //        oRng.NumberFormat = "$0.00";

        //        //AutoFit columns A:D.
        //        oRng = oSheet.get_Range("A1", "D1");
        //        oRng.EntireColumn.AutoFit();

        //        oXL.Visible = false;
        //        oXL.UserControl = false;
        //        oWB.SaveAs(fileLocation, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
        //            false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
        //            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

        //        oWB.Close();


        //        return FilePath;
        //    }
        //    catch (Exception ex)
        //    {
        //        ApplicationLogger.LogError(ex, "CampaignAccess", "CreateCSVFile");
        //        return "";
        //    }
        //}

        //public String WriteXLS(DataTable dt)
        //{          

        //    HttpContext.Current.Response.Clear();
        //    HttpContext.Current.Response.Charset = "";
        //    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //            //If(iDX = 1) Then
        //            //    HttpContext.Current.Response.ContentType = "application/vnd.word"
        //            //    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fname + ".doc")
        //            //Else
        //                HttpContext.Current.Response.ContentType = "application/ms-excel";
        //    HttpContext.Current.Response.ContentType = "application/vnd.xlsx";
        //    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & fname & ".xls");
        //    //        End If
        //    System.IO.StringWriter stringWriter = new StringWriter();
        //    System.Web.UI.HtmlTextWriter htmlTextWriter = new System.Web.UI.HtmlTextWriter(stringWriter);

        //   // ((Table)dt).RenderControl(htmlTextWriter);
        //    HttpContext.Current.Response.Write(Convert.ToString(stringWriter));
        //    HttpContext.Current.Response.Flush();
        //    HttpContext.Current.Response.End();
        //}
    }
}
