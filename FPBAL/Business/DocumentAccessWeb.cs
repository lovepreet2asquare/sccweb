﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class DocumentAccessWeb : IDocumentWeb
    {
        DocumentDataWeb documentData = new DocumentDataWeb();
        public DocumentResponseWeb Add(DocumentModelWeb model)
        {
            Int32 ReturnResult = 0;
            DocumentResponseWeb response = new DocumentResponseWeb();
            response._documentModel = new DocumentModelWeb();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(ReturnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedDocumentId))
                {
                    model.DocumentId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedDocumentId));
                }
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
               
                DataSet ds = documentData.Add(model, out ReturnResult);

                string deviceToken = string.Empty, deviceType = string.Empty, Message = string.Empty, did = string.Empty;
                
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    int result = 0;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        result = Convert.ToInt32(row["DocumentId"]);
                        if (result > 0)
                        {
                            Thread T1 = new Thread(delegate ()
                            {
                                did = Convert.ToInt32(row["DocumentId"]).ToString();

                        deviceToken = Convert.ToString(row["DeviceToken"]);
                        deviceType = Convert.ToString(row["DeviceType"]);
                        Message = Convert.ToString(row["NotifyMessage"]);
                        if (deviceType.ToUpper() == "IOS")
                            APNSNotificationAccess.ApnsNotification(deviceToken, Message, did, "d");
                        else
                           if (deviceType.ToUpper() == "ANDROID")
                            FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType, did, "document");
                        });
                            T1.IsBackground = true;
                            T1.Start();
                   }
                }
                }
                response.ReturnCode = ReturnResult;
                response.ReturnMessage = Response.Message(ReturnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentAccessWeb", "Add");
                return response;
            }
        }
        public DocumentResponseWeb SelectAll(DocumentModelWeb model)
        {
            DocumentResponseWeb response = new DocumentResponseWeb();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            response._documentModel = new DocumentModelWeb();
            Int32 returnResult = 0;
            try
            {
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

                DataSet ds = documentData.SelectAll(model, out returnResult);

                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response._documentModel._DocumentList = DocumentList(ds.Tables[0], returnResult);
                        returnResult = 2;

                    }
                }

                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }
                DocumentModelWeb docinfo = new DocumentModelWeb();
                response._documentModel._StatusList = UtilityAccess.ActiveStatusList(2);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentAccessWeb", "SelectAll");
            }
            return response;
        }
        public DocumentResponseWeb LogSelectAll(DocumentModelWeb model)
        {
            DocumentResponseWeb response = new DocumentResponseWeb();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            response._documentModel = new DocumentModelWeb();
            Int32 returnResult = 0;
            try
            {
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

                DataSet ds = documentData.LogSelectAll(model, out returnResult);

                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response._documentModel._LogBookList = LogList(ds.Tables[0], returnResult);
                        returnResult = 2;

                    }
                }

                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }
                DocumentModelWeb docinfo = new DocumentModelWeb();
                response._documentModel._StatusList = UtilityAccess.ActiveStatusList(2);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentAccessWeb", "SelectAll");
            }
            return response;
        }
        private List<DocumentModelWeb> DocumentList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<DocumentModelWeb> data = new List<DocumentModelWeb>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new DocumentModelWeb
                            {

                                EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"])),
                                EncryptedDocumentId = UtilityAccess.Encrypt(Convert.ToString(row["DocumentId"])),
                                DocumentName =Convert.ToString(row["DocumentName"]),
                                DocumentTitle = Convert.ToString(row["DocumentTitle"]),
                                DocumentPath =Convert.ToString(row["DocumentPath"]),
                                UserId = Convert.ToInt32(row["UserId"]),

                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentModelWeb", "DocumentList");
                ReturnResult = -1;
                return null;
            }
        }
        private List<DocumentModelWeb> LogList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<DocumentModelWeb> data = new List<DocumentModelWeb>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new DocumentModelWeb
                            {

                                EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"])),
                                EncryptedDocumentId = UtilityAccess.Encrypt(Convert.ToString(row["DocumentId"])),
                                UniqueId =Convert.ToString(row["UniqueId"]),
                                FirstName=Convert.ToString(row["FirstName"]),
                                LastName=Convert.ToString(row["LastName"]),
                                MI=Convert.ToString(row["MI"]),
                                CreateDate = Convert.ToString(row["CreateDate"]),
                                ISDCode=Convert.ToString(row["ISDCode"]),
                                Mobile=Convert.ToString(row["Mobile"]),
                                UserId = Convert.ToInt32(row["UserId"]),
                                FileName = Convert.ToString(row["FileName"]),
                                ThumbnailPath = Convert.ToString(row["ThumbnailPath"]),

                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentModelweb", "DocumentList");
                ReturnResult = -1;
                return null;
            }
        }
        public DocumentResponseWeb Delete(DocumentModelWeb model)
        {
            DocumentResponseWeb response= new DocumentResponseWeb();
            DocumentModelWeb docinfo= new DocumentModelWeb();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            if (model != null && model.EncryptedDocumentId != null)
                model.DocumentId= Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedDocumentId));

            model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = documentData.Delete(model);
            if (returnResult > 0)
            {
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                response._documentModel= docinfo;
            }
            return response;
        }
    }
}
