﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using FPDAL.Data;
using MimeKit;
using MailKit;
using MailKit.Net.Smtp;

namespace FPBAL.Business
{
    public class EmailUtilityNew
    {
        public static String ReadHtml(String FileName)
        {
            string body = string.Empty;
            try
            {
                string Url = "~/newsletters/user" + "/" + FileName;
                //string path = Path.Combine(HttpContext.Current.Server.MapPath("/newsletters/user/" + FileName));
                string path = "";
                if (HttpContext.Current != null)
                {
                    path = HttpContext.Current.Server.MapPath(Url);
                }
                else
                {
                    path = System.Web.Hosting.HostingEnvironment.MapPath(Url);
                }
                using (StreamReader reader = new StreamReader(path))
                {
                    body = reader.ReadToEnd();
                }
                return body;
            }
            catch (Exception exc)
            {
                ApplicationLogger.LogError(exc, "EmailUtilityNew", "ReadHtml");
                return "";
            }
        }
        //public static int SendBulkEmails(DataTable dt)
        //{
        //    DirectoryData directoryData = new DirectoryData();
        //    int msgg = 0;
        //    int ReturnValue = -1;

        //    try
        //    {
        //        if (dt != null)
        //        {

        //            string recieptents = string.Empty;
        //            String _Body = EmailUtility.ReadHtml("Invite.html");
        //            String _EmailBody = "";
        //            int returnVal = -1;

        //            Parallel.ForEach(dt.AsEnumerable(), row =>
        //            {
        //                _EmailBody = _Body
        //                         .Replace("{link}", "invitaion")
        //                         .Replace("{FirstName}", row["FirstName"].ToString());
        //                int Userid = Convert.ToInt32(row["UserId"].ToString());
        //                ReturnValue = SendMessageSmtpNew1(row["FirstName"].ToString(), row["Email"].ToString(), _EmailBody);
        //                if (ReturnValue > 0)
        //                {
        //                    string Status = "Invited";
        //                    Int32 ds = directoryData.InviteEmailSendStatusUpdateNew(Userid, Status);

        //                }
        //                if (ReturnValue < 0)
        //                {
        //                    Int32 ds = directoryData.InviteEmailSendDeleteNew(Userid);
        //                }
        //                Thread.Sleep(200);
                        
        //            });

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return -1;
        //    }
        //    return msgg;
        //}

        public static int SendMessageSmtpNew1(String senderName, String emailTo, String emailBody)
        {
            try
            {

                MimeMessage mail = new MimeMessage();
                //mail.From.Add(new MailboxAddress("Excited Admin", "foo@YOUR_DOMAIN_NAME"));
                mail.From.Add(new MailboxAddress("NJSPBA", "njspba@keplerconnect.com"));
                mail.To.Add(new MailboxAddress(senderName, emailTo));
                mail.Subject = "Invitation From NJPBA";
                //mail.Body = new TextPart("plain")
                //{
                //    Text = @emailBody,
                //};
                var bodyBuilder = new BodyBuilder();
                bodyBuilder.HtmlBody = emailBody;

                mail.Body = bodyBuilder.ToMessageBody();
                // Send it!
                using (var client = new SmtpClient())
                {
                    // XXX - Should this be a little different?
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("smtp.mailgun.org", 587, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate("no-reply@keplerconnect.com", "4acdea0508d21bd17d47a5313ce9aab1-7fba8a4e-1e6d99d6");


                    client.Send(mail);
                    client.Disconnect(true);
                }
                return 1;
            }

            catch (Exception exc)
            {
                ApplicationLogger.LogError(exc, "EmailUtilityNew", "SendEmail");
                return -1;
            }

        }
        public static int SendEmail(String senderName, String emailTo, String subject, String emailBody, bool blnIsHtml = false)
        {
            try
            {
                //
                string smtpFromEmail = ConfigurationManager.AppSettings["smtpFromEmail"].ToString();
                string smtpMailPassword = ConfigurationManager.AppSettings["smtpMailPassword"].ToString();
                string smtpClient = ConfigurationManager.AppSettings["smtpClient"].ToString();

                MimeMessage msg = new MimeMessage();
                msg.To.Add(new MailboxAddress(senderName, emailTo));
                //msg.From = new MailAddress("sukhvirs63@gmail.com", strEmailSubject);
                msg.From.Add(new MailboxAddress("NJSPBA", "njspba@keplerconnect.com"));

                msg.Subject = subject;
                var bodyBuilder = new BodyBuilder();
                bodyBuilder.HtmlBody = emailBody;

                msg.Body = bodyBuilder.ToMessageBody();



                /******** Using Gmail Domain ********/
                using (var client = new SmtpClient())
                {
                    // XXX - Should this be a little different?
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("smtp.mailgun.org", 587, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate("no-reply@keplerconnect.com", "4acdea0508d21bd17d47a5313ce9aab1-7fba8a4e-1e6d99d6");


                    client.Send(msg);
                    client.Disconnect(true);
                }
                return 1;

              
            }
            catch (Exception exc)
            {
                ApplicationLogger.LogError(exc, "EmailUtility", "SendEmail");
                return -1;
            }

        }
        public static int SendEmailNew1(string strEmailTo, string fromName, string strEmailSubject, string strEmailBody, out Exception exObj, bool blnIsHtml = false)
        {
            if (string.IsNullOrEmpty(fromName))
                fromName = "SCC Team";
            exObj = null;
            if (!string.IsNullOrEmpty(strEmailTo))
            {
                try
                {

                    MimeMessage msg = new MimeMessage();
                    msg.To.Add(new MailboxAddress(fromName, strEmailTo));
                    //msg.From = new MailAddress("sukhvirs63@gmail.com", strEmailSubject);
                    msg.From.Add(new MailboxAddress("SCC Team", "no-reply@keplerconnect.com"));
                    // msg.Bcc.Add(new MailboxAddress("SCC Team", "josh@gottheimer.com"));
                    msg.Bcc.Add(new MailboxAddress("SCC Team", "jgotthei@yahoo.com"));
                    msg.Bcc.Add(new MailboxAddress("SCC Team", "mike@josh4congress.com"));
                    msg.Bcc.Add(new MailboxAddress("SCC Team", "james@josh4congress.com"));
                    msg.Bcc.Add(new MailboxAddress("SCC Team", "keith@josh4congress.com"));
                    msg.Bcc.Add(new MailboxAddress("SCC Team", "val@josh4congress.com"));
                    msg.Bcc.Add(new MailboxAddress("SCC Team", "nj5blake@gmail.com"));
                    msg.Bcc.Add(new MailboxAddress("SCC Team", "my2asquare@gmail.com"));

                    msg.Subject = strEmailSubject;
                    var bodyBuilder = new BodyBuilder();
                    bodyBuilder.HtmlBody = strEmailBody;

                    msg.Body = bodyBuilder.ToMessageBody();



                    /******** Using MailGun Domain ********/
                    using (var client = new SmtpClient())
                    {

                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                        client.Connect("smtp.mailgun.org", 587, false);
                        client.AuthenticationMechanisms.Remove("XOAUTH2");
                        client.Authenticate("no-reply@keplerconnect.com", "4acdea0508d21bd17d47a5313ce9aab1-7fba8a4e-1e6d99d6");
                        client.Send(msg);
                        client.Disconnect(true);
                    }


                }
                catch (Exception ex)
                {
                    exObj = ex;
                    ApplicationLogger.LogError(ex, "EmailUtilityNew", "SendEmailNew1-----"+ strEmailTo);
                    return -1;
                }
            }
            return 1;
        }

        public static int SendEmailNew(string strEmailTo, string fromName, string strEmailSubject, string strEmailBody, out Exception exObj, bool blnIsHtml = false)
        {
            if (string.IsNullOrEmpty(fromName))
                fromName = "SCC Team";
            exObj = null;
            if (!string.IsNullOrEmpty(strEmailTo))
            {
                try
                {

                    MimeMessage msg = new MimeMessage();
                    msg.To.Add(new MailboxAddress(fromName, strEmailTo));
                    //msg.From = new MailAddress("sukhvirs63@gmail.com", strEmailSubject);
                    msg.From.Add(new MailboxAddress("SCC Team", "no-reply@keplerconnect.com"));

                    msg.Subject = strEmailSubject;
                    var bodyBuilder = new BodyBuilder();
                    bodyBuilder.HtmlBody = strEmailBody;

                    msg.Body = bodyBuilder.ToMessageBody();



                    /******** Using MailGun Domain ********/
                    using (var client = new SmtpClient())
                    {

                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                        client.Connect("smtp.mailgun.org", 587, false);
                        client.AuthenticationMechanisms.Remove("XOAUTH2");
                        client.Authenticate("no-reply@keplerconnect.com", "4acdea0508d21bd17d47a5313ce9aab1-7fba8a4e-1e6d99d6");
                        client.Send(msg);
                        client.Disconnect(true);
                    }
                   }
                catch (Exception ex)
                {
                    exObj = ex;
                    return -1;
                }
            }
            return 1;
        }

        public static int SendMessageSmtpNew(String senderName, String emailTo, String subject, String emailBody, bool blnIsHtml = false)
        {
            try
            {

                MimeMessage mail = new MimeMessage();
                //mail.From.Add(new MailboxAddress("Excited Admin", "foo@YOUR_DOMAIN_NAME"));
                mail.From.Add(new MailboxAddress("SCC Team", "no-reply@keplerconnect.com"));
                mail.To.Add(new MailboxAddress(senderName, emailTo));
                mail.Subject = subject;
                //mail.Body = new TextPart("plain")
                //{
                //    Text = @emailBody,
                //};
                var bodyBuilder = new BodyBuilder();
                bodyBuilder.HtmlBody = emailBody;

                mail.Body = bodyBuilder.ToMessageBody();
                // Send it!
                using (var client = new SmtpClient())
                {
                    // XXX - Should this be a little different?
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("smtp.mailgun.org", 587, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate("no-reply@keplerconnect.com", "4acdea0508d21bd17d47a5313ce9aab1-7fba8a4e-1e6d99d6");

                    client.Send(mail);
                    client.Disconnect(true);
                }
                return 1;
            }
            
            catch (Exception exc)
            {
                ApplicationLogger.LogError(exc, "EmailUtilityNew", "SendEmailForInvitation");
                return -1;
            }

        }

        public static void SendMessageSmtp()
        {
            // Compose a message
            MimeMessage mail = new MimeMessage();
            //mail.From.Add(new MailboxAddress("Excited Admin", "foo@YOUR_DOMAIN_NAME"));
            mail.From.Add(new MailboxAddress("NJSPBA", "njspba@keplerconnect.com"));
            mail.To.Add(new MailboxAddress("Excited User", "lovepreet@2asquare.com"));
            mail.Subject = "Hello";
            mail.Body = new TextPart("plain")
            {
                Text = @"Testing some Mailgun awesomesauce!",
            };

            // Send it!
            using (var client = new SmtpClient())
            {
                // XXX - Should this be a little different?
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.mailgun.org", 587, false);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate("no-reply@keplerconnect.com", "4acdea0508d21bd17d47a5313ce9aab1-7fba8a4e-1e6d99d6");


                client.Send(mail);
                client.Disconnect(true);
            }

        }
   
    }
}
