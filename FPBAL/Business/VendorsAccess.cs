﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FPDAL.Data;
using FPBAL.Interface;
using FPModels.Models;
using System.Threading;

namespace FPBAL.Business
{
    public class VendorsAccess : IVendors
    {
        VendorsData vendorsData = new VendorsData();
        public VendorsResponse SelectAll(VendorsModel vendorsModel)
        {
            List<VendorsModel> vendors = new List<VendorsModel>();
            

            VendorsResponse serviceResponse = new VendorsResponse();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            VendorsModel vendorsModelinfo = new VendorsModel();
            int returnResult = 0;
            if (vendorsModel != null && !string.IsNullOrEmpty(vendorsModel.EncryptedVendorId))
                vendorsModel.VendorId = Convert.ToInt32(UtilityAccess.Decrypt(vendorsModel.EncryptedVendorId));

            vendorsModel.SessionToken = UtilityAccess.Decrypt(vendorsModel.EncryptedSessionToken);
            DataSet ds = vendorsData.SelectAll(vendorsModel);
            if(ds != null && ds.Tables.Count > 0)
            {
                if(ds.Tables[0].Rows.Count > 0)
                {
                    foreach(DataRow row in ds.Tables[0].Rows)
                    {
                        vendorsModelinfo = new VendorsModel();
                        vendorsModelinfo.VendorId = Convert.ToInt32(row["VendorId"] ?? 0);
                        vendorsModelinfo.EncryptedVendorId = UtilityAccess.Encrypt(Convert.ToString(row["VendorId"] ??  string.Empty));
                        vendorsModelinfo.VendorName = Convert.ToString(row["VendorName"] ?? string.Empty);
                        vendorsModelinfo.Category = Convert.ToString(row["Category"] ?? string.Empty);
                        vendorsModelinfo.Location = Convert.ToString(row["Location"] ?? string.Empty);
                        vendorsModelinfo.OfferDescription = Convert.ToString(row["OfferDescription"] ?? string.Empty);
                        vendorsModelinfo.Websitelink = Convert.ToString(row["Websitelink"] ?? string.Empty);
                        vendorsModelinfo.Status = Convert.ToString(row["status"] ?? string.Empty);
                        vendorsModelinfo.LogoPath = Convert.ToString(row["ImagePath"] ?? string.Empty);
                        vendors.Add(vendorsModelinfo);
                    }
                }
                vendorsModelinfo.vendorsModelsList = vendors;
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.Vendorsmodel = vendorsModelinfo;
            }
            return serviceResponse;
        }
        public VendorsResponse AddorEdit(VendorsModel model)
        {
            Int32 returnResult = 0;
            VendorsResponse response = new VendorsResponse();
            response.Vendorsmodel = new VendorsModel();
            int User_Id = model.UserId;
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedUserId))
                {
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                }
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                DataSet ds = vendorsData.AddorEdit(model, out returnResult);
                if (returnResult == -4)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "UniqueId already exists";
                }
                else
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorsAccess", "AddorEdit");
                return response;
            }

        }
        public VendorsResponse SelectById(VendorsModel model)
        {
            VendorsResponse directoryResponse = new VendorsResponse();
            VendorsModel directoryInfo = new VendorsModel();
            directoryResponse.Vendorsmodel = new VendorsModel();
            directoryResponse.ReturnCode = 0;
            directoryResponse.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            int userid = 0;
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

            if (model.EncryptedVendorId == null || model.EncryptedVendorId == "0" )
            {
                model.VendorId = 0;
            }
            else
            {
                model.VendorId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedVendorId));
            }
         
            userid = model.UserId;
            if (model.EncryptedUserId == null)
            {
                model.UserId = 0;
            }
            else
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            }
            DataSet ds = vendorsData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {
    
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        directoryResponse.Vendorsmodel = new VendorsModel();
                        directoryResponse.Vendorsmodel.VendorId = Convert.ToInt32(row["VendorId"] ?? 0);
                        directoryResponse.Vendorsmodel.EncryptedVendorId = UtilityAccess.Encrypt(Convert.ToString(row["VendorId"] ?? string.Empty));
                        directoryResponse.Vendorsmodel.VendorName = Convert.ToString(row["VendorName"] ?? 0);
                        directoryResponse.Vendorsmodel.Category = Convert.ToString(row["Category"] ?? string.Empty);
                        directoryResponse.Vendorsmodel.Location = Convert.ToString(row["Location"] ?? string.Empty);
                        directoryResponse.Vendorsmodel.Websitelink = Convert.ToString(row["Websitelink"] ?? string.Empty);
                        directoryResponse.Vendorsmodel.OfferDescription = Convert.ToString(row["OfferDescription"] ?? string.Empty);
                        directoryResponse.Vendorsmodel.StatusId = Convert.ToString(row["status"] ?? string.Empty);
                        directoryResponse.Vendorsmodel.Status = Convert.ToString(row["status"] ?? string.Empty);
                        directoryResponse.Vendorsmodel.LogoPath = Convert.ToString(row["ImagePath"] ?? string.Empty);
                        directoryResponse.Vendorsmodel.UserId = userid;

                    }
                }
                if(directoryResponse.Vendorsmodel.StatusId==null)
                {
                    directoryResponse.Vendorsmodel.StatusId = "1";
                    directoryResponse.Vendorsmodel.Status = "1";

                }
                directoryResponse.Vendorsmodel._StatusList = UtilityAccess.StatusListValTextNew();
                directoryResponse.ReturnCode = returnResult;
                directoryResponse.ReturnMessage = Response.Message(returnResult);
              }
            return directoryResponse;
        }

        public VendorsResponse Delete(VendorsModel model)
        {
            VendorsResponse serviceResponse = new VendorsResponse();
            VendorsModel userInfo = new VendorsModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            int returnResult = 0;

            if (model != null && model.EncryptedVendorId != null)
                model.VendorId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedVendorId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = vendorsData.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }

            return serviceResponse;
        }
    }
}
