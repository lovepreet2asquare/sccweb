﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FPDAL.Data;
using FPBAL.Interface;
using FPModels.Models;
using System.Threading;

namespace FPBAL.Business
{
    public class MemberWebAccess : IMemberWeb
    {
        MemberWebData memberData = new MemberWebData();

        public MemberResponseWeb SelectAll(MemberWebModel memberModel)
        {
            List<MemberWebModel> members = new List<MemberWebModel>();


            MemberResponseWeb serviceResponse = new MemberResponseWeb();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            MemberWebModel memberModelinfo = new MemberWebModel();
            int returnResult = 0;
            if (memberModel != null && !string.IsNullOrEmpty(memberModel.EncryptedMemberId))
                memberModel.MemberId = Convert.ToInt32(UtilityAccess.Decrypt(memberModel.EncryptedMemberId));

            memberModel.SessionToken = UtilityAccess.Decrypt(memberModel.EncryptedSessionToken);
            DataSet ds = memberData.SelectAll(memberModel);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        memberModelinfo = new MemberWebModel();
                        memberModelinfo.MemberId = Convert.ToInt32(row["MemberId"] ?? 0);
                        memberModelinfo.EncryptedMemberId = UtilityAccess.Encrypt(Convert.ToString(row["MemberId"] ?? string.Empty));
                        memberModelinfo.Name = Convert.ToString(row["Name"] ?? string.Empty);
                        memberModelinfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        memberModelinfo.CreateDate = Convert.ToString(row["CreateDate"] ?? string.Empty);
                        //chapterModelinfo.LogoPath = Convert.ToString(row["ImagePath"] ?? string.Empty);
                        members.Add(memberModelinfo);
                    }
                }
                memberModelinfo.memberModelList = members;
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.membermodel = memberModelinfo;
            }
            return serviceResponse;
        }
        public MemberResponseWeb AddorEdit(MemberWebModel model)
        {
            Int32 returnResult = 0;
            MemberResponseWeb response = new MemberResponseWeb();
            response.membermodel = new MemberWebModel();
            int User_Id = model.UserId;
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedUserId))
                {
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                }
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                DataSet ds = memberData.AddorEdit(model, out returnResult);
                if (returnResult == -4)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "UniqueId already exists";
                }
                else
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "Access", "AddorEdit");
                return response;
            }

        }
        public MemberResponseWeb SelectById(MemberWebModel model)
        {
            MemberResponseWeb directoryResponse = new MemberResponseWeb();
            MemberWebModel directoryInfo = new MemberWebModel();
            directoryResponse.membermodel = new MemberWebModel();
            directoryResponse.ReturnCode = 0;
            directoryResponse.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            int userid = 0;
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

            if (model.EncryptedMemberId == null || model.EncryptedMemberId == "0")
            {
                model.MemberId = 0;
            }
            else
            {
                model.MemberId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedMemberId));
            }

            userid = model.UserId;
            if (model.EncryptedUserId == null)
            {
                model.UserId = 0;
            }
            else
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            }
            DataSet ds = memberData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        directoryResponse.membermodel = new MemberWebModel();
                        directoryResponse.membermodel.MemberId = Convert.ToInt32(row["MemberId"] ?? 0);
                        directoryResponse.membermodel.EncryptedMemberId = UtilityAccess.Encrypt(Convert.ToString(row["MemberId"] ?? string.Empty));
                        directoryResponse.membermodel.Name = Convert.ToString(row["Name"] ?? 0);
                        directoryResponse.membermodel.Title = Convert.ToString(row["Title"] ?? 0);
                        directoryResponse.membermodel.LogoPath = Convert.ToString(row["ImagePath"] ?? string.Empty);
                        directoryResponse.membermodel.UserId = userid;

                    }
                }
                directoryResponse.ReturnCode = returnResult;
                directoryResponse.ReturnMessage = Response.Message(returnResult);
            }
            return directoryResponse;
        }

        public MemberResponseWeb Delete(MemberWebModel model)
        {
            MemberResponseWeb serviceResponse = new MemberResponseWeb();
            MemberWebModel userInfo = new MemberWebModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            int returnResult = 0;

            if (model != null && model.EncryptedMemberId != null)
                model.MemberId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedMemberId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = memberData.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }

            return serviceResponse;
        }
    }
}
