﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Security.Authentication;
using FPModels.Models;
using Org.BouncyCastle.Crypto.Parameters;
using Jose;
using System.Security.Cryptography;
using Org.BouncyCastle.OpenSsl;
using Jose.keys;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;
using System.Text;
using FPDAL.Data;
using System.Web;

namespace FPBAL.Business
{
    public class APNSNotificationAccess
    {
        //  private const string WEB_ADDRESS = "https://api.sandbox.push.apple.com:443/3/device/{0}";
        private const string WEB_ADDRESS = "https://api.push.apple.com:443/3/device/{0}";

        static String TeamID = "A4RG2K5Z2N";
        static String AuthanticationKey = "M2NCKU6474";
        static String BundleID = "com.NJSPBA";
        static String P8_PATH = GetFileLocation();

        static string data1 = File.ReadAllText(P8_PATH);
        public static async Task<bool> ApnsNotification(string deviceToken, string message)
        {

            message = message.Length > 30 ? message.Substring(0, 30) : message;
            string strmsgbody = "";
            int totunreadmsg = 1;
            strmsgbody = message;// +"~~"+ Id + "~~" + type; 
            bool success = true;

            try
            {
                string P8_PATH1 = GetFileLocation();

                string data = System.IO.File.ReadAllText(P8_PATH1);
                List<string> list = data.Split('\n').ToList();



                string prk = list.Where((s, i) => i != 0 && i != list.Count - 1).Aggregate((agg, s) => agg + s);
                ECDsaCng key = new ECDsaCng(CngKey.Import(Convert.FromBase64String(prk), CngKeyBlobFormat.Pkcs8PrivateBlob));

                string token = GetProviderToken();

                string url = string.Format(WEB_ADDRESS, deviceToken);
                HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, url);

                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                httpRequestMessage.Headers.TryAddWithoutValidation("apns-push-type", "alert"); // or background
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-id", Guid.NewGuid().ToString("D"));
                //Expiry
                //
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-expiration", Convert.ToString(0));
                //Send imediately
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-priority", Convert.ToString(10));
                //App Bundle
                // httpRequestMessage.Headers.TryAddWithoutValidation("apns-topic", "com.parkeee-Roadside.llc");
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-topic", BundleID);
                //Category
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-collapse-id", "test");

                var body = "{\"aps\":{\"alert\":\"" + strmsgbody + "\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"},\"acme1\":\"bar\",\"acme2\":42}";

                httpRequestMessage.Version = new Version(2, 0);

                using (var stringContent = new StringContent(body, Encoding.UTF8, "application/json"))
                {
                    //Set Body
                    httpRequestMessage.Content = stringContent;

                    Http2CustomHandler handler = new Http2CustomHandler();

                    handler.SslProtocols = System.Security.Authentication.SslProtocols.Tls12 | System.Security.Authentication.SslProtocols.Tls11 | System.Security.Authentication.SslProtocols.Tls;

                    //handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                    //Continue
                    using (HttpClient client = new HttpClient(handler))
                    {
                        HttpResponseMessage resp = await client.SendAsync(httpRequestMessage).ContinueWith(responseTask =>
                        {
                            return responseTask.Result;
                        });

                        if (resp != null)
                        {
                            string apnsResponseString = await resp.Content.ReadAsStringAsync();

                            handler.Dispose();
                        }

                        handler.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
            }

            return success;

        }
        public static async Task<bool> ApnsNotification1(string deviceToken, string message, string Id, string type, string description)
        {

            message = message.Length > 30 ? message.Substring(0, 30) : message;
            description = description.Length > 70 ? description.Substring(0, 70) + "..." : description;

            string strmsgbody = "";
            int totunreadmsg = 1;
            strmsgbody = message;// +"~~"+ Id + "~~" + type; 
            bool success = true;
            string category = Id + "_" + type;
            try
            {
                string data = "";
                List<string> list = new List<string>();
                try
                {
                    data = System.IO.File.ReadAllText(P8_PATH);

                    list = data.Split('\n').ToList();
                }
                catch (Exception exx)
                {
                    ApplicationLogger.LogError(exx, "NJSPBA", "ReadAllText");

                    data = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"~/P12File/AuthKey_M2NCKU6474.p8"));
                    list = data.Split('\n').ToList();
                }

                string prk = list.Where((s, i) => i != 0 && i != list.Count - 1).Aggregate((agg, s) => agg + s);
                ECDsaCng key = new ECDsaCng(CngKey.Import(Convert.FromBase64String(prk), CngKeyBlobFormat.Pkcs8PrivateBlob));

                string token = GetProviderToken();

                string url = string.Format(WEB_ADDRESS, deviceToken);
                HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, url);

                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                httpRequestMessage.Headers.TryAddWithoutValidation("apns-push-type", "alert"); // or background
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-id", Guid.NewGuid().ToString("D"));
                //Expiry
                //
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-expiration", Convert.ToString(0));
                //Send imediately
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-priority", Convert.ToString(10));
                //App Bundle
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-topic", "com.NJSPBA");

                //Category
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-collapse-id", "test");

                var body = "{\"aps\":{\"alert\":{\"title\":\"" + strmsgbody + "\",\"body\":\"" + description + "\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"}},\"c\":\"" + category + "\"}";

                httpRequestMessage.Version = new Version(2, 0);

                using (var stringContent = new StringContent(body, Encoding.UTF8, "application/json"))
                {
                    //Set Body
                    httpRequestMessage.Content = stringContent;

                    Http2CustomHandler handler = new Http2CustomHandler();

                    handler.SslProtocols = System.Security.Authentication.SslProtocols.Tls12 | System.Security.Authentication.SslProtocols.Tls11 | System.Security.Authentication.SslProtocols.Tls;

                    //handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                    //Continue
                    using (HttpClient client = new HttpClient(handler))
                    {
                        HttpResponseMessage resp = await client.SendAsync(httpRequestMessage).ContinueWith(responseTask =>
                        {
                            return responseTask.Result;
                        });

                        if (resp != null)
                        {
                            string apnsResponseString = await resp.Content.ReadAsStringAsync();

                            handler.Dispose();
                        }

                        handler.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "roadsidenotificationaccess", "ApnsNotification");
                success = false;
            }

            return success;

        }

        public static async Task<bool> ApnsNotification(string deviceToken, string message, string Id, string type)
        {

            message = message.Length > 30 ? message.Substring(0, 30) : message;
            string strmsgbody = "";
            int totunreadmsg = 1;
            strmsgbody = message;// +"~~"+ Id + "~~" + type; 
            bool success = true;
            string category = Id + "_" + type;
            try
            {
                string P8_PATH1 = GetFileLocation();

                string data = System.IO.File.ReadAllText(P8_PATH1);
                List<string> list = data.Split('\n').ToList();



                string prk = list.Where((s, i) => i != 0 && i != list.Count - 1).Aggregate((agg, s) => agg + s);
                ECDsaCng key = new ECDsaCng(CngKey.Import(Convert.FromBase64String(prk), CngKeyBlobFormat.Pkcs8PrivateBlob));

                string token = GetProviderToken();

                string url = string.Format(WEB_ADDRESS, deviceToken);
                HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, url);

                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                httpRequestMessage.Headers.TryAddWithoutValidation("apns-push-type", "alert"); // or background
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-id", Guid.NewGuid().ToString("D"));
                //Expiry
                //
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-expiration", Convert.ToString(0));
                //Send imediately
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-priority", Convert.ToString(10));
                //App Bundle
                // httpRequestMessage.Headers.TryAddWithoutValidation("apns-topic", "com.parkeee-Roadside.llc");
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-topic", BundleID);
                //Category
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-collapse-id", "test");

                var body = "{\"aps\":{\"alert\":\"" + strmsgbody + "\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"},\"c\":\"" + category + "\"}";

                httpRequestMessage.Version = new Version(2, 0);

                using (var stringContent = new StringContent(body, Encoding.UTF8, "application/json"))
                {
                    //Set Body
                    httpRequestMessage.Content = stringContent;

                    Http2CustomHandler handler = new Http2CustomHandler();

                    handler.SslProtocols = System.Security.Authentication.SslProtocols.Tls12 | System.Security.Authentication.SslProtocols.Tls11 | System.Security.Authentication.SslProtocols.Tls;

                    //handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                    //Continue
                    using (HttpClient client = new HttpClient(handler))
                    {
                        HttpResponseMessage resp = await client.SendAsync(httpRequestMessage).ContinueWith(responseTask =>
                        {
                            return responseTask.Result;
                        });

                        if (resp != null)
                        {
                            string apnsResponseString = await resp.Content.ReadAsStringAsync();

                            handler.Dispose();
                        }

                        handler.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
            }

            return success;

        }
        public static async Task<bool> ApnsNotification(string deviceToken, string message, string Id, string type, string description)
        {

            message = message.Length > 30 ? message.Substring(0, 30) : message;
            description = description.Length > 70 ? description.Substring(0, 70) + "..." : description;

            string strmsgbody = "";
            int totunreadmsg = 1;
            strmsgbody = message;// +"~~"+ Id + "~~" + type; 
            bool success = true;
            string category = Id + "_" + type;
            try
            {
                string data = "";
                List<string> list = new List<string>();
                try
                {
                    data = System.IO.File.ReadAllText(P8_PATH);

                    list = data.Split('\n').ToList();
                }
                catch (Exception exx)
                {
                    ApplicationLogger.LogError(exx, "NJSPBA", "ReadAllText");

                    data = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"~/P12File/AuthKey_M2NCKU6474.p8"));
                    list = data.Split('\n').ToList();
                }

                string prk = list.Where((s, i) => i != 0 && i != list.Count - 1).Aggregate((agg, s) => agg + s);
                ECDsaCng key = new ECDsaCng(CngKey.Import(Convert.FromBase64String(prk), CngKeyBlobFormat.Pkcs8PrivateBlob));

                string token = GetProviderToken();

                string url = string.Format(WEB_ADDRESS, deviceToken);
                HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, url);

                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                httpRequestMessage.Headers.TryAddWithoutValidation("apns-push-type", "alert"); // or background
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-id", Guid.NewGuid().ToString("D"));
                //Expiry
                //
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-expiration", Convert.ToString(0));
                //Send imediately
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-priority", Convert.ToString(10));
                //App Bundle
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-topic", "com.NJSPBA");

                //Category
                httpRequestMessage.Headers.TryAddWithoutValidation("apns-collapse-id", "test");

                // var body = "{\"aps\":{\"alert\":{\"title\":\"" + strmsgbody + "\",\"body\":\"" + description + "\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"}},\"c\":\"" + category + "\"}";
                var body = "{\"aps\":{\"alert\":{\"title\":\"" + strmsgbody + "\",\"body\":\"" + description + "\"},\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"default\"},\"c\":\"" + category + "\"}";

                httpRequestMessage.Version = new Version(2, 0);

                using (var stringContent = new StringContent(body, Encoding.UTF8, "application/json"))
                {
                    //Set Body
                    httpRequestMessage.Content = stringContent;

                    Http2CustomHandler handler = new Http2CustomHandler();

                    handler.SslProtocols = System.Security.Authentication.SslProtocols.Tls12 | System.Security.Authentication.SslProtocols.Tls11 | System.Security.Authentication.SslProtocols.Tls;

                    //handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                    //Continue
                    using (HttpClient client = new HttpClient(handler))
                    {
                        HttpResponseMessage resp = await client.SendAsync(httpRequestMessage).ContinueWith(responseTask =>
                        {
                            return responseTask.Result;
                        });

                        if (resp != null)
                        {
                            string apnsResponseString = await resp.Content.ReadAsStringAsync();

                            handler.Dispose();
                        }

                        handler.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "roadsidenotificationaccess", "ApnsNotification");
                success = false;
            }

            return success;

        }
        public static string GetFileLocation()
        {
            string filePath = "";

            string fileName = "AuthKey_M2NCKU6474.p8";

            string root = string.Empty;
            try
            {
                //System.Web.Hosting.HostingEnvironment.MapPath("~/Owner/");
                // string filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "Owner/AppUsers/");

                root = System.Web.Hosting.HostingEnvironment.MapPath("~/P12File/");

                if (root == null || root == string.Empty)
                    root = AppDomain.CurrentDomain.BaseDirectory;

                if (root == null || root == string.Empty)
                    root = System.Web.HttpContext.Current.Server.MapPath("..");


                if (root == null || root == string.Empty)
                    root = System.Web.HttpContext.Current.Server.MapPath("~/");

            }
            catch
            {
                root = AppDomain.CurrentDomain.BaseDirectory;
                root = root + "P12File\\";



            }
            filePath = root + fileName;
            return filePath;
        }


        public class Http2CustomHandler : WinHttpHandler
        {
            protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
            {
                request.Version = new Version("2.0");

                return base.SendAsync(request, cancellationToken);
            }
        }

        private static string GetProviderToken()
        {
            double epochNow = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            Dictionary<string, object> payload = new Dictionary<string, object>()
                {
                    { "iss", "A4RG2K5Z2N" },
                    { "iat", epochNow }
                };
            var extraHeaders = new Dictionary<string, object>()
                {
                    { "kid", "M2NCKU6474" },
                    { "alg", "ES256" }
                };

            CngKey privateKey = GetPrivateKey();

            return JWT.Encode(payload, privateKey, JwsAlgorithm.ES256, extraHeaders);
        }

        private static CngKey GetPrivateKey()
        {
            using (var reader = File.OpenText(P8_PATH))
            {
                ECPrivateKeyParameters ecPrivateKeyParameters = (ECPrivateKeyParameters)new PemReader(reader).ReadObject();

                var x = ecPrivateKeyParameters.Parameters.G.AffineXCoord.GetEncoded();
                var y = ecPrivateKeyParameters.Parameters.G.AffineYCoord.GetEncoded();

                var d = ecPrivateKeyParameters.D.ToByteArrayUnsigned();

                return EccKey.New(x, y, d);
            }
        }

    }
}
