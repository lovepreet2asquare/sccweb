﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using FPModels.Models;
using FPDAL.Data;
using System.Web;
using System.IO;

namespace FPBAL.Business
{
    public class AppHeadlineAccess : IAppHeadlines
    {
        public HeadlinesResponse GetHeadlines(HeadlinesRequest request)
        {
            List<HeadlineModelAPI> Headlines = new List<HeadlineModelAPI>();

            HeadlinesResponse serviceResponse = new HeadlinesResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "No record found.";
            serviceResponse.News = Headlines;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            int returnResult = 0;
            DataSet ds = AppHeadlineData.HeadlinesSelect(request, out returnResult);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["HeadlineId"]);
                if (returnResult > 0)
                {
                    Headlines = HeadlineList(ds, out returnResult);
                    if (returnResult == -1)
                    {
                        serviceResponse.ReturnCode = "-1";
                        serviceResponse.ReturnMessage = "Technical error.";
                    }

                    else if (returnResult == -5)
                    {
                        serviceResponse.ReturnCode = "-5";
                        serviceResponse.ReturnMessage = "Authentication failed.";

                    }
                    else if (returnResult>0)
                    {
                        serviceResponse.ReturnCode = "1";
                        serviceResponse.ReturnMessage = "Retrieved successfully.";
                        serviceResponse.News = Headlines;
                    }

                }
                else if (returnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";

                }
                else if (returnResult == -5)
                {
                    serviceResponse.ReturnCode = "-5";
                    serviceResponse.ReturnMessage = "Authentication failed.";

                }
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }
            return serviceResponse;
        }

        public HeadlinesResponse GetFavHeadlines(HeadlinesRequest request)
        {
            List<HeadlineModelAPI> Headlines = new List<HeadlineModelAPI>();

            HeadlinesResponse serviceResponse = new HeadlinesResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "No record found.";
            serviceResponse.News = Headlines;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            int returnResult = 0;
            DataSet ds = AppHeadlineData.FavHeadlinesSelect(request, out returnResult);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["HeadlineId"]);
                if (returnResult > 0)
                {
                    Headlines = HeadlineList(ds, out returnResult);
                    if (returnResult == -1)
                    {
                        serviceResponse.ReturnCode = "-1";
                        serviceResponse.ReturnMessage = "Technical error.";
                    }
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "Retrieved successfully.";
                    serviceResponse.News = Headlines;

                }
                else if (returnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";

                }
                else if (returnResult == -5)
                {
                    serviceResponse.ReturnCode = "-5";
                    serviceResponse.ReturnMessage = "Authentication failed.";

                }
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }
            return serviceResponse;
        }

        public HeadlineAPIResponse GetHeadlineDetail(HeadlineDetailRequest request)
        {
            HeadlineDetailModel headlineModel = new HeadlineDetailModel();

            HeadlineAPIResponse serviceResponse = new HeadlineAPIResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "No record found.";
            serviceResponse.HeadlineDetail = headlineModel;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            int returnResult = 0;
            DataSet ds = AppHeadlineData.HeadlineDetailSelect(request, out returnResult);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["HeadlineId"]);
                if (returnResult > 0)
                {
                    headlineModel = HeadlineDetail(ds, out returnResult);
                    if (returnResult == -1)
                    {
                        serviceResponse.ReturnCode = "-1";
                        serviceResponse.ReturnMessage = "Technical error.";
                    }
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "Retrieved successfully.";
                    serviceResponse.HeadlineDetail = headlineModel;

                }
                else if (returnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";

                }
                else if (returnResult == -5)
                {
                    serviceResponse.ReturnCode = "-5";
                    serviceResponse.ReturnMessage = "Authentication failed.";

                }
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }
            return serviceResponse;
        }

        public NJResponse HeadLineShareInsert(HeadlineDetailRequest request)
        {
            NJResponse serviceResponse = new NJResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";


            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            returnResult = AppHeadlineData.HeadLineShareInsert(request);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Shared successfully";
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }

        public NJResponse HeadlineFavUnFav(HeadlineFavRequest request)
        {
            NJResponse serviceResponse = new NJResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";

            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

            returnResult = AppHeadlineData.HeadlineFavUnFav(request);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Submitted successfully";
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }




        public static List<HeadlineModelAPI> HeadlineList(DataSet ds, out int returnResult)
        {
            List<HeadlineModelAPI> HeadlineList = new List<HeadlineModelAPI>();
            HeadlineModelAPI headlineModel = null;
            returnResult = 0;

            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        returnResult = Convert.ToInt32(row["HeadlineId"]);

                        if (returnResult > 0)
                        {
                            headlineModel = new HeadlineModelAPI();

                            headlineModel.HeadlineId = returnResult.ToString();
                            headlineModel.EncryptedHeadlineId = UtilityAccess.Encrypt(headlineModel.HeadlineId);
                            headlineModel.Title = Convert.ToString(row["Title"]);
                            headlineModel.ImagePath = Convert.ToString(row["ImagePath"]);
                            headlineModel.PublishDate = Convert.ToString(row["PublishDate"]);
                            headlineModel.PublishTime = Convert.ToString(row["PublishTime"]);
                            headlineModel.IsUpdated = Convert.ToBoolean(row["IsUpdated"]);
                            headlineModel.Content = Convert.ToString(row["Content"]);
                            headlineModel.Location = Convert.ToString(row["Location"]);
                            headlineModel.County = Convert.ToString(row["County"]);
                            headlineModel.ShareLink = Convert.ToString(row["ShareLink"]); //"ShareLink";
                            headlineModel.IsFavorite = Convert.ToBoolean(row["IsFavorite"]);


                            HeadlineList.Add(headlineModel);
                        }

                    }
                }

            }

            return HeadlineList;
        }
        private static HeadlineDetailModel HeadlineDetail(DataSet ds, out int returnResult)
        {
            HeadlineDetailModel headlineModel = new HeadlineDetailModel();
            List<HeadlineImage> imageList = new List<HeadlineImage>();
            HeadlineImage imageModel = null;
            List<HeadlineDocumentModel> NewsDocumentList = new List<HeadlineDocumentModel>();
            HeadlineDocumentModel newsDocumentModel = new HeadlineDocumentModel();
            returnResult = 0;
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        returnResult = Convert.ToInt32(row["HeadlineId"]);
                        if (returnResult > 0)
                        {
                            headlineModel.HeadlineId = returnResult.ToString();
                            headlineModel.EncryptedHeadlineId = UtilityAccess.Encrypt(headlineModel.HeadlineId);
                            headlineModel.Title = Convert.ToString(row["Title"]);
                            headlineModel.PublishDate = Convert.ToString(row["PublishDate"]);
                            headlineModel.PublishTime = Convert.ToString(row["PublishTime"]);
                            headlineModel.IsUpdated = Convert.ToBoolean(row["IsUpdated"]);
                            headlineModel.Content = Convert.ToString(row["Content"]);
                            headlineModel.Location = Convert.ToString(row["Location"]);
                            headlineModel.County = Convert.ToString(row["County"]);
                            headlineModel.ShareLink = Convert.ToString(row["ShareLink"]); //"ShareLink";
                            headlineModel.IsFavorite = Convert.ToBoolean(row["IsFavorite"]);
                        }
                    }
                    if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[1].Rows)
                        {
                            imageModel = new HeadlineImage();
                            imageModel.ImagePath = (row2["Imagepath"] as string) ?? "";
                            imageList.Add(imageModel);
                        }
                    }
                    if (ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            newsDocumentModel = new HeadlineDocumentModel();
                            newsDocumentModel.DocumentId = Convert.ToString(Convert.ToInt32(row2["DocumentId"]));  //UtilityAccess.Encrypt(Convert.ToString(Convert.ToInt32(row2["DocumentId"])));
                            newsDocumentModel.DocumentName = Convert.ToString(row2["DocumentName"]);
                            newsDocumentModel.DocumentPath = Convert.ToString(row2["DocumentPath"]);
                            newsDocumentModel.ThumbnailPath = Convert.ToString(row2["ThumbnailPath"]);
                            NewsDocumentList.Add(newsDocumentModel);
                        }
                    }
                }

                headlineModel.HeadlineDocument = NewsDocumentList;
                headlineModel.Images = imageList;
            }
            return headlineModel;
        }
    }
}
