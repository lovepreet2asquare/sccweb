﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Security.Authentication;
using FPModels.Models;

namespace FPBAL.Business
{
    public class APNSNotificationAccess
    {
        public static FPResponse ApnsNotification(string deviceToken, string message)
        {
            message = message.Length > 30 ? message.Substring(0, 30) : message;
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "Notification not sent.";

            string response = "Sent successfully.";
            try
            {
                int port = 2195;
                String hostname = "gateway.sandbox.push.apple.com";
                hostname = "gateway.push.apple.com";

                string p12FileLocation = GetFileLocation();
                //string certificatePath = Server.MapPath("final.p12");

                string certificatePath = p12FileLocation;
                string certificatePassword = "123456";

                X509Certificate2 clientCertificate = new X509Certificate2(certificatePath, certificatePassword, X509KeyStorageFlags.MachineKeySet);
                X509Certificate2Collection certificatesCollection = new X509Certificate2Collection(clientCertificate);

                TcpClient client = new TcpClient(hostname, port);
                SslStream sslStream = new SslStream(
                                client.GetStream(),
                                false,
                                new RemoteCertificateValidationCallback(ValidateServerCertificate),
                                null
                );

                try
                {
                    sslStream.AuthenticateAsClient(hostname, certificatesCollection, SslProtocols.Default, false);
                }
                catch (AuthenticationException ex)
                {
                    response = ex.Message;
                    serviceResponse.ReturnMessage = ex.Message;
                    //Console.WriteLine("Authentication failed");
                    //client.Close();
                    // Request.SaveAs(Server.MapPath("Authenticationfailed.txt"), true);
                    // return;
                }
                //// Encode a test message into a byte array.
                MemoryStream memoryStream = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(memoryStream);

                writer.Write((byte)0);  //The command
                writer.Write((byte)0);  //The first byte of the deviceId length (big-endian first byte)
                writer.Write((byte)32); //The deviceId length (big-endian second byte)

                byte[] b0 = HexString2Bytes(deviceToken);
                WriteMultiLineByteArray(b0);

                writer.Write(b0);
                String payload;
                string strmsgbody = "";
                int totunreadmsg = 1;
                strmsgbody = message;
                //strmsgbody = strmsgbody 

                //   Debug.WriteLine("during testing via device!");
                // Request.SaveAs(Server.MapPath("APNSduringdevice.txt"), true);

                ///payload = "{\"aps\":{\"alert\":\"" + strmsgbody + "\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"},\"acme1\":\"bar\",\"acme2\":42}";
                ///
                payload = "{\"aps\":{\"alert\":\"" + strmsgbody + "\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"},\"acme1\":\"bar\",\"acme2\":42}";


                writer.Write((byte)0); //First byte of payload length; (big-endian first byte)
                writer.Write((byte)payload.Length);     //payload length (big-endian second byte)

                byte[] b1 = System.Text.Encoding.UTF8.GetBytes(payload);
                writer.Write(b1);
                writer.Flush();

                byte[] array = memoryStream.ToArray();
                //  Debug.WriteLine("This is being sent...\n\n");
                //  Debug.WriteLine(array);
                try
                {
                    sslStream.Write(array);
                    sslStream.Flush();
                }
                catch (Exception ex)
                {
                    response = ex.Message;
                    serviceResponse.ReturnMessage = ex.Message;
                    //Debug.WriteLine("Write failed buddy!!");
                    // Request.SaveAs(Server.MapPath("Writefailed.txt"), true);
                }

                client.Close();
                //Debug.WriteLine("Client closed.");

                string emailBody = message + " and DeviceToken is : " + deviceToken;
                Exception ex11 = null;
                //EmailUtility.SendEmailNew("surander.parkeee@gmail.com", "APNS JOSH iOS", "ios Notification", emailBody, out ex11, true);


                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = response;

                return serviceResponse;
            }
            catch (Exception ex)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = ex.Message;
                //ApplicationLogger.LogError(exc, "EmailUtility", "SendEmail");
                //ApplicationLogger.LogError(ex, "APNSNotificationAccess", "ApnsNotification");
                // ExceptionUtility.LogException(ex, "APNSNotification");
                return serviceResponse;
            }

        }
        public static FPResponse ApnsNotification(string deviceToken, string message, string Id, string type)
        {
            message = message.Length > 30 ? message.Substring(0, 30): message;

            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "Notification not sent.";

            string response = "Sent successfully.";
            try
            {
                int port = 2195;
                String hostname = "gateway.sandbox.push.apple.com";
               hostname = "gateway.push.apple.com";

                string p12FileLocation = GetFileLocation();
                //string certificatePath = Server.MapPath("final.p12");

                string certificatePath = p12FileLocation;
                string certificatePassword = "123456";

                X509Certificate2 clientCertificate = new X509Certificate2(certificatePath, certificatePassword, X509KeyStorageFlags.MachineKeySet);
                X509Certificate2Collection certificatesCollection = new X509Certificate2Collection(clientCertificate);

                TcpClient client = new TcpClient(hostname, port);
                SslStream sslStream = new SslStream(
                                client.GetStream(),
                                false,
                                new RemoteCertificateValidationCallback(ValidateServerCertificate),
                                null
                );

                try
                {
                    sslStream.AuthenticateAsClient(hostname, certificatesCollection, SslProtocols.Default, false);
                }
                catch (AuthenticationException ex)
                {
                    response = ex.Message;
                    serviceResponse.ReturnMessage = ex.Message;
                    //Console.WriteLine("Authentication failed");
                    //client.Close();
                    // Request.SaveAs(Server.MapPath("Authenticationfailed.txt"), true);
                    // return;
                }
                //// Encode a test message into a byte array.
                MemoryStream memoryStream = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(memoryStream);

                writer.Write((byte)0);  //The command
                writer.Write((byte)0);  //The first byte of the deviceId length (big-endian first byte)
                writer.Write((byte)32); //The deviceId length (big-endian second byte)

                byte[] b0 = HexString2Bytes(deviceToken);
                WriteMultiLineByteArray(b0);

                writer.Write(b0);
                String payload;
                string strmsgbody = "";
                int totunreadmsg = 1;
                strmsgbody = message;// +"~~"+ Id + "~~" + type; 
                string category = Id + "_" + type;
                //strmsgbody = strmsgbody 

                //   Debug.WriteLine("during testing via device!");
                // Request.SaveAs(Server.MapPath("APNSduringdevice.txt"), true);

                ///payload = "{\"aps\":{\"alert\":\"" + strmsgbody + "\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"},\"acme1\":\"bar\",\"acme2\":42}";
                ///+ "\",\"category\":" + category
                payload = "{\"aps\":{\"alert\":\"" + strmsgbody  + "\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"},\"c\":\"" + category+ "\"}";
                //payload = "{\"aps\":{\"alert\":{\"title\":\"'" + strmsgbody + "'\",\"body\":\"'" + description + "'\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"}},\"c\":\"" + category + "\"}";


                writer.Write((byte)0); //First byte of payload length; (big-endian first byte)
                writer.Write((byte)payload.Length);     //payload length (big-endian second byte)

                byte[] b1 = System.Text.Encoding.UTF8.GetBytes(payload);
                writer.Write(b1);
                writer.Flush();

                byte[] array = memoryStream.ToArray();
                //  Debug.WriteLine("This is being sent...\n\n");
                //  Debug.WriteLine(array);
                try
                {
                    sslStream.Write(array);
                    sslStream.Flush();
                }
                catch (Exception ex)
                {
                    response = ex.Message;
                    serviceResponse.ReturnMessage = ex.Message;
                    //Debug.WriteLine("Write failed buddy!!");
                    // Request.SaveAs(Server.MapPath("Writefailed.txt"), true);
                }

                client.Close();
                //Debug.WriteLine("Client closed.");

                string emailBody = message + " and DeviceToken is : " + deviceToken + "Response: "+ response;
                Exception ex11 = null;
                //EmailUtility.SendEmailNew("surander.parkeee@gmail.com", "APNS JOSH iOS", "ios Notification", emailBody, out ex11, true);


                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = response;

                return serviceResponse;
            }
            catch (Exception ex)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = ex.Message;
                //ApplicationLogger.LogError(exc, "EmailUtility", "SendEmail");
                //ApplicationLogger.LogError(ex, "APNSNotificationAccess", "ApnsNotification");
                // ExceptionUtility.LogException(ex, "APNSNotification");
                return serviceResponse;
            }

        }
        public static FPResponse ApnsNotification(string deviceToken, string message, string Id, string type, string description)
        {
            message = message.Length > 28 ? message.Substring(0, 28) : message;
            description = description.Length > 60 ? description.Substring(0, 60) + "..." : description;

            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "Notification not sent.";

            string response = "Sent successfully.";
            try
            {
                int port = 2195;
                String hostname = "gateway.sandbox.push.apple.com";
                hostname = "gateway.push.apple.com";

                string p12FileLocation = GetFileLocation();
                //string certificatePath = Server.MapPath("final.p12");

                string certificatePath = p12FileLocation;
                string certificatePassword = "123456";

                X509Certificate2 clientCertificate = new X509Certificate2(certificatePath, certificatePassword, X509KeyStorageFlags.MachineKeySet);
                X509Certificate2Collection certificatesCollection = new X509Certificate2Collection(clientCertificate);

                TcpClient client = new TcpClient(hostname, port);
                SslStream sslStream = new SslStream(
                                client.GetStream(),
                                false,
                                new RemoteCertificateValidationCallback(ValidateServerCertificate),
                                null
                );

                try
                {
                    sslStream.AuthenticateAsClient(hostname, certificatesCollection, SslProtocols.Default, false);
                }
                catch (AuthenticationException ex)
                {
                    response = ex.Message;
                    serviceResponse.ReturnMessage = ex.Message;
                    //Console.WriteLine("Authentication failed");
                    //client.Close();
                    // Request.SaveAs(Server.MapPath("Authenticationfailed.txt"), true);
                    // return;
                }
                //// Encode a test message into a byte array.
                MemoryStream memoryStream = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(memoryStream);

                writer.Write((byte)0);  //The command
                writer.Write((byte)0);  //The first byte of the deviceId length (big-endian first byte)
                writer.Write((byte)32); //The deviceId length (big-endian second byte)

                byte[] b0 = HexString2Bytes(deviceToken);
                WriteMultiLineByteArray(b0);

                writer.Write(b0);
                String payload;
                string strmsgbody = "";
                int totunreadmsg = 1;
                strmsgbody = message;// +"~~"+ Id + "~~" + type; 
                string category = Id + "_" + type;
                //strmsgbody = strmsgbody 

                //   Debug.WriteLine("during testing via device!");
                // Request.SaveAs(Server.MapPath("APNSduringdevice.txt"), true);

                ///payload = "{\"aps\":{\"alert\":\"" + strmsgbody + "\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"},\"acme1\":\"bar\",\"acme2\":42}";
                ///+ "\",\"category\":" + category

                //payload = "{\"aps\":{\"alert\":\"" + strmsgbody + "\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"},\"c\":\"" + category + "\"}";
                // payload = "{\"aps\":{\"alert\":{\"title\":\"'" + strmsgbody + "'\",\"body\":\"'" + description + "'\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"}},\"c\":\"" + category + "\"}";
                payload = "{\"aps\":{\"alert\":{\"title\":\"" + strmsgbody + "\",\"body\":\"" + description + "\"},\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"default\"},\"c\":\"" + category + "\"}";
                writer.Write((byte)0); //First byte of payload length; (big-endian first byte)
                writer.Write((byte)payload.Length);     //payload length (big-endian second byte)

                byte[] b1 = System.Text.Encoding.UTF8.GetBytes(payload);
                writer.Write(b1);
                writer.Flush();

                byte[] array = memoryStream.ToArray();
                //  Debug.WriteLine("This is being sent...\n\n");
                //  Debug.WriteLine(array);
                try
                {
                    sslStream.Write(array);
                    sslStream.Flush();
                }
                catch (Exception ex)
                {
                    response = ex.Message;
                    serviceResponse.ReturnMessage = ex.Message;
                    //Debug.WriteLine("Write failed buddy!!");
                    // Request.SaveAs(Server.MapPath("Writefailed.txt"), true);
                }

                client.Close();
                //Debug.WriteLine("Client closed.");

                string emailBody = payload; //message + " and DeviceToken is : " + deviceToken + "Response: " + response;
                Exception ex11 = null;
                //EmailUtility.SendEmailNew("surander.parkeee@gmail.com", "APNS Cory iOS", "ios Notification", emailBody, out ex11, true);


                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = response;

                return serviceResponse;
            }
            catch (Exception ex)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = ex.Message;
                //ApplicationLogger.LogError(exc, "EmailUtility", "SendEmail");
                //ApplicationLogger.LogError(ex, "APNSNotificationAccess", "ApnsNotification");
                // ExceptionUtility.LogException(ex, "APNSNotification");
                return serviceResponse;
            }

        }

        public static string GetFileLocation()
        {

            string filePath = "";
            //string fileName = System.Configuration.ConfigurationManager.AppSettings["p12file"];
            string fileName = "SCCPush.p12";//"Josh_Dev_Kapler.p12";//"Josh_Pro_Kapler.p12";


            string root = string.Empty;
            try
            {
                //System.Web.Hosting.HostingEnvironment.MapPath("~/Owner/");
                // string filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "Owner/AppUsers/");

                root = System.Web.Hosting.HostingEnvironment.MapPath("~/P12File/");

                if (root == null || root == string.Empty)
                    root = AppDomain.CurrentDomain.BaseDirectory;

                if (root == null || root == string.Empty)
                    root = System.Web.HttpContext.Current.Server.MapPath("..");


                if (root == null || root == string.Empty)
                    root = System.Web.HttpContext.Current.Server.MapPath("~/");

            }
            catch
            {
                root = AppDomain.CurrentDomain.BaseDirectory;
                root = root + "P12File\\";



            }
            filePath = root + fileName;
            return filePath;
        }

        // The following method is invoked by the RemoteCertificateValidationDelegate.
        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            //Console.WriteLine("Certificate error: {0}", sslPolicyErrors);

            // Do not allow this client to communicate with unauthenticated servers.
            return false;
        }
        public static void WriteMultiLineByteArray(byte[] bytes)
        {
            const int rowSize = 20;
            int iter;

            Console.WriteLine("initial byte array");
            Console.WriteLine("------------------");

            for (iter = 0; iter < bytes.Length - rowSize; iter += rowSize)
            {
                Console.Write(
                    BitConverter.ToString(bytes, iter, rowSize));
                Console.WriteLine("-");
            }

            Console.WriteLine(BitConverter.ToString(bytes, iter));
            Console.WriteLine();
        }

        private static byte[] HexString2Bytes(string hexString)
        {
            //check for null
            if (hexString == null) return null;
            //get length
            int len = hexString.Length;
            if (len % 2 == 1) return null;
            int len_half = len / 2;
            //create a byte array
            byte[] bs = new byte[len_half];
            try
            {
                //convert the hexstring to bytes
                for (int i = 0; i != len_half; i++)
                {
                    bs[i] = (byte)Int32.Parse(hexString.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Exception : " + ex.Message);
            }
            //return the byte array
            return bs;
        }

    }
}
