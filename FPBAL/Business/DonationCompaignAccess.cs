﻿using System;
using System.Collections.Generic;
using System.Linq;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System.Data;
using System.Threading;
using System.Web;

namespace FPBAL.Business
{
    public class DonationCompaignAccess : IDonationCompaign
    {
        public DonationCampaignResponse AddOrEdit(DonationCompaign model)
        {
            DonationCompaignData compaignData = new DonationCompaignData();
            Int32 returnResult = 0;
            DonationCampaignResponse response = new DonationCampaignResponse();
            response.CampaignModel = new DonationCompaign();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.StatusAIList = UtilityAccess.StatusListValTextNew(1);
                //model.OfficeId = UtilityAccess.Decrypt(model.OfficeId);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                if (model.EncryptedCampaignId != null)
                    model.CampaignId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedCampaignId));
                else
                    model.CampaignId = 0;
                DataSet ds = null;
                ds = compaignData.AddOrEdit(model, out returnResult);
                
                if (returnResult > 0)
                {
                    response.ReturnCode = returnResult;
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            int result = 0;
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                result = Convert.ToInt32(row["CampaignId"]);
                                if (result > 0)
                                {
                                    model.EncryptedCampaignId= UtilityAccess.Encrypt(Convert.ToString(row["CampaignId"]));

                                    //Thread T1 = new Thread(delegate ()
                                    //{
                                    //    string deviceToken = Convert.ToString(row["DeviceToken"] ?? string.Empty);
                                    //    string deviceType = Convert.ToString(row["DeviceType"] ?? string.Empty);
                                    //    string Message = Convert.ToString(row["NotificationMessage"] ?? string.Empty);

                                    //    if (deviceType.ToUpper() == "IOS")
                                    //        APNSNotificationAccess.ApnsNotification(deviceToken, Message);
                                    //    else
                                    //         if (deviceType.ToUpper() == "ANDROID")
                                    //        FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType);
                                    //});
                                }
                            }
                        }
                    }
                }
                response.CampaignModel = model;
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationCampaignAccess", "AddOrEdit");
                return response;
            }
        }
        public DonationCampaignResponse CompaignSelectAll(DonationCompaign model)
        {
            DonationCampaignResponse response = new DonationCampaignResponse();
            response.CampaignModel = new DonationCompaign();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            List<DonationCompaign> compaignList = new List<DonationCompaign>();
            DonationCompaignData compaignData = new DonationCompaignData();
            DonationCompaign data = new DonationCompaign();
            DonationCompaign compaignModel = new DonationCompaign();
            int returnResult = 0;
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                DataSet ds = compaignData.SelectAll(model, out returnResult);

                model.StatusAIList = UtilityAccess.StatusListValTextNew(1);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        data = new DonationCompaign();
                        data.CampaignId = Convert.ToInt32(row["CampaignId"]);
                        data.CampaignTitle = Convert.ToString(row["CampaignTitle"]);
                        data.CampaignDesc = Convert.ToString(row["CampaignDesc"]);
                        data.CampaignNumber = Convert.ToInt32(row["CampaignNumber"]);
                        data.CampaignUrl = Convert.ToString(row["CampaignUrl"]);
                        data.EncryptedCampaignId = UtilityAccess.Encrypt(Convert.ToInt32(row["CampaignId"]).ToString());
                        data.CreateDate = Convert.ToString(row["CreateDate"]);
                        data.EndDate = Convert.ToString(row["EndDate"]);
                        data.StartDate = Convert.ToString(row["StartDate"]);
                        data.Status = Convert.ToString(row["Status"]);
                        data.IsDefaultCampaign = Convert.ToBoolean(row["IsDefaultCampaign"]);
                        data.TransactionCount = Convert.ToInt32(row["TransactionCount"]);
                        data.TotalRefundAmount = Convert.ToDecimal(row["TotalRefundAmount"]);
                        data.TotalTransactionAmount = Convert.ToDecimal(row["TotalTransactionAmount"]);
                        data.CampaignUrl = System.Configuration.ConfigurationManager.AppSettings["donationurl"] +data.EncryptedCampaignId;
                        //serviceResponse.donationURL= HttpContext.Current.Server.UrlEncode(donationURL);
                        compaignList.Add(data);
                    }
                }
                model.Campaignlist = compaignList;
                response.CampaignModel = model;
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DonationSelectAll");
                return response;
            }
        }
        public DonationCampaignResponse Delete(DonationCompaign model)
        {
            DonationCampaignResponse serviceResponse = new DonationCampaignResponse();
            DonationCompaignData compaignData = new DonationCompaignData();
            NewsModel userInfo = new NewsModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            DonationCompaign headlineInfo = null;
            int returnResult = 0;
            if (model != null && model.EncryptedCampaignId != null)
                model.EncryptedCampaignId = (UtilityAccess.Decrypt(model.EncryptedCampaignId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = compaignData.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.CampaignModel = headlineInfo;
            }
            else if(returnResult == -2)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = "The campaign can't be deleted as there is transaction in this campaign.";
                serviceResponse.CampaignModel = headlineInfo;
            }
            return serviceResponse;
        }
        public DonationCampaignResponse CompaignSelect(DonationCompaign model)
        {
            DonationCampaignResponse response = new DonationCampaignResponse();
            response.CampaignModel = new DonationCompaign();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);

            DonationCompaignData compaignData = new DonationCompaignData();
            DonationCompaign data = new DonationCompaign();
            int returnResult = 0;
            try
            {
                string donationURL = System.Configuration.ConfigurationManager.AppSettings["donationurl"];
                string donationURLNew = System.Configuration.ConfigurationManager.AppSettings["donationurlnew"];
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                DataSet ds = compaignData.Select(model, out returnResult);

                
                if (ds != null && ds.Tables.Count > 0)
                {
      

                    
                    // data.Campaignlist = new List<DonationCompaign>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        data = new DonationCompaign
                        {
                            CampaignId = Convert.ToInt32(row["CampaignId"]),
                            CampaignTitle = Convert.ToString(row["CampaignTitle"]),
                            CampaignDesc = Convert.ToString(row["CampaignDesc"]),
                            CampaignNumber = Convert.ToInt32(row["CampaignNumber"]),
                            CampaignUrl = Convert.ToString(row["CampaignUrl"]),
                            EncryptedCampaignId = UtilityAccess.Encrypt(Convert.ToInt32(row["CampaignId"]).ToString()),
                            CreateDate = Convert.ToString(row["CreateDate"]),
                            EndDate = Convert.ToString(row["EndDate"]),
                            StartDate = Convert.ToString(row["StartDate"]),
                            Status = Convert.ToString(row["Status"]),
                            IsDefaultCampaign = Convert.ToBoolean(row["IsDefaultCampaign"]),
                           
                        };
                    }
                }
                data.StatusAIList = UtilityAccess.StatusListValTextNew(1);
                //if (data.CampaignId == 1)
                //{
                //    data.CampaignUrl = donationURL + data.EncryptedCampaignId; ///HttpContext.Current.Server.UrlEncode(donationURL)+data.EncryptedCampaignId;

                //}
                //else
                //{
                //    data.CampaignUrl = donationURLNew + data.EncryptedCampaignId;
                //}
                data.CampaignUrl = donationURL + data.EncryptedCampaignId;
                data.CampaignWebUrl = donationURLNew + data.EncryptedCampaignId;
                response.CampaignModel = data;
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationAccess", "DonationSelect");
                return response;
            }
        }



    }
}
