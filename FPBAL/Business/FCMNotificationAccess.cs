﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.IO;
using System.Net.Security;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

using FPModels.Models;

namespace FPBAL.Business
{
    public class FCMNotificationAccess
    {
        public static string SendFCMNotifications(string deviceId, string message, string deviceType)
        {
            message = message.Length > 30 ? message.Substring(0, 30) : message;
            //, string description
            //description = description.Length > 100 ? description.Substring(0, 100) : description;
            string response = string.Empty;
            string applicationID = "AAAAqbjz9Es:APA91bEC4DPom6Cczzo5Ir3Z-JuCyoB3LpH-2ACo5YLOy_NuQuTrjF7e3-WxyPw3GZy2auXdXlK58GUzqY3OUwuU3254Mrd_NlI1njrf3xIYFLnCqYMrKhDu_UtO8WXpQXxwFCbqQ2Vt";
            var senderId = "728952468555";

            try
            {
                if (deviceType.ToUpper() == "IOS")
                {
                    //applicationID = "AIzaSyAGilhrBYaWbMewlU6IDwvyz4xLdrcD9rM";
                    //senderId = "816399987209";
                }
                //else //for - Andriod -Bistro- updated credentials
                //{
                //    applicationID = "AIzaSyDHX9SRHZe33e_f6GzSib8CLc_fCAY-C7k";
                //    senderId = "533611150594";

                //}

                // deviceId = "";
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body = message,
                        title = "SIKH CHAMBER OF COMMERCE",
                        icon = "http://scc.keplerconnect.com/Content/images/logo.png",
                        key = "ricky"
                        //,
                        //desc = description
                    },
                    priority = "high"
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();

                            FCMResponse fcmResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<FCMResponse>(sResponseFromServer);
                            if (fcmResponse.success == 1)
                            {
                                response = "Successfully sent." + " : " + sResponseFromServer;
                            }
                            else if (fcmResponse.failure == 1)
                            {
                                List<FCMResult> results = fcmResponse.results;
                                response = results[0].message_id + " : " + sResponseFromServer;
                            }

                            return response;
                            // Response.Write(sResponseFromServer);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        public static string SendFCMNotifications(string deviceId, string message, string deviceType, string hid, string serviceType, string description)
        {
            message = message.Length > 30 ? message.Substring(0, 30) : message;
            description = description.Length > 70 ? description.Substring(0, 70) + "..." : description;
            string response = string.Empty;
            string applicationID = "AAAAqbjz9Es:APA91bEC4DPom6Cczzo5Ir3Z-JuCyoB3LpH-2ACo5YLOy_NuQuTrjF7e3-WxyPw3GZy2auXdXlK58GUzqY3OUwuU3254Mrd_NlI1njrf3xIYFLnCqYMrKhDu_UtO8WXpQXxwFCbqQ2Vt";
            var senderId = "728952468555";
            //Josh propolitician
            // string applicationID = "AIzaSyDuqLRe_fXVwPKQMwHU07nUxo5rzsimDGA";
            // var senderId = "352053018641";

            try
            {
                if (deviceType.ToUpper() == "IOS")
                {
                    //applicationID = "AIzaSyAGilhrBYaWbMewlU6IDwvyz4xLdrcD9rM";
                    //senderId = "816399987209";
                }
                else //for - Andriod -Bistro- updated credentials
                {
                   // applicationID = "AIzaSyAeUZOkCuDOyiGjbVItrn8kX3pnPAQgSKI";
                   // senderId = "467421246895";
                    //applicationID = "AIzaSyDHX9SRHZe33e_f6GzSib8CLc_fCAY-C7k";
                    //senderId = "533611150594";

                }

                // deviceId = "";
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body = description,
                        title = message,
                        icon = "http://scc.keplerconnect.com/Content/images/logo.png",
                        key = "SK",
                        Id = hid,
                        Type = serviceType
                    },
                    priority = "high"
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();

                            FCMResponse fcmResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<FCMResponse>(sResponseFromServer);
                            if (fcmResponse.success == 1)
                            {
                                response = "Successfully sent." + " : " + sResponseFromServer;
                            }
                            else if (fcmResponse.failure == 1)
                            {
                                List<FCMResult> results = fcmResponse.results;
                                response = results[0].message_id + " : " + sResponseFromServer;
                            }

                            return response;
                            // Response.Write(sResponseFromServer);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        public static string SendFCMNotifications(string deviceId, string message, string deviceType, string hid, string serviceType)
        {
            message = message.Length > 30 ? message.Substring(0, 30) : message;
            
            string response = string.Empty;
            string applicationID = "AAAAqbjz9Es:APA91bEC4DPom6Cczzo5Ir3Z-JuCyoB3LpH-2ACo5YLOy_NuQuTrjF7e3-WxyPw3GZy2auXdXlK58GUzqY3OUwuU3254Mrd_NlI1njrf3xIYFLnCqYMrKhDu_UtO8WXpQXxwFCbqQ2Vt";
            var senderId = "728952468555";

            try
            {
                if (deviceType.ToUpper() == "IOS")
                {
                    //applicationID = "AIzaSyAGilhrBYaWbMewlU6IDwvyz4xLdrcD9rM";
                    //senderId = "816399987209";
                }
                //else //for - Andriod -Bistro- updated credentials
                //{
                //    applicationID = "AIzaSyDHX9SRHZe33e_f6GzSib8CLc_fCAY-C7k";
                //    senderId = "533611150594";

                //}

                // deviceId = "";
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body = message,
                        title = "SIKH CHAMBER OF COMMERCE",
                        icon = "http://scc.keplerconnect.com/Content/images/logo.png",
                        key = "SK",
                        Id = hid,
                        Type = serviceType
                    },
                    priority = "high"
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();

                            FCMResponse fcmResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<FCMResponse>(sResponseFromServer);
                            if (fcmResponse.success == 1)
                            {
                                response = "Successfully sent." + " : " + sResponseFromServer;
                            }
                            else if (fcmResponse.failure == 1)
                            {
                                List<FCMResult> results = fcmResponse.results;
                                response = results[0].message_id + " : " + sResponseFromServer;
                            }

                            return response;
                            // Response.Write(sResponseFromServer);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

    }
    public class FCMResult
    {
        public string message_id { get; set; }
    }

    public class FCMResponse
    {
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<FCMResult> results { get; set; }
    }

}
