﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace FPBAL.Business
{
     public  class ContactAccess:IContact
    {
        ContactData contactData = new ContactData();
        public ContactResponse SelectAll(ContactModel model)
        {
            Int32 returnResult = 0;
            ContactResponse response = new ContactResponse();
            response.ContactModel = new ContactModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                DataSet ds = contactData.SelectAll(model, out returnResult);
                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response.ContactModel._Contactlist = ContactList(ds.Tables[0], returnResult);
                        returnResult = 12;
                    }
                }
                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }
                // response.CampaignModel.StatusList = UtilityAccess.StatusList(1);
                response.ReturnCode = 12;
                response.ReturnMessage = Response.Message(returnResult);

                //response.InAppMessageModel._DateFilterList = UtilityAccess.DateFilterList;
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ContactAccess", "SelectAll");
                return response;
            }
        }
        private List<ContactModel> ContactList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<ContactModel> data = new List<ContactModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new ContactModel
                            {
                                Name = Convert.ToString(row["Name"]),
                                Email = Convert.ToString(row["Email"]),
                                ISDCode = Convert.ToString(row["ISDCode"]),
                                Mobile = Convert.ToString(row["Mobile"]),
                                Message = Convert.ToString(row["Message"]),
                                Response = Convert.ToBoolean(row["IsEmailResponse"]),
                                Status = Convert.ToString(row["Status"]),
                                ContactId=UtilityAccess.Encrypt(Convert.ToString(row["ContactId"]))
                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ContactModel", "ContactList");
                ReturnResult = -1;
                return null;
            }
        }
        public ContactResponse Select(ContactModel model)
        {
            ContactResponse serviceResponse = new ContactResponse();
            ContactModel userInfo = new ContactModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            ContactModel Model = null;
            int returnResult = 0;

            //if (!String.IsNullOrEmpty(model.ContactId))
            //    model.ContactId = Convert.ToString(UtilityAccess.Decrypt(model.ContactId));

            model.ContactId = UtilityAccess.Decrypt(model.ContactId);
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            DataSet ds = contactData.SelectById(model, out returnResult);
            
            if (ds != null && ds.Tables.Count > 0)
            {
                Model = new ContactModel();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Model.ContactId = Convert.ToString(row["ContactId"] ?? 0);
                        Model.Name = Convert.ToString(row["Name"]);
                        Model.Email = Convert.ToString(row["Email"]);
                        Model.ISDCode = Convert.ToString(row["ISDCode"]);
                        Model.Mobile = Convert.ToString(row["Mobile"] ?? 0);
                        Model.Response = Convert.ToBoolean(row["IsEmailResponse"]);
                        Model.Message = Convert.ToString(row["Message"]);
                        Model.Status = Convert.ToString(row["Status"]);
                    }
                    Model._StatusList = UtilityAccess.ContactStatusList(2);
                }
               
                serviceResponse.ReturnCode = 12;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.ContactModel = Model;
            }
            return serviceResponse;
        }
        public ContactResponse AddOrEdit(ContactModel model)
        {
            Int32 returnResult = 0;
            ContactResponse response = new ContactResponse();
            response.ContactModel = new ContactModel();
            //default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                if (!string.IsNullOrEmpty(model.ContactId))
                {
                    //model.ContactId = Convert.ToString(UtilityAccess.Decrypt(model.ContactId));
                }

                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                
                // get rawoffset by station address -- end
                DataSet ds = contactData.AddorEdit(model, out returnResult);


                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            int result = 0;
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                result = Convert.ToInt32(row["FollowerId"]);
                                if (result > 0)
                                {
                                    Thread T1 = new Thread(delegate ()
                                    {
                                        string deviceToken = Convert.ToString(row["DeviceToken"] ?? string.Empty);
                                        string deviceType = Convert.ToString(row["DeviceType"] ?? string.Empty);
                                        string Message = Convert.ToString(row["NotificationMessage"] ?? string.Empty);

                                        if (deviceType.ToUpper() == "IOS")
                                            APNSNotificationAccess.ApnsNotification(deviceToken, Message);
                                        else
                                             if (deviceType.ToUpper() == "ANDROID")
                                            FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType);
                                    });
                                }
                                
                            }
                            
                        }
                    }
                }
                if (returnResult == -1)
                {
                    response.ReturnCode = 2;// 
                    response.ReturnMessage = "Retrieved Sucessfully";
                    
                }
                else
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ContactAccess", "AddorEdit");
                return response;
            }
        }



    }
}
