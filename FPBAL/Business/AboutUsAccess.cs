﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class AboutUsAccess:IAboutUs
    {
        AboutUsData aboutUsData = new AboutUsData();
        public AboutUsResponse AddOrEdit(AboutUsModel model)
        {
            Int32 returnResult = 0;
            AboutUsResponse response = new AboutUsResponse();
            
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                
                Int32 UserId = aboutUsData.AddOrEdit(model, out returnResult);
                if (returnResult > 0)
                {
                    response.AboutUsModel = new AboutUsModel();
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AboutUsAccess", "AddOrEdit");
                return response;
            }
        }
        public AboutUsResponse Select(AboutUsModel model)
        {
            Int32 returnResult = 0;
            AboutUsResponse response = new AboutUsResponse();

            response.AboutUsModel = new AboutUsModel();
            response.ReturnCode = 0;
            response.ReturnMessage = "No data found.";

            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                //string DateFrom = string.Empty;
                //string DateTo = string.Empty;
                //UtilityAccess.GetFromToDate(model.DateFilterText, out DateFrom, out DateTo);
                //model.DateFrom = DateFrom;
                //model.DateTo = DateTo;
                DataSet ds = aboutUsData.Select(model, out returnResult);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Int32 Id = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                    if (returnResult > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                response.AboutUsModel.Content = Convert.ToString(row["Content"]);
                                response.AboutUsModel.ImagePath= Convert.ToString(row["ImagePath"]);
                                response.AboutUsModel.AboutId=UtilityAccess.Encrypt(Convert.ToString(row["AboutId"]));
                                response.AboutUsModel.ModifyDate= Convert.ToString(row["ModifiedDate"]);
                                response.AboutUsModel.ModifyTime= Convert.ToString(row["Time"]);
                            }
                        }
                    }
                }

                //response.AboutUsModel.DayList = dayList(ds.Tables[1], returnResult);
                //response.AboutUsModel.CountryList = UtilityAccess.RenderList(ds.Tables[2], 1);

                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                if (returnResult == 0)
                    response.ReturnMessage = "No data found.";

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AboutUsAccess", "SelectAll");
                return response;
            }
        }

        public AboutUsResponse Delete(AboutUsModel model)
        {
            Int32 returnResult = 0;
            AboutUsResponse response = new AboutUsResponse();

            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.AboutId = UtilityAccess.Decrypt(model.AboutId);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                returnResult = aboutUsData.Delete(model);
                if (returnResult > 0)
                {
                    response.AboutUsModel = new AboutUsModel();
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AboutUsAccess", "Delete");
                return response;
            }
        }
        //public AboutUsResponse SelectAll(AboutUsModel model)
        //{
        //    Int32 returnResult = 0;
        //    AboutUsResponse response = new AboutUsResponse();
        //    response.AboutUsModel = new AboutUsModel();
        //    response.ReturnCode = 0;
        //    response.ReturnMessage = Response.Message(returnResult);

        //    try
        //    {
        //        model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
        //        model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

        //        DataSet ds = aboutUsData.SelectAll(model, out returnResult);
        //        if (returnResult > 0)
        //        {
        //            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //            {
        //                //response.AboutUsModel._campaignlist = CampaignList(ds.Tables[0], returnResult);
        //                returnResult = 2;
        //            }
        //        }
        //        else
        //        {
        //            response.ReturnCode = 0;
        //            response.ReturnMessage = "No Data Found";
        //        }
        //        // response.CampaignModel.StatusList = UtilityAccess.StatusList(1);
        //        response.ReturnCode = returnResult;
        //        response.ReturnMessage = Response.Message(returnResult);

        //        //response.InAppMessageModel._DateFilterList = UtilityAccess.DateFilterList;
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        ApplicationLogger.LogError(ex, "AboutUsAccess", "SelectAll");
        //        return response;
        //    }
        //}
        //public Int32 ProfilePicUpdate(Int32 UserId, String SessionToken, String FilePath)
        //{
        //    int returnResult = 0;
        //    if (!String.IsNullOrEmpty(SessionToken))
        //        SessionToken = UtilityAccess.Decrypt(SessionToken);

        //    returnResult = AboutUsData.ProfilePicUpdate(UserId, SessionToken, FilePath);
        //    return returnResult;
        //}

    }
}
