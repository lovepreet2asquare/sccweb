﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;


namespace FPBAL.Business
{
    public class DashboardAccess : IDashboard
    {
        DashboardData dashboardData = new DashboardData();
        public DashboardResponse LoadData(DashboardSearch model)
        {


            DashboardResponse dashboardResponse = new DashboardResponse();
            try
            {
                Int32 returnResult = 0;
                dashboardResponse.ReturnCode = returnResult;
                dashboardResponse.ReturnMessage = Response.Message(returnResult);
                if (!String.IsNullOrEmpty(model.UserId))
                    model.UserId = UtilityAccess.Decrypt(model.UserId);
                //
                dashboardResponse.DashboardModel = new DashboardModel();
                DataSet ds = dashboardData.LoadData(model, out returnResult);

                if (ds != null && ds.Tables.Count > 0)
                {
                    #region Trend
                    dashboardResponse.DashboardModel.TrendModel = new DbTrendModel();
                    dashboardResponse.DashboardModel.TrendModel._TrendGraph = new List<GraphModel>();
                    // trend detail
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            dashboardResponse.DashboardModel.TrendModel.AppDownload = Convert.ToInt32(row["AppDownload"]);
                            dashboardResponse.DashboardModel.TrendModel.Share = Convert.ToInt32(row["Share"]);
                            dashboardResponse.DashboardModel.TrendModel.Donation = Convert.ToInt32(row["Donation"]);
                        }
                    }

                    // trend graph
                    if (ds != null && ds.Tables.Count > 1)
                    {                        
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[1].Rows)
                            {
                                dashboardResponse.DashboardModel.TrendModel._TrendGraph.Add(new GraphModel
                                {
                                    Lebel = Convert.ToString(row["Lebel"]),
                                    Value = Convert.ToInt64(row["Value"]),
                                    Value1 = Convert.ToInt64(row["Share"]),
                                    Value2 = Convert.ToInt64(row["Donation"])
                                });                     
                            }
                            dashboardResponse.DashboardModel.TrendModel.JsonTrendGraph = JsonConvert.SerializeObject(dashboardResponse.DashboardModel.TrendModel._TrendGraph);
                        }
                    }
                    #endregion Trend

                    #region Activity
                    dashboardResponse.DashboardModel.ActivityModel = new DbActivityModel();
                    // trend detail
                    if (ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[2].Rows)
                        {
                            dashboardResponse.DashboardModel.ActivityModel.NewDownload = Convert.ToInt32(row["NewDownload"]);
                            dashboardResponse.DashboardModel.ActivityModel.ActiveUser = Convert.ToInt32(row["ActiveUser"]);
                            dashboardResponse.DashboardModel.ActivityModel.ExclusiveHeadline = Convert.ToInt32(row["ExclusiveHeadline"]);
                            dashboardResponse.DashboardModel.ActivityModel.News = Convert.ToInt32(row["News"]);
                        }
                    }
                    #endregion Activity

                    #region ExclusiveHeadlines
                    dashboardResponse.DashboardModel.HeadlineModel = new DbHeadlineModel();
                    // trend detail
                    if (ds.Tables.Count > 3 && ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[3].Rows)
                        {
                            dashboardResponse.DashboardModel.HeadlineModel.HeadlineId = UtilityAccess.Encrypt(Convert.ToString(row["HeadlineId"]));
                            dashboardResponse.DashboardModel.HeadlineModel.PostTime = Convert.ToString(row["PostTime"]);
                            dashboardResponse.DashboardModel.HeadlineModel.Title = Convert.ToString(row["Title"]);
                            dashboardResponse.DashboardModel.HeadlineModel.Content = Convert.ToString(row["Content"]);
                            dashboardResponse.DashboardModel.HeadlineModel.ImagePath = Convert.ToString(row["ImagePath"]);
                            dashboardResponse.DashboardModel.HeadlineModel.UserTitle = Convert.ToString(row["UserTitle"]);
                            dashboardResponse.DashboardModel.HeadlineModel.UserName = Convert.ToString(row["UserName"]);
                            dashboardResponse.DashboardModel.HeadlineModel.ProfilePic = Convert.ToString(row["ProfilePic"]);
                            dashboardResponse.DashboardModel.HeadlineModel.Share = Convert.ToInt32(row["Share"]);
                        }
                    }
                    #endregion ExclusiveHeadlines

                    #region TrendingArticles 
                    dashboardResponse.DashboardModel._TrendArticle = new List<DbTrendArticleModel>();

                    if (ds.Tables.Count > 4 && ds.Tables[4].Rows.Count > 0)
                    {
                        if (ds.Tables[4].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[4].Rows)
                            {
                                dashboardResponse.DashboardModel._TrendArticle.Add(new DbTrendArticleModel
                                {
                                    NewsId = UtilityAccess.Encrypt(Convert.ToString(row["NewsId"])),
                                    Title = Convert.ToString(row["Title"]),
                                    Content = Convert.ToString(row["Content"]),
                                    PuplishDate = Convert.ToString(row["PuplishDate"]),
                                    Share = Convert.ToInt32(row["Share"]),
                                    Viewer = Convert.ToInt32(row["Viewer"]),
                                    UserName = Convert.ToString(row["UserName"]),
                                    DurationType = Convert.ToString(row["DurationType"]),
                                });

                            }
                        }
                    }
                    #endregion TrendingArticles

                    #region Volunteers 
                    dashboardResponse.DashboardModel.VolunteerModel = new DbVolunteerModel();
                    dashboardResponse.DashboardModel.VolunteerModel._VolunteerGraph = new List<GraphModel>();

                    if (ds.Tables.Count > 5 && ds.Tables[5].Rows.Count > 0)
                    {
                        // graph

                        if (ds.Tables[5].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[5].Rows)
                            {
                                dashboardResponse.DashboardModel.VolunteerModel._VolunteerGraph.Add(new GraphModel
                                {
                                    Lebel = Convert.ToString(row["Lebel"]),
                                    Value = Convert.ToInt64(row["Value"])
                                });
                            }

                            // total volunteers
                            dashboardResponse.DashboardModel.VolunteerModel.Volunteer = UtilityAccess.NumberFormat(Convert.ToInt32(dashboardResponse.DashboardModel.VolunteerModel._VolunteerGraph.Sum(x => x.Value)));
                        }
                    }
                    #endregion Volunteers 

                    #region Donations
                    dashboardResponse.DashboardModel.DonationModel = new DbDonationModel();
                    dashboardResponse.DashboardModel.DonationModel._DonationGraph = new List<GraphModel>();
                    decimal refundAmount = 0;
                    if (ds.Tables.Count > 6 && ds.Tables[6].Rows.Count > 0)
                    {
                        // graph

                        if (ds.Tables[6].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[6].Rows)
                            {
                                refundAmount = Convert.ToDecimal(row["RefundAmount"]);
                                dashboardResponse.DashboardModel.DonationModel._DonationGraph.Add(new GraphModel
                                {
                                    Lebel = Convert.ToString(row["Lebel"]),
                                    Value = Convert.ToDecimal(row["Value"])
                                });
                            }

                            // total donation
                            //dashboardResponse.DashboardModel.DonationModel.Donation = UtilityAccess.CurrencyFormat(Convert.ToDecimal(dashboardResponse.DashboardModel.DonationModel._DonationGraph.Sum(x => x.Value)));
                            dashboardResponse.DashboardModel.DonationModel.Donation = UtilityAccess.CurrencyFormat(Decimal.Subtract(Convert.ToDecimal(dashboardResponse.DashboardModel.DonationModel._DonationGraph.Sum(x => x.Value)), refundAmount));
                        }
                    }
                    #endregion Donations 

                    #region InAppMessagesSummary  
                    dashboardResponse.DashboardModel.AppSummaryModel = new DbAppSummaryModel();

                    if (ds.Tables.Count > 7 && ds.Tables[7].Rows.Count > 0)
                    {
                        if (ds.Tables[7].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[7].Rows)
                            {
                                dashboardResponse.DashboardModel.AppSummaryModel.NewSent = UtilityAccess.NumberFormat(Convert.ToInt32(row["NewSent"]));
                                dashboardResponse.DashboardModel.AppSummaryModel.Opened = UtilityAccess.NumberFormat(Convert.ToInt32(row["Opened"]));
                                dashboardResponse.DashboardModel.AppSummaryModel.Unopened = UtilityAccess.NumberFormat(Convert.ToInt32(row["Unopened"]));
                                dashboardResponse.DashboardModel.AppSummaryModel.Share = UtilityAccess.NumberFormat(Convert.ToInt32(row["Share"]));
                                dashboardResponse.DashboardModel.AppSummaryModel.SuccessRate = Convert.ToDecimal(row["SuccessRate"]);
                            }
                        }
                    }
                    #endregion InAppMessagesSummary  

                    #region Users   
                    dashboardResponse.DashboardModel.FollowerModel = new DbFollowerModel();

                    if (ds.Tables.Count > 8 && ds.Tables[8].Rows.Count > 0)
                    {
                        if (ds.Tables[8].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[8].Rows)
                            {
                                dashboardResponse.DashboardModel.FollowerModel.TotalFollower = UtilityAccess.NumberFormat(Convert.ToInt32(row["TotalFollower"]));
                                dashboardResponse.DashboardModel.FollowerModel.ActiveFollower = UtilityAccess.NumberFormat(Convert.ToInt32(row["ActiveFollower"]));
                                dashboardResponse.DashboardModel.FollowerModel.InactiveFollower = UtilityAccess.NumberFormat(Convert.ToInt32(row["InactiveFollower"]));
                                dashboardResponse.DashboardModel.FollowerModel.IOS = Convert.ToInt32(row["IOS"]);
                                dashboardResponse.DashboardModel.FollowerModel.IOSText = UtilityAccess.NumberFormat(Convert.ToInt32(row["IOS"]));
                                dashboardResponse.DashboardModel.FollowerModel.Android = Convert.ToInt32(row["Android"]);
                                dashboardResponse.DashboardModel.FollowerModel.AndroidText = UtilityAccess.NumberFormat(Convert.ToInt32(row["Android"]));
                            }
                        }
                    }
                    #endregion Users   

                    #region PageAccess
                    dashboardResponse.DashboardModel.PageAccessModel = new DbPageAccessModel();
                    // trend detail
                    if (ds.Tables.Count > 9 && ds.Tables[9].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[9].Rows)
                        {
                            if (Convert.ToInt32(row["MenuId"]) == 12)
                                dashboardResponse.DashboardModel.PageAccessModel.Headline = Convert.ToBoolean(row["Access"]);
                            if (Convert.ToInt32(row["MenuId"]) == 17)
                                dashboardResponse.DashboardModel.PageAccessModel.Donation = Convert.ToBoolean(row["Access"]);
                            if (Convert.ToInt32(row["MenuId"]) == 18)
                                dashboardResponse.DashboardModel.PageAccessModel.involvement = Convert.ToBoolean(row["Access"]);

                        }
                    }
                    #endregion PageAccess

                    dashboardResponse.ReturnCode = returnResult;
                    dashboardResponse.ReturnMessage = Response.Message(returnResult);
                }

                return dashboardResponse;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return dashboardResponse;
            }
        }

        public string PdfPrintArticle(List<DbTrendArticleModel> _List, out int ReturnResult)
        {
            ReturnResult = 1;


            try
            {

                String _directory = HttpContext.Current.Server.MapPath(@"~/Invoices/");

                if (!Directory.Exists(_directory))
                    Directory.CreateDirectory(_directory);

                string reutnPath = "/Articles/josh_gottheimer_top_3_trending_articles" + ".pdf";
                string filePath = _directory + reutnPath;
                String _logo = HttpContext.Current.Server.MapPath("/content/images/logo@2x.png");

                using (FileStream msReport = new FileStream(filePath, FileMode.OpenOrCreate))
                {
                    Document pdfDocument = new Document(PageSize.A4, 50f, 20f, 70f, 80f);
                  

                    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, msReport);
                    PdfPTable pdfPTable = null;
                    Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
                    Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
                    Phrase phrase = null;


                    //Phrase p1Header = new Phrase("Top 3 Trending Articles", normalFont);
                    //pdfPTable.AddCell(p1Header);

                    // pdfWriter.PageEvent = new ITextEvents();
                    pdfDocument.Open();

             

                    pdfPTable = new PdfPTable(3);
                    pdfPTable.DefaultCell.Phrase = new Phrase() { Font = normalFont };
                    pdfPTable.TotalWidth = 500f;
                    pdfPTable.LockedWidth = true;
                    pdfPTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    pdfPTable.SetWidths(new float[] { 0.3f, 0.3f, 0.3f });
                    // header
                    Image logo = Image.GetInstance(_logo);
                    logo.ScaleAbsoluteWidth(80);
                    logo.ScaleAbsoluteHeight(30);
                    pdfPTable.AddCell(logo);
                    pdfPTable.AddCell("");
                    pdfPTable.AddCell("");

                    // row 1
                    var paragraph = new Paragraph();
                    paragraph.Add("Top 3 Trending Articles");
                    pdfPTable.AddCell(paragraph);

                    //paragraph = new Paragraph();
                    //pdfPTable.AddCell(paragraph);
                    // add table into document
                    pdfDocument.Add(pdfPTable);
                    try
                    {
                        // item list
                        pdfPTable = new PdfPTable(5);
                        pdfPTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        pdfPTable.DefaultCell.Phrase = new Phrase() { Font = normalFont };
                        pdfPTable.TotalWidth = 500f;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.SpacingBefore = 10f;
                        pdfPTable.SetWidths(new float[] { 0.3f, 0.3f, 0.3f, 0.3f, 0.3f });
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    //pdfPCell=new PdfPCell()                           
                    // phrase = new Phrase("Items", boldFont);
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Title", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(251, 82, 118) });
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Date", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(251, 82, 118) });
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Shares", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(251, 82, 118) });
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Views ", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(251, 82, 118) });
                    pdfPTable.AddCell(new PdfPCell(new Phrase("Author", boldFont)) { BackgroundColor = new iTextSharp.text.BaseColor(251, 82, 118 )});


                    if (_List != null && _List.Count > 0)
                    {
                        foreach (var item in _List)
                        {
                            phrase = new Phrase(item.Title, boldFont);
                            pdfPTable.AddCell(phrase);

                            //pdfPTable.AddCell(phrase);
                            phrase = new Phrase(item.PuplishDate, boldFont);
                            pdfPTable.AddCell(phrase);

                            //pdfPTable.AddCell(phrase);
                            phrase = new Phrase(item.Share.ToString(), boldFont);
                            pdfPTable.AddCell(phrase);
                            // phrase = new Phrase(model._BillingInvoices.Sum(x => x.PlanAmount).ToString("0.##"), boldFont);
                            //pdfPTable.AddCell(phrase);
                            phrase = new Phrase(item.Viewer.ToString(), boldFont);
                            pdfPTable.AddCell(phrase);

                            //pdfPTable.AddCell(phrase);
                            phrase = new Phrase(item.UserName, boldFont);
                            pdfPTable.AddCell(phrase);
                        }
                    }

                    // add table into document
                    pdfDocument.Add(pdfPTable);
                    //using footer class
                    //pdfPTable = new PdfPTable(2);
                    //pdfPTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    //pdfPTable.DefaultCell.Phrase = new Phrase() { Font = normalFont };
                    //pdfPTable.TotalWidth = 500f;
                    //pdfPTable.LockedWidth = true;
                    //pdfPTable.SpacingBefore = 10f;
                    //pdfPTable.SetWidths(new float[] { 0.3f, 0.3f });

                    //logo = Image.GetInstance(HttpContext.Current.Server.MapPath("~/content/images/logo_final1.png"));
                    //logo.ScaleAbsoluteWidth(80);
                    //logo.ScaleAbsoluteHeight(30);

                    //pdfPTable.AddCell("Designed & Developed by");                    
                    //pdfPTable.AddCell(logo);
                    //pdfDocument.Add(pdfPTable);
                    //if (pdfPTable.TotalWidth == 0)
                    //    pdfPTable.SetTotalWidth((pdfDocument.Right - pdfDocument.Left) * pdfPTable.WidthPercentage / 100f);
                    //pdfPTable.WriteSelectedRows(FIRST_ROW, LAST_ROW, pdfDocument.Left, pdfDocument.Bottom + pdfPTable.TotalHeight, pdfWriter.DirectContent);

                    pdfDocument.Close();
                }

                return reutnPath;
            }

            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DashboardAccess", "TopArticles");
                return "";
            }
        }

    }

    public partial class Footer : PdfPageEventHelper

    {

        public override void OnEndPage(PdfWriter writer, Document doc)

        {

            Paragraph footer = new Paragraph("THANK YOU", FontFactory.GetFont(FontFactory.TIMES, 10, iTextSharp.text.Font.NORMAL));

            footer.Alignment = Element.ALIGN_RIGHT;

            PdfPTable footerTbl = new PdfPTable(1);

            footerTbl.TotalWidth = 300;

            footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell cell = new PdfPCell(footer);

            cell.Border = 0;

            cell.PaddingLeft = 10;

            footerTbl.AddCell(cell);

            footerTbl.WriteSelectedRows(0, -1, 415, 30, writer.DirectContent);

        }

    }
}
