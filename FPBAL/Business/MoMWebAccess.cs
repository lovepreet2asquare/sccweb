﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;

namespace FPBAL.Business
{
    public class MoMWebAccess: IMoMWeb
    {
        public MoMResponse AddorEdit(MoMWebModel model)
        {
            MoMWebData momData = new MoMWebData();
            int returnResult = 0;
            MoMResponse response = new MoMResponse();
            response.MinutesOfMeeting = new MoMWebModel();

            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                if (!string.IsNullOrEmpty(model.EncryptedMoMId))
                {
                    model.MoMId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedMoMId));
                }

                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                string deviceToken = string.Empty, deviceType = string.Empty, Message = string.Empty;
                // Message = model.Title;
                //string timeZoneid = "India Standard Time";
                //if (HttpContext.Current.Session["timezoneid"] != null)
                //    timeZoneid = HttpContext.Current.Session["timezoneid"].ToString();
                //double timezoneSeconds = UtilityAccess.GetTimeZoneInSeconds(!string.IsNullOrEmpty(model.PublishDate) ? Convert.ToDateTime(model.PublishDate) : DateTime.UtcNow, timeZoneid);
                //model.TimeZone = Convert.ToInt32(timezoneSeconds);

                DataSet ds = momData.AddorEdit(model, out returnResult);

                string mid = "";
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    int result = 0; //Convert.ToString(ds.Tables[0].Rows[0]["FollowerId"]);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        result = Convert.ToInt32(row["UserId"]);
                        if (result > 0)
                        {
                            mid = "";
                            Thread T1 = new Thread(delegate ()
                            {
                                deviceToken = Convert.ToString(row["DeviceToken"]);
                                deviceType = Convert.ToString(row["DeviceType"]);
                                Message = Convert.ToString(row["NotificationMessage"]);
                                mid = Convert.ToInt32(row["MoMId"]).ToString();
                                if (deviceType.ToUpper() == "IOS")
                                    APNSNotificationAccess.ApnsNotification(deviceToken, Message, mid, "m");
                                else
                                   if (deviceType.ToUpper() == "ANDROID")
                                    FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType, mid, "mom");
                            });
                            T1.IsBackground = true;
                            T1.Start();
                        }
                    }
                }
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MoMAccess", "AddorEdit");
                return response;
            }

        }
        public MoMResponse SelectAll(MoMWebModel model)
        {
            List<MoMWebModel> MinutesOfMeetings = new List<MoMWebModel>();
            MoMWebData momData = new MoMWebData();

            MoMResponse serviceResponse = new MoMResponse();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            MoMWebModel minutesOfMeeting = new MoMWebModel();

            MoMWebModel momModel = new MoMWebModel();

            int returnResult = 0;
            if (model != null && !String.IsNullOrEmpty(model.EncryptedUserId))
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            DataSet ds = momData.SelectAll(model, out returnResult);
            if(returnResult>0)
            { if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            minutesOfMeeting = new MoMWebModel();
                            minutesOfMeeting.MoMId = Convert.ToInt32(row["MoMId"] ?? 0);
                            minutesOfMeeting.EncryptedMoMId= UtilityAccess.Encrypt(minutesOfMeeting.MoMId.ToString());
                            minutesOfMeeting.DocumentName = Convert.ToString(row["DocumentName"] ?? string.Empty);
                            minutesOfMeeting.DocumentPath = Convert.ToString(row["DocumentPath"] ?? string.Empty);
                            minutesOfMeeting.Month = Convert.ToString(row["MonthsName"] ?? string.Empty);
                            minutesOfMeeting.ThumbnailPath = Convert.ToString(row["ThumbnailPath"] ?? string.Empty);
                            minutesOfMeeting.Year = Convert.ToInt32(row["YearName"] ?? 0);
                            MinutesOfMeetings.Add(minutesOfMeeting);
                        }
                    }
                }
                
                momModel.MonthList = UtilityAccess.MonthList(0);
                momModel.YearList = UtilityAccess.YearList(0);

                momModel.MinutesOfMeetings = MinutesOfMeetings;
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.MinutesOfMeeting = momModel;
            }
            return serviceResponse;
        }
        public MoMValidateResponse ValidateMoM(string month , string year)
        {
            MoMValidateResponse response = new MoMValidateResponse();
            MoMValidateResponse response2 = new MoMValidateResponse();
            MoMWebData momData = new MoMWebData();
            int returnResult = momData.ValidateMoM(month, year);

            response.ReturnCode = returnResult;
            response2.ReturnCode = returnResult; ;
            response.Response = response2;
            return response;
        }
        public MoMResponse Select(MoMWebModel model)
        {
            MoMWebData momData = new MoMWebData();

            MoMResponse serviceResponse = new MoMResponse();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            MoMWebModel minutesOfMeeting = new MoMWebModel();
            minutesOfMeeting.MonthList = UtilityAccess.MonthList(2);
            minutesOfMeeting.YearList = UtilityAccess.YearList(2);

            int returnResult = 0;
            if (model != null && !String.IsNullOrEmpty(model.EncryptedMoMId))
                model.MoMId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedMoMId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            DataSet ds = momData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        minutesOfMeeting.MoMId = Convert.ToInt32(row["MoMId"] ?? 0);
                        minutesOfMeeting.EncryptedMoMId = UtilityAccess.Encrypt(minutesOfMeeting.MoMId.ToString());
                        minutesOfMeeting.DocumentName = Convert.ToString(row["DocumentName"] ?? string.Empty);
                        minutesOfMeeting.DocumentPath = Convert.ToString(row["DocumentPath"] ?? string.Empty);
                        minutesOfMeeting.Month = Convert.ToString(row["MonthsName"] ?? string.Empty);
                        minutesOfMeeting.ThumbnailPath = Convert.ToString(row["ThumbnailPath"] ?? string.Empty);
                        minutesOfMeeting.Year = Convert.ToInt32(row["YearName"] ?? 0);

                    }
                }


                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.MinutesOfMeeting = minutesOfMeeting;
            }
            return serviceResponse;
        }
        public MoMResponse Delete(MoMWebModel model)
        {
            MoMWebData momData = new MoMWebData();
            int returnResult = 0;
            MoMResponse response = new MoMResponse();
            response.MinutesOfMeeting = new MoMWebModel();

            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                if (!string.IsNullOrEmpty(model.EncryptedMoMId))
                {
                    model.MoMId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedMoMId));
                }

                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                returnResult = momData.Delete(model);


                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MoMAccess", "Delete");
                return response;
            }

        }
        public MoMResponse LogSelectAll(MoMWebModel model)
        {
            MoMWebData momData = new MoMWebData();
            MoMResponse response = new MoMResponse();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(0);
            response.MinutesOfMeeting = new MoMWebModel();
            int returnResult = 0;
            try
            {
                model.EncryptedSessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

                DataSet ds = momData.LogSelectAll(model, out returnResult);

                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response.MinutesOfMeeting.MinutesOfMeetings = LogList(ds.Tables[0], out returnResult);
                        //returnResult = 2;

                    }
                }

                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }
                
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MOMAccess", "LogSelectAll");
            }
            return response;
        }

        public FPResponse ApnsNotification(string deviceToken, string message)
        {
            FPResponse response = new FPResponse();
            APNSNotificationAccess.ApnsNotification(deviceToken, message);
            return response;
        }
        private List<MoMWebModel> LogList(DataTable dt, out int ReturnResult)
        {
            ReturnResult = -2;
            try
            {
                List<MoMWebModel> data = new List<MoMWebModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            ReturnResult = 2;
                            data.Add(new MoMWebModel
                            {

                                EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"])),
                                MoMId = Convert.ToInt32(row["MoMId"]),
                                EncryptedMoMId = UtilityAccess.Encrypt(Convert.ToString(row["MoMId"])),
                                UniqueId = Convert.ToInt32(row["UniqueId"]),
                                FirstName = Convert.ToString(row["FirstName"]),
                                LastName = Convert.ToString(row["LastName"]),
                                MI = Convert.ToString(row["MI"]),
                                CreateDate = Convert.ToString(row["CreateDate"]),
                                ISDCode = Convert.ToString(row["ISDCode"]),
                                Mobile = Convert.ToString(row["Mobile"]),
                                UserId = Convert.ToInt32(row["UserId"]),
                                DocumentName = Convert.ToString(row["DocumentName"]),
                                ThumbnailPath = Convert.ToString(row["ThumbnailPath"]),

                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RestrictedDocumentModel", "DocumentList");
                ReturnResult = -1;
                return null;
            }
        }
    }
}
