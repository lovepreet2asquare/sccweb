﻿using System;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;
using FPModels.Models;

namespace FPBAL.Business
{
    public class CreateCustomerPaymentProfile
    {
        //public static ANetApiResponse Run(String ApiLoginID, String ApiTransactionKey, string customerProfileId, ref CustomerProfileInfo payorUserCreditCardObj)
        //{
        //    payorUserCreditCardObj.CustomerProfileId = customerProfileId;
        //    // Console.WriteLine("CreateCustomerPaymentProfile Sample");
        //    ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
        //    ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
        //    {
        //        name = ApiLoginID,
        //        ItemElementName = ItemChoiceType.transactionKey,
        //        Item = ApiTransactionKey,
        //    };
        //    string expiryMonth = payorUserCreditCardObj.Expiry_Month.ToString().Length == 1 ? "0" + payorUserCreditCardObj.Expiry_Month.ToString() : payorUserCreditCardObj.Expiry_Month.ToString();

        //    string expiryYear = payorUserCreditCardObj.Expiry_Year.ToString().Length == 4 ? payorUserCreditCardObj.Expiry_Year.ToString().Substring(payorUserCreditCardObj.Expiry_Year.ToString().Length - 2) : payorUserCreditCardObj.Expiry_Year.ToString().Substring(payorUserCreditCardObj.Expiry_Year.ToString().Length - 2);

        //    var creditCard = new creditCardType
        //    {
        //        cardNumber = payorUserCreditCardObj.Card_No.ToString(),
        //        expirationDate = expiryMonth + expiryYear,
        //        cardCode = payorUserCreditCardObj.CardSecurity.ToString()
        //    };
        //    string address = payorUserCreditCardObj.Address1 + " " + payorUserCreditCardObj.Address2 ?? string.Empty;
        //    address = address.Trim();

        //    var billTo = new customerAddressType
        //    {
        //        firstName = payorUserCreditCardObj.FirstName,
        //        lastName = payorUserCreditCardObj.LastName,
        //        address = address,
        //        city = payorUserCreditCardObj.City,
        //        state = payorUserCreditCardObj.State,
        //        zip = payorUserCreditCardObj.Zipcode,
        //        country = payorUserCreditCardObj.Country,
        //    };

        //    paymentType cc = new paymentType { Item = creditCard };

        //    customerPaymentProfileType ccPaymentProfile = new customerPaymentProfileType();
        //    ccPaymentProfile.payment = cc;
        //    ccPaymentProfile.billTo = billTo;

        //    var request = new createCustomerPaymentProfileRequest
        //    {
        //        customerProfileId = customerProfileId,
        //        paymentProfile = ccPaymentProfile,
        //        validationMode = validationModeEnum.none
        //    };

        //    //Prepare Request
        //    var controller = new createCustomerPaymentProfileController(request);
        //    controller.Execute();

        //    //Send Request to EndPoint
        //    createCustomerPaymentProfileResponse response = controller.GetApiResponse();
        //    if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
        //    {
        //        if (response != null && response.messages.message != null)
        //        {
        //            var paymentProfileRequest = new getCustomerPaymentProfileRequest();
        //            paymentProfileRequest.customerProfileId = response.customerProfileId;
        //            paymentProfileRequest.customerPaymentProfileId = response.customerPaymentProfileId;
        //            // instantiate the controller that will call the service
        //            var controller2 = new getCustomerPaymentProfileController(paymentProfileRequest);
        //            controller2.Execute();

        //            // get the response from the service (errors contained if any)
        //            var response2 = controller2.GetApiResponse();
        //            if (response2 != null && response2.messages.resultCode == messageTypeEnum.Ok)
        //            {
        //                //Console.WriteLine(response.messages.message[0].text);
        //                //Console.WriteLine("Customer Payment Profile Id: " + response.paymentProfile.customerPaymentProfileId);
        //                if (response2.paymentProfile.payment.Item is creditCardMaskedType)
        //                {
        //                    payorUserCreditCardObj.Card_No = (response2.paymentProfile.payment.Item as creditCardMaskedType).cardNumber;
        //                    payorUserCreditCardObj.CustomerPaymentProfileId = response.customerPaymentProfileId;
        //                    payorUserCreditCardObj.ResultCode = response2.messages.resultCode.ToString();
        //                }
        //            }
        //            else if (response != null)
        //            {
        //                //Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
        //                //                  response.messages.message[0].text);
        //            }

        //            //Console.WriteLine("Success, createCustomerPaymentProfileID : " + response.customerPaymentProfileId);
        //        }
        //    }
        //    else
        //    {
        //        //Console.WriteLine("Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text);
        //        if (response.messages.message[0].code == "E00039")
        //        {

        //            Console.WriteLine("Duplicate ID: " + response.customerPaymentProfileId);
        //        }
        //    }

        //    return response;

        //}
        public static ANetApiResponse Run(String ApiLoginID, String ApiTransactionKey, string customerProfileId, ref CustProfileInfo parkeeeUserCreditCardObj)
        {
            parkeeeUserCreditCardObj.CustomerProfileId = customerProfileId;
            // Console.WriteLine("CreateCustomerPaymentProfile Sample");
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };
            string expiryMonth = parkeeeUserCreditCardObj.ExpiryMonth.ToString().Length == 1 ? "0" + parkeeeUserCreditCardObj.ExpiryMonth.ToString() : parkeeeUserCreditCardObj.ExpiryMonth.ToString();

            string expiryYear = parkeeeUserCreditCardObj.ExpiryYear.ToString().Length == 4 ? parkeeeUserCreditCardObj.ExpiryYear.ToString().Substring(parkeeeUserCreditCardObj.ExpiryYear.ToString().Length - 2) : parkeeeUserCreditCardObj.ExpiryYear.ToString().Substring(parkeeeUserCreditCardObj.ExpiryYear.ToString().Length - 2);

            var creditCard = new creditCardType
            {
                cardNumber = parkeeeUserCreditCardObj.CardNumber.ToString(),
                expirationDate = expiryMonth + expiryYear,
                cardCode = parkeeeUserCreditCardObj.CardSecurity.ToString()
            };
            string address = parkeeeUserCreditCardObj.Address1 + " " + parkeeeUserCreditCardObj.Address2 ?? string.Empty;
            address = address.Trim();

            var billTo = new customerAddressType
            {
                firstName = parkeeeUserCreditCardObj.FirstName,
                lastName = parkeeeUserCreditCardObj.LastName,
                address = address,
                city = parkeeeUserCreditCardObj.City,
                state = parkeeeUserCreditCardObj.State,
                zip = parkeeeUserCreditCardObj.Zipcode,
                country = parkeeeUserCreditCardObj.Country,
            };

            paymentType cc = new paymentType { Item = creditCard };

            customerPaymentProfileType ccPaymentProfile = new customerPaymentProfileType();
            ccPaymentProfile.payment = cc;
            ccPaymentProfile.billTo = billTo;

            var request = new createCustomerPaymentProfileRequest
            {
                customerProfileId = customerProfileId,
                paymentProfile = ccPaymentProfile,
                validationMode = validationModeEnum.none
            };

            //Prepare Request
            var controller = new createCustomerPaymentProfileController(request);
            controller.Execute();

            //Send Request to EndPoint
            createCustomerPaymentProfileResponse response = controller.GetApiResponse();
            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response != null && response.messages.message != null)
                {
                    var paymentProfileRequest = new getCustomerPaymentProfileRequest();
                    paymentProfileRequest.customerProfileId = response.customerProfileId;
                    paymentProfileRequest.customerPaymentProfileId = response.customerPaymentProfileId;
                    // instantiate the controller that will call the service
                    var controller2 = new getCustomerPaymentProfileController(paymentProfileRequest);
                    controller2.Execute();

                    // get the response from the service (errors contained if any)
                    var response2 = controller2.GetApiResponse();
                    if (response2 != null && response2.messages.resultCode == messageTypeEnum.Ok)
                    {
                        //Console.WriteLine(response.messages.message[0].text);
                        //Console.WriteLine("Customer Payment Profile Id: " + response.paymentProfile.customerPaymentProfileId);
                        if (response2.paymentProfile.payment.Item is creditCardMaskedType)
                        {
                            parkeeeUserCreditCardObj.CardNumber = (response2.paymentProfile.payment.Item as creditCardMaskedType).cardNumber;
                            parkeeeUserCreditCardObj.CustomerPaymentProfileId = response.customerPaymentProfileId;
                            parkeeeUserCreditCardObj.ResultCode = response2.messages.resultCode.ToString();
                        }
                    }
                    else if (response != null)
                    {
                        //Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
                        //                  response.messages.message[0].text);
                    }

                    //Console.WriteLine("Success, createCustomerPaymentProfileID : " + response.customerPaymentProfileId);
                }
            }
            else
            {
                //Console.WriteLine("Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text);
                if (response.messages.message[0].code == "E00039")
                {

                    Console.WriteLine("Duplicate ID: " + response.customerPaymentProfileId);
                }
            }

            return response;

        }

        public static ANetApiResponse RunNew(String ApiLoginID, String ApiTransactionKey, string customerProfileId, ref CustProfileInfoNew parkeeeUserCreditCardObj)
        {
            parkeeeUserCreditCardObj.CustomerProfileId = customerProfileId;
            // Console.WriteLine("CreateCustomerPaymentProfile Sample");
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };
            string expiryMonth = parkeeeUserCreditCardObj.ExpiryMonth.ToString().Length == 1 ? "0" + parkeeeUserCreditCardObj.ExpiryMonth.ToString() : parkeeeUserCreditCardObj.ExpiryMonth.ToString();

            string expiryYear = parkeeeUserCreditCardObj.ExpiryYear.ToString().Length == 4 ? parkeeeUserCreditCardObj.ExpiryYear.ToString().Substring(parkeeeUserCreditCardObj.ExpiryYear.ToString().Length - 2) : parkeeeUserCreditCardObj.ExpiryYear.ToString().Substring(parkeeeUserCreditCardObj.ExpiryYear.ToString().Length - 2);

            var creditCard = new creditCardType
            {
                cardNumber = parkeeeUserCreditCardObj.CardNumber.ToString(),
                expirationDate = expiryMonth + expiryYear,
                cardCode = parkeeeUserCreditCardObj.CardSecurity.ToString()
            };
            string address = parkeeeUserCreditCardObj.Address1 + " " + parkeeeUserCreditCardObj.Address2 ?? string.Empty;
            address = address.Trim();

            var billTo = new customerAddressType
            {
                firstName = parkeeeUserCreditCardObj.FirstName,
                lastName = parkeeeUserCreditCardObj.LastName,
                address = address,
                city = parkeeeUserCreditCardObj.City,
                state = parkeeeUserCreditCardObj.State,
                zip = parkeeeUserCreditCardObj.Zipcode,
                country = parkeeeUserCreditCardObj.Country,
            };

            paymentType cc = new paymentType { Item = creditCard };

            customerPaymentProfileType ccPaymentProfile = new customerPaymentProfileType();
            ccPaymentProfile.payment = cc;
            ccPaymentProfile.billTo = billTo;

            var request = new createCustomerPaymentProfileRequest
            {
                customerProfileId = customerProfileId,
                paymentProfile = ccPaymentProfile,
                validationMode = validationModeEnum.none
            };

            //Prepare Request
            var controller = new createCustomerPaymentProfileController(request);
            controller.Execute();

            //Send Request to EndPoint
            createCustomerPaymentProfileResponse response = controller.GetApiResponse();
            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response != null && response.messages.message != null)
                {
                    var paymentProfileRequest = new getCustomerPaymentProfileRequest();
                    paymentProfileRequest.customerProfileId = response.customerProfileId;
                    paymentProfileRequest.customerPaymentProfileId = response.customerPaymentProfileId;
                    // instantiate the controller that will call the service
                    var controller2 = new getCustomerPaymentProfileController(paymentProfileRequest);
                    controller2.Execute();

                    // get the response from the service (errors contained if any)
                    var response2 = controller2.GetApiResponse();
                    if (response2 != null && response2.messages.resultCode == messageTypeEnum.Ok)
                    {
                        //Console.WriteLine(response.messages.message[0].text);
                        //Console.WriteLine("Customer Payment Profile Id: " + response.paymentProfile.customerPaymentProfileId);
                        if (response2.paymentProfile.payment.Item is creditCardMaskedType)
                        {
                            parkeeeUserCreditCardObj.CardNumber = (response2.paymentProfile.payment.Item as creditCardMaskedType).cardNumber;
                            parkeeeUserCreditCardObj.CustomerPaymentProfileId = response.customerPaymentProfileId;
                            parkeeeUserCreditCardObj.ResultCode = response2.messages.resultCode.ToString();
                        }
                    }
                    else if (response != null)
                    {
                        //Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
                        //                  response.messages.message[0].text);
                    }

                    //Console.WriteLine("Success, createCustomerPaymentProfileID : " + response.customerPaymentProfileId);
                }
            }
            else
            {
                //Console.WriteLine("Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text);
                if (response.messages.message[0].code == "E00039")
                {

                    Console.WriteLine("Duplicate ID: " + response.customerPaymentProfileId);
                }
            }

            return response;

        }

    }
}
