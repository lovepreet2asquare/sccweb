﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class PresidentMessageWebAccess : IPresidentMessageWeb
    {
        PresidentMessageWebData presidentMessageData = new PresidentMessageWebData();

        public PresidentMessageResponse AddOrEdit(PresidentMessageWebModel model)
        {
            Int32 returnResult = 0;
            PresidentMessageResponse response = new PresidentMessageResponse();

            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                Int32 UserId = presidentMessageData.AddOrEdit(model, out returnResult);
                if (returnResult > 0)
                {
                    response.PresidentMessageModel = new PresidentMessageWebModel();
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PresidentMessageAccess", "AddOrEdit");
                return response;
            }
        }

        public PresidentMessageResponse Select(PresidentMessageWebModel model)
        {
            Int32 returnResult = 0;
            PresidentMessageResponse response = new PresidentMessageResponse();

            response.PresidentMessageModel = new PresidentMessageWebModel();
            response.ReturnCode = 0;
            response.ReturnMessage = "No data found.";

            try
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                DataSet ds = presidentMessageData.Select(model, out returnResult);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Int32 Id = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                    if (returnResult > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                response.PresidentMessageModel.Description = Convert.ToString(row["Description"]);
                                response.PresidentMessageModel.ImagePath = Convert.ToString(row["ImagePath"]);
                                response.PresidentMessageModel.PresidentMessageId = Convert.ToInt32(row["PresidentMessageId"]);
                                response.PresidentMessageModel.ModifyDate = Convert.ToString(row["ModifyDate"]);
                                response.PresidentMessageModel.CreateDate = Convert.ToString(row["CreateDate"]);
                            }
                        }
                    }
                }

                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                if (returnResult == 0)
                    response.ReturnMessage = "No data found.";

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AboutUsAccess", "SelectAll");
                return response;
            }
        }

        public PresidentMessageResponse Delete(PresidentMessageWebModel model)
        {
            Int32 returnResult = 0;
            PresidentMessageResponse response = new PresidentMessageResponse();

            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.PresidentMessageId = model.PresidentMessageId;
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                returnResult = presidentMessageData.Delete(model);
                if (returnResult > 0)
                {
                    response.PresidentMessageModel = new PresidentMessageWebModel();
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PresidentMessageAccess", "Delete");
                return response;
            }
        }

    }
}
