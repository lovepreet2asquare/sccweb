﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class ChangePasswordAccess : IChangePassword
    {
        public ChangePasswordResponse ChangePassword(ChangePasswordModel model)
        {
            ChangePasswordResponse response = new ChangePasswordResponse();
            try
            {
                //model.UserId = Convert.ToString(UtilityAccess.Decrypt(model.UserId));
                //model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                model.Password = UtilityAccess.Encrypt(model.Password);
                DataSet ds = ChangePasswordData.ChangePassword(model);
                if (ds != null)
                {
                    response.ChangePasswordModel = SelectDetail(ds);
                }
                response.ReturnCode = response.ChangePasswordModel.ReturnValue;
                response.ReturnMessage = Response.Message(response.ChangePasswordModel.ReturnValue);
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChangePasswordAccess", "ChangePassword");
                return null;
            }
        }

        private ChangePasswordModel SelectDetail(DataSet ds)
        {
            ChangePasswordModel data = new ChangePasswordModel();
            data.ReturnValue = -1;
            try
            {
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        data.Email = Convert.ToString(row["Email"]);
                        data.UserId = (row["UserId"] as string ?? "");
                        data.FirstName = Convert.ToString(row["FirstName"]);
                        data.MobileNumber = Convert.ToString(row["MobileNumber"]);
                        data.ReturnValue = Convert.ToInt32(row["ReturnValue"]);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                data.ReturnValue = -1;
                ApplicationLogger.LogError(ex, "ChangePasswordAccess", "SelectDetail");
                return null;
            }
        }

        public static int SendForChangepassword(ChangePasswordModel model)
        {
            Int32 RetVal = -1;
            try
            {
                //if (model.Email)
                //{
                String url = ConfigurationManager.AppSettings["baseurl"].ToString();
                String _subject = "password Change";
                String _body = EmailUtility.ReadHtml("user/Changepassword.html");
                if (!String.IsNullOrEmpty(_body))
                {
                    _body = _body.Replace("{firstname}", model.FirstName)


                        .Replace("{urllink}", url);
                    //.Replace("{Email}", model.Email ?? "")
                    //.Replace("{ExpDate}", model.MobileNumber ?? "");
                    //.Replace("{teamtalkid}", model.A ?? "")
                    //.Replace("{teamtalkpassword}", model.TeamTalkPassword ?? "");

                    RetVal = EmailUtility.SendEmail("SCC Team", model.Email, _subject, _body, true);
                }
                //}
                return RetVal;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChangePasswordAccess", "SendForChangepassword");
                return -1;
            }
        }
        public static int SendPasswordForChangepassword(UserModel model)
        {
            Int32 RetVal = -1;
            try
            {
                //if (model.Email)
                //{
                String url = ConfigurationManager.AppSettings["baseurl"].ToString();
                String _subject = "password Change";
                String _body = EmailUtility.ReadHtml("user/Changepassword.html");
                if (!String.IsNullOrEmpty(_body))
                {
                    _body = _body.Replace("{firstname}", model.ChangePassworMMOdel.FirstName);
                    _body = _body.Replace("{emailto}", model.ChangePassworMMOdel.Email)


                        .Replace("{urllink}", url);
                    //.Replace("{Email}", model.Email ?? "")
                    //.Replace("{ExpDate}", model.MobileNumber ?? "");
                    //.Replace("{teamtalkid}", model.A ?? "")
                    //.Replace("{teamtalkpassword}", model.TeamTalkPassword ?? "");

                    RetVal = EmailUtility.SendEmail("SCC Team", model.ChangePassworMMOdel.Email, _subject, _body, true);
                }
                //}
                return RetVal;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChangePasswordAccess", "SendForChangepassword");
                return -1;
            }
        }
        public Int32 ReadUnRead(string UserId)
        {
            try
            {
                UserId = UtilityAccess.Decrypt(UserId);
                return ChangePasswordData.ReadUnRead(UserId);
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChangePasswordAccess", "ReadUnRead");
                return -1;
            }
        }

        public UserResponse ChangePassword(UserModel model)
        {
            UserResponse response = new UserResponse();
            try
            {
                //model.UserId = Convert.ToString(UtilityAccess.Decrypt(model.UserId));
                //model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                model.Password = UtilityAccess.Encrypt(model.Password);
                DataSet ds = ChangePasswordData.ChangePassword(model);
                if (ds != null)
                {
                    response.UserModel.ChangePassworMMOdel = SelectDetailUser(ds);
                }
                response.ReturnCode = response.UserModel.ChangePassworMMOdel.ReturnValue;
                response.ReturnMessage = Response.Message(response.UserModel.ChangePassworMMOdel.ReturnValue);
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChangePasswordAccess", "ChangePassword");
                return null;
            }
        }
        private ChangePasswordModell SelectDetailUser(DataSet ds)
        {
            ChangePasswordModell data = new ChangePasswordModell();
            data.ReturnValue = -1;
            try
            {
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        data.Email = Convert.ToString(row["Email"]);
                        data.UserId = (row["UserId"] as string ?? "");
                        data.FirstName = Convert.ToString(row["FirstName"]);
                        data.MobileNumber = Convert.ToString(row["MobileNumber"]);
                        data.ReturnValue = Convert.ToInt32(row["ReturnValue"]);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                data.ReturnValue = -1;
                ApplicationLogger.LogError(ex, "ChangePasswordAccess", "SelectDetail");
                return null;
            }
        }
    }
}
