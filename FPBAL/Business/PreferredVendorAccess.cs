﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
   public class PreferredVendorAccess: IPreferredVendor
    {
        public FPResponse VendorSignupInsert(VendorSignupInsertRequest request)
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";

            int returnResult = 0;
            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
            DataSet ds = VendorData.VendorSignupInsert(request);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["BusinessId"]);
                if (returnResult > 0)
                {
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "Success SCC business Signup form submitted successfully. Please check your email for confirmation.";
                }
                else if (returnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";

                }
                else if (returnResult == -5)
                {
                    serviceResponse.ReturnCode = "-5";
                    serviceResponse.ReturnMessage = "Authentication failed.";

                }
            }
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Success SCC business Signup form submitted successfully. Please check your email for confirmation.";
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }

        public FPResponse Offerlikedislike(OfferLikeDisliketRequest request)
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";

            int returnResult = 0;
            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
            DataSet ds = VendorData.offerLikeDislikeInsert(request);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["DealId"]);
                if (returnResult ==11)
                {
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "Deal Like Successfully.";
                }
                else if (returnResult == 12)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Deal Dislike Successfully.";

                } 
                else if (returnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";

                }
                else if (returnResult == -5)
                {
                    serviceResponse.ReturnCode = "-5";
                    serviceResponse.ReturnMessage = "Authentication failed.";

                }
            }
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "Submitted Successfully.";
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }

        public FPResponse ReferAVendorInsert(ReferVendorInsertRequest request)
        {
            FPResponse serviceResponse = new FPResponse();
            serviceResponse.ReturnCode = "0";
            serviceResponse.ReturnMessage = "We hit a snag, please try again after some time.";


            int returnResult = 0;

            request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
            request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
           DataSet ds = VendorData.ReferVendorInsert(request);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["BusinessId"]);
                if (returnResult > 0)
                {
                    serviceResponse.ReturnCode = "1";
                    serviceResponse.ReturnMessage = "SCC Refer a business form submitted successfully. Thank you for your support. We’ll reach out to vendor for further information";
                }
                else if (returnResult == -1)
                {
                    serviceResponse.ReturnCode = "-1";
                    serviceResponse.ReturnMessage = "Technical error.";

                }
                else if (returnResult == -5)
                {
                    serviceResponse.ReturnCode = "-5";
                    serviceResponse.ReturnMessage = "Authentication failed.";

                }
            }
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = "1";
                serviceResponse.ReturnMessage = "SCC Refer a business form submitted successfully. Thank you for your support. We’ll reach out to vendor for further information";
            }
            else if (returnResult == -1)
            {
                serviceResponse.ReturnCode = "-1";
                serviceResponse.ReturnMessage = "Technical error.";

            }
            else if (returnResult == -5)
            {
                serviceResponse.ReturnCode = "-5";
                serviceResponse.ReturnMessage = "Authentication failed.";

            }

            return serviceResponse;
        }
        public APIPreferredVendorCategoryResponse SelectPreferredVendorsCategory(VendorRequest request)
        {
            int returnResult = -2;
            List<APIPreferredVendorCategoryModel> vendorList = new List<APIPreferredVendorCategoryModel>();
            APIPreferredVendorCategoryResponse response = new APIPreferredVendorCategoryResponse();
            response.PreferredVendorList = vendorList;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = VendorData.GetPreferredVendorCategories(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["CategoryId"]);
                    if (returnResult > 0)
                    {
                        response = PackageList(ds, out returnResult);
                    }
                }
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }
        public APIPreferredVendorResponse SelectSCCVendors(PreferredVendorRequest request)
        {
            int returnResult = -2;
            List<APIPreferredVendorModel> vendorList = new List<APIPreferredVendorModel>();
            APIPreferredVendorResponse response = new APIPreferredVendorResponse();
            response.PreferredVendorList = vendorList;


            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = VendorData.GetSccPreferredVendorsList(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["BusinessId"]);
                    if (returnResult > 0)
                    {
                        response = VendorList(ds, out returnResult);
                    }
                }
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }
        public APIOfferAndDealResponse SelectOfferAndDeals(PreferredVendorRequest request)
        {
            int returnResult = -2;
            List<APIOfferAndDealModel> vendorList = new List<APIOfferAndDealModel>();
            APIOfferAndDealResponse response = new APIOfferAndDealResponse();
            response.OfferList = vendorList;


            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = VendorData.GetofferanddealsList(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["BusinessId"]);
                    if (returnResult > 0)
                    {
                        response = OfferAndDealsList(ds, out returnResult);
                    }
                }
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }

        public APIOfferAndDealDetailResponse SelectOfferAndDealDetail(APIDealDetailRequest request)
        {
            int returnResult = -2;
            APIOfferAndDealDetailModel vendorList = new APIOfferAndDealDetailModel();
            APIOfferAndDealDetailResponse response = new APIOfferAndDealDetailResponse();
            response.OfferDetail = vendorList;


            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = VendorData.GetofferandDealdetail(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["BusinessId"]);
                    if (returnResult > 0)
                    {
                        response = OfferAndDealDetail(ds, out returnResult);
                    }
                }
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }
        private APIOfferAndDealDetailResponse OfferAndDealDetail(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            APIOfferAndDealDetailResponse response = new APIOfferAndDealDetailResponse();
            APIOfferAndDealDetailModel data = new APIOfferAndDealDetailModel();
            List<APIDealsModel> Deals = new List<APIDealsModel>();
            APIDealsModel detail = new APIDealsModel();

            response.OfferDetail = data;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                APIOfferAndDealDetailModel model = null;
                
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                model = new APIOfferAndDealDetailModel();
                                model.BusinessId = UtilityAccess.Encrypt(Convert.ToString(Convert.ToInt32(row2["BusinessId"])));
                                model.BusinessName = Convert.ToString(row2["BusinessName"]);
                                model.BusinessType = Convert.ToString(row2["BusinessType"]);
                                model.BusinessDescription = Convert.ToString(row2["BusinessDescription"]);
                                model.BusinessLogo = Convert.ToString(row2["BusinessLogo"]);
                                model.Company = Convert.ToString(row2["Company"]);
                                model.Address1 = Convert.ToString(row2["Address1"]);
                                model.Address2 = Convert.ToString(row2["Address2"]);
                                model.City = Convert.ToString(row2["City"]);
                                model.State = Convert.ToString(row2["State"]);
                                model.ZipCode = Convert.ToString(row2["ZipCode"]);
                                model.BusinessServiceType = Convert.ToString(row2["BusinessServiceType"]);
                                model.BusinessPhone = Convert.ToString(row2["BusinessPhone"]);
                                model.BusinessWebsite = Convert.ToString(row2["BusinessWebsite"]);
                                model.OfferTitle = Convert.ToString(row2["OffersTitle"]);
                                model.OffersTerms = Convert.ToString(row2["OffersTerms"]);
                                model.ExpiryDate = Convert.ToString(row2["ExpiryDate"]);
                                model.ServiceId = Convert.ToString(row2["ServiceId"]);

                                GeoAddress geoAddress = new GeoAddress
                                {
                                    AddressLine1 = model.Address1,
                                    AddressLine2 = model.Address2,
                                    StateName = model.State,
                                    ZipCode = model.ZipCode,
                                    CountryName = model.CountryName,
                                    CityName = model.City
                                };
                                GeoTimezone geoTimezone = Timezone.GeoTimezone(geoAddress);
                                //model.Timezone = Convert.ToInt32(geoTimezone.rawOffset);
                                model.Latitude = geoTimezone.latitude.ToString();
                                model.Longitude = geoTimezone.longitude.ToString();
                                
                                ReturnResult = 2;
                            }


                            if (ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                            {
                                foreach (DataRow row2 in ds.Tables[1].Rows)
                                {
                                    detail = new APIDealsModel();
                                    detail.DealsId = Convert.ToString(row2["DealsId"]);
                                    detail.DealTitle = Convert.ToString(row2["DealTitle"]);
                                    detail.DealCode = Convert.ToString(row2["DealCode"]);
                                    detail.IsLike = Convert.ToInt32(row2["IsLike"]);
                                    Deals.Add(detail);

                                }
                               
                            }
                            data = model;
                            data.Deals = Deals;
                            response.OfferDetail = data;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorList");
                ReturnResult = -1;
                return response;
            }
        }

        public APIPreferredVendorResponse SelectPreferredVendors(PreferredVendorRequest request)
        {
            int returnResult = -2;
            List<APIPreferredVendorModel> vendorList = new List<APIPreferredVendorModel>();
            APIPreferredVendorResponse response = new APIPreferredVendorResponse();
            response.PreferredVendorList = vendorList;


            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = VendorData.GetPreferredVendorsList(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["BusinessId"]);
                    if (returnResult > 0)
                    {
                        response = VendorList(ds, out returnResult);
                    }
                }
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }

        private APIPreferredVendorCategoryResponse PackageList(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            APIPreferredVendorCategoryResponse response = new APIPreferredVendorCategoryResponse();
            List<APIPreferredVendorCategoryModel> data = new List<APIPreferredVendorCategoryModel>();
            response.PreferredVendorList = data;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                APIPreferredVendorCategoryModel model = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                model = new APIPreferredVendorCategoryModel();
                                model.CategoryId =Convert.ToString(row2["CategoryId"]);
                                model.CategoryName = Convert.ToString(row2["CategoryName"]);
                                model.ImagePath = Convert.ToString(row2["ImagePath"]);

                                data.Add(model);
                                ReturnResult = 2;
                            }
                            response.PreferredVendorList = data;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorList");
                ReturnResult = -1;
                return response;
            }
        }
        private APIOfferAndDealResponse OfferAndDealsList(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            APIOfferAndDealResponse response = new APIOfferAndDealResponse();
            List<APIOfferAndDealModel> data = new List<APIOfferAndDealModel>();
            response.OfferList = data;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                APIOfferAndDealModel model = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                model = new APIOfferAndDealModel();
                                model.BusinessId = UtilityAccess.Encrypt(Convert.ToString(Convert.ToInt32(row2["BusinessId"])));
                                model.BusinessName = Convert.ToString(row2["BusinessName"]);
                                model.BusinessType = Convert.ToString(row2["BusinessType"]);
                                model.BusinessDescription = Convert.ToString(row2["BusinessDescription"]);
                                model.BusinessLogo = Convert.ToString(row2["BusinessLogo"]?? "Upload/ProfilePics/50/2019052718025621/2019052718025621.png");
                                model.Company = Convert.ToString(row2["Company"]);
                                model.Address1 = Convert.ToString(row2["Address1"]);
                                model.Address2 = Convert.ToString(row2["Address2"]);
                                model.City = Convert.ToString(row2["City"]);
                                model.State = Convert.ToString(row2["State"]);
                                model.ZipCode = Convert.ToString(row2["ZipCode"]);
                                model.BusinessServiceType = Convert.ToString(row2["BusinessServiceType"]);
                                model.BusinessPhone = Convert.ToString(row2["BusinessPhone"]);
                                model.BusinessWebsite = Convert.ToString(row2["BusinessWebsite"]);
                                model.OfferTitle = Convert.ToString(row2["OffersTitle"]);
                                model.ExpiryDate = Convert.ToString(row2["ExpiryDate"]);
                                model.ServiceId = Convert.ToString(row2["ServiceId"]);
                                model.CountryName = Convert.ToString(row2["CountryName"]);

                                GeoAddress geoAddress = new GeoAddress
                                {
                                    AddressLine1 = model.Address1,
                                    AddressLine2 = model.Address2,
                                    StateName = model.State,
                                    ZipCode = model.ZipCode,
                                    CountryName = model.CountryName,
                                    CityName = model.City
                                };
                                GeoTimezone geoTimezone = Timezone.GeoTimezone(geoAddress);
                                //model.Timezone = Convert.ToInt32(geoTimezone.rawOffset);
                                model.Latitude = geoTimezone.latitude.ToString();
                                model.Longitude = geoTimezone.longitude.ToString();
                                data.Add(model);
                                ReturnResult = 2;
                            }
                            response.OfferList = data;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorList");
                ReturnResult = -1;
                return response;
            }
        }

        private APIPreferredVendorResponse VendorList(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            APIPreferredVendorResponse response = new APIPreferredVendorResponse();
            List<APIPreferredVendorModel> data = new List<APIPreferredVendorModel>();
            response.PreferredVendorList = data;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                APIPreferredVendorModel model = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                model = new APIPreferredVendorModel();
                                model.BusinessId = UtilityAccess.Encrypt(Convert.ToString(Convert.ToInt32(row2["BusinessId"])));
                                model.BusinessName = Convert.ToString(row2["BusinessName"]);
                                model.BusinessType = Convert.ToString(row2["BusinessType"]);
                                model.BusinessDescription = Convert.ToString(row2["BusinessDescription"]);
                                model.BusinessLogo = Convert.ToString(row2["BusinessLogo"]?? "Upload/ProfilePics/50/2019052718025621/2019052718025621.png");
                                if (string.IsNullOrEmpty(model.BusinessLogo))
                                {
                                    model.BusinessLogo = "Upload/ProfilePics/50/2019052718025621/2019052718025621.png";
                                }
                                model.Company = Convert.ToString(row2["Company"]);
                                model.Address1 = Convert.ToString(row2["Address1"]);
                                model.Address2 = Convert.ToString(row2["Address2"]);
                                model.City = Convert.ToString(row2["City"]);
                                model.State = Convert.ToString(row2["State"]);
                                model.ZipCode = Convert.ToString(row2["ZipCode"]);
                                model.BusinessServiceType = Convert.ToString(row2["BusinessServiceType"]);
                                model.BusinessPhone = Convert.ToString(row2["BusinessPhone"]);
                                model.BusinessEmail = Convert.ToString(row2["BusinessEmail"]);
                                model.BusinessWebsite = Convert.ToString(row2["BusinessWebsite"]);
                                GeoAddress geoAddress = new GeoAddress
                                {
                                    AddressLine1 = model.Address1,
                                    AddressLine2 = model.Address2,
                                    StateName = model.State,
                                    ZipCode = model.ZipCode,
                                    CountryName = model.CountryName,
                                    CityName = model.City
                                };
                                GeoTimezone geoTimezone = Timezone.GeoTimezone(geoAddress);
                                //model.Timezone = Convert.ToInt32(geoTimezone.rawOffset);
                                model.Latitude = geoTimezone.latitude.ToString();
                                model.Longitude = geoTimezone.longitude.ToString();

                                data.Add(model);
                                ReturnResult = 2;

                            }
                            response.PreferredVendorList = data;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorList");
                ReturnResult = -1;
                return response;
            }
        }

    }
}
