﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Threading;

namespace FPBAL.Business
{
    public class DirectoryAccess: IDirectory
    {
        DirectoryData directoryData = new DirectoryData();

        public DirectoryResponse AddorEdit(DirectoryModel model)
        {
            Int32 returnResult = 0;
            DirectoryResponse response = new DirectoryResponse();
            response._directoryModel = new DirectoryModel();
            int User_Id = model.UserId;
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedUserId))
                {
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                } 
                if (!string.IsNullOrEmpty(model.EncryptedFollowerId))
                {
                    model.FollowerId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedFollowerId));
                }
            
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                DataSet ds = directoryData.AddorEdit(model, out returnResult);
                if (returnResult == -4)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "UniqueId already exists";
                }
                else
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "Directoryaccess", "AddorEdit");
                return response;
            }

        }
        public DirectoryResponse Delete(DirectoryModel model)
        {
            Int32 returnResult = 0;
            DirectoryResponse response = new DirectoryResponse();

            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.LoggedUserId =Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedLoggedId));
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                returnResult = directoryData.Delete(model);
                if (returnResult > 0)
                {
                    response._directoryModel= new DirectoryModel();
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "Delete");
                return response;
            }
        }



        public DirectoryResponse SelectAll(DirectoryModel directoryModel)
        {
            List<DirectoryModel> directory = new List<DirectoryModel>();


            DirectoryResponse serviceResponse = new DirectoryResponse();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            DirectoryModel directoryModelinfo = new DirectoryModel();
            int returnResult = 0;
            if (directoryModel != null && !string.IsNullOrEmpty(directoryModel.EncryptedFollowerId))
                directoryModel.FollowerId = Convert.ToInt32(UtilityAccess.Decrypt(directoryModel.EncryptedFollowerId));

            directoryModel.SessionToken = UtilityAccess.Decrypt(directoryModel.EncryptedSessionToken);
            DataSet ds = directoryData.SelectAll(directoryModel);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        directoryModelinfo = new DirectoryModel();
                        directoryModelinfo.FollowerId = Convert.ToInt32(row["FollowerId"] ?? 0);
                        //categoryModelinfo.UserId = Convert.ToInt32(row["UserId"] ?? 0);
                        directoryModelinfo.EncryptedFollowerId = UtilityAccess.Encrypt(Convert.ToString(row["FollowerId"] ?? string.Empty));
                        directoryModelinfo.FirstName = Convert.ToString(row["FirstName"] ?? string.Empty);
                        directoryModelinfo.MemberTypeId = Convert.ToInt32(row["MemberTypeId"] ?? string.Empty);
                        directoryModelinfo.ModifyDate = Convert.ToString(row["ModifyDate"] ?? string.Empty);
                        directory.Add(directoryModelinfo);
                    }
                }
                directoryModelinfo.directoryModelsList = directory;
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse._directoryModel = directoryModelinfo;
            }
            return serviceResponse;
        }

        public DirectoryResponse SelectById(DirectoryModel model)
        {
            DirectoryResponse directoryResponse = new DirectoryResponse();
            DirectoryModel directoryInfo = new DirectoryModel();
            directoryResponse._directoryModel = new DirectoryModel();
            directoryResponse.ReturnCode = 0;
            directoryResponse.ReturnMessage = Response.Message(0);
            
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            
            if (!string.IsNullOrEmpty(model.EncryptedFollowerId))
                model.FollowerId =Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedFollowerId));

            DataSet ds = directoryData.SelectById(model);
            if (ds != null && ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        directoryResponse._directoryModel = new DirectoryModel();
                        directoryResponse._directoryModel.FollowerId = Convert.ToInt32(row["FollowerId"] ?? 0);
                        directoryResponse._directoryModel.EncryptedFollowerId = UtilityAccess.Encrypt(Convert.ToString(row["FollowerId"] ?? string.Empty));
                        directoryResponse._directoryModel.FirstName = Convert.ToString(row["FirstName"] ?? 0);
                        directoryResponse._directoryModel.MI = Convert.ToString(row["MI"] ?? string.Empty);
                        directoryResponse._directoryModel.LastName = Convert.ToString(row["LastName"] ?? string.Empty);
                        directoryResponse._directoryModel.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        directoryResponse._directoryModel.MemberType = Convert.ToString(row["MemberType"] ?? string.Empty);
                        directoryResponse._directoryModel.AddressLine1 = Convert.ToString(row["AddressLine1"] ?? string.Empty);
                        directoryResponse._directoryModel.AddressLine2 = Convert.ToString(row["AddressLine2"] ?? string.Empty);
                        directoryResponse._directoryModel.State = Convert.ToString(row["StateName"] ?? string.Empty);
                        directoryResponse._directoryModel.CityName = Convert.ToString(row["CityName"] ?? string.Empty);
                        directoryResponse._directoryModel.Zip = Convert.ToString(row["Zipcode"] ?? string.Empty);
                        directoryResponse._directoryModel.County = Convert.ToString(row["CountryName"] ?? string.Empty);
                        directoryResponse._directoryModel.Email = Convert.ToString(row["Email"] ?? string.Empty);
                        directoryResponse._directoryModel.Mobile = Convert.ToString(row["MobileNumber"] ?? string.Empty);
                        //directoryResponse._directoryModel.Mobile = Convert.ToString(row["ISDCode"] ?? string.Empty);
                        

                    }
                }
                directoryResponse._directoryModel._StateList = UtilityAccess.RenderList(ds.Tables[1], 1);
                directoryResponse._directoryModel._CountryList = UtilityAccess.RenderList(ds.Tables[2], 1);
                directoryResponse._directoryModel._MemberTypeList = UtilityAccess.RenderList(ds.Tables[3], 1);
                directoryResponse._directoryModel._ISDCodeList = UtilityAccess.RenderISDCodeListNew( 1);

            }
            return directoryResponse;
        }
    }
}
