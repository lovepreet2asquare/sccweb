﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class PresidentMessageAccess : IPresidentMessage
    {
        public PresidentMessageNewResponse PresidentMessageSelect(NJRequest request)
        {
            int returnResult = -2;
            PresidentMessageModel PresidentMessageDetail = new PresidentMessageModel();
            PresidentMessageNewResponse response = new PresidentMessageNewResponse();
            response.presidentMessageModel = PresidentMessageDetail;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = PresidentMessageData.PresidentMessageSelect(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["PresidentMessageId"]);
                    if (returnResult > 0)
                    {
                        PresidentMessageDetail = AboutDetail(ds, out returnResult);
                        response.presidentMessageModel = PresidentMessageDetail;

                    }

                    // response message
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AboutUsAccess", "AboutUsSelect");
                return response;
            }
        }
        private PresidentMessageModel AboutDetail(DataSet ds, out int ReturnResult)
        {
            ReturnResult = -2;
            try
            {
                PresidentMessageModel data = new PresidentMessageModel();
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                data.ImagePath = Convert.ToString(row2["ImagePath"]);
                                data.Content = Convert.ToString(row2["Content"]);
                                ReturnResult = 2;
                            }
                        }
                    }
                }
                return data;
            }

            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AboutUsAccess", "AboutDetail");
                ReturnResult = -1;
                return null;
            }
        }
    }
}
