﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;

namespace FPBAL.Business
{
    public class AppuserListAccess : IAppuserListAccess
    {
        AppUserListData apData = new AppUserListData();

        #region RegisteredFollowers
        public AppUserListResponse AppUserSelectAll(AppUserListModel model)
        {
            AppUserListResponse apResponse = new AppUserListResponse();
            apResponse.ReturnCode = 0;
            apResponse.ReturnMessage = Response.Message(0);
            List<AppUserListModel> apUserList = new List<AppUserListModel>();
            AppUserListModel apUserInfo = new AppUserListModel();
            try
            {
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                DataSet ds = apData.AppUserListSelectAll(model);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            apUserInfo = new AppUserListModel();
                            //newsInfo.UserId = Convert.ToInt32(row["UserId"]);
                            //newsInfo.EncryptedUserId = UtilityAccess.Encrypt(row["UserId"].ToString());
                            apUserInfo.FirstName = Convert.ToString(row["FirstName"] ?? string.Empty);
                            apUserInfo.MI = UtilityAccess.Encrypt(row["MI"].ToString());
                            apUserInfo.Lastname = Convert.ToString(row["Lastname"] ?? string.Empty);
                            apUserInfo.MobileNumber = Convert.ToString(row["MobileNumber"] ?? string.Empty);
                            apUserInfo.Email = Convert.ToString(row["Email"] ?? string.Empty);
                            apUserInfo.AddressLine1 = Convert.ToString(row["AddressLine1"] ?? string.Empty);
                            apUserInfo.AddressLine2 = Convert.ToString(row["AddressLine2"] ?? string.Empty);
                            apUserInfo.CityName = Convert.ToString(row["CityName"] ?? string.Empty);
                            apUserInfo.StateName = Convert.ToString(row["StateName"] ?? string.Empty);
                            apUserInfo.CountryName = Convert.ToString(row["CountryName"] ?? string.Empty);
                            apUserInfo.ZIPCode = Convert.ToString(row["ZIPCode"] ?? string.Empty);
                            apUserInfo.Occupation = Convert.ToString(row["Accupation"] ?? string.Empty);
                            apUserInfo.Employer = Convert.ToString(row["Employer"] ?? DateTime.Now);
                            apUserInfo.Status = Convert.ToString(row["Status"] ?? DateTime.Now);
                            //userInfo.EncryptedSessionToken = UtilityAccess.Encrypt(Convert.ToString(row["TokenKey"]));
                            //userInfo.EncryptedUserId = UtilityAccess.Encrypt(Convert.ToString(row["UserId"]));
                            apUserList.Add(apUserInfo);
                        }
                    }
                    apResponse.ApUserModel = new AppUserListModel();
                    apResponse.ApUserModel.ApUserModelList = new List<AppUserListModel>();
                    apResponse.ApUserModel.ApUserModelList = apUserList;
                    apResponse.ReturnCode = 1;
                    apResponse.ReturnMessage = Response.Message(1);
                }
                else
                {
                    apResponse.ReturnCode = -2;
                    apResponse.ReturnMessage = Response.Message(-2);
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AppuserListAccess", "AppUserSelectAll");
            }
            return apResponse;
        }


        //public AppUserListResponse SendBulkEmail(AppUserListModel model)
        //{
        //    AppUserListResponse apResponse = new AppUserListResponse();
        //    apResponse.ReturnCode = 0;
        //    apResponse.ReturnMessage = Response.Message(0);
        //    List<AppUserListModel> apUserList = new List<AppUserListModel>();
        //    AppUserListModel apUserInfo = new AppUserListModel();
        //    try
        //    {
        //        model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
        //        model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
        //        DataSet ds = apData.AppUserListSelectAll(model);
        //        if (ds != null && ds.Tables.Count > 0)
        //        {
        //            string recieptents = string.Empty;
        //            //if (ds.Tables[0].Rows.Count > 0)
        //            //{

        //            //    foreach (DataRow row in ds.Tables[0].Rows)
        //            //    {
        //            //        recieptents += row["Email"].ToString() + ',';
        //            //        //EmailUtility.SendEmail(
        //            //        //row["FirstName"].ToString(), row["Email"].ToString(),
        //            //        //"Invitation FamousPerson",
        //            //        //EmailUtility.ReadHtml("Invite.html").Replace("{link}", "invitaion").Replace("{FirstName}", "User"),
        //            //        //true);
        //            //    }
        //            //}
        //            foreach (DataRow row in ds.Tables[0].Rows)
        //            {
        //                Thread T1 = new Thread(delegate ()
        //                {
        //                    EmailUtility.SendEmail(row["FirstName"].ToString(), row["Email"].ToString(), "Invitation from FP", EmailUtility.ReadHtml("Invite.html").Replace("{link}", "invitaion").Replace("{FirstName}", row["FirstName"].ToString()), true);
        //                });
        //                T1.IsBackground = true;
        //                T1.Start();
        //            }
        //            apResponse.ApUserModel = new AppUserListModel();
        //            apResponse.ApUserModel.ApUserModelList = new List<AppUserListModel>();
        //            apResponse.ApUserModel.ApUserModelList = apUserList;
        //            apResponse.ReturnCode = 1;
        //            apResponse.ReturnMessage = Response.Message(1);
        //        }
        //        else
        //        {
        //            apResponse.ReturnCode = -2;
        //            apResponse.ReturnMessage = Response.Message(-2);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ApplicationLogger.LogError(ex, "AppuserListAccess", "SendBulkEmail");
        //    }
        //    return apResponse;
        //}

        #endregion RegisteredFollowers

        #region InviteUser
        public InviteResponse ExcellDataSelectByFileId(String InviteDataFileId)
        {
            InviteResponse inviteResponse = new InviteResponse();

            Int32 ReturnValue = 0;
            inviteResponse.ReturnCode = 0;
            inviteResponse.ReturnMessage = "Failed";
            try
            {
                // Insert data into database
                InviteDataFileModel inviteDataFileModel = new InviteDataFileModel();
                DataSet ds = apData.FollowerExcellDataSelectByFileId(InviteDataFileId, out ReturnValue);
                inviteResponse.InviteDataFileModel = InviteDataFile(ds);

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AppuserListAccess", "ExcellDataSelectByFileId");

            }

            return inviteResponse;

        }


        public InviteResponse UploadHttpPostedFile(InviteDataFileModel model)
        {
            InviteResponse inviteResponse = new InviteResponse();
            inviteResponse.InviteDataFileModel = new InviteDataFileModel();
            Int32 ReturnValue = 0;
            inviteResponse.ReturnCode = 0;
            inviteResponse.ReturnMessage = "Failed";
            try
            {
                if(model.HttpPostedFile!=null)
                { 
                    // Insert data into database
                    InviteDataFileModel inviteDataFileModel = new InviteDataFileModel();
                    String _FilePath = "";
                    //read excel file
                    inviteDataFileModel.dtInviteData= ReadExcelFile(model.HttpPostedFile, out _FilePath);

                    //add & get inserted data 
                    inviteDataFileModel.FilePath = _FilePath;
                    DataSet ds = apData.FollowerExcellDataAdd(inviteDataFileModel, out ReturnValue);

                    inviteResponse.InviteDataFileModel = InviteDataFile(ds);
                }              

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AppuserListAccess", "UploadHttpPostedFile");

            }
            return inviteResponse;

        }

        private InviteDataFileModel InviteDataFile(DataSet ds)
        {
            InviteDataFileModel inviteDataFileModel = new InviteDataFileModel();
            try
            {
                if (ds != null && ds.Tables.Count > 0)
                {

                    inviteDataFileModel._InviteData = new List<InviteDataModel>();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            inviteDataFileModel.InviteDataFileId =UtilityAccess.Encrypt(Convert.ToString(row["InviteDataFileId"]));
                            inviteDataFileModel.FileNumber = Convert.ToString(row["FileNumber"]);
                            inviteDataFileModel.FilePath = Convert.ToString(row["FilePath"]);
                            inviteDataFileModel.CreateDate = Convert.ToString(row["CreateDate"]);
                        }
                    }

                    //
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            inviteDataFileModel._InviteData.Add(new InviteDataModel
                            {
                                InviteDataId = Convert.ToInt32(row["InviteDataId"]),
                                FirstName = Convert.ToString(row["FirstName"]),
                                MiddleInitial = Convert.ToString(row["MiddleInitial"]),
                                LastName = Convert.ToString(row["LastName"]),
                                Email = Convert.ToString(row["Email"]),
                                IsRegister = Convert.ToBoolean(row["IsRegister"]),
                                Status = Convert.ToString(row["Status"]),
                                FileNumber = Convert.ToString(row["FileNumber"]),
                                CreateDate = Convert.ToString(row["CreateDate"]),
                            });

                        }
                    }
                }

                return inviteDataFileModel;
            }
            catch(Exception ex)
            {
                return inviteDataFileModel;
            }
        }



        public InviteResponse MultiInvite(String InviteDataFileId)
        {
            InviteResponse inviteResponse = new InviteResponse();

            Int32 ReturnValue = 0;
            inviteResponse.ReturnCode = 0;
            inviteResponse.ReturnMessage = "Failed";
            
            try
            {                

                DataSet ds = apData.FollowerExcellDataSelectByFileId(InviteDataFileId, out ReturnValue);
                inviteResponse.InviteDataFileModel = InviteDataFile(ds);

                if (inviteResponse.InviteDataFileModel._InviteData !=null && inviteResponse.InviteDataFileModel._InviteData.Count>0)
                {
                    ReturnValue= SendEmail(inviteResponse.InviteDataFileModel._InviteData);
                }


               if(ReturnValue>0)
                {
                    inviteResponse.ReturnCode = 1;
                    inviteResponse.ReturnMessage = "Success";
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AppuserListAccess", "SendInvitation");

            }
            return inviteResponse;
        }

        public InviteResponse SingleInvite(Int32 _InviteDataId,String _FirstName, String _Email)
        {
            InviteResponse inviteResponse = new InviteResponse();

            Int32 ReturnValue = 0;
            inviteResponse.ReturnCode = 0;
            inviteResponse.ReturnMessage = "Failed";

            try
            {


                inviteResponse.InviteDataFileModel = new InviteDataFileModel();
                inviteResponse.InviteDataFileModel._InviteData = new List<InviteDataModel>();
                inviteResponse.InviteDataFileModel._InviteData.Add(new InviteDataModel
                {
                    InviteDataId= _InviteDataId,
                    FirstName= _FirstName,
                    Email =_Email

                });


                //send email
                ReturnValue = SendEmail(inviteResponse.InviteDataFileModel._InviteData);               


                if (ReturnValue > 0)
                {
                    inviteResponse.ReturnCode = ReturnValue;
                    inviteResponse.ReturnMessage = "Success";
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AppuserListAccess", "SendInvitation");

            }
            return inviteResponse;
        }

        private Int32 SendEmail(List<InviteDataModel> _EmailList)
        {     
            
            try
            {
               
                if (_EmailList != null && _EmailList.Count > 0)
                {
                    string recieptents = string.Empty;
                    String _Body = EmailUtility.ReadHtml("user/Invite.html");
                    String _EmailBody = "";
                   
                    int returnVal = -1;
                    foreach (var item in _EmailList)
                    {
                        if (!item.IsRegister)// && item.Status!="Sent"
                        {
                            try
                            {
                                _EmailBody = _Body
                                       .Replace("{link}", "invitaion")
                                       .Replace("{FirstName}", item.FirstName);

                                //Thread T1 = new Thread(delegate ()
                                //{
                                returnVal = EmailUtility.SendEmail(
                                    item.FirstName,
                                    item.Email,
                                    "Invitation from SCC",
                                    _EmailBody
                                    , true);
                                //});
                                //T1.IsBackground = true;
                                //T1.Start();
                                // updating email sent status

                                apData.InviteEmailSendStatusUpdate(item.InviteDataId, returnVal > 0 ? "Sent" : "Failed");
                            }
                            catch (Exception ex)
                            {
                                return returnVal;
                            }
                        }

                        return returnVal;
                    }
                   
                }

                return 1;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AppuserListAccess", "SendInvitation");
                return -1;
            }
            
        }


        public DataTable ReadExcelFile(HttpPostedFileBase httpPostedFile,out String FilePath)
        {
            DataSet ds = null;
            FilePath = "";
            try
            {
                if (httpPostedFile != null)
                {
                    ds = new DataSet();
                    if (httpPostedFile.ContentLength > 0)
                    {
                        String fileExtension = System.IO.Path.GetExtension(httpPostedFile.FileName);
                        FilePath =Convert.ToString(DateTimeOffset.UtcNow.ToUnixTimeSeconds());

                        if (fileExtension == ".xls" || fileExtension == ".xlsx")
                        {
                            String fileLocation =
                                System.Web.HttpContext.Current.Server.MapPath("~/App_Data/DataFiles/") + FilePath;
                                //+ "/" + Path.GetFileName(httpPostedFile.FileName);

                            //if (System.IO.File.Exists(fileLocation))
                            //{
                            //    System.IO.File.Delete(fileLocation);
                            //}

                            if(!System.IO.File.Exists(fileLocation))
                            {
                                System.IO.Directory.CreateDirectory(fileLocation);
                            }

                            fileLocation+= "/" + Path.GetFileName(httpPostedFile.FileName);
                            // save file path into database
                            FilePath += Path.GetFileName(httpPostedFile.FileName);

                            httpPostedFile.SaveAs(fileLocation);
                            String excelConnectionString = String.Empty;

                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                            fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                            //connection String for xls file format.
                            if (fileExtension == ".xls")
                            {
                                excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                                fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                            }
                            //connection String for xlsx file format.
                            else if (fileExtension == ".xlsx")
                            {
                                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                            }
                            //Create Connection to Excel work book and add oledb namespace
                            OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                            excelConnection.Open();
                            DataTable dt = new DataTable();

                            dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            if (dt == null)
                            {
                                return null;
                            }

                            String[] excelSheets = new String[dt.Rows.Count];
                            Int32 t = 0;
                            //excel data saves in temp file here.
                            foreach (DataRow row in dt.Rows)
                            {
                                excelSheets[t] = row["TABLE_NAME"].ToString();
                                t++;
                            }

                            OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);

                            String query = String.Format("Select * from [{0}]", excelSheets[0]);
                            using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                            {
                                dataAdapter.Fill(ds);
                            }
                        }

                        if (fileExtension.ToString().ToLower().Equals(".xml"))
                        {
                            String fileLocation = System.Web.HttpContext.Current.Server.MapPath("~/Content/") + httpPostedFile.FileName; //System.Web.HttpContext.Current.Request.Files["FileUpload"].FileName;
                            if (System.IO.File.Exists(fileLocation))
                            {
                                System.IO.File.Delete(fileLocation);
                            }
                            httpPostedFile.SaveAs(fileLocation);

                            //System.Web.HttpContext.Current.Request.Files["FileUpload"].SaveAs(fileLocation);

                            XmlTextReader xmlreader = new XmlTextReader(fileLocation);
                            // DataSet ds = new DataSet();
                            ds.ReadXml(xmlreader);
                            xmlreader.Close();
                        }

                       
                    }
                }

                return ExcelData(ds);
            }
            catch (Exception ex)
            {

                ApplicationLogger.LogError(ex, "AppuserListAccess", "ReadExcelFile");
                return null;
            }
        }
        private DataTable ExcelData(DataSet ds)
        {
            DataTable dt = new DataTable();
        
            dt.Columns.Add("FirstName", typeof(String));
            dt.Columns.Add("MiddleInitial", typeof(String));
            dt.Columns.Add("LastName", typeof(String));
            dt.Columns.Add("Email", typeof(String));
            dt.Columns.Add("IsRegister", typeof(Boolean));
            DataRow row = null;

            try
            {

                if (ds != null && ds.Tables.Count>0 && ds.Tables[0].Rows.Count > 0)
                {
                   // foreach (DataRow dr in ds.Tables[0].Rows)
                   for(int i=0;i<ds.Tables[0].Rows.Count;i++)
                    {
                        if (IsValidEmail(Convert.ToString(ds.Tables[0].Rows[i][2])))
                        {
                            row = dt.NewRow();
                            row["FirstName"] = Convert.ToString(ds.Tables[0].Rows[i][0]);
                            row["MiddleInitial"] = "";
                            row["LastName"] = Convert.ToString(ds.Tables[0].Rows[i][1]);
                            row["Email"] = Convert.ToString(ds.Tables[0].Rows[i][2]);
                            row["IsRegister"] = false;
                            dt.Rows.Add(row);
                        }
                    }
                }

                return dt;
            }
            catch(Exception ex)
            {
                string err = ex.Message;
                return dt;
            }
        }
        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        #endregion InviteUser
    }
}
