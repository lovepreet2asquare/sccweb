﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FPDAL.Data;
using FPBAL.Interface;
using FPModels.Models;
using System.Threading;

namespace FPBAL.Business
{
    public class ChapterAccessWeb : IChapterWeb
    {
        ChapterDataWeb chapterData = new ChapterDataWeb();

        public ChapterResponseWeb SelectAll(ChapterModelWeb chapterModel)
        {
            List<ChapterModelWeb> chapters = new List<ChapterModelWeb>();


            ChapterResponseWeb serviceResponse = new ChapterResponseWeb();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            ChapterModelWeb chapterModelinfo = new ChapterModelWeb();
            int returnResult = 0;
            if (chapterModel != null && !string.IsNullOrEmpty(chapterModel.EncryptedChapterId))
                chapterModel.ChapterId = Convert.ToInt32(UtilityAccess.Decrypt(chapterModel.EncryptedChapterId));

            chapterModel.SessionToken = UtilityAccess.Decrypt(chapterModel.EncryptedSessionToken);
            DataSet ds = chapterData.SelectAll(chapterModel);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        chapterModelinfo = new ChapterModelWeb();
                        chapterModelinfo.ChapterId = Convert.ToInt32(row["ChapterId"] ?? 0);
                        chapterModelinfo.EncryptedChapterId = UtilityAccess.Encrypt(Convert.ToString(row["ChapterId"] ?? string.Empty));
                        chapterModelinfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        chapterModelinfo.Location = Convert.ToString(row["Location"] ?? string.Empty);
                        chapterModelinfo.CreateDate = Convert.ToString(row["CreateDate"] ?? string.Empty);
                        //chapterModelinfo.LogoPath = Convert.ToString(row["ImagePath"] ?? string.Empty);
                        chapters.Add(chapterModelinfo);
                    }
                }
                chapterModelinfo.ChapterModelsList = chapters;
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.Chaptermodel = chapterModelinfo;
            }
            return serviceResponse;
        }
        public ChapterResponseWeb AddorEdit(ChapterModelWeb model)
        {
            Int32 returnResult = 0;
            ChapterResponseWeb response = new ChapterResponseWeb();
            response.Chaptermodel = new ChapterModelWeb();
            int User_Id = model.UserId;
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedUserId))
                {
                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                }
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                DataSet ds = chapterData.AddorEdit(model, out returnResult);
                if (returnResult == -4)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "UniqueId already exists";
                }
                else
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChapterAccess", "AddorEdit");
                return response;
            }

        }
        public ChapterResponseWeb SelectById(ChapterModelWeb model)
        {
            ChapterResponseWeb directoryResponse = new ChapterResponseWeb();
            ChapterModelWeb directoryInfo = new ChapterModelWeb();
            directoryResponse.Chaptermodel = new ChapterModelWeb();
            directoryResponse.ReturnCode = 0;
            directoryResponse.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            int userid = 0;
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

            if (model.EncryptedChapterId == null || model.EncryptedChapterId == "0")
            {
                model.ChapterId = 0;
            }
            else
            {
                model.ChapterId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedChapterId));
            }

            userid = model.UserId;
            if (model.EncryptedUserId == null)
            {
                model.UserId = 0;
            }
            else
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            }
            DataSet ds = chapterData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        directoryResponse.Chaptermodel = new ChapterModelWeb();
                        directoryResponse.Chaptermodel.ChapterId = Convert.ToInt32(row["ChapterId"] ?? 0);
                        directoryResponse.Chaptermodel.EncryptedChapterId = UtilityAccess.Encrypt(Convert.ToString(row["ChapterId"] ?? string.Empty));
                        directoryResponse.Chaptermodel.Title = Convert.ToString(row["Title"] ?? 0);
                        directoryResponse.Chaptermodel.Location = Convert.ToString(row["Location"] ?? 0);
                        directoryResponse.Chaptermodel.LogoPath = Convert.ToString(row["ImagePath"] ?? string.Empty);
                        directoryResponse.Chaptermodel.UserId = userid;

                    }
                }
                directoryResponse.ReturnCode = returnResult;
                directoryResponse.ReturnMessage = Response.Message(returnResult);
            }
            return directoryResponse;
        }

        public ChapterResponseWeb Delete(ChapterModelWeb model)
        {
            ChapterResponseWeb serviceResponse = new ChapterResponseWeb();
            ChapterModelWeb userInfo = new ChapterModelWeb();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            int returnResult = 0;

            if (model != null && model.EncryptedChapterId != null)
                model.ChapterId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedChapterId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = chapterData.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }

            return serviceResponse;
        }
    }
}
