﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class VendorAccess : IVendor
    {
        public DirectoryAPIResponse DirectorySelectByName(DirectorySearchRequest request)
        {
            int returnResult = -2;
            List<DirectoryAPIModel> Directories = new List<DirectoryAPIModel>();
            DirectoryAPIResponse response = new DirectoryAPIResponse();
            response.Directories = Directories;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = VendorData.DirectorySelectBySearch(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["FollowerId"]);
                    if (returnResult > 0)
                    {
                        response = DirectorList(ds, out returnResult);
                        //response.Directories = Directories;
                        //returnResult = 2;
                    }
                }
                // response message
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "DirectorySelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }

        public DirectoryAPIResponse DirectorySelect(DirectoryRequest request)
        {
            int returnResult = -2;
            List<DirectoryAPIModel> Directories = new List<DirectoryAPIModel>();


            DirectoryAPIResponse response = new DirectoryAPIResponse();
            response.Directories = Directories;
            

            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            //DirectoryModel model = new DirectoryModel();
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = VendorData.DirectorySelect(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["FollowerId"]);
                    if (returnResult > 0)
                    {
                        response = DirectorList(ds, out returnResult);
                        //response.Directories = Directories;
                        //returnResult = 2;
                    }
                }
                // response message
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "DirectorySelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }
        private DirectoryAPIResponse DirectorList(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            DirectoryAPIResponse response = new DirectoryAPIResponse();
            List<DirectoryAPIModel> data = new List<DirectoryAPIModel>();
            response.Directories = data;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                DirectoryAPIModel model = null;
    
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                model = new DirectoryAPIModel();
                                model.FollowerId = UtilityAccess.Encrypt(Convert.ToString(Convert.ToInt32(row2["FollowerId"])));
                                model.FirstName = Convert.ToString(row2["FirstName"]);
                                model.MI = Convert.ToString(row2["MI"]);
                                model.LastName = Convert.ToString(row2["LastName"]);
                                model.Email = Convert.ToString(row2["Email"]);
                                model.ISDCode = Convert.ToString(row2["ISDCode"]);
                                model.MobileNumber = Convert.ToString(row2["MobileNumber"]);
                                model.ProfilePic = Convert.ToString(row2["ProfilePic"]);
                                model.StateName = Convert.ToString(row2["StateName"]);
                                model.Title = Convert.ToString(row2["Title"]);
                                model.NickName = Convert.ToString(row2["NickName"]);
                                model.MemberType = Convert.ToString(row2["MemberType"]);
                                model.IsEmailPrivacy = Convert.ToBoolean(row2["IsEmailPrivacy"]);
                                model.IsMobilePrivacy = Convert.ToBoolean(row2["IsMobilePrivacy"]);
                           
                                model.IsVerified = Convert.ToBoolean(row2["IsVerified"]);


                                data.Add(model);
                                ReturnResult = 2;
                            }
                            response.Directories = data;
                        }
                       

                    }
                }
                return response;
            }

            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryAccess", "DirectoryList");
                ReturnResult = -1;
                return response;
            }
        }

        public UserPackageResponse SelectUserPackages(VendorRequest request)
        {
            int returnResult = -2;
            List<PackageModel> vendorList = new List<PackageModel>();
            UserPackageResponse response = new UserPackageResponse();
            response.PackageList = vendorList;


            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = VendorData.GetPackagesList(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["PackageId"]);
                    if (returnResult > 0)
                    {
                        response = PackageList(ds, out returnResult);
                    }
                }
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }

        public SSCChapterResponse SelectSscChapter(VendorRequest request)
        {
            int returnResult = -2;
            List<SSCChapterModel> vendorList = new List<SSCChapterModel>();
            SSCChapterResponse response = new SSCChapterResponse();
            response.ChapterList = vendorList;

            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = VendorData.GetSSCChapterList(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["ChapterId"]);
                    if (returnResult > 0)
                    {
                        response = ChapterList(ds, out returnResult);
                    }
                }
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }

        public BoardMemberResponse BoardMemberSelect(VendorRequest request)
        {
            int returnResult = -2;
            List<BoardMemberModel> vendorList = new List<BoardMemberModel>();
            BoardMemberResponse response = new BoardMemberResponse();
            response.Members = vendorList;


            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = VendorData.GetBoardMemberInfo(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["MemberId"]);
                    if (returnResult > 0)
                    {
                        response = MemberList(ds, out returnResult);
                    }
                }
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }
        private UserPackageResponse PackageList(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            UserPackageResponse response = new UserPackageResponse();
            List<PackageModel> data = new List<PackageModel>();
            response.PackageList = data;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                PackageModel model = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                model = new PackageModel();
                                //model.PackageId = UtilityAccess.Encrypt(Convert.ToString(Convert.ToInt32(row2["PackageId"])));
                                model.PackageId =Convert.ToString(row2["PackageId"]);
                                model.PackageName = Convert.ToString(row2["PackageName"]);
                                model.Price = Convert.ToString(row2["Price"]);
                                
                                data.Add(model);
                                ReturnResult = 2;
                            }
                            response.PackageList = data;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorList");
                ReturnResult = -1;
                return response;
            }
        }
          private SSCChapterResponse ChapterList(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            SSCChapterResponse response = new SSCChapterResponse();
            List<SSCChapterModel> data = new List<SSCChapterModel>();
            response.ChapterList = data;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                SSCChapterModel model = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                model = new SSCChapterModel();
                                model.ChapterId = UtilityAccess.Encrypt(Convert.ToString(Convert.ToInt32(row2["ChapterId"])));
                                model.ImagePath = Convert.ToString(row2["ImagePath"]);
                                model.Title = Convert.ToString(row2["Title"]);
                                model.Location = Convert.ToString(row2["Location"]);
                                
                                data.Add(model);
                                ReturnResult = 2;
                            }
                            response.ChapterList = data;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorList");
                ReturnResult = -1;
                return response;
            }
        }
        private BoardMemberResponse MemberList(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            BoardMemberResponse response = new BoardMemberResponse();
            List<BoardMemberModel> data = new List<BoardMemberModel>();
            response.Members = data;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                BoardMemberModel model = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                model = new BoardMemberModel();
                                model.MemberId = UtilityAccess.Encrypt(Convert.ToString(Convert.ToInt32(row2["MemberId"])));
                                model.MemberName = Convert.ToString(row2["Name"]);
                                model.Title = Convert.ToString(row2["Title"]);
                                model.ImagePath = Convert.ToString(row2["ImagePath"]);
                                data.Add(model);
                                ReturnResult = 2;
                            }
                            response.Members = data;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorList");
                ReturnResult = -1;
                return response;
            }
        }
        public VendorResponse VendorSelect(VendorRequest request)
        {
            int returnResult = -2;
            List<VendorModel> vendorList = new List<VendorModel>();
            VendorResponse response = new VendorResponse();
            response.Vendors = vendorList;


            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = VendorData.GetVendorsInfo(request, out returnResult);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["VendorId"]);
                    if (returnResult > 0)
                    {
                        response = VendorList(ds, out returnResult);
                    }
                }
                if (returnResult == 2)
                {
                    response.ReturnCode = "1";
                    response.ReturnMessage = "Retrieved successfully";
                }
                else
                {
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);

                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorSelect");
                returnResult = -1;
                response.ReturnCode = returnResult.ToString();
                response.ReturnMessage = Response.Message(returnResult);
                return response;
            }
        }
        private VendorResponse VendorList(DataSet ds, out int ReturnResult)
        {
            ReturnResult = 0;
            VendorResponse response = new VendorResponse();
            List<VendorModel> data = new List<VendorModel>();
            response.Vendors = data;
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(-2);
            try
            {
                VendorModel model = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                model = new VendorModel();
                                model.VendorId = UtilityAccess.Encrypt(Convert.ToString(Convert.ToInt32(row2["VendorId"])));
                                model.VendorName = Convert.ToString(row2["VendorName"]);
                                model.Description = Convert.ToString(row2["OfferDescription"]);
                                model.Websitelink = Convert.ToString(row2["Websitelink"]);
                                model.ImagePath = Convert.ToString(row2["ImagePath"]);
                                model.Category = Convert.ToString(row2["Category"]);
                                model.Location = Convert.ToString(row2["Location"]); 
                                data.Add(model);
                                ReturnResult = 2;
                            }
                            response.Vendors = data;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "VendorAccess", "VendorList");
                ReturnResult = -1;
                return response;
            }
        }
    }
}
