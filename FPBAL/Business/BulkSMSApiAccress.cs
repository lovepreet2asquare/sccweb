﻿using System;
using System.Web;
using System.Text;
using System.Net;
using System.IO;
using System.Collections;

namespace FPBAL.Business
{
    public class BulkSMSAccress
    {
        public static string SendBulkSMS(BulkSMSInfo bulkSMSInfo)
        {
            int returnValue;
            Exception ex = null;
            //if (bulkSMSInfo.msisdn.Length > 10)
            //    bulkSMSInfo.msisdn = bulkSMSInfo.msisdn.Substring(bulkSMSInfo.msisdn.Length - 10);


            string result = SendBulkSMS(bulkSMSInfo.body, bulkSMSInfo.msisdn, out returnValue, out ex);
            if (returnValue == -1)
            {
               // ApplicationLogger.LogError(ex, "FiolockOperation", "SendBulkSMS");
                // ExceptionUtility.LogException(ex, "ParkeeeUser_SendBulkSMS");
            }
            return result;
        }
        public static string SendBulkSMS(string body, string msisdn, out int returnValue, out Exception ex)
        {
            string api_message = string.Empty;
            returnValue = 0;
            string returnResult = string.Empty;
            string username = "parkurcity";
            string password = "Mj03201989";
            ex = null;
            //what if no numbers & error ocurres

            string phoneList = msisdn;
            if (msisdn != null && msisdn.Length < 10)
                phoneList = "-1";

            //string phoneList = ValidatePhoneNumbers(msisdn, out ex);
            //List<string> phoneList = ValidatePhoneNumbers(msisdn);
            //List<string> returnPhoneList = new List<string>();

            // msisdn = "447123123123";
            if (phoneList == "-1")
            {
                returnResult = "-1";
            }
            else if (!string.IsNullOrEmpty(phoneList))
            {

                string url = "http://usa.bulksms.com/eapi/submission/send_sms/2/2.0";
                string seven_bit_msg = body;

                Hashtable result;

                /*
                * Upon a transient (retryable) error, sleep this many seconds:
                */
                int sleep_time = 3;
                /*
                * The time it takes to retry transient errors will increase with
                * each retry attempt:
                */
                int retry_growth_factor = 8;
                int num_retries = 5;
                int index = 0;

                //if (phoneList != null && phoneList.Count > 0)
                //{
                //foreach (string phone in phoneList)
                //{

                string data = seven_bit_message(username, password, phoneList, seven_bit_msg);

                //for (int x = 0; x < num_retries; x++)
                //{
                result = send_sms(data, url);
                if ((int)result["success"] == 1)
                {

                    returnResult = formatted_server_response(result, out api_message);
                    returnValue = 1;

                    //Console.WriteLine(formatted_server_response(result));

                    //break;
                }
                else
                {
                    returnResult = formatted_server_response(result, out api_message);

                    returnValue = -1;
                    //if (returnPhoneList != null && returnPhoneList.Count > 0)
                    //    index = returnPhoneList.Count;

                    //bool alreadyExist = returnPhoneList.Contains(phone);
                    //if (alreadyExist == false)
                    //    returnPhoneList.Add(phone + " : " + formatted_server_response(result));
                    //Console.WriteLine(formatted_server_response(result));
                }

                //System.Threading.Thread.Sleep(sleep_time);
                //sleep_time *= retry_growth_factor;
                //}
                //}
                //}

            }
            else
            {
                returnValue = 0;
            }
            return api_message;
        }
        public static string formatted_server_response(Hashtable result, out string api_message)
        {
            api_message = string.Empty;
            string ret_string = "";
            if ((int)result["success"] == 1)
            {
                ret_string += "Success: batch ID " + (string)result["api_batch_id"] + "API message: " + (string)result["api_message"] + "\nFull details " + (string)result["details"];
                api_message = (string)result["api_message"];
            }
            else
            {
                ret_string += "Fatal error: HTTP status " + (string)result["http_status_code"] + " API status " + (string)result["api_status_code"] + " API message " + (string)result["api_message"] + "\nFull details " + (string)result["details"];
                api_message = (string)result["api_message"];
            }

            return ret_string;
        }

        public static string formatted_server_response(Hashtable result)
        {
            string ret_string = "";
            if ((int)result["success"] == 1)
            {
                ret_string += "Success: batch ID " + (string)result["api_batch_id"] + "API message: " + (string)result["api_message"] + "\nFull details " + (string)result["details"];
            }
            else
            {
                ret_string += "Fatal error: HTTP status " + (string)result["http_status_code"] + " API status " + (string)result["api_status_code"] + " API message " + (string)result["api_message"] + "\nFull details " + (string)result["details"];
            }

            return ret_string;
        }

        public static Hashtable send_sms(string data, string url)
        {
            string sms_result = Post(url, data);

            Hashtable result_hash = new Hashtable();

            string tmp = "";
            tmp += "Response from server: " + sms_result + "\n";
            string[] parts = sms_result.Split('|');

            string statusCode = parts[0];
            string statusString = parts[1];

            result_hash.Add("api_status_code", statusCode);
            result_hash.Add("api_message", statusString);

            if (parts.Length != 3)
            {
                tmp += "Error: could not parse valid return data from server.\n";
            }
            else
            {
                if (statusCode.Equals("0"))
                {
                    result_hash.Add("success", 1);
                    result_hash.Add("api_batch_id", parts[2]);
                    tmp += "Message sent - batch ID " + parts[2] + "\n";
                }
                else if (statusCode.Equals("1"))
                {
                    // Success: scheduled for later sending.
                    result_hash.Add("success", 1);
                    result_hash.Add("api_batch_id", parts[2]);
                }
                else
                {
                    result_hash.Add("success", 0);
                    tmp += "Error sending: status code " + parts[0] + " description: " + parts[1] + "\n";
                }
            }
            result_hash.Add("details", tmp);
            return result_hash;
        }
        public static string Post(string url, string data)
        {

            string result = null;
            try
            {
                byte[] buffer = Encoding.Default.GetBytes(data);

                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(url);
                WebReq.Method = "POST";
                WebReq.ContentType = "application/x-www-form-urlencoded";
                WebReq.ContentLength = buffer.Length;
                Stream PostData = WebReq.GetRequestStream();

                PostData.Write(buffer, 0, buffer.Length);
                PostData.Close();
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                // Console.WriteLine(WebResp.StatusCode);

                Stream Response = WebResp.GetResponseStream();
                StreamReader _Response = new StreamReader(Response);
                result = _Response.ReadToEnd();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                result = ex.Message;
            }
            return result.Trim() + "\n";
        }

        public static string seven_bit_message(string username, string password, string msisdn, string message)
        {

            /********************************************************************
            * Construct data                                                    *
            *********************************************************************/
            /*
            * Note the suggested encoding for the some parameters, notably
            * the username, password and especially the message.  ISO-8859-1
            * is essentially the character set that we use for message bodies,
            * with a few exceptions for e.g. Greek characters. For a full list,
            * see: http://developer.bulksms.com/eapi/submission/character-encoding/
            */

            string data = "";
            data += "username=" + HttpUtility.UrlEncode(username, System.Text.Encoding.GetEncoding("ISO-8859-1"));
            data += "&password=" + HttpUtility.UrlEncode(password, System.Text.Encoding.GetEncoding("ISO-8859-1"));
            data += "&message=" + HttpUtility.UrlEncode(character_resolve(message), System.Text.Encoding.GetEncoding("ISO-8859-1"));
            data += "&msisdn=" + msisdn;
            data += "&want_report=1";

            return data;
        }
        public static string character_resolve(string body)
        {
            Hashtable chrs = new Hashtable();
            chrs.Add('Ω', "Û");
            chrs.Add('Θ', "Ô");
            chrs.Add('Δ', "Ð");
            chrs.Add('Φ', "Þ");
            chrs.Add('Γ', "¬");
            chrs.Add('Λ', "Â");
            chrs.Add('Π', "º");
            chrs.Add('Ψ', "Ý");
            chrs.Add('Σ', "Ê");
            chrs.Add('Ξ', "±");

            string ret_str = "";
            foreach (char c in body)
            {
                if (chrs.ContainsKey(c))
                {
                    ret_str += chrs[c];
                }
                else
                {
                    ret_str += c;
                }
            }
            return ret_str;
        }

        
        
    }

    public class BulkSMSInfo
    {
        public string body { get; set; }
        public string msisdn { get; set; }

    }
}
