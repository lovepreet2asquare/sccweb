﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;

namespace FPBAL.Business
{
    public class MemberBenefitAccessWeb : IMemberBenefitWeb
    {
        public MemberBenefitResponseWeb AddorEdit(MemberBenefitModelWeb model)
        {
            MemberBenefitDataWeb memberBenefitData = new MemberBenefitDataWeb();
            int returnResult = 0;
            MemberBenefitResponseWeb response = new MemberBenefitResponseWeb();
            response.MemberBenefit = new MemberBenefitModelWeb();

            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                if (!string.IsNullOrEmpty(model.EncryptedMemberBenefitId))
                {
                    model.MemberBenefitId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedMemberBenefitId));
                }

                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                string deviceToken = string.Empty, deviceType = string.Empty, Message = string.Empty;
               // Message = model.Title;
                //string timeZoneid = "India Standard Time";
                //if (HttpContext.Current.Session["timezoneid"] != null)
                //    timeZoneid = HttpContext.Current.Session["timezoneid"].ToString();
                //double timezoneSeconds = UtilityAccess.GetTimeZoneInSeconds(!string.IsNullOrEmpty(model.PublishDate) ? Convert.ToDateTime(model.PublishDate) : DateTime.UtcNow, timeZoneid);
                //model.TimeZone = Convert.ToInt32(timezoneSeconds);

                DataSet ds = memberBenefitData.AddorEdit(model, out returnResult);
                Message = model.Title;
                string desc = model.Descriptiion;
                string mid = "";
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    int result = 0; //Convert.ToString(ds.Tables[0].Rows[0]["FollowerId"]);
                    //foreach (DataRow row in ds.Tables[0].Rows)
                    //{
                    //    result = Convert.ToInt32(row["UserId"]);
                    //    if (result > 0)
                    //    {
                    //        mid = "";
                    //        Thread T1 = new Thread(delegate ()
                    //        {
                    //            deviceToken = Convert.ToString(row["DeviceToken"]);
                    //            deviceType = Convert.ToString(row["DeviceType"]);
                    //            Message = Convert.ToString(row["NotificationMessage"]);
                    //            mid = Convert.ToInt32(row["MemberBenefitId"]).ToString();
                    //            if (deviceType.ToUpper() == "IOS")
                    //                APNSNotificationAccess.ApnsNotification(deviceToken, Message, mid, "mb", desc);
                    //            else
                    //               if (deviceType.ToUpper() == "ANDROID")
                    //                FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType, mid, "memberbenefits", desc);
                    //        });
                    //        T1.IsBackground = true;
                    //        T1.Start();
                    //    }
                    //}
                }
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MemberBenefitAccessWeb", "AddorEdit");
                return response;
            }

        }
        public MemberBenefitResponseWeb SelectAll(MemberBenefitModelWeb model)
        {
            List<MemberBenefitModelWeb> MemberBenefits = new List<MemberBenefitModelWeb>();
            MemberBenefitDataWeb memberBenefitData = new MemberBenefitDataWeb();

            MemberBenefitResponseWeb serviceResponse = new MemberBenefitResponseWeb();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            
            MemberBenefitModelWeb memberBenefitInfo = new MemberBenefitModelWeb();
            int returnResult = 0;
            //if (model != null && !String.IsNullOrEmpty(model.EncryptedMemberBenefitId))
            //    model.MemberBenefitId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedMemberBenefitId));
            if (model != null && !String.IsNullOrEmpty(model.EncryptedUserId))
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            DataSet ds = memberBenefitData.SelectAll(model);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        memberBenefitInfo = new MemberBenefitModelWeb();
                        memberBenefitInfo.MemberBenefitId = Convert.ToInt32(row["MemberBenefitId"] ?? 0);
                        memberBenefitInfo.EncryptedMemberBenefitId = UtilityAccess.Encrypt(memberBenefitInfo.MemberBenefitId.ToString());
                        memberBenefitInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        memberBenefitInfo.Descriptiion = Convert.ToString(row["Description"] ?? string.Empty);
                        memberBenefitInfo.AddedBy = Convert.ToString(row["AddedBy"] ?? string.Empty);
                        memberBenefitInfo.AddedByDate = Convert.ToString(row["AddedByDate"] ?? string.Empty);
                        MemberBenefits.Add(memberBenefitInfo);
                    }
                }

                memberBenefitInfo.MemberBenefits = MemberBenefits;
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.MemberBenefit = memberBenefitInfo;
            }
            return serviceResponse;
        }

        public MemberBenefitResponseWeb Select(MemberBenefitModelWeb model)
        {
            MemberBenefitDataWeb memberBenefitData = new MemberBenefitDataWeb();

            MemberBenefitResponseWeb serviceResponse = new MemberBenefitResponseWeb();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            MemberBenefitModelWeb memberBenefitInfo = new MemberBenefitModelWeb();
            int returnResult = 0;
            if (model != null && !String.IsNullOrEmpty(model.EncryptedMemberBenefitId))
                model.MemberBenefitId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedMemberBenefitId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            DataSet ds = memberBenefitData.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {
                
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        memberBenefitInfo.MemberBenefitId = Convert.ToInt32(row["MemberBenefitId"] ?? 0);
                        memberBenefitInfo.EncryptedMemberBenefitId = UtilityAccess.Encrypt(memberBenefitInfo.MemberBenefitId.ToString());
                        memberBenefitInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        memberBenefitInfo.Descriptiion = Convert.ToString(row["Description"] ?? string.Empty);
                        memberBenefitInfo.AddedBy = Convert.ToString(row["AddedBy"] ?? string.Empty);
                        memberBenefitInfo.AddedByDate = Convert.ToString(row["AddedByDate"] ?? string.Empty);
                        
                    }
                }
                

                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.MemberBenefit = memberBenefitInfo;
            }
            return serviceResponse;
        }

        public MemberBenefitResponseWeb DetailById(MemberBenefitModelWeb model)
        {
            MemberBenefitDataWeb memberBenefitData = new MemberBenefitDataWeb();

            MemberBenefitResponseWeb serviceResponse = new MemberBenefitResponseWeb();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            MemberBenefitModelWeb memberBenefitInfo = new MemberBenefitModelWeb();
            int returnResult = 0;
            if (model != null && !String.IsNullOrEmpty(model.EncryptedMemberBenefitId))
                model.MemberBenefitId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedMemberBenefitId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            DataSet ds = memberBenefitData.DetailById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        memberBenefitInfo.MemberBenefitId = Convert.ToInt32(row["MemberBenefitId"] ?? 0);
                        memberBenefitInfo.EncryptedMemberBenefitId = UtilityAccess.Encrypt(memberBenefitInfo.MemberBenefitId.ToString());
                        memberBenefitInfo.Title = Convert.ToString(row["Title"] ?? string.Empty);
                        memberBenefitInfo.Descriptiion = Convert.ToString(row["Description"] ?? string.Empty);
                        memberBenefitInfo.AddedBy = Convert.ToString(row["AddedBy"] ?? string.Empty);
                        memberBenefitInfo.AddedByDate = Convert.ToString(row["AddedByDate"] ?? string.Empty);

                    }
                }


                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.MemberBenefit = memberBenefitInfo;
            }
            return serviceResponse;
        }

        public MemberBenefitResponseWeb Delete(MemberBenefitModelWeb model)
        {
            MemberBenefitDataWeb memberBenefitData = new MemberBenefitDataWeb();
            int returnResult = 0;
            MemberBenefitResponseWeb response = new MemberBenefitResponseWeb();
            response.MemberBenefit = new MemberBenefitModelWeb();

            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                if (!string.IsNullOrEmpty(model.EncryptedMemberBenefitId))
                {
                    model.MemberBenefitId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedMemberBenefitId));
                }

                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);

                returnResult = memberBenefitData.Delete(model);

                
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MemberBenefitAccessWeb", "Delete");
                return response;
            }

        }

    }
}
