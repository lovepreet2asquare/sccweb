﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Threading;
using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;

namespace FPBAL.Business
{

    public class dealaccess : Ideal
    {
        Dealdata dealdata = new Dealdata();
        public Dealresponse AddOrEdit(Dealmodel model)
        {
            Int32 returnResult = 0;
            Dealresponse response = new Dealresponse();
            response.dealmodel = new Dealmodel();
            int userid = model.UserId;
            // default response message
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {

                if (!string.IsNullOrEmpty(model.EncryptedUserId))
                {

                    model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                }
                if (!string.IsNullOrEmpty(model.SessionToken))
                {

                    model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);
                }
                if (!string.IsNullOrEmpty(model.EncryptedDealID))
                {

                    model.DealsId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedDealID));
                }
                if (!string.IsNullOrEmpty(model.EncryptedServiceId))
                {

                    model.ServiceId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedServiceId));
                }


                DataSet ds = dealdata.AddorEdit(model, out returnResult);
                if (returnResult == -4)
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = "UniqueId already exists";
                }
                else
                {
                    response.ReturnCode = returnResult;
                    response.ReturnMessage = Response.Message(returnResult);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "deal access", "AddorEdit");
                return response;
            }

        }
        public Dealresponse SelectById(Dealmodel model)
        {
            Dealresponse directoryResponse = new Dealresponse();
            Dealmodel directoryInfo = new Dealmodel();
            directoryResponse.dealmodel = new Dealmodel();
            directoryResponse.ReturnCode = 0;
            directoryResponse.ReturnMessage = Response.Message(0);
            int returnResult = 0;
            int userid = 0;
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            // model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));

            if (!string.IsNullOrEmpty(model.EncryptedDealID))
            {

                model.DealsId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedDealID));
            }
            if (!string.IsNullOrEmpty(model.EncryptedServiceId))
            {

                model.ServiceId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedServiceId));
            }

            userid = model.UserId;
            if (model.EncryptedUserId == null)
            {
                model.UserId = 0;
            }
            else
            {
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            }
            if (!string.IsNullOrEmpty(model.EncryptedServiceId))
            {

                model.ServiceId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedServiceId));
            }
            DataSet ds = dealdata.SelectById(model, out returnResult);
            if (ds != null && ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        directoryResponse.dealmodel = new Dealmodel();
                        directoryResponse.dealmodel.DealsId = Convert.ToInt32(row["DealsId"] ?? 0);
                        //directoryResponse.dealmodel.ServiceId = Convert.ToInt32(row["ServiceId"] ?? 0);
                        directoryResponse.dealmodel.EncryptedDealID = UtilityAccess.Encrypt(Convert.ToString(row["DealsId"] ?? string.Empty));
                        directoryResponse.dealmodel.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(row["ServiceId"] ?? string.Empty));
                        directoryResponse.dealmodel.DealTitle = Convert.ToString(row["DealTitle"] ?? 0);
                        directoryResponse.dealmodel.DealCode = Convert.ToString(row["DealCode"]);
                        directoryResponse.dealmodel.DealStatus = Convert.ToString(row["DealStatus"] ?? string.Empty);

                        directoryResponse.dealmodel.UserId = userid;

                    }
                }
                //directoryResponse.dealmodel._StatusList = UtilityAccess.RenderList(ds.Tables[1], 1);

            }
            directoryResponse.dealmodel._StatusList = UtilityAccess.ActiveStatusListNew(1);
            directoryResponse.ReturnCode = returnResult;
            directoryResponse.ReturnMessage = Response.Message(returnResult);
            return directoryResponse;
        }
        public Dealresponse Delete(Dealmodel model)
        {
            Dealresponse serviceResponse = new Dealresponse();
            Dealmodel userInfo = new Dealmodel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);

            int returnResult = 0;

            if (model != null && model.EncryptedDealID != null)
                model.DealsId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedDealID));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            returnResult = dealdata.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
            }

            return serviceResponse;
        }

    }
}