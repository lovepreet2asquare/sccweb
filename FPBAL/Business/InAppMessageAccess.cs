﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Threading;
using System.Web;

namespace FPBAL.Business
{
    public class InAppMessageAccess : IAppMessage
    {
        InAppMessageData appMessageData = new InAppMessageData();
        public InAppMessageResponse AddOrEdit(InAppMessageModel model)
        {
            Int32 returnResult = 0;
            InAppMessageResponse response = new InAppMessageResponse();
            response.InAppMessageModel = new InAppMessageModel();

            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);
            try
            {
                model.MessageId = (UtilityAccess.Decrypt(model.MessageId));
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
                //model.IsDonate = Convert.ToString(model.IsDonateNo);
                GeoAddress geoAddress = new GeoAddress
                {
                    AddressLine1 = model.AddressLine1,
                    AddressLine2 = model.AddressLine2,
                    StateName = model.StateName,
                    ZipCode = model.Zip,
                    CountryName = model.CountryName,
                    CityName = model.CityName
                };
                GeoTimezone geoTimezone = Timezone.GeoTimezone(geoAddress);
                //model.Timezone = Convert.ToInt32(geoTimezone.rawOffset);
                model.Latitude = geoTimezone.latitude;
                model.Longitude = geoTimezone.longitude;
                string timeZoneid = "India Standard Time";
                if (HttpContext.Current.Session["timezoneid"] != null)
                    timeZoneid = HttpContext.Current.Session["timezoneid"].ToString();
                double timezoneSeconds = UtilityAccess.GetTimeZoneInSeconds(!string.IsNullOrEmpty(model.PublishDate) ? Convert.ToDateTime(model.PublishDate) : DateTime.UtcNow, timeZoneid);
                model.TimeZone = Convert.ToInt32(timezoneSeconds);
                string Message = model.Title;
                string description = model.Content;
                DataSet ds = appMessageData.AddOrEdit(model, out returnResult);
                string mid = string.Empty;
                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            int result = 0;
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                result = Convert.ToInt32(row["FollowerId"]);
                                if (result > 0)
                                {
                                    Thread T1 = new Thread(delegate ()
                                    {
                                        string deviceToken = Convert.ToString(row["DeviceToken"] ?? string.Empty);
                                        string deviceType = Convert.ToString(row["DeviceType"] ?? string.Empty);
                                        mid = Convert.ToInt32(row["MessageId"]).ToString();
                                      //  string Message = model.Title;//Convert.ToString(row["NotificationMessage"] ?? string.Empty);

                                        if (deviceType.ToUpper() == "IOS")
                                            APNSNotificationAccess.ApnsNotification(deviceToken, Message, mid, "m", description);
                                        else
                                             if (deviceType.ToUpper() == "ANDROID")
                                                FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType, mid, "message", description);
                                    });
                                    T1.IsBackground = true;
                                    T1.Start();
                                }
                                //if (deviceType.ToUpper() == "IOS")
                                //    APNSNotificationAccess.ApnsNotification(deviceToken, message);
                                //else
                                //     if (deviceType.ToUpper() == "ANDROID")
                                //    FCMNotificationAccess.SendFCMNotifications(deviceToken, message, deviceType);
                            }
                            //}
                        }
                    }
                }
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);


                return response;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InAppMessageAccess", "AddOrEdit");
                return response;
            }
        }
        public InAppMessageResponse Detail(InAppMessageModel model)
        {
            Int32 returnResult = 0;
            InAppMessageResponse response = new InAppMessageResponse();
            response.InAppMessageModel = new InAppMessageModel();
            response.ReturnCode = 0;
            response.ReturnMessage = Response.Message(returnResult);

            try
            {
                model.DateFrom = !string.IsNullOrEmpty(model.DateFrom) ? UtilityAccess.FromDate(model.DateFrom) : null;
                model.DateTo = !string.IsNullOrEmpty(model.DateTo) ? UtilityAccess.FromDate(model.DateTo) : null;

                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
                model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

                DataSet ds = appMessageData.SelectAll(model, out returnResult);
                if (returnResult > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        response.InAppMessageModel._messagelist = MessageList(ds.Tables[0], returnResult);
                        returnResult = 2;
                    }
                }
                else
                {
                    response.ReturnCode = 0;
                    response.ReturnMessage = "No Data Found";
                }

                response.InAppMessageModel.StatusList = UtilityAccess.StatusList(0);
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);

                //response.InAppMessageModel._DateFilterList = UtilityAccess.DateFilterList;
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InAppMessageAccess", "SelectAll");
                return response;
            }
        }
        public InAppMessageResponse DetailView(InAppMessageModel model)
        {
            Int32 returnResult = 0;
            InAppMessageResponse response = new InAppMessageResponse();
            response.InAppMessageModel = new InAppMessageModel();
            InAppMessageModel result = null;
            model.MessageId = UtilityAccess.Decrypt(model.MessageId);
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
            model.SessionToken = UtilityAccess.Decrypt(model.SessionToken);

            DataSet ds = appMessageData.Select(model, out returnResult);
            string DateFrom = string.Empty;
            string DateTo = string.Empty;
            UtilityAccess.GetFromToDate(model.DateFilterText, out DateFrom, out DateTo);
            model.DateFrom = DateFrom;
            if (ds != null && ds.Tables.Count > 0)
            {
                result = new InAppMessageModel();
                //returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                //if (returnResult > 0)
                //{
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        result.MessageId = UtilityAccess.Encrypt(Convert.ToString(row["MessageId"]));
                        result.Title = Convert.ToString(row["Title"]);
                        result.Content = Convert.ToString(row["Content"]);
                        result.CreateDate = Convert.ToString(row["CreateDate"]);
                        result.AddressLine1 = Convert.ToString(row["AddressLine1"]);
                        result.AddressLine2 = Convert.ToString(row["AddressLine2"]);
                        result.PublishDate = Convert.ToString(row["PublishDate"]);
                        result.PublishCalDate = Convert.ToString(row["PublishCalDate"] ?? string.Empty);
                        result.IsDonate = Convert.ToString(row["IsDonate"]);
                        result.Button = Convert.ToString(row["Donate"]);
                        result.Status = Convert.ToString(row["Status"]);
                        result.Latitude = Convert.ToDouble(row["Latitude"]);
                        result.Longitude = Convert.ToDouble(row["Longitude"]);
                        result.SelectedRadius = Convert.ToInt32(row["Radius"]);
                        result.Zip = Convert.ToString(row["ZipCode"]);
                        result.CityName = Convert.ToString(row["CityName"] ?? string.Empty);
                        result.CountryId = Convert.ToInt32(row["CountryId"]);
                        result.CountryName = Convert.ToString(row["CountryName"] ?? string.Empty);
                        result.StateId = Convert.ToInt32(row["StateId"]);
                        result.StateName = Convert.ToString(row["StateName"] ?? string.Empty);
                        result.URL = Convert.ToString(row["DonateURL"] ?? string.Empty);
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    result._CountryList = UtilityAccess.RenderCountryList(ds.Tables[1]);

                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    result._StateList = UtilityAccess.RenderStateList(ds.Tables[2]);
                }
                List<SelectListItem> _radius = new List<SelectListItem>();
                _radius.Add(new SelectListItem { Value = "", Text = "--Select--" });
                for (int i = 1; i < 11; i++)
                {
                    _radius.Add(new SelectListItem { Value = i.ToString(), Text = string.Concat(i.ToString(), " Miles") });
                }
                result.Radius = _radius;
                response.ReturnCode = returnResult;
                response.ReturnMessage = Response.Message(returnResult);
                response.InAppMessageModel = result;

            }
            return response;
        }
        private List<InAppMessageModel> MessageList(DataTable dt, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                List<InAppMessageModel> data = new List<InAppMessageModel>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new InAppMessageModel
                            {
                                UserId = Convert.ToInt32(row["UserId"]),
                                MessageId = UtilityAccess.Encrypt(Convert.ToString(row["MessageId"])),
                                Content = Convert.ToString(row["Content"]),
                                Title = Convert.ToString(row["Title"]),
                                PublishDate = Convert.ToString(row["PublishDate"]),
                                //CreateDate = Convert.ToString(row["CreateDate"]),
                                CreateTime = Convert.ToString(row["CreateTime"]),
                                Status = Convert.ToString(row["Status"]),
                                ModifyDate = Convert.ToString(row["ModifyDate"]),
                                Shares = Convert.ToInt32(row["ShareCount"]==DBNull.Value?0: row["ShareCount"]),
                                View = Convert.ToInt32(row["IsOpen"]==DBNull.Value?0: row["IsOpen"]),
                            });
                        }
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InAppMessageModel", "MessageList");
                ReturnResult = -1;
                return null;
            }
        }
        public void PublishNSendNotification()
        {
            try
            {
                string deviceToken = string.Empty, deviceType = string.Empty, Message = string.Empty, mid = string.Empty;
                int returnResult = 0;
                DataSet ds = appMessageData.PublishNSendNotification(out returnResult);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                     

                        //if (ds.Tables[1].Rows.Count > 0)
                        //{
                        //    foreach (DataRow row in ds.Tables[1].Rows)
                        //    {
                                Message = Convert.ToString(row["Title"]);
                                mid = Convert.ToInt32(row["MessageId"]).ToString();
                                deviceToken = Convert.ToString(row["DeviceToken"]);
                                deviceType = Convert.ToString(row["DeviceType"]);
                                
                                if (deviceType.ToUpper() == "IOS")
                                    APNSNotificationAccess.ApnsNotification(deviceToken, Message, mid, "m");
                                else
                                   if (deviceType.ToUpper() == "ANDROID")
                                    FCMNotificationAccess.SendFCMNotifications(deviceToken, Message, deviceType, mid, "message");
                        //    }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InAppMessageAccess", "PublishNSendNotification");

            }
        }

        //public InAppMessageResponse Delete(InAppMessageModel model)
        //{
        //    Int32 returnResult = 0;
        //    InAppMessageResponse response = new InAppMessageResponse();
        //    response.InAppMessageModel = new InAppMessageModel();
        //    // default response message
        //    response.ReturnCode = 0;
        //    response.ReturnMessage = Response.Message(returnResult);
        //    try
        //    {
        //        model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(model.EncryptedUserId));
        //        model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
        //        model.MessageId = UtilityAccess.Decrypt(model.MessageId);
        //        Int32 res = appMessageData.Delete(model, out returnResult);
        //        if (returnResult > 0)
        //        {
        //            // response message
        //            response.ReturnCode = returnResult;// 
        //            response.ReturnMessage = Response.Message(returnResult);
        //        }
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        ApplicationLogger.LogError(ex, "InAppMessageAccess", "Delete");
        //        return response;
        //    }
        //}
        public InAppMessageResponse Delete(InAppMessageModel model)
        {
            InAppMessageResponse serviceResponse = new InAppMessageResponse();
            InAppMessageModel userInfo = new InAppMessageModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            InAppMessageModel AppInfo = null;
            int returnResult = 0;
            if (model != null && model.MessageId != null)
                model.MessageId = (UtilityAccess.Decrypt(model.MessageId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = appMessageData.Delete(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.InAppMessageModel = AppInfo;
            }
            return serviceResponse;
        }
        public InAppMessageResponse Archive(InAppMessageModel model)
        {
            InAppMessageResponse serviceResponse = new InAppMessageResponse();
            InAppMessageModel AppInfo = new InAppMessageModel();
            serviceResponse.ReturnCode = 0;
            serviceResponse.ReturnMessage = Response.Message(0);
            InAppMessageModel InAppInfo = null;
            int returnResult = 0;
            if (model != null && model.MessageId != null)
                model.MessageId = (UtilityAccess.Decrypt(model.MessageId));

            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            returnResult = appMessageData.Archive(model);
            if (returnResult > 0)
            {
                serviceResponse.ReturnCode = returnResult;
                serviceResponse.ReturnMessage = Response.Message(returnResult);
                serviceResponse.InAppMessageModel = InAppInfo;
            }
            return serviceResponse;
        }

    }
}
