﻿using FPBAL.Interface;
using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Business
{
    public class InvolvedApiAccess : IAppInvolved
    {
        public FPInvolvedResponse AddInvolved(GetInvolvedRequest request)
        {
            int returnResult = 0;
            FPInvolvedResponse response = new FPInvolvedResponse();
            try
            {
                request.FollowerId = (UtilityAccess.Decrypt(request.FollowerId));
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);
                int returnvalues = InvolvedApiData.AddInvolved(request);
                if (returnvalues > 0)
                {
                    response.ReturnCode = Convert.ToString(returnvalues);
                    response.ReturnMessage = Response.Message(returnvalues);
                }
                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvedApiAccess", "AddInvolved");
                return response;
            }
        }

        public FP_InvolvedApiResponse GetInvolveByEmail(FP_InvolvedApiRequest request)
        {
            int returnResult = 0;
            FP_InvolvedApiResponse response = new FP_InvolvedApiResponse();
            response.involvedApiModel = new InvolvedApiModel();
            response.ReturnCode = "0";
            response.ReturnMessage = Response.Message(returnResult);
            InvolvedApiModel model = new InvolvedApiModel();
            try
            {
                //request.FollwerId = UtilityAccess.Encrypt(request.FollwerId);
                //request.SessionToken = UtilityAccess.Encrypt(request.SessionToken);
                request.FollowerId = UtilityAccess.Decrypt(request.FollowerId);
                request.SessionToken = UtilityAccess.Decrypt(request.SessionToken);

                DataSet ds = InvolvedApiData.GetInvolveByEmail(request, returnResult);
                if (ds != null && ds.Tables.Count > 0)
                {
                    returnResult = Convert.ToInt32(ds.Tables[0].Rows[0]["InvolvedId"]);
                    if (returnResult > 0)
                    {
                        response.involvedApiModel = GetInvolveDetail(ds, returnResult);
                        returnResult = 2;
                    }

                    // response message
                    response.ReturnCode = returnResult.ToString();
                    response.ReturnMessage = Response.Message(returnResult);
                }

                return response;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "InvolvedApiAccess", "AddInvolved");
                return response;
            }
        }

        private InvolvedApiModel GetInvolveDetail(DataSet ds, Int32 ReturnResult)
        {
            ReturnResult = 1;
            try
            {
                InvolvedApiModel data = new InvolvedApiModel();
                InvolvedApiModel newsApiModel = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[0].Rows)
                            {
                                data.InvolvedId = Convert.ToInt32(row2["InvolvedId"]);
                                data.FirstName = Convert.ToString(row2["FirstName"]);
                                data.MI = Convert.ToString(row2["MI"]);
                                data.LastName = Convert.ToString(row2["LastName"]);
                                data.Email= (row2["Email"] as string ?? "");
                                data.ISDCode = (row2["ISDCode"] as string) ?? "";
                                data.Mobile = Convert.ToString(row2["Mobile"]);
                                data.CountryName = (row2["CountryName"]as string??"");
                                data.AddressLine1 = (row2["AddressLine1"] as string ?? "");
                                data.AddressLine2 = (row2["AddressLine2"] as string ?? "");
                                data.CityName= (row2["CityName"] as string ?? "");
                                data.stateName= (row2["CityName"] as string ?? "");
                                data.ZipCode = (row2["ZipCode"] as string ?? "");
                                data.CreateDate= (row2["CreateDate"] as string ?? "");
                                data.Status =Convert.ToBoolean(row2["Status"]);
                                data.Stateid= Convert.ToInt32(row2["StateId"]);
                                data.Countryid = Convert.ToInt32(row2["Countryid"]);
                            }
                        }
                    }

                }
                return data;
            }


            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsApiAccess", "NewsList");
                ReturnResult = -1;
                return null;
            }
        }
    }
}
