﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IChangePassword
    {
        ChangePasswordResponse ChangePassword(ChangePasswordModel model);
        UserResponse ChangePassword(UserModel model);
        Int32 ReadUnRead(string UserId);
    }
}
