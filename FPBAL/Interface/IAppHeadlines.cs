﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Business
{
    public interface IAppHeadlines
    {
        HeadlinesResponse GetHeadlines(HeadlinesRequest request);

        HeadlinesResponse GetFavHeadlines(HeadlinesRequest request);

        HeadlineAPIResponse GetHeadlineDetail(HeadlineDetailRequest request);

        NJResponse HeadLineShareInsert(HeadlineDetailRequest request);

        NJResponse HeadlineFavUnFav(HeadlineFavRequest request);

    }
}
