﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;
using static FPModels.Models.UserAPIModel;

namespace FPBAL.Business
{
    public interface IAppUser
    {
        string RandomString(int length);
        SignUpRespone SignUp(SignUpAPIModel request);
        SignUpRespone Login(LoginRequest request);
        FPResponse ForgotPassword(string Email);
        CommonDataRespone GetCommonData();
        NJResponse SettingUpdate(SettingRequest request);


        ProfileUpdateAPIRespone ProfileUpdate(APIProfileupdateModel request);
        FPResponse DeleteAccount(AccountDeleteRequest request);
        FPResponse Logout(LogoutRequest request);

        SignUpRespone GetFollowerInfo(LogoutRequest request);

        FPResponse SendFriendInvitations(FPInviteRequests request);

        ProfileUpdateAPIRespone UploadProfilePic(ProfilePicAPIModel request);

        ProfileUpdateAPIRespone ValidateEmail(FPRequests request);


        FPResponse ApnsNotification(string deviceToken, string Message);
    }
}
