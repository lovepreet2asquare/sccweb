﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Interface
{
    public interface IReferVendor
    {
        ReferVendorResponse signupSelectAll(ReferVendorModel vendorModel);
        ReferVendorResponse SelectAll(ReferVendorModel model);
    }
}
