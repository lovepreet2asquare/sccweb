﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IDocumentWeb
    {
        DocumentResponseWeb Add(DocumentModelWeb model);
        DocumentResponseWeb SelectAll(DocumentModelWeb model);
        DocumentResponseWeb LogSelectAll(DocumentModelWeb model);
        DocumentResponseWeb Delete(DocumentModelWeb model);

    }
}
