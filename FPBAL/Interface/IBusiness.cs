﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Interface
{
    public interface IBusiness
    {
        BusinessResponse AddorEdit(BusinessModel model);
        BusinessResponse Edit(BusinessModel model);
        BusinessResponse SelectById(BusinessModel model);
        BusinessResponse SelectAll(BusinessModel model);
        BusinessResponse Delete(BusinessModel model);
        BusinessResponse SelectByIdforrefervendor(BusinessModel model);
    }
}
