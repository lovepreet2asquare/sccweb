﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Interface
{
    public interface IAuthCustomerProfile
    {
        
        
         ///FOR API OLD////
        PaymentMethodResponse PaymentMethodInsert(CustProfileInfo customerProfileInfo);

        PaymentMethodsResponse GetPaymentMethods(FPRequests request);
        DonationDetailResponse GetDonationDetailNew(string CampaignId);
        FPResponse PaymentMethodDelete(DeletePaymentMethodRequest request);
        FPResponse PaymentMethodSetPrimary(SetPrimaryRequest request);
        DonarDetailResponse GetDonarDetail(string FollowerId, string SessionToken, string CampaignId);
        DonationDetailResponse GetDonationDetail(string FollowerId, string SessionToken, string CampaignId);
        FPResponse DonateNew(DonateAPIModel request);
        FPResponse DonationNew(CustProfileInfoNew request);
        FPResponse Donate(DonateAPIModel request);
        FPResponse AppUserBuyPackageNew(BuyPackageAPINewModel request1);
    }
}
