﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IMOM
    {
        MOMResponse GetMinutesOfMeeting(MOMRequest request);

        NJResponse MOMLogInsert(MOMLogRequest request);
    }
}
