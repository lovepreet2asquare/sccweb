﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IVendor
    {
        VendorResponse VendorSelect(VendorRequest request);
        BoardMemberResponse BoardMemberSelect(VendorRequest request);
        SSCChapterResponse SelectSscChapter(VendorRequest request);
        UserPackageResponse SelectUserPackages(VendorRequest request);
        DirectoryAPIResponse DirectorySelect(DirectoryRequest request);
        DirectoryAPIResponse DirectorySelectByName(DirectorySearchRequest request);
    }
}
