﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Interface
{
    public interface Ideal

    {

        Dealresponse AddOrEdit(Dealmodel model);
        Dealresponse SelectById(Dealmodel model);
        Dealresponse Delete(Dealmodel model);
    }
}