﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Interface
{
    public interface IMemberWeb
    {
        MemberResponseWeb SelectAll(MemberWebModel model);
        MemberResponseWeb Delete(MemberWebModel model);
        MemberResponseWeb SelectById(MemberWebModel model);
        MemberResponseWeb AddorEdit(MemberWebModel model);
    }
}
