﻿using FPModels.Models;
namespace FPBAL.Interface
{
    public interface IMoMWeb
    {
        MoMResponse AddorEdit(MoMWebModel model);
        MoMResponse SelectAll(MoMWebModel model);
        MoMResponse Select(MoMWebModel model);
        MoMResponse Delete(MoMWebModel model);

        MoMValidateResponse ValidateMoM(string month, string year);

        MoMResponse LogSelectAll(MoMWebModel model);

        FPResponse ApnsNotification(string deviceToken, string message);
    }
}
