﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface INews 
    {
        NewsResponse NewsSelectAll(NewsModel model);
        NewsResponse NewsDetail(NewsModel model);
        Int32 DeleteNewsImage(ImageListModel imgModel, NewsModel model);
        NewsResponse AddorEdit(NewsModel model);
        NewsResponse ReadMore(NewsModel model);
        //NewsModel SelectDetail(DataSet ds, out Int32 ReturnResult);
        NewsResponse DeleteNews(NewsModel model);
        NewsResponse Archive(NewsModel model);
        Int32 Updatenewnotification(string EncryptedUserId);
    }
}
