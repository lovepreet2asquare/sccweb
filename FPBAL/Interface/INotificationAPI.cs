﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface INotificationAPI
    {
        FPResponse UpdateNotification(UpdateSettingRequest request);
        NotificationAPIResponse GetNotifications(NJRequest request);

        NJResponse NotificationRead(NotificationRequest request);
    }
}
