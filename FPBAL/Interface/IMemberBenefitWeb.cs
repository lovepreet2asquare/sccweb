﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;
namespace FPBAL.Interface
{
    public interface IMemberBenefitWeb
    {
        MemberBenefitResponseWeb AddorEdit(MemberBenefitModelWeb model);
        MemberBenefitResponseWeb SelectAll(MemberBenefitModelWeb model);
        MemberBenefitResponseWeb Select(MemberBenefitModelWeb model);
        MemberBenefitResponseWeb DetailById(MemberBenefitModelWeb model);
        MemberBenefitResponseWeb Delete(MemberBenefitModelWeb model);
    }
}
