﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IEvent
    {
        EventAPIResponse GetEvents(EventAPIRequest request);
        EPAboutResponse aboutSelect(EpAboutRequest request);
        NJResponse EventAttendeeInsert(EventAttendeeRequest request);
        EventDetailResponse GetEventDetail(EventDetailRequest request);
        FPOfficeLocationResponse OfficeLocationSelect(EpAboutRequest request);
    }
}
