﻿using FPModels.Models;
using System;
using System.Collections.Generic;

namespace FPBAL.Interface
{
    public interface IEventWebAccess
    {
        EventResponse EventSelectAll(EventWebModel model);
        EventResponse EventDetail(EventWebModel model);
        EventResponse AddorEdit(EventWebModel model);
        EventResponse Archive(EventWebModel model);
        EventResponse Delete(EventWebModel model);
        EventResponse AddAgenda(EventWebModel model);
        EventResponse EventCount(EventWebModel model);
        string PdfPrintShare(List<EventWebModel> model, EventWebModel modell, string type, string senderName, string emailTo, out int ReturnResult);

    }
}
