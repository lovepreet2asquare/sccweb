﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IMenu
    {
       // MenuModel MenusById(Int32 MenuId);
        MenuModel MenusByUser(String url, Int32 LoggedId,  String MenuId);
    }
}
