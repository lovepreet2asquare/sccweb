﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
   public interface IAppInvolved
    {
        FPInvolvedResponse AddInvolved(GetInvolvedRequest request);
        FP_InvolvedApiResponse GetInvolveByEmail(FP_InvolvedApiRequest request);
    }
}
