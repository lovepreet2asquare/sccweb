﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IPresidentMessageWeb
    {
        PresidentMessageResponse AddOrEdit(PresidentMessageWebModel model);
        PresidentMessageResponse Select(PresidentMessageWebModel model);
        PresidentMessageResponse Delete(PresidentMessageWebModel model);
    }
}
