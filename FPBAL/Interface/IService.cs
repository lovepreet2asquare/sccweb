﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Interface
{
    public interface IService
    {
        ServiceResponse SelectAll(ServiceModel model);
        ServiceResponse AddorEdit(ServiceModel model);
        ServiceResponse ServiceTypeSelectAll(ServiceModel model);
        ServiceResponse SelectById(ServiceModel model);
        ServiceResponse SelectServiceDetailById(ServiceModel model);
        ServiceResponse Delete(ServiceModel model);
    }
}
