﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IDonationCompaign
    {
        DonationCampaignResponse AddOrEdit(DonationCompaign model);
        DonationCampaignResponse CompaignSelectAll(DonationCompaign model);
        DonationCampaignResponse CompaignSelect(DonationCompaign model);
        DonationCampaignResponse Delete(DonationCompaign model);

    }
}
