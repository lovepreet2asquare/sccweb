﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Interface
{
   public interface ICategory
    {
        CategoryResponse SelectAll(CategoryModel model);
        CategoryResponse SelectById(CategoryModel model);
        CategoryResponse AddorEdit(CategoryModel model);
        CategoryResponse Delete(CategoryModel model);
    }
}
