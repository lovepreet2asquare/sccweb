﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IPreferredVendor
    {
        APIPreferredVendorCategoryResponse SelectPreferredVendorsCategory(VendorRequest request);
         APIPreferredVendorResponse SelectPreferredVendors(PreferredVendorRequest request);
        FPResponse ReferAVendorInsert(ReferVendorInsertRequest request);
        APIPreferredVendorResponse SelectSCCVendors(PreferredVendorRequest request);
        APIOfferAndDealResponse SelectOfferAndDeals(PreferredVendorRequest request);
        APIOfferAndDealDetailResponse SelectOfferAndDealDetail(APIDealDetailRequest request);
        FPResponse VendorSignupInsert(VendorSignupInsertRequest request);
        FPResponse Offerlikedislike(OfferLikeDisliketRequest request);
    }
}
