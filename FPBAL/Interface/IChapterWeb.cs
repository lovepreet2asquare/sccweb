﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;

namespace FPBAL.Interface
{
    public interface IChapterWeb
    {
        ChapterResponseWeb SelectAll(ChapterModelWeb model);
        ChapterResponseWeb Delete(ChapterModelWeb model);
        ChapterResponseWeb SelectById(ChapterModelWeb model);
        ChapterResponseWeb AddorEdit(ChapterModelWeb model);
    }
}
