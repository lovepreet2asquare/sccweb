﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IShareAndOpenApi
    {
        FP_ShareAndOpenApiResponse ShareCount(FP_ShareAndOpenApiRequest request);
        FP_ShareAndOpenApiResponse OpenMessage(FP_ShareAndOpenApiRequest request);
    }
}
