﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPBAL.Interface
{
    public interface IUser
    {
        LoginRespone Login(LoginModel request);
        ForgotPasswordModel  IsEmailExist(ForgotPasswordModel request);
        UserResponse UserSelectAll(UserModel model);
        UserResponse GetUser(UserModel model);
        UserResponse AddOrEditUser(UserModel model);
        UserResponse PermissionByRole(Int32 TitleId);
        UserResponse EditUserProfile(UserModel model);
        UserResponse NewProfileEmail(UserModel model);
        UserResponse Delete(UserModel model);
        UserResponse Disable(UserModel model);
        Int32 ProfilePicUpdate(Int32 UserId, String SessionToken, String ProfilePicPath);

        UserResponse ChangePassword(UserModel model);
    }
}
