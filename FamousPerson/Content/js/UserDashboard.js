﻿



var trendGraph = null;
var volunteerGraph = null;
var donationGraph = null;
function LoadTrends(data) {
    $("#spanAppDownload").removeClass('m--font-danger m--font-success');
    $("#spanShare").removeClass('m--font-danger m--font-success');
    $("#spanPctDonation").removeClass('m--font-danger m--font-success');
    // Stats

    if (data.AppDownload >= 0) {
        $("#spanAppDownload").addClass('m--font-success');
        $("#spanAppDownload").text((data.AppDownload > 0 ? '+' : '') + data.AppDownload);
    }
    else {
        $("#spanAppDownload").addClass('m--font-danger');
        $("#spanAppDownload").text(data.AppDownload);
    }


    // $("#spanShare").text(data.Share);
    // share

    if (data.Share >= 0) {
        $("#spanShare").addClass('m--font-success');
        $("#spanShare").text((data.Share > 0 ? '+' : '') + data.Share);
    }
    else {
        $("#spanShare").addClass('m--font-danger');
        $("#spanShare").text(data.Share);
    }


    //$("#spanDonation").text('$'+ data.Donation);
    // donation    

    if (data.Donation > 0) {
        $("#spanPctDonation").addClass('m--font-success');
        $("#spanPctDonation").text('+' + data.Donation + '%');
    }
    else {
        $("#spanPctDonation").addClass('m--font-danger');
        $("#spanPctDonation").text(data.Donation + '%');
    }

    // Graph
    var arrLebel = [];
    var arrData = [];

    //if (trendGraph != null)
    //    trendGraph.destroy();



    //if (data._TrendGraph.length > 0) {
    //    if (data._TrendGraph.length == 1) {
    //        arrLebel.push('');
    //        arrData.push(0);
    //    }
    //    for (var i = 0; i < data._TrendGraph.length; i++)
    //    {
    //        arrLebel.push(data._TrendGraph[i].Lebel);
    //        arrData.push(data._TrendGraph[i].Value);
    //    }


    //    if (0 != $("#m_chart_trends_stats").length) {
    //        // clear previous graph if exists           

    //        var e = document.getElementById("m_chart_trends_stats").getContext("2d"),
    //            t = e.createLinearGradient(0, 0, 0, 240);


    //        t.addColorStop(0, Chart.helpers.color("#D8DFE5").alpha(.7).rgbString()),
    //            t.addColorStop(1, Chart.helpers.color("#D8DFE5").alpha(0).rgbString());
    //        var a = {
    //            type: "line",
    //            data: {
    //                labels: arrLebel,
    //                datasets: [{
    //                    label: "Trend Stats", backgroundColor: t, borderColor: "#224379", pointBackgroundColor: Chart.helpers.color("#ffffff").alpha(0).rgbString(), pointBorderColor: Chart.helpers.color("#ffffff").alpha(0).rgbString(), pointHoverBackgroundColor: "blue", pointHoverBorderColor: Chart.helpers.color("#000000").alpha(.2).rgbString(),
    //                    data: arrData
    //                }
    //                ]
    //            }
    //            ,
    //            options: {
    //                title: {
    //                    display: !1
    //                }
    //                ,
    //                tooltips: {
    //                    intersect: !1, mode: "nearest", xPadding: 10, yPadding: 10, caretPadding: 10
    //                }
    //                ,
    //                legend: {
    //                    display: !1
    //                }
    //                ,
    //                responsive: !0,
    //                maintainAspectRatio: !1,
    //                hover: {
    //                    mode: "index"
    //                }
    //                ,
    //                scales: {
    //                    xAxes: [{
    //                        display: true,
    //                        gridLines: true,
    //                        scaleLabel: {
    //                            display: false, labelString: "Month"
    //                        }
    //                    }
    //                    ],
    //                    yAxes: [{
    //                        display: !1,
    //                        gridLines: !1,
    //                        scaleLabel: {
    //                            display: !0, labelString: "Value"
    //                        }
    //                        ,
    //                        ticks: {
    //                            beginAtZero: !0
    //                        }
    //                    }
    //                    ]
    //                }
    //                ,
    //                elements: {
    //                    line: {
    //                        tension: .19
    //                    }
    //                    ,
    //                    point: {
    //                        radius: 4, borderWidth: 12
    //                    }
    //                }
    //                ,
    //                layout: {
    //                    padding: {
    //                        left: 0, right: 0, top: 5, bottom: 0
    //                    }
    //                }
    //            }
    //        };

    //        trendGraph = new Chart(e, a);
    //    }       
    //}

    ///  it works with div
    //if (morrisTrendChart != null)
    //    morrisTrendChart.destroy();

    $("#m_chart_trends_stats").empty();

  //  var arrayTrend = [];
   // arrayTrend.push(data.JsonTrendGraph);
    //    for(i = 0; i<data._TrendGraph.length; i++) {
    //    arrayTrend.push({Lebel: data._TrendGraph[i].Lebel, Value: data._TrendGraph[i].Value, Value1: data._TrendGraph[i].Value1, Value2: data._TrendGraph[i].Value2 });
    //}
    
        
    var morrisTrendChart = {
        init: function () {
            new Morris.Line({
                element: "m_chart_trends_stats",
                data: data._TrendGraph,
                //padding: 10,
               // behaveLikeLine: true,
                gridEnabled: false,
               // gridLineColor: '#dddddd',
                axes: true,
                parseTime: false,
                //fillOpacity: .7,                
               // lineColors: ['#d1f1ec', '#D6D23A', '#FED9E0'],
                xkey: "Lebel",
                ykeys: ['Value', 'Value1'],
                labels: ['Download','Share']
            }
            )
        }
    }
    

    morrisTrendChart.init();
}

function LoadActivity(data) {
    // Stats
    $("#spanNewDownloads").text(data.NewDownload);
    $("#spanExclusiveHeadlines").text(data.ExclusiveHeadline + ' Views');
    $("#spanActiveUsers").text(data.ActiveUser);
    $("#spanNews").text(data.News + ' Views');
}

function LoadVolunteer(data) {

    // Stats
    $("#spanVolunteer").text(data.Volunteer);

    // Graph
    if (data._VolunteerGraph.length > 0) {
        var arrLebel = [];
        var arrData = [];
        if (data._VolunteerGraph.length == 1) {
            arrLebel.push('');
            arrData.push(0);
        }
        //
        for (var i = 0; i < data._VolunteerGraph.length; i++) {
            arrLebel.push(data._VolunteerGraph[i].Lebel);
            arrData.push(data._VolunteerGraph[i].Value);
        }

        if (0 != $("#m_chart_bandwidth2").length) {

            if (volunteerGraph != null)
                volunteerGraph.destroy();

            var e = document.getElementById("m_chart_bandwidth2").getContext("2d"),
                t = e.createLinearGradient(0, 0, 0, 240);
            t.addColorStop(0, Chart.helpers.color("#d1f1ec").alpha(1).rgbString()),
                t.addColorStop(1, Chart.helpers.color("#d1f1ec").alpha(.3).rgbString());
            var a = {
                type: "line",
                data: {
                    labels: arrLebel,
                    datasets: [{
                        label: "Volunteer Stats", backgroundColor: t, borderColor: mApp.getColor("success"), pointBackgroundColor: Chart.helpers.color("#000000").alpha(0).rgbString(), pointBorderColor: Chart.helpers.color("#000000").alpha(0).rgbString(), pointHoverBackgroundColor: mApp.getColor("success"), pointHoverBorderColor: Chart.helpers.color("#000000").alpha(.2).rgbString(),
                        data: arrData
                    }
                    ]
                }
                ,
                options: {
                    title: {
                        display: !1
                    }
                    ,
                    tooltips: {
                        intersect: !1, mode: "nearest", xPadding: 10, yPadding: 10, caretPadding: 10
                    }
                    ,
                    legend: {
                        display: !1
                    }
                    ,
                    responsive: !0,
                    maintainAspectRatio: !1,
                    hover: {
                        mode: "index"
                    }
                    ,
                    scales: {
                        xAxes: [{
                            display: true,
                            gridLines: true,
                            scaleLabel: {
                                display: !1, labelString: "Month"
                            }
                        }
                        ],
                        yAxes: [{
                            display: !1,
                            gridLines: !1,
                            scaleLabel: {
                                display: !0, labelString: "Value"
                            }
                            ,
                            ticks: {
                                beginAtZero: !0
                            }
                        }
                        ]
                    }
                    ,
                    elements: {
                        line: {
                            tension: .19
                        }
                        ,
                        point: {
                            radius: 4, borderWidth: 12
                        }
                    }
                    ,
                    layout: {
                        padding: {
                            left: 0, right: 0, top: 5, bottom: 0
                        }
                    }
                }
            }
                ;
            volunteerGraph = new Chart(e, a);
        }
    }
}

function LoadDonation(data) {
    // Stats
    $("#spanDonation").text('$' + data.Donation);

    // Graph
    if (data._DonationGraph.length > 0) {
        var arrLebel = [];
        var arrData = [];
        if (data._DonationGraph.length == 1) {
            arrLebel.push('');
            arrData.push(0);
        }
        for (var i = 0; i < data._DonationGraph.length; i++) {
            arrLebel.push(data._DonationGraph[i].Lebel);
            arrData.push(data._DonationGraph[i].Value);
        }

        if (0 != $("#m_chart_bandwidth1").length) {

            if (donationGraph != null)
                donationGraph.destroy();

            var e = document.getElementById("m_chart_bandwidth1").getContext("2d"),
                t = e.createLinearGradient(0, 0, 0, 240);
            t.addColorStop(0, Chart.helpers.color("#FED9E0").alpha(1).rgbString()),
                t.addColorStop(1, Chart.helpers.color("#FED9E0").alpha(.3).rgbString());
            var a = {
                type: "line",
                data: {
                    labels: arrLebel,
                    datasets: [{
                        label: "Donation Stats", backgroundColor: t, borderColor: mApp.getColor("danger"), pointBackgroundColor: Chart.helpers.color("#000000").alpha(0).rgbString(), pointBorderColor: Chart.helpers.color("#000000").alpha(0).rgbString(), pointHoverBackgroundColor: mApp.getColor("danger"), pointHoverBorderColor: Chart.helpers.color("#000000").alpha(.2).rgbString(),
                        data: arrData
                    }
                    ]
                }
                ,
                options: {
                    title: {
                        display: !1
                    }
                    ,
                    tooltips: {
                        intersect: !1, mode: "nearest", xPadding: 10, yPadding: 10, caretPadding: 10
                    }
                    ,
                    legend: {
                        display: !1
                    }
                    ,
                    responsive: !0,
                    maintainAspectRatio: !1,
                    hover: {
                        mode: "index"
                    }
                    ,
                    scales: {
                        xAxes: [{
                            display: true,
                            gridLines: true,
                            scaleLabel: {
                                display: !1, labelString: "Month"
                            }
                        }
                        ],
                        yAxes: [{
                            display: !1,
                            gridLines: !1,
                            scaleLabel: {
                                display: !0, labelString: "Value"
                            }
                            ,
                            ticks: {
                                beginAtZero: !0
                            }
                        }
                        ]
                    }
                    ,
                    elements: {
                        line: {
                            tension: .19
                        }
                        ,
                        point: {
                            radius: 4, borderWidth: 12
                        }
                    }
                    ,
                    layout: {
                        padding: {
                            left: 0, right: 0, top: 5, bottom: 0
                        }
                    }
                }
            }
                ;
            donationGraph = new Chart(e, a);
        }
    }
}

function LoadAppMessage(data) {
    // Stats
    $("#spanAppNewSent").text(data.NewSent);
    $("#spanAppOpened").text(data.Opened);
    $("#spanAppUnopened").text(data.Unopened);
    $("#spanAppShare").text(data.Share);
    $("#divAppSummaryPB").css("width", data.SuccessRate + '%');
    $("#spanAppSummaryPBText").text(data.SuccessRate + '%');
}

function LoadFollower(data) {
    // Stats
    $("#spanTotalFollower").text(data.TotalFollower);
    $("#spanActiveFollower").text(data.ActiveFollower);
    $("#spanInactiveFollower").text(data.InactiveFollower);
    $("#spanIOSFollower").text(data.IOSText);
    $("#spanAndroidFollower").text(data.AndroidText);

    if (0 != $("#m_chart_support_tickets2").length) {

        var chartFollower = new Chartist.Pie("#m_chart_support_tickets2", {
            series: [{
                value: data.IOS, className: "ct-series custom", meta: {
                    color: mApp.getColor("danger")
                }
            }
                , {
                value: data.Android, className: "ct-series custom", meta: {
                    color: mApp.getColor("brand")
                }
            }
                //, {
                //value: 60, className: "ct-series custom", meta: {
                //    color: mApp.getColor("warning")
                //}
                //  }
            ], labels: [1, 2, 3]
        }
            , {
                donut: !0, donutWidth: 17, showLabel: !1
            }
        ).on("draw", function (e) {

            if ("slice" === e.type) {
                var t = e.element._node.getTotalLength();
                e.element.attr({
                    "stroke-dasharray": t + "px " + t + "px"
                }
                );
                var a = {
                    "stroke-dashoffset": {
                        id: "anim" + e.index, dur: 1e3, from: -t + "px", to: "0px", easing: Chartist.Svg.Easing.easeOutQuint, fill: "freeze", stroke: e.meta.color
                    }
                };

                0 !== e.index && (a["stroke-dashoffset"].begin = "anim" + (e.index - 1) + ".end"), e.element.attr(
                    { "stroke-dashoffset": -t + "px", stroke: e.meta.color }), e.element.animate(a, !1)
            }
        })

        // chartFollower.init();
    }
}

function LoadData(CallType, Duration) {

    $.ajax({
        url: '/dashboard/loaddata',
        type: 'POST',
        data: { 'CallType': CallType, 'Duration': Duration },
        dataType: 'json',
        success: function (data) {
            //Trend Stats & graph
            if (CallType === 'Loaded' || CallType === 'Trend')
                LoadTrends(data.DashboardModel.TrendModel);

            //Activity Stats
            if (CallType === 'Loaded' || CallType === 'Activity')
                LoadActivity(data.DashboardModel.ActivityModel);

            //Load Volunteer
            if (CallType === 'Loaded' || CallType === 'Volunteer')
                LoadVolunteer(data.DashboardModel.VolunteerModel);

            //Load Donation
            if (CallType === 'Loaded' || CallType === 'Donation')
                LoadDonation(data.DashboardModel.DonationModel);

            //Load App Message Stats
            if (CallType === 'Loaded' || CallType === 'Message')
                LoadAppMessage(data.DashboardModel.AppSummaryModel);

            //Load Follower
            if (CallType === 'Loaded' || CallType === 'Follower')
                LoadFollower(data.DashboardModel.FollowerModel);
        },
        error: function (request, error) {
            alert("Request: " + JSON.stringify(request));
        }
    });

}

$(document).ready(function () {
    LoadData('Loaded', 7);

    $("#ddlTrend").change(function () {
        LoadData('Trend', $(this).val());
    });
    $("#ddlActivity").change(function () {
        LoadData('Activity', $(this).val());
    });
    $("#ddlVolunteer").change(function () {
        LoadData('Volunteer', $(this).val());
    });
    $("#ddlDonation").change(function () {
        LoadData('Donation', $(this).val());
    });
    $("#ddlAppMessage").change(function () {
        LoadData('Message', $(this).val());
    });
    $("#ddlFollower").change(function () {
        LoadData('Follower', $(this).val());
    });
});


$(document).ready(function () {
    $(".GenerateReportclick").click(function () {
        var _filter = $(this).attr("db");
        $.post("/dashboard/PrintArticle", { async: false }, function (data) {
            if (data.ReturnCode > 0) {
                window.open('/download.ashx?type=invoice&file=' + data.FilePath + '&filter=' + _filter, 'Top 3 Trending Articles', '_blank');
            }
            else {
                AlertWithTitleIcon("Error", "The action couldn't be completed. Please try again later.", "error");
            }

        });
    });
});


//    function printDiv(divid) {
//        var html = "";
//        $('link').each(function () { // find all <link tags that have
//            if ($(this).attr('rel').indexOf('stylesheet') != -1) { // rel="stylesheet"
//        html += '<link rel="stylesheet" href="' + $(this).attr("href") + '" />';
//    }
//});
//var contents = document.getElementById(divid).innerHTML;
//var frame1 = document.createElement('iframe');
//frame1.name = "frame1";
//frame1.style.position = "absolute";
//frame1.style.top = "-1000000px";
//document.body.appendChild(frame1);
//var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
//frameDoc.document.open();
//        //frameDoc.document.write('<html><head><title>DIV Contents</title>');
//        frameDoc.document.write('<html><head><title></title>');
//        frameDoc.document.write('</head><body>');
//        //frameDoc.document.write('<img src="/content/images/logo-blue.png"><div style="margin-bottom:20px;"></div>');
//                frameDoc.document.write(html);
//                frameDoc.document.write(contents);
//        frameDoc.document.write('</body></html>');
//            frameDoc.document.close();
//        setTimeout(function () {
//                window.frames["frame1"].focus();
//            window.frames["frame1"].print();
//            document.body.removeChild(frame1);
//        }, 500);
//        return false;
//    }

//    //$(document).ready(function () {
//    //    printDiv('printMail');
//    //});
