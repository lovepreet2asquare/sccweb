﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web;
using System.Threading.Tasks;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;
using static FPModels.Models.UserAPIModel;

namespace FamousPerson.Areas.API.Controllers
{
    [RoutePrefix("apis/1/user")]
    public class FPAPIController : ApiController
    {
        [Route("signup")]
        [HttpPost]
        [ResponseType(typeof(SignUpRespone))]
        public IHttpActionResult SignUp(SignUpAPIModel request)
        {
            IAppUser user = new AppUserAccess();
            SignUpRespone serviceResponse = user.SignUp(request);
            return Ok<SignUpRespone>(serviceResponse);
        }

        [Route("login")]
        [HttpPost]
        [ResponseType(typeof(SignUpRespone))]
        public IHttpActionResult Login(LoginRequest request)
        {
            IAppUser user = new AppUserAccess();
            SignUpRespone serviceResponse = user.Login(request);
            return Ok<SignUpRespone>(serviceResponse);
        }

        [Route("forgotpassword")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult ForgotPassword(ForgotRequest request)
        {
            IAppUser user = new AppUserAccess();
            FPResponse serviceResponse = user.ForgotPassword(request.Email);

            return Ok<FPResponse>(serviceResponse);
        }
        [Route("getnews")]
        [HttpPost]
        [ResponseType(typeof(HeadlinesResponse))]
        public IHttpActionResult GetHealines(HeadlinesRequest request)
        {
            IAppHeadlines headline = new AppHeadlineAccess();
            HeadlinesResponse serviceResponse = headline.GetHeadlines(request);

            return Ok<HeadlinesResponse>(serviceResponse);
        }
        [Route("getfavnews")]
        [HttpPost]
        [ResponseType(typeof(HeadlinesResponse))]
        public IHttpActionResult GetFavHealines(HeadlinesRequest request)
        {
            IAppHeadlines headline = new AppHeadlineAccess();
            HeadlinesResponse serviceResponse = headline.GetFavHeadlines(request);

            return Ok<HeadlinesResponse>(serviceResponse);
        }
        [Route("getnewsdetail")]
        [HttpPost]
        [ResponseType(typeof(HeadlineAPIResponse))]
        public IHttpActionResult GetHealineDetail(HeadlineDetailRequest request)
        {
            IAppHeadlines headline = new AppHeadlineAccess();
            HeadlineAPIResponse serviceResponse = headline.GetHeadlineDetail(request);

            return Ok<HeadlineAPIResponse>(serviceResponse);
        }
        [Route("newsshare")]
        [HttpPost]
        [ResponseType(typeof(NJResponse))]
        public IHttpActionResult HeadlineShareInsert(HeadlineDetailRequest request)
        {
            IAppHeadlines headline = new AppHeadlineAccess();
            NJResponse serviceResponse = headline.HeadLineShareInsert(request);

            return Ok<NJResponse>(serviceResponse);
        }
        [Route("newsfavunfav")]
        [HttpPost]
        [ResponseType(typeof(NJResponse))]
        public IHttpActionResult HeadlineFavUnfav(HeadlineFavRequest request)
        {
            IAppHeadlines headline = new AppHeadlineAccess();
            NJResponse serviceResponse = headline.HeadlineFavUnFav(request);

            return Ok<NJResponse>(serviceResponse);
        }
        [Route("getminutesofmeeting")]
        [HttpPost]
        [ResponseType(typeof(MOMResponse))]
        public IHttpActionResult GetMinutesOfMeeting(MOMRequest request)
        {
            IMOM obj = new MOMAccess();
            MOMResponse serviceResponse = obj.GetMinutesOfMeeting(request);

            return Ok<MOMResponse>(serviceResponse);
        }
        [Route("getmemberbenefits")]
        [HttpPost]
        [ResponseType(typeof(MemberBenefitResponse))]
        public IHttpActionResult GetMemberBenefits(MemberBenefitRequest request)
        {
            IMemberBenefit obj = new MemberBenefitAccess();
            MemberBenefitResponse serviceResponse = obj.GetMemberBenefits(request);

            return Ok<MemberBenefitResponse>(serviceResponse);
        }
        [Route("momviewinsert")]
        [HttpPost]
        [ResponseType(typeof(NJResponse))]
        public IHttpActionResult MOMLogInsert(MOMLogRequest request)
        {
            IMOM obj = new MOMAccess();
            NJResponse serviceResponse = obj.MOMLogInsert(request);
            return Ok<NJResponse>(serviceResponse);
        }
        [Route("getdocuments")]
        [HttpPost]
        [ResponseType(typeof(DocumentResponse))]
        public IHttpActionResult GetDocuments(DocumentRequest request)
        {
            IDocument obj = new DocumentAccess();
            DocumentResponse serviceResponse = obj.GetDocuments(request);

            return Ok<DocumentResponse>(serviceResponse);
        }
        [Route("documentviewinsert")]
        [HttpPost]
        [ResponseType(typeof(NJResponse))]
        public IHttpActionResult DocumentLogInsert(RestrictedDocLogRequest request)
        {
            IDocument obj = new DocumentAccess();
            NJResponse serviceResponse = obj.DocumentLogInsert(request);
            return Ok<NJResponse>(serviceResponse);
        }
        [Route("getcommondata")]
        [HttpGet]
        [ResponseType(typeof(CommonDataRespone))]
        public IHttpActionResult GetCommonData()
        {
            IAppUser user = new AppUserAccess();
            CommonDataRespone serviceResponse = user.GetCommonData();
            return Ok<CommonDataRespone>(serviceResponse);
        }
        [Route("getpresidentmessage")]
        [HttpPost]
        [ResponseType(typeof(PresidentMessageNewResponse))]
        public IHttpActionResult GetPresidentMessage(NJRequest request)
        {
            IPresidentMessage obj = new PresidentMessageAccess();
            PresidentMessageNewResponse serviceResponse = obj.PresidentMessageSelect(request);
            return Ok<PresidentMessageNewResponse>(serviceResponse);
        }
        [Route("getboardmembers")]
        [HttpPost]
        [ResponseType(typeof(BoardMemberResponse))]
        public IHttpActionResult BoardMemberSelect(VendorRequest request)
        {
            IVendor obj = new VendorAccess();
            BoardMemberResponse serviceResponse = obj.BoardMemberSelect(request);
            return Ok<BoardMemberResponse>(serviceResponse);
        }
        [Route("getUserPackages")]
        [HttpPost]
        [ResponseType(typeof(UserPackageResponse))]
        public IHttpActionResult SelectUserPackages(VendorRequest request)
        {
            IVendor obj = new VendorAccess();
            UserPackageResponse serviceResponse = obj.SelectUserPackages(request);
            return Ok<UserPackageResponse>(serviceResponse);
        }
        [Route("getsscchapter")]
        [HttpPost]
        [ResponseType(typeof(SSCChapterResponse))]
        public IHttpActionResult SelectSscChapter(VendorRequest request)
        {
            IVendor obj = new VendorAccess();
            SSCChapterResponse serviceResponse = obj.SelectSscChapter(request);
            return Ok<SSCChapterResponse>(serviceResponse);
        }
        [Route("getnotifications")] 
        [HttpPost]
        [ResponseType(typeof(NotificationAPIResponse))]
        public IHttpActionResult GetNotifications(NJRequest request)
        {
            INotificationAPI obj = new NotificationApiAccess();
            NotificationAPIResponse serviceResponse = obj.GetNotifications(request);

            return Ok<NotificationAPIResponse>(serviceResponse);
        }
        [Route("getsccvendors")]
        [HttpPost]
        [ResponseType(typeof(APIPreferredVendorResponse))]
        public IHttpActionResult getvendors(PreferredVendorRequest request)
        {
            IPreferredVendor obj = new PreferredVendorAccess();
            APIPreferredVendorResponse serviceResponse = obj.SelectSCCVendors(request);

            return Ok<APIPreferredVendorResponse>(serviceResponse);
        }
        [Route("notificationread")]
        [HttpPost]
        [ResponseType(typeof(NJResponse))]
        public IHttpActionResult NotificationRead(NotificationRequest request)
        {
            INotificationAPI obj = new NotificationApiAccess();
            NJResponse serviceResponse = obj.NotificationRead(request);
            return Ok<NJResponse>(serviceResponse);
        }

        [Route("profilepicupload")]
        //[HttpPost]
        [AllowAnonymous]
        [ResponseType(typeof(HttpResponseMessage))]
        public async Task<HttpResponseMessage> ProfilePicUpload(string FollowerId)
        {
            //userId = userId.Replace(" ", "+");
            //sessionToken = sessionToken.Replace(" ", "+");
            FollowerId = FollowerId.Replace(" ", "+");
            //userId = UtilityAccess.Decrypt(userId);
            //sessionToken = UtilityAccess.Decrypt(sessionToken);
            FollowerId = UtilityAccess.Decrypt(FollowerId);
            string rootPath = string.Empty;
            int returnResult = 0;
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    string filename = "";//DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + Convert.ToString(userId);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB  
                        //string baseRoot = HttpContext.Current.Server.MapPath("~/ProfilePics/");
                        //string root = baseRoot + userId.ToString();
                        string baseRoot = HttpContext.Current.Server.MapPath("~/Follower/");
                        string root = baseRoot + FollowerId + "/" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        rootPath = "Follower/" + FollowerId + "/" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "/";
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            //dict.Add("error", message);
                            dict.Add("ReturnCode", "0");
                            dict.Add("ReturnMessage", message);
                            dict.Add("FilePath", rootPath);
                            return Request.CreateResponse(HttpStatusCode.OK, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {
                            var message = string.Format("Please Upload a file upto 1 mb.");

                            //dict.Add("error", message);
                            dict.Add("ReturnCode", "0");
                            dict.Add("ReturnMessage", message);
                            dict.Add("FilePath", rootPath);
                            return Request.CreateResponse(HttpStatusCode.OK, dict);
                        }
                        else
                        {
                            if (!System.IO.Directory.Exists(root))
                                System.IO.Directory.CreateDirectory(root);
                            //filename = filename + extension;
                            var filePath = root + "\\" + postedFile.FileName; // postedFile.FileName;
                            postedFile.SaveAs(filePath);

                            filename = rootPath + postedFile.FileName;
                            returnResult = 1;
                            //returnResult = UserAccessLayer.ProfilePicUpdate(Convert.ToInt32(userId), sessionToken, filename);
                        }
                    }
                    if (returnResult == 1)
                    {
                        var message1 = string.Format("Uploaded Successfully.");
                        dict.Add("ReturnCode", "1");
                        dict.Add("ReturnMessage", message1);
                        dict.Add("FilePath", rootPath);
                    }
                    else
                    {
                        dict.Add("ReturnCode", "0");
                        dict.Add("ReturnMessage", "");
                        dict.Add("FilePath", "");
                    }

                    //return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                    return Request.CreateResponse(HttpStatusCode.OK, dict);
                }
                var res = string.Format("Please Upload a image.");
                //dict.Add("error", res);
                dict.Add("ReturnCode", "0");
                dict.Add("ReturnMessage", res);
                dict.Add("FilePath", "");
                return Request.CreateResponse(HttpStatusCode.OK, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format(ex.Message);
                //dict.Add("error", res);
                dict.Add("ReturnCode", "-1");
                dict.Add("ReturnMessage", res);
                dict.Add("FilePath", "");
                return Request.CreateResponse(HttpStatusCode.OK, dict);
            }
        }
        [Route("uploadprofilepic")]
        //[HttpPost]
        [AllowAnonymous]
        [ResponseType(typeof(HttpResponseMessage))]
        public async Task<HttpResponseMessage> UploadProfilePic(string FollowerId)
        {
            //userId = userId.Replace(" ", "+");
            //sessionToken = sessionToken.Replace(" ", "+");
            FollowerId = FollowerId.Replace(" ", "+");
            //userId = UtilityAccess.Decrypt(userId);
            //sessionToken = UtilityAccess.Decrypt(sessionToken);
            FollowerId = UtilityAccess.Decrypt(FollowerId);
            string rootPath = string.Empty;
            int returnResult = 0;
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    string filename = "";//DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + Convert.ToString(userId);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB  
                        //string baseRoot = HttpContext.Current.Server.MapPath("~/ProfilePics/");
                        //string root = baseRoot + userId.ToString();
                        string baseRoot = HttpContext.Current.Server.MapPath("~/Follower/");
                        string root = baseRoot + FollowerId + "/" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        rootPath = "Follower/" + FollowerId + "/" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "/";
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            //dict.Add("error", message);
                            dict.Add("ReturnCode", "0");
                            dict.Add("ReturnMessage", message);
                            dict.Add("FilePath", rootPath);
                            return Request.CreateResponse(HttpStatusCode.OK, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {
                            var message = string.Format("Please Upload a file upto 1 mb.");

                            //dict.Add("error", message);
                            dict.Add("ReturnCode", "0");
                            dict.Add("ReturnMessage", message);
                            dict.Add("FilePath", rootPath);
                            return Request.CreateResponse(HttpStatusCode.OK, dict);
                        }
                        else
                        {
                            if (!System.IO.Directory.Exists(root))
                                System.IO.Directory.CreateDirectory(root);
                            //filename = filename + extension;
                            var filePath = root + "\\" + postedFile.FileName; // postedFile.FileName;
                            postedFile.SaveAs(filePath);

                            filename = rootPath + postedFile.FileName;
                            returnResult = 1;
                            IAppUser user = new AppUserAccess();
                            ProfilePicAPIModel request = new ProfilePicAPIModel();
                            request.FollowerId = FollowerId;
                            request.ProfilePic = filename;
                            ProfileUpdateAPIRespone responseUpload = user.UploadProfilePic(request);
                            returnResult = Convert.ToInt32(responseUpload.ReturnCode);
                            
                        }
                    }
                    if (returnResult == 1)
                    {
                        var message1 = string.Format("Uploaded Successfully.");
                        dict.Add("ReturnCode", "1");
                        dict.Add("ReturnMessage", message1);
                        dict.Add("FilePath", filename);
                    }
                    else if (returnResult == -1)
                    {
                        dict.Add("ReturnCode", "-1");
                        dict.Add("ReturnMessage", "Technical error.");
                        dict.Add("FilePath", "");
                    }
                    else if (returnResult == -5)
                    {
                        dict.Add("ReturnCode", "0");
                        dict.Add("ReturnMessage", "Authentication failed.");
                        dict.Add("FilePath", "");
                    }
                    else if (returnResult == 0)
                    {
                        dict.Add("ReturnCode", "0");
                        dict.Add("ReturnMessage", "We hit a snag, please try again after some time.");
                        dict.Add("FilePath", "");
                    }

                    //return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                    return Request.CreateResponse(HttpStatusCode.OK, dict);
                }
                var res = string.Format("Please Upload a image.");
                //dict.Add("error", res);
                dict.Add("ReturnCode", "0");
                dict.Add("ReturnMessage", res);
                dict.Add("FilePath", "");
                return Request.CreateResponse(HttpStatusCode.OK, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format(ex.Message);
                //dict.Add("error", res);
                dict.Add("ReturnCode", "-1");
                dict.Add("ReturnMessage", res);
                dict.Add("FilePath", "");
                return Request.CreateResponse(HttpStatusCode.OK, dict);
            }
        }
        [Route("profileupdate")]
        [HttpPost]
        [ResponseType(typeof(ProfileUpdateAPIRespone))]
        public IHttpActionResult ProfileUpdate(APIProfileupdateModel request)
        {
            IAppUser user = new AppUserAccess();
            ProfileUpdateAPIRespone serviceResponse = user.ProfileUpdate(request);
            return Ok<ProfileUpdateAPIRespone>(serviceResponse);
        }

        [Route("getinvolvedadd")]
        [HttpPost]
        [ResponseType(typeof(FPInvolvedResponse))]
        public IHttpActionResult AddInvolved(GetInvolvedRequest request)
        {
            IAppInvolved user = new InvolvedApiAccess();
            FPInvolvedResponse serviceResponse = user.AddInvolved(request);

            return Ok<FPInvolvedResponse>(serviceResponse);
        }

        [Route("deleteaccount")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult DeleteAccount(AccountDeleteRequest request)
        {
            IAppUser user = new AppUserAccess();
            FPResponse serviceResponse = user.DeleteAccount(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("logout")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult Logout(LogoutRequest request)
        {
            IAppUser user = new AppUserAccess();
            FPResponse serviceResponse = user.Logout(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("getevents")]
        [HttpPost]
        [ResponseType(typeof(EventAPIResponse))]
        public IHttpActionResult EventSelect(EventAPIRequest request)
        {
            IEvent user = new EventApiAccess();
            EventAPIResponse serviceResponse = user.GetEvents(request);

            return Ok<EventAPIResponse>(serviceResponse);
        }
        [Route("geteventdetail")]
        [HttpPost]
        [ResponseType(typeof(EventDetailResponse))]
        public IHttpActionResult GetEventDetail(EventDetailRequest request)
        {
            IEvent obj = new EventApiAccess();
            EventDetailResponse serviceResponse = obj.GetEventDetail(request);

            return Ok<EventDetailResponse>(serviceResponse);
        }


        [Route("eventattendeeinsert")]
        [HttpPost]
        [ResponseType(typeof(NJResponse))]
        public IHttpActionResult EventAttendeeInsert(EventAttendeeRequest request)
        {
            IEvent obj = new EventApiAccess();
            NJResponse serviceResponse = obj.EventAttendeeInsert(request);

            return Ok<NJResponse>(serviceResponse);
        }

        [Route("getcampaignlocations")]
        [HttpPost]
        [ResponseType(typeof(FPOfficeLocationResponse))]
        public IHttpActionResult GetCampaignLocations(EpAboutRequest request)
        {
            IEvent user = new EventApiAccess();
            FPOfficeLocationResponse serviceResponse = user.OfficeLocationSelect(request);

            return Ok<FPOfficeLocationResponse>(serviceResponse);
        }

        [Route("getaboutdetail")]
        [HttpPost]
        [ResponseType(typeof(EPAboutResponse))]
        public IHttpActionResult getaboutdetail(EpAboutRequest request)
        {
            IEvent user = new EventApiAccess();
            EPAboutResponse serviceResponse = user.aboutSelect(request);

            return Ok<EPAboutResponse>(serviceResponse);
        }

        [Route("contactus")]
        [HttpPost]
        [ResponseType(typeof(FPInvolvedResponse))]
        public IHttpActionResult AddContactUs(EPContactUsRequest request)
        {
            IContactUs user = new ContactUsAccess();
            FPInvolvedResponse serviceResponse = user.AddContactUs(request);

            return Ok<FPInvolvedResponse>(serviceResponse);
        }
        [Route("changepassword")]
        [HttpPost]
        [ResponseType(typeof(FPInvolvedResponse))]
        public IHttpActionResult ChangePassword([FromBody] ChangePasswordApiModel request)
        {
            IContactUs user = new ContactUsAccess();
            FPInvolvedResponse serviceResponse = user.ChangePassword(request);
            return Ok<FPInvolvedResponse>(serviceResponse);
        }
        [Route("notificationsetting")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult NotificationSetting(UpdateSettingRequest request)
        {
            INotificationAPI user = new NotificationApiAccess();
            FPResponse serviceResponse = user.UpdateNotification(request);

            return Ok<FPResponse>(serviceResponse);
        }
        [Route("getinboxmessages")]
        [HttpPost]
        [ResponseType(typeof(InboxApiResponse))]
        public IHttpActionResult GetInboxMessage(FPInboxApiRequest request)
        {
            IInbox user = new InboxApiAccess();
            InboxApiResponse serviceResponse = user.InboxMessageSelect(request);

            return Ok<InboxApiResponse>(serviceResponse);
        }
        [Route("getmessagedetail")]
        [HttpPost]
        [ResponseType(typeof(InboxDetailApiResponse))]
        public IHttpActionResult InAppMessageDetail(InAppMessageDetailApiRequest request)
        {
            IInbox user = new InboxApiAccess();
            InboxDetailApiResponse serviceResponse = user.InAppMessageDetail(request);

            return Ok<InboxDetailApiResponse>(serviceResponse);
        }

        [Route("sharemessagecount")]
        [HttpPost]
        [ResponseType(typeof(FP_ShareAndOpenApiResponse))]
        public IHttpActionResult ShareMessage(FP_ShareAndOpenApiRequest request)
        {
            IShareAndOpenApi user = new ShareAndOpenApiAccess();
            FP_ShareAndOpenApiResponse serviceResponse = user.ShareCount(request);

            return Ok<FP_ShareAndOpenApiResponse>(serviceResponse);
        }

        [Route("openinboxmessage")]
        [HttpPost]
        [ResponseType(typeof(FP_ShareAndOpenApiResponse))]
        public IHttpActionResult OpenMessage(FP_ShareAndOpenApiRequest request)
        {
            IShareAndOpenApi user = new ShareAndOpenApiAccess();
            FP_ShareAndOpenApiResponse serviceResponse = user.OpenMessage(request);

            return Ok<FP_ShareAndOpenApiResponse>(serviceResponse);
        }

        [Route("headlineviewercount")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult HeadLineViewerInsert(FP_HeadlineViewerAndShareRequest request)
        {
            IApiHeadlineViewerAndShare user = new ApiHeadlineViewerAndShareAccess();
            FPResponse serviceResponse = user.HeadLineViewerInsert(request);

            return Ok<FPResponse>(serviceResponse);
        }

       

       
        [Route("getfollowerinfo")]
        [HttpPost]
        [ResponseType(typeof(SignUpRespone))]
        public IHttpActionResult GetFollowerInfo(LogoutRequest request)
        {
            IAppUser user = new AppUserAccess();
            SignUpRespone serviceResponse = user.GetFollowerInfo(request);

            return Ok<SignUpRespone>(serviceResponse);
        }

        [Route("getinvolvebyemail")]
        [HttpPost]
        [ResponseType(typeof(FP_InvolvedApiResponse))]
        public IHttpActionResult GetInvolveByEmail(FP_InvolvedApiRequest request)
        {
            IAppInvolved user = new InvolvedApiAccess();
            FP_InvolvedApiResponse serviceResponse = user.GetInvolveByEmail(request);

            return Ok<FP_InvolvedApiResponse>(serviceResponse);
        }

        [Route("paymentmethodinsert")]
        [HttpPost]
        [ResponseType(typeof(PaymentMethodResponse))]
        public IHttpActionResult PaymentMethodInsert(CustProfileInfo request)
        {
            IAuthCustomerProfile obj = new AuthCustomerProfileAccess();
            PaymentMethodResponse serviceResponse = obj.PaymentMethodInsert(request);
            

            return Ok<PaymentMethodResponse>(serviceResponse);
        }
        [Route("getpaymentmethods")]
        [HttpPost]
        [ResponseType(typeof(PaymentMethodsResponse))]
        public IHttpActionResult GetPaymentMethods(FPRequests request)
        {
            IAuthCustomerProfile obj = new AuthCustomerProfileAccess();
            PaymentMethodsResponse serviceResponse = obj.GetPaymentMethods(request);

            return Ok<PaymentMethodsResponse>(serviceResponse);
        }
        [Route("paymentmethoddelete")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult PaymentMethodDelete(DeletePaymentMethodRequest request)
        {
            IAuthCustomerProfile obj = new AuthCustomerProfileAccess();
            FPResponse serviceResponse = obj.PaymentMethodDelete(request);

            return Ok<FPResponse>(serviceResponse);
        }
        [Route("paymentmethodsetprimary")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult PaymentMethodSetPrimary(SetPrimaryRequest request)
        {
            IAuthCustomerProfile obj = new AuthCustomerProfileAccess();
            FPResponse serviceResponse = obj.PaymentMethodSetPrimary(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("donate")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult  Donate(DonateAPIModel request)
        {
            IAuthCustomerProfile obj = new AuthCustomerProfileAccess();
            FPResponse serviceResponse = obj.Donate(request);

            return Ok<FPResponse>(serviceResponse);
        }


        [Route("sendfriendinvitation")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult SendFriendInvitations(FPInviteRequests request)
        {
            IAppUser user = new AppUserAccess();
            FPResponse serviceResponse = user.SendFriendInvitations(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("validateemail")]
        [HttpPost]
        [ResponseType(typeof(ProfileUpdateAPIRespone))]
        public IHttpActionResult ValidateEmail(FPRequests request)
        {
            IAppUser user = new AppUserAccess();
            ProfileUpdateAPIRespone serviceResponse = user.ValidateEmail(request);

            return Ok<ProfileUpdateAPIRespone>(serviceResponse);
        }

        [Route("SendApnsNotification")]
        [HttpGet]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult ApnsNotification(string deviceToken, string Message)
        {
            IAppUser user = new AppUserAccess();
            FPResponse serviceResponse = user.ApnsNotification(deviceToken, Message);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("getkeys")]
        [HttpGet]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult getkeys()
        {
            DonationAccess obj = new DonationAccess();
            string ApiLogin = string.Empty, TransactionKey = string.Empty;
            obj.EncryptAuthorizeKeys(out ApiLogin, out TransactionKey);

            FPResponse serviceResponse = new FPResponse();
            return Ok<FPResponse>(serviceResponse);
        }

        [Route("getpreferredvendorCategory")]
        [HttpPost]
        [ResponseType(typeof(APIPreferredVendorCategoryResponse))]
        public IHttpActionResult getpreferredvendorCategory(VendorRequest request)
        {
            IPreferredVendor user = new PreferredVendorAccess();
            APIPreferredVendorCategoryResponse serviceResponse = user.SelectPreferredVendorsCategory(request);

            return Ok<APIPreferredVendorCategoryResponse>(serviceResponse);
        }
        [Route("getpreferredvendors")]
        [HttpPost]
        [ResponseType(typeof(APIPreferredVendorResponse))]
        public IHttpActionResult SelectPreferredVendors(PreferredVendorRequest request)
        {
            IPreferredVendor user = new PreferredVendorAccess();
            APIPreferredVendorResponse serviceResponse = user.SelectPreferredVendors(request);

            return Ok<APIPreferredVendorResponse>(serviceResponse);
        }
        [Route("BuyPremimumMembership")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult BuyPremimumMembership(BuyPackageAPINewModel request)
        {
            IAuthCustomerProfile obj = new AuthCustomerProfileAccess();
            FPResponse serviceResponse = obj.AppUserBuyPackageNew(request);

            return Ok<FPResponse>(serviceResponse);
        }

        [Route("refervendor")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult ReferAVendorInsert(ReferVendorInsertRequest request)
        {
            IPreferredVendor user = new PreferredVendorAccess();
            FPResponse serviceResponse = user.ReferAVendorInsert(request);

            return Ok<FPResponse>(serviceResponse);
        }
        [Route("settingupdate")]
        [HttpPost]
        [ResponseType(typeof(NJResponse))]
        public IHttpActionResult SettingUpdate(SettingRequest request)
        {
            IAppUser user = new AppUserAccess();
            NJResponse serviceResponse = user.SettingUpdate(request);
            return Ok<NJResponse>(serviceResponse);
        }
        [Route("getdirectories")]
        [HttpPost]
        [ResponseType(typeof(DirectoryAPIResponse))]
        public IHttpActionResult getdirectories(DirectoryRequest request)
        {
            IVendor user = new VendorAccess();
            DirectoryAPIResponse serviceResponse = user.DirectorySelect(request);

            return Ok<DirectoryAPIResponse>(serviceResponse);
        }
        [Route("getdirectoriesbyname")]
        [HttpPost]
        [ResponseType(typeof(DirectoryAPIResponse))]
        public IHttpActionResult getdirectoriesbyname(DirectorySearchRequest request)
        {
            IVendor user = new VendorAccess();
            DirectoryAPIResponse serviceResponse = user.DirectorySelectByName(request);

            return Ok<DirectoryAPIResponse>(serviceResponse);
        }
        [Route("getoffersanddeals")]
        [HttpPost]
        [ResponseType(typeof(APIOfferAndDealResponse))]
        public IHttpActionResult SelectOfferAndDeals(PreferredVendorRequest request)
        {
            IPreferredVendor user = new PreferredVendorAccess();
            APIOfferAndDealResponse serviceResponse = user.SelectOfferAndDeals(request);

            return Ok<APIOfferAndDealResponse>(serviceResponse);
        }
        [Route("getofferanddealdetail")]
        [HttpPost]
        [ResponseType(typeof(APIOfferAndDealDetailResponse))]
        public IHttpActionResult SelectOfferAndDealDetail(APIDealDetailRequest request)
        {
            IPreferredVendor user = new PreferredVendorAccess();
            APIOfferAndDealDetailResponse serviceResponse = user.SelectOfferAndDealDetail(request);

            return Ok<APIOfferAndDealDetailResponse>(serviceResponse);
        }
        [Route("vendorsignup")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult VendorSignupInsert(VendorSignupInsertRequest request)
        {
            IPreferredVendor user = new PreferredVendorAccess();
            FPResponse serviceResponse = user.VendorSignupInsert(request);

            return Ok<FPResponse>(serviceResponse);
        }
        [Route("offerlikedislike")]
        [HttpPost]
        [ResponseType(typeof(FPResponse))]
        public IHttpActionResult Offerlikedislike(OfferLikeDisliketRequest request)
        {
            IPreferredVendor user = new PreferredVendorAccess();
            FPResponse serviceResponse = user.Offerlikedislike(request);

            return Ok<FPResponse>(serviceResponse);
        }
    }

}
