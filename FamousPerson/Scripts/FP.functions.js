﻿function GetStates(CountryId) {
    try {

        $('.state').empty();

        var params = { CountryId: CountryId, IsAll: false }
        $.ajax({
            type: "POST",
            url: "/utility/getstates",
            data: JSON.stringify(params),
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $.each(data, function () {
                    $('.state').append("<option value=" + this.Value + ">" + this.Text + "</option>");
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('4300: ' + errorThrown);
                //  alert(jqXHR.responseText + ' Error:' + errorThrown);
            }
        });
    }
    catch (ex) {
        console.log('4301: ' + ex.message);
    }
}

function GetDonationStates(CountryId) {
    try {

        $('.Donationstate').empty();

        var params = { CountryId: CountryId, IsAll: false }
        $.ajax({
            type: "POST",
            url: "/utility/getstates",
            data: JSON.stringify(params),
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $.each(data, function () {
                    $('.Donationstate').append("<option value=" + this.Value + ">" + this.Text + "</option>");
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('4300: ' + errorThrown);
                //  alert(jqXHR.responseText + ' Error:' + errorThrown);
            }
        });
    }
    catch (ex) {
        console.log('4301: ' + ex.message);
    }
}

function GetEventStates(CountryId) {
    try {

        $('.stateto').empty();

        var params = { CountryId: CountryId, IsAll: false }
        $.ajax({
            type: "POST",
            url: "/utility/getstates",
            data: JSON.stringify(params),
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $.each(data, function () {
                    $('.stateto').append("<option value=" + this.Value + ">" + this.Text + "</option>");
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('4300: ' + errorThrown);
                //  alert(jqXHR.responseText + ' Error:' + errorThrown);
            }
        });
    }
    catch (ex) {
        console.log('4301: ' + ex.message);
    }
}


function GetCities(city, stateId) {
    //
    var isAll = true;
    if ($('#hfIsAll').length)
        isAll = $('#hfIsAll').val();
    //if (isAll)
    $(city).find('option').remove();
    //  else
    //     $(city).find('option').not(':first').remove();

    $("#hdn_stateId").val(stateId);
    $.ajax({
        type: "post",
        url: "/city/getcities",
        data: { StateId: stateId, IsAll: isAll },
        datatype: "json",
        traditional: true,
        success: function (data) {
            $.each(data, function (index, value) {
                $(city).append('<option value="' + value.Value + '">' + value.Text + '</option>');
            });
        }
    });
}
function GetEventCities(Eventcity, EventstateId) {
    //
    var isAll = true;
    if ($('#hfIsAll').length)
        isAll = $('#hfIsAll').val();
    //if (isAll)
    $(Eventcity).find('option').remove();
    //  else
    //     $(city).find('option').not(':first').remove();

    $("#hdn_stateId").val(EventstateId);
    $.ajax({
        type: "post",
        url: "/city/geteventcities",
        data: { EventStateId: EventstateId, IsAll: isAll },
        datatype: "json",
        traditional: true,
        success: function (data) {
            $.each(data, function (index, value) {
                $(Eventcity).append('<option value="' + value.Value + '">' + value.Text + '</option>');
            });
        }
    });
}

$(document).on('change', '.country', function () {
    GetStates($('.country').val());
});

$(document).on('change', '.states', function () {
    GetCities('.cities', $('.states').val());
});
$(document).on('change', '.countryto', function () {
    GetEventStates($('.countryto').val());
});

$(document).on('change', '.statesto', function () {
    GetEventCities('.cities', $('.statesto').val());
});