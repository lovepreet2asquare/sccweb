var date = new Date();
var today = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes());

var BootstrapDatetimepicker = {
    init: function () {
        $("#m_datetimepicker_1").datetimepicker({ todayHighlight: !0, autoclose: !0, format: "yyyy.mm.dd hh:ii" }),
            $("#m_datetimepicker_1_modal").datetimepicker({ todayHighlight: !0, autoclose: !0, format: "yyyy.mm.dd hh:ii" }),
            $("#m_datetimepicker_2, #m_datetimepicker_1_validate, #m_datetimepicker_2_validate, #m_datetimepicker_3_validate").datetimepicker({ useCurrent: true, todayHighlight: !0, autoclose: !0, pickerPosition: "bottom-left", format: "mm/dd/yyyy hh:ii" }),
            $("#m_datetimepicker_2_modal").datetimepicker({ useCurrent: true, todayHighlight: !0, autoclose: !0, pickerPosition: "bottom-left", format: "mm/dd/yyyy hh:ii" }),
            $("#m_datetimepicker_3").datetimepicker({ useCurrent: true, todayHighlight: !0, autoclose: !0, pickerPosition: "bottom-left", todayBtn: !0, format: "mm/dd/yyyy hh:ii" }), $("#m_datetimepicker_3_modal").datetimepicker({ todayHighlight: !0, autoclose: !0, pickerPosition: "bottom-left", todayBtn: !0, format: "mm/dd/yyyy hh:ii" }), $("#m_datetimepicker_4_1").datetimepicker({ todayHighlight: !0, autoclose: !0, pickerPosition: "bottom-left", format: "yyyy.mm.dd hh:ii" }), $("#m_datetimepicker_4_2").datetimepicker({ todayHighlight: !0, autoclose: !0, pickerPosition: "bottom-right", format: "mm/dd/yyyy hh:ii" }), $("#m_datetimepicker_4_3").datetimepicker({ todayHighlight: !0, autoclose: !0, pickerPosition: "top-left", format: "yyyy-mm-dd hh:ii" }), $("#m_datetimepicker_4_4").datetimepicker({ todayHighlight: !0, autoclose: !0, pickerPosition: "top-right", format: "yyyy-mm-dd hh:ii" }), $("#m_datetimepicker_5").datetimepicker({ format: "dd MM yyyy - HH:ii P", showMeridian: !0, todayHighlight: !0, autoclose: !0, pickerPosition: "bottom-left" }), $("#m_datetimepicker_6").datetimepicker({ format: "mm/dd/yyyy", todayHighlight: !0, autoclose: !0, startView: 2, minView: 2, forceParse: 0, pickerPosition: "bottom-left" }), $("#m_datetimepicker_7").datetimepicker({ format: "hh:ii", showMeridian: !0, todayHighlight: !0, autoclose: !0, startView: 1, minView: 0, maxView: 1, forceParse: 0, pickerPosition: "bottom-left" }),

            $(".datepicker_nopast").datetimepicker({
                useCurrent: true,
                todayHighlight: !0,
                autoclose: !0,
                pickerPosition: "bottom-left",
                format: "mm/dd/yyyy hh:ii",
                startDate: today,
                endDate: 0,
                autoclose: true
            }),
            $(".myCalendar").datepicker({
                // useCurrent: true,
                todayHighlight: !0,
                autoclose: !0,
                pickerPosition: "bottom-left",
                format: "mm/dd/yyyy",
                //startDate: today,               
                autoclose: true
            }),
            $(".dateFrom").datepicker({
                todayHighlight: !0,
                autoclose: !0,
                pickerPosition: "bottom-left",
                format: "mm/dd/yyyy",
                minDate: 'dateToday',
                //autoclose: true,
                onSelect: function (date) {                   
                }
            }).on('hide', function (e) {
                
                if (new Date($(".dateFrom").val()) > new Date($(".dateTo").val())) {
                    $(".dateFrom").val(dateFormat($(".dateTo").val()));
                }
            }),
            $('.dateTo').datepicker({
                todayHighlight: !0,
                autoclose: !0,
                pickerPosition: "bottom-left",
                format: "mm/dd/yyyy",
                //startDate: today,               
                // autoclose: true,
                minDate: 1,
                onClose: function () {

                }
        }).on('hide', function (e) {               
                if (new Date($(".dateTo").val()) < new Date($(".dateFrom").val())) {
                    $(".dateTo").val(dateFormat($(".dateFrom").val()));
                }
            });
    }
};

jQuery(document).ready(function () {

    BootstrapDatetimepicker.init()

    $(".datepicker_nopast").blur(function () {
        if (new Date($(this).val()) <= new Date(today)) {//compare end <=, not >=
            $(this).val('');
            //AlertWithTitleIcon('Alert', 'Schedule date can�t be a past date', 'Alert');
        }
    });

});

function dateFormat(date)
{   
    date = new Date(date);
    var _date = (date.getMonth() + 1 < 10 ? '0' + (parseInt(date.getMonth()) + 1) : parseInt(date.getMonth())+1) + '/' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + '/' + date.getFullYear();
    return _date;
    
}

