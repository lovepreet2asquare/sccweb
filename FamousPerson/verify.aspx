﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="verify.aspx.cs" Inherits="FamousPerson.verify" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <%--   <title>Welcome to Saavor</title>
    <link href="https://demosaavorapi.saavor.io/css/bootstrap.min.css" rel="stylesheet"/>
<link href="style.css" rel="stylesheet"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />

       
    
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&amp;subset=latin,greek,greek-ext,vietnamese,cyrillic-ext,latin-ext,cyrillic" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800" rel="stylesheet" type="text/css"/>
 --%>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta content="telephone=no" name="format-detection" />
    <link rel="icon" href="favicon.ico" type="image/x-icon">


	<!-- Facebook sharing information tags -->
	<title>Email Template</title>
	
<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet" /> 
<style>

.email_ver_sec {
	text-align: center;
}
.email_ver_sec p {
	width: 50%;
	margin: 0 auto;
	padding-top: 20px;
	line-height: 23px;
}
@media (max-width: 1366px) {
.email_ver_sec p {
	width: 60%;
}
}
@media (max-width: 1024px) {
.email_ver_sec p {
	width:90%;
	margin: 0 auto;
}
}
@media (max-width: 767px) {
.email_ver_sec p {
	width:100%;
	margin: 0 auto;
}
}
</style>


</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnlMsg" runat="server" Visible="false">
 
            <div class="email_ver_sec">
	<img src="/content/images/josh-logo.png" />
	<p>This link is no longer available. Please request a new link.</p>
	</div>

                    <%--<div class="download-app-container thankyou">
	
                    <div id="noresult" runat="server" style="text-align:center;color:red">
                         This link is no longer available. Please request a new link.

                     </div>
                    </div>--%>
                    </asp:Panel>
        <asp:Panel ID="pnlSuccess" runat="server" Visible="false">
 <div class="email_ver_sec">
	<img src="images/logo.png" />
	<p>Congratulations! Verification completed successfully.
</p>
	</div>
	
      <%--<div class="download-app-container thankyou">
	<div class="parkeee-logo-container">
     

    <a href="#">  <img src="images/Logo_Saavor_App.png" alt="Saavor" class="logo-saavor-app" /></a>
    </div>
    <h1> Congratulations! Verification completed successfully.</h1>

</div>--%>
            </asp:Panel>
    </form>
</body>
</html>
