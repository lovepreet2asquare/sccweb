﻿using FPBAL.Business;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace FamousPerson
{
    /// <summary>
    /// Summary description for download
    /// </summary>
    public class download1 : IHttpHandler
    {
        DonationAccess donationAccess = new DonationAccess();
        String url = System.Configuration.ConfigurationManager.AppSettings["webUrl"];
        public void ProcessRequest(HttpContext context)
        {
            try
            {

                if (context != null)
                {
                    String _file = context.Request.QueryString["file"];
                    String _type = context.Request.QueryString["type"];
                    if (!String.IsNullOrEmpty(_file))
                    {
                        // certificate
                        if (_type != null && _type == "invoice")
                        {
                            _file = "invoices" + UtilityAccess.Decrypt(_file);
                        }
                        else
                            _file = donationAccess.DownloadFile(_file);

                        string filePath = _file;
                        if (!String.IsNullOrEmpty(_file))
                        {
                            String mailAtthUrl = String.IsNullOrEmpty(url) ? "~/" : url;
                            _file = mailAtthUrl + _file;
                            HttpWebResponse response = null;
                            var request = (HttpWebRequest)WebRequest.Create(_file);
                            request.Method = "HEAD";
                            try
                            {
                                response = (HttpWebResponse)request.GetResponse();
                            }
                            catch (Exception ex1)
                            {
                                String err = ex1.Message;
                                HttpContext.Current.Response.Redirect(url + "nofilefound.html");
                            }

                            if (response == null)
                            {
                                context.Response.Redirect("/download.ashx");
                            }
                            else
                            {
                                HttpContext.Current.Response.Clear();
                                HttpContext.Current.Response.ContentType = "application/octet-stream";
                                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(_file));
                                HttpContext.Current.Response.TransmitFile("~/" + filePath);
                                HttpContext.Current.Response.End();
                            }
                        }
                        else
                            HttpContext.Current.Response.Redirect(url + "nofilefound.html");
                    }
                    else
                        HttpContext.Current.Response.Redirect(url + "nofilefound.html");
                }
            }
            catch (Exception ex)
            {
                String _err = ex.Message;

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}