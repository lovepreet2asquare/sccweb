﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="reset.aspx.cs" Inherits="FamousPerson.reset" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="favicon.png" type="image/x-icon">


<title>Reset Password</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style>
#reset-password-container {
	max-width: 314px;
	margin: 150px auto 0;
	box-shadow: 0 0 10px #ddd;
	padding: 30px;
	border-radius: 5px;
	text-align: center;
}
#reset-password-container2 {
	max-width: 314px;
	margin: 150px auto 0;
	box-shadow: 0 0 10px #ddd;
	padding: 30px;
	border-radius: 5px;
	text-align: center;
}
.logo_sec {
	width: 314px;
	margin: 0 auto;
	top: 119px;
	position: relative;
	text-align: center;
}
#reset-password-container ::-moz-placeholder {
	color: #fff !important;
}
#reset-password-container2 ::-moz-placeholder {
	color: #fff !important;
}
#reset-password-container h2 {
	font-size: 24px;
	color: #a5a5a5;
	margin: 0 0 20px;
}
#reset-password-container2 h2 {
	font-size: 24px;
	color: #a5a5a5;
	margin: 0 0 20px;
}
#reset-password-container h6 {
	font-size: 12px;
	color: #717171;
	margin: 0;
	font-weight: normal;
}
.reset-password-form {
	padding-top: 30px;
	text-align: left;
}
.reset-password-form .form-group {
	margin-bottom: 24px;
}
.form-control::-moz-placeholder {
    color: #afafaf !important;
    opacity: 1 !important;
}
.reset-password-form .form-control {
	border-radius: 5px;
	background: #ccc;
	height: 38px;
	color: #fff;
	padding: 5px 10px;
	width: 100%;
	border: none;
}
.reset-password-form .help-block {
	font-size: 11px;
	color: #717171;
	margin-bottom: 30px;
}
.reset-password-form .btn-success {
	background: #383434;
	border: 0;
}
.reset-password-form .form-group:last-child {
	margin-bottom: 0;
}

@media (max-width: 767px){
.logo_sec {
 top: 40px;
 margin-bottom: 30px;
}

#reset-password-container {
 max-width: 100%;
 margin: 70px 20px;
}

#reset-password-container2 {
 max-width: 100%;
 margin: 70px 20px;

}
}
</style>

    
<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
<link rel="icon" href="favicon.png" type="image/x-icon" />
      <script language="javascript" type="text/javascript">

         function  NewPasswordChange(obj)
         {
             var s1 = document.getElementById('newspan1');

             if (obj.value == "" || obj.value != "") {
                 if (obj.value == "") {
                     obj.style.width = '100%';
                     obj.style.border = '1px solid red';
                     
                     s1.style.visibility = "visible";
                     s1.innerHTML = "New password cannot be empty!";
                 }

                 if (obj.value != "") {

                     var expresion = obj.value;
                     if (expresion.length >= 8 && expresion.length < 17) {
                        // var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
                        // //var decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
                        //// alert(decimal);
                        // // alert(regularExpression.test(obj.value));

                         obj.style.width = '100%';

                         s1.innerHTML = "";

                         //if (regularExpression.test(obj.value)) {
                         //    obj.style.width = '100%';

                         //    s1.innerHTML = "";
                         //}
                         //else
                         //{
                         //    obj.style.width = '100%';
                         //    obj.style.border = '1px solid red';

                         //    s1.style.visibility = "visible";

                         //    s1.innerHTML = "Should contain at least one lowercase, one uppercase, one numeric, and one special character!";
                         //    //8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character
                         //    //s1.innerHTML = "Password should be at least 8 characters!";

                         //    // returnVal = false;
                         //    //return false;
                         //};
                        //// returnVal = true;

                     }
                     else {
                         obj.style.width = '100%';
                         obj.style.border = '1px solid red';

                         s1.style.visibility = "visible";
                         
                         //s1.innerHTML = "Minimum length 8 characters!";
                         s1.innerHTML = "Password must be 8-16 characters long";
                         //s1.innerHTML = "Password should be at least 8 characters!";

                        // returnVal = false;
                         //return false;
                     };
                 }



             }
         }

         function ConfirmPasswordChange(obj) {
             var s2 = document.getElementById('confirmspan1');
              var obj1 = document.getElementById("<%=txtNewPassword.ClientID%>");
             if (obj.value == "" || obj.value != "") {
                 if (obj.value == "") {
                    
                     obj.style.width = '100%';
                     obj.style.border = '1px solid red'

                     s2.style.visibility = "visible";
                     s2.innerHTML = "Confirm password cannot be empty!";

                     
                 }
                 if (obj.value != "") {
                     if (obj.value == obj1.value) {
                         
                         obj.style.width = '100%';
                         s2.innerHTML = "";
                         
                     }
                     else {
                        
                         obj.style.width = '100%';
                         obj.style.border = '1px solid red';

                         s2.style.visibility = "visible";
                         
                         s2.innerHTML = "Password does not match!";
                     }
                 }


             }
         }
         function validatereset() {

             var obj1 = document.getElementById("<%=txtNewPassword.ClientID%>");
             var obj2 = document.getElementById("<%=txtConfirmPassword.ClientID%>");
             
             var s1 = document.getElementById('newspan1');
             var s2 = document.getElementById('confirmspan1');
             s1.innerHTML = "";
             s2.innerHTML = "";
             //s1.style.visibility = "hidden";
             //s2.style.visibility = "hidden";



             var returnVal;
             // Validate new password
             if (obj1.value == "" || obj1.value != "") {
                 if (obj1.value == "") {
                     obj1.style.width = '100%';
                     obj1.style.border = '1px solid red';
                     
                     s1.style.visibility = "visible";
                     s1.innerHTML = "Password cannot be empty!";

                     returnVal = false;
                 }
                 if (obj1.value != "") {
                     var expresion = obj1.value;
                     if (expresion.length >= 8) {
                         obj1.style.width = '100%';

                         returnVal = true;
                     }
                     else {
                         obj1.style.width = '100%';
                         obj1.style.border = '1px solid red';

                         s1.style.visibility = "visible";
                         s1.innerHTML = "Minimum length 8 characters!";
                         //s1.innerHTML = "Password should be at least 8 characters!";

                         returnVal = false;
                         return false;
                     };
                 }
             }
             // Validate confirm password
             if (obj2.value == "" || obj2.value != "") {
                 if (obj2.value == "") {
                     obj2.style.width = '100%';
                     obj2.style.border = '1px solid red'

                     s2.style.visibility = "visible";
                     s2.innerHTML = "Confirm password cannot be empty!";

                     returnVal = false;
                 }
                 if (obj2.value != "") {
                     if (obj2.value == obj1.value) {
                         obj2.style.width = '100%';
                         returnVal = true;
                     }
                     else {
                         obj2.style.width = '100%';
                         obj2.style.border = '1px solid red';

                         s2.style.visibility = "visible";
                         s2.innerHTML = "Password does not match!";

                         returnVal = false;
                     }
                 }
             }
             return returnVal;
         }


         function validatepassword(digit) {

             if (digit.length < 8) {

                 return false;
             }
             return true;

         } 
         </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlcontainer" runat="server" >
    <div class="logo_sec">
		<img src="/content/images/josh-logo.png"/>
	</div>

    <div id="reset-password-container">

    <asp:Panel runat="server" ID="pnlForm">
   	<h2>Reset Password</h2>
      <h6>Your user ID is &nbsp;<b id="lblEmail" runat="server"></b></h6>
    <div class="reset-password-form">
    	<div class="form-group">
            
        <div id="newpass" ><span class="alert-danger" style="color:#a94442 !important;background-color:#ffffff !important ;border-color:#ffffff !important" id="newspan1"></span></div>     
             <asp:TextBox TextMode="Password" ID="txtNewPassword" MaxLength="20" onchange="NewPasswordChange(this)"  class="form-control input-lg" placeholder="New Password" runat="server"></asp:TextBox>

            <span class="help-block" title="Minimum length 8 characters and should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character!">At least 8 characters</span>
		</div>
        <div class="form-group">
        <div id="confirmpass" ><span class="alert-danger" style="color:#a94442 !important;background-color:#ffffff !important ;border-color:#ffffff !important" id="confirmspan1"></span></div>
                   <asp:TextBox TextMode="Password" ID="txtConfirmPassword" MaxLength="20" runat="server" onchange="ConfirmPasswordChange(this)"
                    class="form-control input-lg" placeholder="Confirm New Password"></asp:TextBox>
           
		</div>
        <div class="form-group">

              <asp:Button class="btn btn-success btn-block btn-lg" ID="btnSubmit" OnClientClick ="return validatereset()" 
                    runat="server" Text="Reset Password" OnClick="btnSubmit_Click" /> 
            <asp:Label style="float:none;" ID="lblMsg" runat="server" CssClass="error">Oops! Something went wrong. Please try again later.</asp:Label>
           
		</div>
    </div>

    </asp:Panel>
                    <asp:Panel ID="pnlMsg" runat="server" Visible="false">
                    <div class="download-app-container thankyou">
                    <div id="noresult" runat="server" style="text-align:center;color:red">
                         This link is no longer available. Please request a new link.
                    </div>
                    </div>
                    </asp:Panel>
    </div>
        </asp:Panel>
   

 <asp:Panel ID="pnlSuccess" runat="server" Visible="false">
 <div class="logo_sec">
		<img src="images/logo_home.png"/>
	</div>
    <div id="reset-password-container2">

        
    <h2> Congratulations! Your password has been reset successfully.</h2>

        </div>
    </asp:Panel>

    </form>
</body>
</html>


