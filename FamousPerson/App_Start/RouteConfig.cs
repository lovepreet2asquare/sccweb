﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FamousPerson
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();



            routes.MapRoute(
        name: "PrivacyPolicy",
        url: "PrivacyPolicy/{action}/{id}",
        defaults: new
        {
            controller = "Condition",
            action = "PrivacyPolicy",
            id = UrlParameter.Optional
        });           


            //    routes.MapRoute(
            //name: "TermsAndCondition",
            //url: "TermsAndCondition/{action}/{id}",
            //defaults: new
            //{
            //    controller = "Condition",
            //    action = "TermsAndCondition",
            //    id = UrlParameter.Optional
            //});
            routes.MapRoute(
        name: "DonationStatement",
        url: "DonationStatement/{action}/{id}",
        defaults: new
        {
            controller = "Condition",
            action = "DonationStatement",
            id = UrlParameter.Optional
        });
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "account", action = "login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
           name: "donate",                                              // Route name
            url: "{controller}/{action}/{id}",                           // URL with parameters
            new { controller = "donate", action = "donations", id = UrlParameter.Optional }  // Parameter defaults
        );


        }
    }
}
