﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
//using FPDAL.Data;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class ChangePasswordController : BaseController
    {
        // GET: ChangePassword
        IChangePassword access = new ChangePasswordAccess();
        public ActionResult ChangePassword()
        {
            ViewBag.Message = "";
            ChangePasswordModel model = new ChangePasswordModel();
            //try
            //{
                if (Request["ur"] != null && Request["tm"] != null)
                {
                    if (String.IsNullOrEmpty(Request["ur"]) == false && String.IsNullOrEmpty(Request["tm"]) == false)
                    {
                        double _time = Convert.ToDouble(UtilityAccess.Decrypt(Request["tm"]));
                        DateTime _date = UtilityAccess.UnixTimeStampToDateTime(_time);
                        DateTime _now = DateTime.Now;
                        if ((_now - _date).TotalMinutes < 60)
                        {
                            model.UserId = (UtilityAccess.Decrypt(Request["ur"]));
                        }
                        else
                        {
                            ViewBag.Message = "Password reset link has been expired.";
                        }
                    }
                }
                //else
                //    return Redirect(UserAccess.IsAuthorize());

                return View(model);
            //}
            //catch (Exception ex)
            //{
            //    ApplicationLogger.LogError(ex, "AccountController", "ResetPassword");
            //    return View(model);
            //}


        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            ViewBag.Message = "";
            //try
            //{
            //    //model.UserId = Function.ReadCookie("EncryptedUserId");
                //model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
               
                if (ModelState.IsValid)
                {
                    
                    ChangePasswordResponse data = access.ChangePassword(model);
                   int ISEmailSend= ChangePasswordAccess.SendForChangepassword(data.ChangePasswordModel);
                    if (data!=null)
                    {
                        
                        TempData["ReturnCode"] = data.ReturnCode;
                    TempData["ReturnMessage"] = data.ReturnMessage;
                    TempData["href"] = "/dashboard/index";
                    }

                }

                return View(model);
            //}
            //catch (Exception ex)
            //{
            //    ApplicationLogger.LogError(ex, "AccountController", "ResetPassword");
            //    return View(model);
            //}
        }
        [HttpPost]
        public ActionResult ChangePassword(UserModel model)
        {
            ViewBag.Message = "";
            //try
            //{
            //    //model.UserId = Function.ReadCookie("EncryptedUserId");
            //model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            if (ModelState.IsValid)
            {

                UserResponse data = access.ChangePassword(model);
                int ISEmailSend = ChangePasswordAccess.SendPasswordForChangepassword(data.UserModel);
                if (data != null)
                {

                    TempData["ReturnCode"] = data.ReturnCode;
                    TempData["ReturnMessage"] = data.ReturnMessage;
                    TempData["href"] = "/dashboard/index";
                }

            }

            return View(model);
            //}
            //catch (Exception ex)
            //{
            //    ApplicationLogger.LogError(ex, "AccountController", "ResetPassword");
            //    return View(model);
            //}
        }
        [HttpPost]
        public ActionResult ReadUnRead(string f)
        {
            string UserId = Function.ReadCookie("EncryptedUserId");
            return Json(access.ReadUnRead(UserId), JsonRequestBehavior.AllowGet);
        }
    }
}