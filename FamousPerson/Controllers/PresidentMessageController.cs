﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;

namespace FamousPerson.Controllers
{
    public class PresidentMessageController : BaseController
    {
        IPresidentMessageWeb presidentMessageAccess = new PresidentMessageWebAccess();
        // GET: PresidentMessage
        public ActionResult Index()
        {
            PresidentMessageWebModel model = new PresidentMessageWebModel();
            PresidentMessageResponse response = new PresidentMessageResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            PresidentMessageResponse result = presidentMessageAccess.Select(model);
            if (result.PresidentMessageModel != null)
            {
                //TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.PresidentMessageModel;
                TempData["ImagePath"] = result.PresidentMessageModel.ImagePath;
                return View(result.PresidentMessageModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult Index(PresidentMessageWebModel model)
        {

            PresidentMessageResponse response = new PresidentMessageResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            PresidentMessageResponse result = presidentMessageAccess.Select(model);
            if (result.PresidentMessageModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;

                return View(result.PresidentMessageModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        public ActionResult CreateOrEdit()
        {
            PresidentMessageWebModel model = new PresidentMessageWebModel();

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            var Result = TempData["ImagePath"];
            model.ImagePath = Convert.ToString(Result);
            PresidentMessageResponse result = presidentMessageAccess.Select(model);

            //InAppMessageResponse result = appMessageAccess.AddOrEdit(model);
            if (result != null)
            {
                //result.PresidentMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                //result.PresidentMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                return View(result.PresidentMessageModel);
            }
            return View();
        }
        [HttpPost]
        public ActionResult CreateOrEdit(PresidentMessageWebModel model)
        {

            PresidentMessageResponse result = new PresidentMessageResponse();

            model.PresidentMessageId = model.PresidentMessageId;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            String UserId = UtilityAccess.Decrypt(model.EncryptedUserId);


            if (!String.IsNullOrEmpty(model.ImageBase64))
                model.ImagePath = Function.UploadCropImage(model.ImageBase64, "President");

            result = presidentMessageAccess.AddOrEdit(model);

            if (result.PresidentMessageModel != null)
            {
                TempData["AboutUsModel"] = result.PresidentMessageModel;
                return RedirectToAction("index", "presidentmessage");
            }
            else
                return RedirectToAction("error", "home");
        }

        public ActionResult Delete(String aid)
        {
            PresidentMessageWebModel model = new PresidentMessageWebModel();
            model.PresidentMessageId = Convert.ToInt32(aid);
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            PresidentMessageResponse result = presidentMessageAccess.Delete(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                //TempData["href"] = "/about/index";
            }

            return RedirectToAction("index", "presidentmessage", new { aid = aid });
        }

    }
}