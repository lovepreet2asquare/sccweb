﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FamousPerson.Models;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;

namespace FamousPerson.Controllers
{
    public class ServiceController : BaseController
    {
        IService serviceAccess = new ServiceAccess();
        // GET: Service
        public ActionResult Index(string bid)
        {
            ServiceModel serviceModel = new ServiceModel();

            serviceModel.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            serviceModel.EncryptedBusinessId = bid;
            ServiceResponse result = serviceAccess.SelectAll(serviceModel);
            ViewBag.Header = "Service";
            return View(result._ServiceModel);
        }
        [HttpPost]
        public ActionResult Index(ServiceModel model)
        {
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            ServiceResponse result = serviceAccess.SelectAll(model);

            ViewBag.Header = "Service";
            return View(result._ServiceModel);
        }

        public ActionResult ViewServiceType(string bid)
        {
            ServiceModel serviceModel = new ServiceModel();

            serviceModel.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            serviceModel.EncryptedBusinessId = bid;
            //serviceModel.EncryptedServiceId = sid;
            ServiceResponse result = serviceAccess.ServiceTypeSelectAll(serviceModel);
            ViewBag.Header = "Service";
            return View(result._ServiceModel);
        }
        [HttpPost]
        public ActionResult ViewServiceType(ServiceModel model)
        {
            //model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            //  ServiceResponse result = serviceAccess.ServiceTypeSelectAll(model);
            if(model.ServiceType=="1")
            {
                model.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(model.ServiceId));
                model.ServiceType = UtilityAccess.Decrypt(Convert.ToString(model.ServiceType));
                ViewBag.Header = "Service";
                return RedirectToAction("EndorsmentCreate", "Service", new { bid = model.EncryptedBusinessId, sid = model.EncryptedServiceId });
            }
            if (model.ServiceType == "2")
            {
                model.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(model.ServiceId));
                model.ServiceType = UtilityAccess.Encrypt(Convert.ToString(model.ServiceType));
                ViewBag.Header = "Service";
                return RedirectToAction("Create", "Service", new { bid = model.EncryptedBusinessId, sid = model.EncryptedServiceId ,stid= model.ServiceType });

            }
            if(model.ServiceType == "3")
            {
                model.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(model.ServiceId));
                model.ServiceType = "3";
                ViewBag.Header = "Service";
                return RedirectToAction("VendorCreate", "Service", new { bid = model.EncryptedBusinessId, sid = model.ServiceType });
            }
            return View();
        }



        //GET: Service/Create
        [HttpGet]
        public ActionResult Create(string bid,string sid,string stid)
        {
            ServiceModel model = new ServiceModel();
            ServiceResponse request = new ServiceResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(model.ServiceId));

            model.ServiceType = UtilityAccess.Decrypt(Convert.ToString(stid));
            model.EncryptedBusinessId = bid;
            model.EncryptedServiceId = sid;
            ServiceResponse result = serviceAccess.SelectById(model);
            ViewBag.PageHeader = "Service Info";
            if (result._ServiceModel != null)
            {
                if (string.IsNullOrEmpty(result._ServiceModel.ServiceType))
                    result._ServiceModel.ServiceType= model.ServiceType;   
                
                if (string.IsNullOrEmpty(result._ServiceModel.EncryptedBusinessId))
                    result._ServiceModel.EncryptedBusinessId = model.EncryptedBusinessId;

                return View(result._ServiceModel);
            }
            else
                return RedirectToAction("error", "home");

        }

        //GET: Service/Edit
        [HttpGet]
        public ActionResult Edit(string sid)
        {
            ServiceModel model = new ServiceModel();
            ServiceResponse request = new ServiceResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
          
            model.EncryptedServiceId = sid;
            ServiceResponse result = serviceAccess.SelectById(model);
            ViewBag.PageHeader = "Service Info";
            if (result._ServiceModel != null)
            {

                return View(result._ServiceModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult CreateOrEdit(ServiceModel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            
            //model.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(model.ServiceId));
            model.ServiceType = Convert.ToString(model.ServiceType);
            ServiceResponse result = null;
            result = serviceAccess.AddorEdit(model);
            if (result._ServiceModel != null)
            {
                result._ServiceModel.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(result._ServiceModel.ServiceId));
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
               // return RedirectToAction("index", "Service");
                return RedirectToAction("edit", "Service", new { @sid = result._ServiceModel.EncryptedServiceId });
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }

        [HttpPost]
        public ActionResult VendorCreateOrEdit(ServiceModel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(model.ServiceId));
            //model.ServiceType = UtilityAccess.Decrypt(Convert.ToString(model.ServiceType));
            ServiceResponse result = null;
            result = serviceAccess.AddorEdit(model);
            if (result._ServiceModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                return RedirectToAction("index", "Service");
            }
            else
            {
                return RedirectToAction("error", "home");
            }
        }

        //GET: PreferredVendor/Create
        [HttpGet]
        public ActionResult VendorCreate(string bid, string sid)
        {
            ServiceModel model = new ServiceModel();
            ServiceResponse request = new ServiceResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(model.ServiceId));
            //model.ServiceType = UtilityAccess.Decrypt(Convert.ToString(model.ServiceType));
            model.EncryptedBusinessId = bid;
            model.ServiceType = sid;
            ServiceResponse result = serviceAccess.SelectById(model);
            ViewBag.PageHeader = "Service Info";
            if (result._ServiceModel != null)
            {
                result._ServiceModel.EncryptedBusinessId = bid;
                result._ServiceModel.ServiceType =sid;


                return View(result._ServiceModel);
            }
            else
                return RedirectToAction("error", "home");

        }

        //GET: PreferredVendor/Edit
        [HttpGet]
        public ActionResult VendorEdit(string sid)
        {
            ServiceModel model = new ServiceModel();
            ServiceResponse request = new ServiceResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(model.ServiceId));
            //model.ServiceType = UtilityAccess.Decrypt(Convert.ToString(model.ServiceType));
            model.EncryptedServiceId = sid;
            ServiceResponse result = serviceAccess.SelectById(model);
            ViewBag.PageHeader = "Service Info";
            if (result._ServiceModel != null)
            {

                return View(result._ServiceModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult EndorsmentCreateOrEdit(ServiceModel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(model.ServiceId));
            model.ServiceType = UtilityAccess.Decrypt(Convert.ToString(model.ServiceType));
            ServiceResponse result = null;
            result = serviceAccess.AddorEdit(model);
            if (result._ServiceModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                return RedirectToAction("index", "Service");
            }
            else
            {
                return RedirectToAction("error", "home");
            }
        }

        //GET: EndorsmentVendor/Create
        [HttpGet]
        public ActionResult EndorsmentCreate(string bid, string sid)
        {
            ServiceModel model = new ServiceModel();
            ServiceResponse request = new ServiceResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(model.ServiceId));
            model.ServiceType = UtilityAccess.Decrypt(Convert.ToString(model.ServiceType));
            model.EncryptedBusinessId = bid;
            model.EncryptedServiceId = sid;
            ServiceResponse result = serviceAccess.SelectById(model);
            ViewBag.PageHeader = "Service Info";
            if (result._ServiceModel != null)
            {

                return View(result._ServiceModel);
            }
            else
                return RedirectToAction("error", "home");

        }

        //GET: EndorsmentVendor/Edit
        [HttpGet]
        public ActionResult EndorsmentEdit(string bid)
        {
            ServiceModel model = new ServiceModel();
            ServiceResponse request = new ServiceResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedServiceId = UtilityAccess.Encrypt(Convert.ToString(model.ServiceId));
            model.ServiceType = UtilityAccess.Decrypt(Convert.ToString(model.ServiceType));
            model.EncryptedBusinessId = bid;
            ServiceResponse result = serviceAccess.SelectById(model);
            ViewBag.PageHeader = "Service Info";
            if (result._ServiceModel != null)
            {

                return View(result._ServiceModel);
            }
            else
                return RedirectToAction("error", "home");
        }


        public ActionResult Delete(string sid)
        {
            ServiceModel model = new ServiceModel();
            model.EncryptedServiceId = sid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            ServiceResponse result = serviceAccess.Delete(model);
            //ViewBag.PageHeader = "Delete headline";
            //return View(result._HeadlineModel);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "Service");
        }

        public ActionResult DealandOfferDetail(string hid)
        {
            ServiceModel model = new ServiceModel();
            ServiceResponse request = new ServiceResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedServiceId = hid;
            ServiceResponse result = serviceAccess.SelectServiceDetailById(model);
            ViewBag.PageHeader = "Business Info";
            if (result._ServiceModel != null)
            {

                return View(result._ServiceModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        public ActionResult PreferVendorDetail(string hid)
        {
            ServiceModel model = new ServiceModel();
            ServiceResponse request = new ServiceResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedServiceId = hid;
            ServiceResponse result = serviceAccess.SelectServiceDetailById(model);
            ViewBag.PageHeader = "Business Info";
            if (result._ServiceModel != null)
            {

                return View(result._ServiceModel);
            }
            else
                return RedirectToAction("error", "home");
        }
    }
}