﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Error()
        {

            return View();
        }
        public ActionResult ComingSoon()
        {

            return View();
        }
        public ActionResult Agreement()
        {

            return View();
        }
        public ActionResult Download()
        {

            return RedirectToAction("download", "about");
        }
        public ActionResult FAQ()
        {

            return View();
        }
       
        public ActionResult Privacy()
        {
            return View();
        }
        
        public ActionResult TermsCondition()
        {
            return View();
        }
        public ActionResult PaymentMethod()
        {
            return View();
        }
       
    }
}