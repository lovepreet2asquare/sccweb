﻿using FamousPerson.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;
using System.IO;
namespace FamousPerson.Controllers
{
    public class NewsController : BaseController
    {
        INews newsAccess = new NewsAccess();
        // GET: News
        public ActionResult Index()
        {
            NewsModel model = new NewsModel();
            NewsResponse request = new NewsResponse();
            string fid = TempData["ReturnCode"] == null ? "1": TempData["ReturnCode"].ToString();
            model.IsView = fid;
            
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.Status = string.Empty;
            NewsResponse result = newsAccess.NewsSelectAll(model);          
            ViewBag.PageHeader = "News";
            return View(result._newsModel);
        }

        [HttpPost]
        public ActionResult Index(NewsModel model)
        {
            NewsResponse request = new NewsResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            NewsResponse result = newsAccess.NewsSelectAll(model);
            //result.UserModel.EncryptedUserId = model.EncryptedUserId;
            ViewBag.PageHeader = "News";
            return View(result._newsModel);
        }

        public ActionResult Detail(string nid)
        {
            NewsModel model = new NewsModel();
            model.EncryptedNewsId = nid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            //NewsResponse result = newsAccess.NewsSelectAll(model);
            model.NewsId = Convert.ToInt32(UtilityAccess.Decrypt(nid));

            NewsResponse result = newsAccess.NewsDetail(model);
            //ViewBag.PageHeader = "Basic Info";
            //return View(result.UserModel);
            if (result._newsModel != null)
            {
                result._newsModel.EncryptedNewsId = nid;
                TempData["NewsModel"] = result._newsModel;
                return View(result._newsModel);
            }
            else
                return RedirectToAction("error", "home");
            //return View();
        }
     
        private NewsResponse GetDetail(NewsModel model)
        {            
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            NewsResponse result = newsAccess.NewsDetail(model);
            if(result._newsModel!=null)
            {
                result._newsModel._ImagePathList = new List<ImageListModel>();
                for(int i=0;i<5;i++)
                {
                    result._newsModel._ImagePathList.Add(new ImageListModel { });
                }
            }
            return result;
        }

        public ActionResult Create(string nid)
        {
            NewsModel model = new NewsModel();
            model.EncryptedNewsId = nid;
            //   model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            //  model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            NewsResponse result = GetDetail(model); //newsAccess.NewsDetail(model);

            ViewBag.PageHeader = "Basic Info";
            if (result._newsModel != null)
            {

                TempData["NewsModel"] = result._newsModel;
                return View(result._newsModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult Create(NewsModel model, FormCollection form)
        {
            //NewsModel model = new NewsModel();
            if (model.PublishDate == "undefined")
            {
                model.PublishDate = null;
            }
            model.NewsId = model.NewsId;
            string Message = "Maximum file size is 2 MB";
            string MessageExt = "Only .jpeg,.jpg,.png format";

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            if (model._ImagePathList != null && model._ImagePathList.Count > 0)
            {
                for (int i = 0; i < model._ImagePathList.Count; i++)
                {
                    if (!String.IsNullOrEmpty(model._ImagePathList[i].ImageBase64))
                    {
                        model._ImagePathList[i].ImagePath = Function.UploadCropImage(model._ImagePathList[i].ImageBase64, "News");
                    }

                    //if (model._ImagePathList[i].HttpPostedFileBase != null && model._ImagePathList[i].HttpPostedFileBase.ContentLength > 0)
                    //{
                    //    model.Size = Function.UploadFile((model._ImagePathList[i].HttpPostedFileBase as HttpPostedFileBase), "News/");
                    //    if (model.Size != Message && model.Size != MessageExt)
                    //    {
                    //        model._ImagePathList[i].ImagePath = model.Size;
                    //    }
                    //    else
                    //    {

                    //        break;
                    //    }

                    //}

                    //if (model.Size == "Maximum file size is 2 MB")

                    //{
                    //    ViewBag.Message = "Maximum file size is 2 MB";

                    //}

                }
            }
            /*&& (model.Extension != MessageExt)*/
            if (model.Size != Message && model.Size != MessageExt)
            {

                NewsResponse result = newsAccess.AddorEdit(model);
                if (result._newsModel != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["NewsModel"] = result._newsModel;
                    return RedirectToAction("index", "news");
                }
            }
            return View(model);
        }


        public ActionResult Edit(string nid)
        {
            NewsModel model = new NewsModel();
            model.EncryptedNewsId = String.IsNullOrEmpty(nid)?nid:Server.UrlDecode(nid);
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            NewsResponse result = newsAccess.NewsDetail(model);

            ViewBag.PageHeader = "Basic Info";
            if (result._newsModel != null)
            {
                if (result._newsModel.Status != "Scheduled")
                {
                    result._newsModel.PublishDate = string.Empty;
                }
                TempData["NewsModel"] = result._newsModel;
                result._newsModel.PublishDate = result._newsModel.Status == "Scheduled" ? result._newsModel.PublishCalDate : "";

                return View(result._newsModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        //public ActionResult CreateorEdit(NewsModel model,FormCollection form)
        public ActionResult Edit(NewsModel model, FormCollection form)
        {
            //NewsModel model = new NewsModel();
            if (model.PublishDate == "undefined")
            {
                model.PublishDate = null;
            }
            string Message = "Maximum file size is 2 MB";
            string MessageExt = "Only .jpeg,.jpg,.png format";

            model.NewsId = model.NewsId;

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            if (model._ImagePathList != null && model._ImagePathList.Count > 0)
            {
                for (int i = 0; i < model._ImagePathList.Count; i++)
                {
                    if (!String.IsNullOrEmpty(model._ImagePathList[i].ImageBase64))
                    {
                        model._ImagePathList[i].ImagePath = Function.UploadCropImage(model._ImagePathList[i].ImageBase64, "News");
                    }

                    //if (model._ImagePathList[i].HttpPostedFileBase != null && model._ImagePathList[i].HttpPostedFileBase.ContentLength > 0)
                    //{
                    //    model.Size = Function.UploadFile((model._ImagePathList[i].HttpPostedFileBase as HttpPostedFileBase), "News/");
                    //    if (model.Size != Message && model.Size != MessageExt)
                    //    {
                    //        model._ImagePathList[i].ImagePath = model.Size;
                    //    }
                    //    else
                    //    {

                    //        break;
                    //    }

                    //}

                    //if (model.Size == "Maximum file size is 2 MB")

                    //{
                    //    ViewBag.Message = "Maximum file size is 2 MB";

                    //}

                }
            }
            if (model.Size != Message && model.Size != MessageExt)
            {
                NewsResponse result = newsAccess.AddorEdit(model);
                if (result._newsModel != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["NewsModel"] = result._newsModel;
                    return RedirectToAction("index", "news");
                }
            }
            return View(model);
        }


        public JsonResult SaveImages(NewsModel model, FormCollection data)
        {
            HttpPostedFileBase httpPostedFileBase = null;
            int count = default(int);
            if (Request.Files.Count > 0)
            {
                if (Request.Files.Count > 5)
                {
                    count = 0;
                }
                else
                {
                    model._ImagePathList = new List<ImageListModel>();
                    int i = 0;
                    foreach (var item in Request.Files)
                    {
                        //save profile pic
                        httpPostedFileBase = Request.Files[i] as HttpPostedFileBase;
                        if (httpPostedFileBase != null && httpPostedFileBase.ContentLength > 0)
                        {
                            string fileName = Path.GetFileName(httpPostedFileBase.FileName);
                            string ext = Path.GetExtension(httpPostedFileBase.FileName);
                            String filePath = "/Upload/News/";
                            model.ImagePath = filePath + fileName;
                            if (System.IO.File.Exists(Server.MapPath(model.ImagePath)))
                            {
                                System.IO.File.Delete(Server.MapPath(model.ImagePath));
                            }
                            model._ImagePathList.Add(new ImageListModel
                            {

                                ImagePath = model.ImagePath
                            });
                            httpPostedFileBase.SaveAs(Server.MapPath("~/" + filePath) + "/" + fileName);

                        }
                        i++;
                    }
                    count = 1;
                }
            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }

     

        public ActionResult Delete(string nid)
        {
            NewsModel model = new NewsModel();
            model.EncryptedNewsId= nid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            NewsResponse result = newsAccess.DeleteNews(model);
            //ViewBag.PageHeader = "Delete headline";
            //return View(result._HeadlineModel);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "news");
        }
        public ActionResult Archive(string nid)
        {
            NewsModel model = new NewsModel();
            model.EncryptedNewsId= nid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            NewsResponse result = newsAccess.Archive(model);
            ///ViewBag.PageHeader = "Archive headline";
            //return View(result._HeadlineModel);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                //TempData["href"] = "/certificate/index";
            }
            return RedirectToAction("index", "news");
            //return RedirectToAction("index", "headline", new { hid = id });
        }



        [HttpPost]
        public JsonResult DeleteNewsImage(int ImgId, string ImageSrc, string Idx)
        {
            string result = string.Empty;
            if (ImgId != 0)
            {
                NewsModel model = new NewsModel();
                model._ImagePathList = new List<ImageListModel>();
                model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(Function.ReadCookie("EncryptedUserId")));
                model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
                ImageListModel imgModel = new ImageListModel();
                imgModel.ImageId = ImgId;
                imgModel.ImagePath = ImageSrc;
                result = newsAccess.DeleteNewsImage(imgModel, model).ToString();
            }
            if (System.IO.File.Exists(Server.MapPath(ImageSrc)))
            {
                System.IO.File.Delete(Server.MapPath(ImageSrc));
            }

            var resultReturn = new { Result = result, idx = Idx };
            return Json(resultReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReadMore(string nid)
        {

            NewsModel model = new NewsModel();
            if(!String.IsNullOrEmpty(nid))
                model.NewsId = Convert.ToInt32(UtilityAccess.Decrypt(nid));
            NewsResponse response = new NewsResponse();

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            NewsResponse result = newsAccess.NewsDetail(model);
            if (result._newsModel != null)
            {
                result._newsModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                result._newsModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                return View(result._newsModel);
            }
            else
                return RedirectToAction("error", "home");



        }

        public ActionResult Updatenewnotification()
        {
            NewsModel model = new NewsModel();
            string EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            Int32 result = newsAccess.Updatenewnotification(EncryptedUserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }


}