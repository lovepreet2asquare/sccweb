﻿using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class AboutController : Controller
    {
        // GET: Condition
        public ActionResult PrivacyPolicy()
        {
            return View();
        }
        
        public ActionResult TermsAndCondition()
        {
            return View();
        }
        public ActionResult Eula()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult Support()
        {
            return View();
        } public ActionResult PurchaseLicense()
        {
            return View();
        }public ActionResult supportcenter()
        {
            return View();
        }
        public ActionResult Exclusive_NewsDetail()
        {
            return View();
        }

        public ActionResult Donationstatement()
        {
            return View();
        }
        public ActionResult Download()
        {
            return View();
        }
        public ActionResult Thankyou()
        {
            return View();
        }
        public ActionResult Donate(string sid, string fid, string cid, string Is )
        {
            TempData["sid"] = sid;
            TempData["fid"] = fid;
            TempData["cid"] = cid;

            if (Is == "login")
            {
                DonarDetailResponse response = new DonarDetailResponse();
                IAuthCustomerProfile access = new AuthCustomerProfileAccess();
           
                response = access.GetDonarDetail(fid, sid, cid);
                if (response != null)
                {
                    return View(response.DonarDetailModel);
                }
                else
                    return View();

            }
            else
            {
                return RedirectToAction("donation", "about");
            }
        }
        [HttpPost]
        public ActionResult Donate(DonateAPIModel request)
        {
            FPResponse Response = new FPResponse();
           
            DonarDetailResponse response = new DonarDetailResponse();
            IAuthCustomerProfile access = new AuthCustomerProfileAccess();

            Response = access.DonateNew(request);
            if (Response != null)
            {
               // TempData["TransactioID"] = response.ReturnMessage;
                return RedirectToAction("thankyou", "about");
            }
            else
                return View();
        }
        public ActionResult Donation(string sid, string fid, string cid)
        {
            DonationDetailResponse response = new DonationDetailResponse();
            IAuthCustomerProfile access = new AuthCustomerProfileAccess();
           

            if (TempData.ContainsKey("sid"))
                sid = TempData["sid"].ToString();

            if (TempData.ContainsKey("fid"))
                fid = TempData["fid"].ToString();

            if (TempData.ContainsKey("cid"))
                cid = TempData["cid"].ToString();

            response = access.GetDonationDetail(fid, sid,cid);
            if (response != null)
            {
                return View(response.DonationDetailModel);
            }
            else
                return View();
           }
        [HttpPost]
        public ActionResult Donation(CustProfileInfoNew request)
        {
            FPResponse Response = new FPResponse();
            request.CardType = "Visa";
            
            IAuthCustomerProfile access = new AuthCustomerProfileAccess();
            
            //request.SessionToken = "9LPTCCTU1UN8RHYETEFUPG0815RMKC";
            //request.FollowerId = "385";
            //request.FollowerId = UtilityAccess.Encrypt(request.FollowerId);
            //request.FollowerId = UtilityAccess.Encrypt(request.FollowerId);
            Response = access.DonationNew(request);
            if (Response != null)
            {
               // TempData["TransactioID"] = Response.ReturnMessage;
                return RedirectToAction("thankyou","about");
            }
            else
                return View();
            
        }
    }
}