﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class DirectoryController : BaseController
    {
        IDirectory directoryAccess = new DirectoryAccess();
        
         

        [HttpPost]
        public ActionResult CreateOrEdit(DirectoryModel model)
        {

            model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            DirectoryResponse result = null;
            result = directoryAccess.AddorEdit(model);
            if (result._directoryModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                TempData["DirectoryModel"] = result._directoryModel;
                return RedirectToAction("index", "directory");
            }
            else
            {
                return RedirectToAction("error", "home");
            }

        }
        [HttpGet]
        public ActionResult Create(string uid)
        {
            //ModelState.Clear();

            DirectoryModel model = new DirectoryModel();
            DirectoryResponse request = new DirectoryResponse();
            model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DirectoryResponse result = directoryAccess.SelectById(model);
            ViewBag.PageHeader = "Basic Info";
            if (result._directoryModel!= null)
            {
                TempData["DirectoryModel"] = result._directoryModel;
                return View(result._directoryModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpGet]
        public ActionResult Edit(string cid)
        {
            DirectoryModel model = new DirectoryModel();
            DirectoryResponse request = new DirectoryResponse();
            model.EncryptedFollowerId =cid;
            model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DirectoryResponse result = directoryAccess.SelectById(model);
            if (result._directoryModel != null)
            {
                 TempData["DirectoryModel"] = result._directoryModel;
                return View(result._directoryModel);
            }
            else
                return RedirectToAction("error", "home");
        }
       
        public ActionResult Delete(String lid)
        {
            DirectoryModel model = new DirectoryModel();
            model.EncryptedLoggedId = lid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            DirectoryResponse result = directoryAccess.Delete(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "directory");
        }

        public ActionResult Index()

        
        {
            DirectoryModel directoryModel = new DirectoryModel();

            directoryModel.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            DirectoryResponse result = directoryAccess.SelectAll(directoryModel);
            ViewBag.Header = "Directory";
            return View(result._directoryModel);
        }
        [HttpPost]
        public ActionResult Index(DirectoryModel model)
        {
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            DirectoryResponse result = directoryAccess.SelectAll(model);

            ViewBag.Header = "Directory";
            return View(result._directoryModel);
        }

        public ActionResult Detail(string cid)
        {
            DirectoryModel model = new DirectoryModel();
            DirectoryResponse request = new DirectoryResponse();
            //model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedFollowerId = cid;
            DirectoryResponse result = directoryAccess.SelectById(model);
            ViewBag.PageHeader = "Directory Info";
            if (result._directoryModel != null)
            {

                return View(result._directoryModel);
            }
            else
                return RedirectToAction("error", "home");
        }
    }
}