﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FamousPerson.Models;

namespace FamousPerson.Controllers
{
    public class ChapterWebController : BaseController
    {
        IChapterWeb chapteraccess = new ChapterAccessWeb();
        // GET: ChapterWeb

        public ActionResult Index()
        
        {
            ChapterModelWeb model = new ChapterModelWeb();

            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            ChapterResponseWeb result = chapteraccess.SelectAll(model);

            ViewBag.Header = "Chapter";
            return View(result.Chaptermodel);
        }
        [HttpPost]
        public ActionResult Index(ChapterModelWeb model)
        {
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            ChapterResponseWeb result = chapteraccess.SelectAll(model);

            ViewBag.Header = "Chapter";
            return View(result.Chaptermodel);
        }

        [HttpGet]
        public ActionResult Create(string vid)
        {
            //ModelState.Clear();

            ChapterModelWeb model = new ChapterModelWeb();
            //DirectoryResponse request = new DirectoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedChapterId = vid;
            ChapterResponseWeb result = chapteraccess.SelectById(model);
            ViewBag.PageHeader = "Chapter Info";
            if (result.Chaptermodel != null)
            {

                return View(result.Chaptermodel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpGet]
        public ActionResult Edit(String vid)
        {

            ChapterModelWeb model = new ChapterModelWeb();
            //DirectoryResponse request = new DirectoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedChapterId = vid;
            ChapterResponseWeb result = chapteraccess.SelectById(model);
            ViewBag.PageHeader = "Chapter Info";
            if (result.Chaptermodel != null)
            {

                return View(result.Chaptermodel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult CreateOrEdit(ChapterModelWeb model)
        {

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            ChapterResponseWeb result = null;
            if (!String.IsNullOrEmpty(model.ImageBase64))
                model.LogoPath = Function.UploadCropImage(model.ImageBase64, "Chapter");
            result = chapteraccess.AddorEdit(model);
            if (result.Chaptermodel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                return RedirectToAction("index", "ChapterWeb");
            }
            else
            {
                return RedirectToAction("error", "home");
            }

        }
        public ActionResult Detail(string vid)
        {
            ChapterModelWeb model = new ChapterModelWeb();
            //DirectoryResponse request = new DirectoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedChapterId = vid;
            ChapterResponseWeb result = chapteraccess.SelectById(model);
            ViewBag.PageHeader = "Chapter Info";
            if (result.Chaptermodel != null)
            {

                return View(result.Chaptermodel);
            }
            else
                return RedirectToAction("error", "home");
        }

        public ActionResult Delete(string vid)
        {
            ChapterModelWeb model = new ChapterModelWeb();
            model.EncryptedChapterId = vid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            ChapterResponseWeb result = chapteraccess.Delete(model);
            //ViewBag.PageHeader = "Delete headline";
            //return View(result._HeadlineModel);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "ChapterWeb");
        }
    }
}