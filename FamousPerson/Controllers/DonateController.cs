﻿using FPBAL.Interface;
using FPBAL.Business;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.Design;

namespace FamousPerson.Controllers
{
    public class DonateController : Controller
    {
       
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult thanks()
        {
            return View();
        }
        public ActionResult unauthorized()
        {
            return View();
        }
        public ActionResult Thankyou()
        {
            return View();
        }
        public ActionResult Donate(string cid,string sid, string fid,  string Is)
        {
            TempData["sid"] = sid;
            TempData["fid"] = fid;
            TempData["cid"] = cid;
            FPRequests request = new FPRequests();
            request.FollowerId = fid;
            request.SessionToken = sid;
            FPResponse serviceResponse = new FPResponse();
            serviceResponse = AppUserAccess.ValidateSessionNew(request);
            if (serviceResponse.ReturnCode == "1")
            {
                if (Is == "login")
                {
                    DonarDetailResponse response = new DonarDetailResponse();
                    IAuthCustomerProfile access = new AuthCustomerProfileAccess();

                    response = access.GetDonarDetail(fid, sid, cid);
                    if (response != null)
                    {
                        return View(response.DonarDetailModel);
                    }
                    else
                        return View();

                }
                else
                {
                    return RedirectToAction("donation", "donate");
                }
            }
            else
                return RedirectToAction("unauthorized", "donate");

        }
        [HttpPost]
        public ActionResult Donate(DonateAPIModel request)
        {
            FPResponse Response = new FPResponse();

            DonarDetailResponse response = new DonarDetailResponse();
            IAuthCustomerProfile access = new AuthCustomerProfileAccess();
            FPRequests request1 = new FPRequests();
            request1.FollowerId = request.FollowerId;
            request1.SessionToken = request.SessionToken;
            FPResponse serviceResponse = new FPResponse();
            serviceResponse = AppUserAccess.ValidateSessionNew(request1);
            if (serviceResponse.ReturnCode == "1")
            {
                Response = access.DonateNew(request);
                if (Response != null)
                {
                    // TempData["TransactioID"] = response.ReturnMessage;
                    return RedirectToAction("thanks", "Donate");
                }
                else
                    return RedirectToAction("donations", "Donate");
            }
            else
                return RedirectToAction("unauthorized", "donate");
        }
       
        public ActionResult Donation()
        {
            Session["IsProcess"] = "0";
            DonationDetailResponse response = new DonationDetailResponse();
            IAuthCustomerProfile access = new AuthCustomerProfileAccess();
            string sid="";
            string fid ="";
            string cid ="";


            if (TempData.ContainsKey("sid"))
                sid = TempData["sid"].ToString();

            if (TempData.ContainsKey("fid"))
                fid = TempData["fid"].ToString();

            if (TempData.ContainsKey("cid"))
                cid = TempData["cid"].ToString();
            FPRequests request = new FPRequests();
            request.FollowerId = fid;
            request.SessionToken = sid;
            FPResponse serviceResponse = new FPResponse();
            serviceResponse = AppUserAccess.ValidateSessionNew(request);
            if (serviceResponse.ReturnCode == "1")
            {
                response = access.GetDonationDetail(fid, sid, cid);
                if (response != null)
                {
                    return View(response.DonationDetailModel);
                }
                else
                    return RedirectToAction("donations", "Donate");
            }
            else
                return RedirectToAction("unauthorized", "donate");
        }
        [HttpPost]
        public ActionResult Donation(CustProfileInfoNew request)
        {
            string process = "0";
            process = Session["IsProcess"] as string;
            if (process == "0")
            {

                Session["IsProcess"] = "1";
           
                FPResponse Response = new FPResponse();
            request.CardType = "Visa";

            IAuthCustomerProfile access = new AuthCustomerProfileAccess();

            //request.SessionToken = "9LPTCCTU1UN8RHYETEFUPG0815RMKC";
            //request.FollowerId = "385";
            //request.FollowerId = UtilityAccess.Encrypt(request.FollowerId);
            //request.FollowerId = UtilityAccess.Encrypt(request.FollowerId);
            FPRequests request1 = new FPRequests();
            request1.FollowerId = request.FollowerId;
            request1.SessionToken = request.SessionToken;

            FPResponse serviceResponse = new FPResponse();
            serviceResponse = AppUserAccess.ValidateSessionNew(request1);
            if (serviceResponse.ReturnCode == "1")
            {
                Response = access.DonationNew(request);
                if (Response.ReturnCode =="1")
                {
                     return RedirectToAction("thanks", "Donate");
                }
                else
                    return RedirectToAction("donations", "Donate");
            }
            else
                return RedirectToAction("unauthorized", "donate");
            }
            return View();

        }
        public ActionResult Donations(string id)
        {
            Session["IsProcess"] = "0";
            DonationDetailResponse response = new DonationDetailResponse();
            IAuthCustomerProfile access = new AuthCustomerProfileAccess();
        
            string cid = "";
                    cid = id;
               response = access.GetDonationDetailNew(cid);
                if (response != null)
                {
                    return View(response.DonationDetailModel);
                }
                else
                      return RedirectToAction("unauthorized", "donate");
        }

        [HttpPost]
        public ActionResult Donations(CustProfileInfoNew request)
        {
            string process = "0";
            process= Session["IsProcess"] as string;

            if(process=="0")
            {

                Session["IsProcess"] = "1";
                string random = "";
                FPResponse Response = new FPResponse();
                request.CardType = "Visa";

                IAuthCustomerProfile access = new AuthCustomerProfileAccess();
                SignUpAPIModel requests = new SignUpAPIModel();
                IAppUser user = new AppUserAccess();
                random = user.RandomString(16);
                requests.DeviceType = "Web";
                requests.IsSkip = true;
                requests.DeviceId = random;
                SignUpRespone ServiceResponse = user.SignUp(requests);

                if (Convert.ToInt32(ServiceResponse.ReturnCode) > 0)
                {
                    FPRequests request1 = new FPRequests();
                    request1.FollowerId = ServiceResponse.UserDetail.EncryptedFollowerId;
                    request1.SessionToken = ServiceResponse.UserDetail.EncryptedSessionToken;

                    request.FollowerId = ServiceResponse.UserDetail.EncryptedFollowerId;
                    request.SessionToken = ServiceResponse.UserDetail.EncryptedSessionToken;
                    FPResponse serviceResponse = new FPResponse();
                    serviceResponse = AppUserAccess.ValidateSessionNew(request1);
                    if (serviceResponse.ReturnCode == "1")
                    {
                        Response = access.DonationNew(request);
                        if (Response.ReturnCode == "1")
                        {
                            return RedirectToAction("thanks", "Donate");
                            
                        }
                        else
                            return RedirectToAction("donations", "Donate");
                    }
                    else
                        return RedirectToAction("unauthorized", "donate");
                }
                else
                    return RedirectToAction("unauthorized", "donate");
            }
            return View();

        }
    }
}