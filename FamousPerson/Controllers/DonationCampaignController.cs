﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class DonationCampaignController : BaseController
    {
        IDonationCompaign campaignAccess = new DonationCompaignAccess();

        public ActionResult Index()
        {
            DonationCompaign model = new DonationCompaign();
            CampaignResponse response = new CampaignResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.DateTo = UtilityAccess.ToDate(model.DateTo);
            //model.DateFrom = UtilityAccess.FromDate(model.DateFrom);

            DonationCampaignResponse result = campaignAccess.CompaignSelectAll(model);
            if (result.CampaignModel != null)
            {
                //result.CampaignModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                //result.CampaignModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                //TempData["ReturnCode"] = result.ReturnCode;
                //TempData["ReturnMessage"] = result.ReturnMessage;
                return View(result.CampaignModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult Index(DonationCompaign model, FormCollection frm)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            DonationCampaignResponse result = new DonationCampaignResponse();
            result.CampaignModel = new DonationCompaign();
            model.StartDate = UtilityAccess.FromDate(model.StartDate);
            model.EndDate = UtilityAccess.ToDate(model.EndDate);
            result = campaignAccess.CompaignSelectAll(model);
            if (result != null)
            {

                return View(result.CampaignModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        public ActionResult Create()

        {
            DonationCompaign model = new DonationCompaign();
            //model.DayId = Convert.ToInt32(UtilityAccess.Decrypt(oid));
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            DonationCampaignResponse result = campaignAccess.CompaignSelect(model);
            //InAppMessageResponse result = appMessageAccess.AddOrEdit(model);
            if (result != null)
            {
                result.CampaignModel.StartDate = UtilityAccess.FromDate(model.StartDate);
                result.CampaignModel.EndDate = UtilityAccess.FromDate(model.EndDate);
                return View(result.CampaignModel);
            }
            return View();
        }
        [HttpPost]
        public ActionResult Create(DonationCompaign model)
        {
            DonationCampaignResponse result = new DonationCampaignResponse();
            //var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
            //if(ModelState.IsValid)
            //{
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.IsChecked = Convert.ToBoolean("true");
            //model.ISDCode = model.ISDCode.Replace("+", "");

            result = campaignAccess.AddOrEdit(model);
            //}
            TempData["ReturnCode"] = result.ReturnCode;
            TempData["ReturnMessage"] = result.ReturnMessage;
            return RedirectToAction("index", "DonationCampaign");
        }

        public ActionResult CreateOrEdit(string cid)
        {
            DonationCompaign model = new DonationCompaign();

            model.CampaignId = Convert.ToInt32(UtilityAccess.Decrypt(cid));
            //model.DayId = Convert.ToInt32(UtilityAccess.Decrypt(oid));
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            DonationCampaignResponse result = campaignAccess.CompaignSelect(model);
            //InAppMessageResponse result = appMessageAccess.AddOrEdit(model);
            if (result != null)
            {
                //if(!String.IsNullOrEmpty(result.CampaignModel.ISDCode))
                //    result.CampaignModel.ISDCode = result.CampaignModel.ISDCode.Replace("+", "");

                result.CampaignModel.StartDate = UtilityAccess.FromDate(model.StartDate);
                result.CampaignModel.EndDate = UtilityAccess.FromDate(model.EndDate);
                return View(result.CampaignModel);
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateOrEdit(DonationCompaign model)
        {
            DonationCampaignResponse result = new DonationCampaignResponse();
            //var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
            //if(ModelState.IsValid)
            //{
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.IsChecked = Convert.ToBoolean("true");

            //model.ISDCode = model.ISDCode.Replace("+", "");
            result = campaignAccess.AddOrEdit(model);
            //}
            TempData["ReturnCode"] = result.ReturnCode;
            TempData["ReturnMessage"] = result.ReturnMessage;
            //return RedirectToAction("index", "DonationCompaign");
            return RedirectToAction("detail", "DonationCampaign", new { cid = result.CampaignModel.EncryptedCampaignId });
           
        }

        public ActionResult Detail(string cid)
        {
            DonationCompaign model = new DonationCompaign();
            DonationCampaignResponse response = new DonationCampaignResponse();
            model.CampaignId = Convert.ToInt32(UtilityAccess.Decrypt(cid));
            model.EncryptedCampaignId = cid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            DonationCampaignResponse result = campaignAccess.CompaignSelect(model);
            if (result.CampaignModel != null)
            {
                result.CampaignModel.StartDate = UtilityAccess.FromDate(model.StartDate);
                result.CampaignModel.EndDate = UtilityAccess.FromDate(model.EndDate);
                return View(result.CampaignModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        public ActionResult Edit(string cid)
        {
            DonationCompaign model = new DonationCompaign();
            DonationCampaignResponse response = new DonationCampaignResponse();
            model.CampaignId = Convert.ToInt32(UtilityAccess.Decrypt(cid));

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            DonationCampaignResponse result = campaignAccess.CompaignSelect(model);
            if (result.CampaignModel != null)
            {
                result.CampaignModel.StartDate = UtilityAccess.FromDate(model.StartDate);
                result.CampaignModel.EndDate = UtilityAccess.FromDate(model.EndDate);
                return View(result.CampaignModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        public ActionResult Delete(string id)
        {
            DonationCompaign model = new DonationCompaign();
            model.EncryptedCampaignId = id;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            DonationCampaignResponse result = campaignAccess.Delete(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "DonationCampaign");
        }
        [HttpPost]
        public ActionResult Detail(string cid, DonationCompaign model)
        {
           // DonationCampaignResponse response = new DonationCampaignResponse();
            model.CampaignId = Convert.ToInt32(UtilityAccess.Decrypt(cid));

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            //InAppMessageResponse result = appMessageAccess.DetailView(model);
            //if (result.InAppMessageModel != null)
            //{
            //    result.InAppMessageModel.DateFilterSelect = model.DateFilterSelect;
            //    result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            //    result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
            //    return View(result.InAppMessageModel);
            //}
            //else
            return RedirectToAction("createoredit", "DonationCampaign");
        }
    }
}