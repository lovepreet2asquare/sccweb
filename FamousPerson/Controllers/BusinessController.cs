﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System.Web.Hosting;

namespace FamousPerson.Controllers
{
    public class BusinessController : BaseController
    {
        IBusiness businessAccess = new BusinessAccess();
        // GET: Business

        [HttpPost]
        public ActionResult CreateOrEdit(BusinessModel model)
        {
            //model.EncryptedBusinessId = Function.ReadCookie("EncryptedBusinessId");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            //model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            BusinessResponse result = null;
            if (!String.IsNullOrEmpty(model.ImageBase64))
                model.LogoPath = Function.UploadCropImage(model.ImageBase64, "Business");
            result = businessAccess.AddorEdit(model);
            if (result._businessModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                TempData["BusinessModel"] = result._businessModel;
                return RedirectToAction("index", "Business");
            }
            else
            {
                return RedirectToAction("error", "home");
            }

        }

        [HttpGet]
        public ActionResult Create(string uid)
        {
            //ModelState.Clear();

            BusinessModel model = new BusinessModel();
            BusinessResponse request = new BusinessResponse();
            //model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            BusinessResponse result = businessAccess.SelectById(model);
            ViewBag.PageHeader = "Basic Info";
            if (result._businessModel != null)
            {
             
                TempData["BusinessModel"] = result._businessModel;
                return View(result._businessModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpGet]
        public ActionResult Edit(String bid)
        {
            BusinessModel model = new BusinessModel();
            BusinessResponse request = new BusinessResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.EncryptedBusinessId = Function.ReadCookie("EncryptedBusinessId");
            model.EncryptedBusinessId = bid;
            BusinessResponse result = businessAccess.SelectById(model);
            ViewBag.PageHeader = "Business Info";
            if (result._businessModel != null)
            {
                result._businessModel.EncryptedBusinessId = bid;
                return View(result._businessModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        public ActionResult Index()
        {
            BusinessModel businessModel = new BusinessModel();
            businessModel.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            businessModel.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            BusinessResponse result = businessAccess.SelectAll(businessModel);
            ViewBag.Header = "Business";
            return View(result._businessModel);
        }
        [HttpPost]
        public ActionResult Index(BusinessModel model)
        {
            model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            BusinessResponse result = businessAccess.SelectAll(model);

            ViewBag.Header = "Business";
            return View(result._businessModel);
        }

        public ActionResult Detail(string did)
        {
            BusinessModel model = new BusinessModel();
            BusinessResponse request = new BusinessResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedBusinessId = did;
            BusinessResponse result = businessAccess.SelectById(model);
            ViewBag.PageHeader = "Business Info";
            if (result._businessModel != null)
            {

                return View(result._businessModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        public ActionResult Delete(string lid)
        {
            BusinessModel model = new BusinessModel();
            model.EncryptedBusinessId = lid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            BusinessResponse result = businessAccess.Delete(model);
            //ViewBag.PageHeader = "Delete headline";
            //return View(result._HeadlineModel);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "Business");
        }
    }
}