﻿using FPBAL.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class UtilityController : Controller
    {
        // GET: Utility
        [HttpPost]
        public ActionResult GetStates(String CountryId, Boolean IsAll)
        {
            if (String.IsNullOrEmpty(CountryId))
                CountryId = "0";
            UtilityAccess utilityAccess = new UtilityAccess();
            var stateData = utilityAccess.StatesByCountry(Convert.ToInt32(CountryId), IsAll);
            return Json(stateData, JsonRequestBehavior.AllowGet);
        }
    }
}