﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FamousPerson.Models;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;


namespace FamousPerson.Controllers
{
    public class Dealcontroller : BaseController
    {
        //IVendors vendorsaccess = new VendorsAccess();
        Ideal dealaccess = new dealaccess();
        [HttpGet]
        public ActionResult Create(string sid)
        {
            Dealmodel model = new Dealmodel();
            Dealresponse request = new Dealresponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedServiceId = sid;
            Dealresponse result = dealaccess.SelectById(model);
            if (result.dealmodel != null)
            {
                result.dealmodel.EncryptedServiceId = sid;
                return View(result.dealmodel);
            }
            else
                return RedirectToAction("error", "home");

        }

        [HttpGet]
        public ActionResult Edit(string hid)
        {
            Dealmodel model = new Dealmodel();
            Dealresponse request = new Dealresponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedDealID = hid;

            Dealresponse result = dealaccess.SelectById(model);

            if (result.dealmodel != null)
            {

                return View(result.dealmodel);
            }
            else
                return RedirectToAction("error", "home");

        }

        [HttpPost]
        public ActionResult CreateOrEdit(Dealmodel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            Dealresponse result = null;
            result = dealaccess.AddOrEdit(model);
            if (result.dealmodel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                //return RedirectToAction("Create", "Dealcontroller");
                return RedirectToAction("edit", "Service", new { @sid = model.EncryptedServiceId });
            }
            else
            {
                return RedirectToAction("error", "home");
            }
        }

        public ActionResult Delete(string cid)
        {
            Dealmodel model = new Dealmodel();
            model.EncryptedDealID = cid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            Dealresponse result = dealaccess.Delete(model);
            //ViewBag.PageHeader = "Delete headline";
            //return View(result._HeadlineModel);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("Create", "Dealcontroller");
        }

    }
}