﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class UserController : BaseController
    {
        IUser userAccess = new UserAccess();
        // GET: User
        public ActionResult Index()
        {
            UserModel model = new UserModel();
            UserResponse request = new UserResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            UserResponse result = userAccess.UserSelectAll(model);
            //result.UserModel.EncryptedUserId = model.EncryptedUserId;
            ViewBag.PageHeader = "View Users";
            return View(result.UserModel);
        }

        [HttpPost]
        public ActionResult Index(UserModel model)
        {
            UserResponse request = new UserResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            UserResponse result = userAccess.UserSelectAll(model);
            ViewBag.PageHeader = "View Users";
            ViewBag.Message = result.ReturnMessage;
            return View(result.UserModel);

        }

        public ActionResult UserDetail(string uid)
        {
            UserModel model = new UserModel();
            UserResponse request = new UserResponse();
            model.UserId = Convert.ToInt32(String.IsNullOrEmpty(uid) ? "0" : UtilityAccess.Decrypt(uid));
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            UserResponse result = userAccess.GetUser(model);
            //ViewBag.PageHeader = "Basic Info";
            //return View(result.UserModel);
            if (result.UserModel != null)
            {
                result.UserModel.EncryptedUserId = uid;
                TempData["UserModel"] = result.UserModel;
                return View(result.UserModel);
            }
            else
                return RedirectToAction("error", "home");
            //return View();
        }

        [HttpGet]
        public ActionResult Create(string uid)
        {
            ModelState.Clear();

            UserModel model = new UserModel();
            UserResponse request = new UserResponse();
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(uid));
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            UserResponse result = userAccess.GetUser(model);
            ViewBag.PageHeader = "Basic Info";
            if (result.UserModel != null)
            {
                TempData["UserModel"] = result.UserModel;
                return View(result.UserModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult Create(UserModel model)
        {
            UserResponse request = new UserResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            UserResponse result = userAccess.AddOrEditUser(model);
            if (!string.IsNullOrEmpty(result.ReturnMessage))
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                if (result.ReturnCode == -3)
                {
                    TempData["href"] = "/user/create?uid=SDl9pQGF3IJhwxcjlDL3eA==";
                    return RedirectToAction("create", "user", new { uid = "SDl9pQGF3IJhwxcjlDL3eA==" });
                }
                else
                {
                    TempData["href"] = "/user/index";
                    return RedirectToAction("index", "user");
                }
            }

            ViewBag.PageHeader = "Basic Info";
            return View(result.UserModel);
        }

        [HttpGet]
        public ActionResult Edit(String id)
        {
            UserModel model = new UserModel();
            UserResponse request = new UserResponse();
            model.UserId = Convert.ToInt32(String.IsNullOrEmpty(id) ? "0" : UtilityAccess.Decrypt(id));
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            UserResponse result = userAccess.GetUser(model);
            //ViewBag.PageHeader = "Basic Info";
            //return View(result.UserModel);
            if (result.UserModel != null)
            {
                //result.UserModel.Password = "**********";
                //result.UserModel.ConfirmPassword = "**********";
                TempData["UserModel"] = result.UserModel;
                return View(result.UserModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        // [ValidateInput(false)]
        public ActionResult Edit(UserModel model)
        {
            UserResponse request = new UserResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            // if (ModelState.IsValid)
            //{
            //if (model.Password == "**********")
            //    model.Password = UtilityAccess.Decrypt(model.CurrentPassword);

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            UserResponse result = userAccess.AddOrEditUser(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                if (result.ReturnCode > 0)
                    TempData["href"] = "/user/index";

                model = (UserModel)TempData["UserModel"];
                return RedirectToAction("index", "user");
            }
            else
                return RedirectToAction("error", "home");
            // }
            //else {

            //  TempData["ReturnMessage"] = "Invalid";
            // }

            // request = userAccess.GetUser(model);
            // return View(request.UserModel);

        }

        [HttpPost]
        public JsonResult PermissionByRole(String tid)
        {
            UserResponse result = userAccess.PermissionByRole(Convert.ToInt32(String.IsNullOrEmpty(tid) ? "0" : tid));
            return Json(result.UserModel._PermissionList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(String id)
        {
            UserModel model = new UserModel();
            model.EncryptedUserId = id;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            UserResponse result = userAccess.Delete(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                TempData["href"] = "/user/index";
            }

            return RedirectToAction("userdetail", "user", new { uid = id });
        }

        public ActionResult Disable(String id)
        {
            UserModel model = new UserModel();
            model.EncryptedUserId = id;
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            UserResponse result = userAccess.Disable(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                TempData["href"] = "/user/index";
            }

            return RedirectToAction("userdetail", "user", new { uid = id });
        }

        public ActionResult MyProfile(String pf)
        {
            ModelState.Clear();
            UserModel model = new UserModel();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            
            model.UserId = Convert.ToInt32(UtilityAccess.Decrypt(Function.ReadCookie("EncryptedUserId")));
            model.SessionToken = UtilityAccess.Decrypt(model.EncryptedSessionToken);
            UserResponse result = userAccess.GetUser(model);
            if (!String.IsNullOrEmpty(pf))
                result.UserModel.IsProfileComplete = false;
            else
                result.UserModel.IsProfileComplete = true;

            ViewBag.PageHeader = "Profile";
            ViewBag.Message = result.ReturnMessage;
            return View(result.UserModel);
        }

        [HttpPost]
        public ActionResult MyProfile(UserModel model)
        {
            UserResponse result = new UserResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            if (model.Email == model.OldEmail)
            {
                result = userAccess.EditUserProfile(model);
                TempData["ReturnMessage"] = result.ReturnMessage;
            }
            else
            {
                //if email is updated 
                result = userAccess.EditUserProfile(model);
                userAccess.NewProfileEmail(model);
                result.ReturnMessage = "Verification mail sent to your new email account. Kindly verify it in order to update.";
                TempData["ReturnMessage"] = result.ReturnMessage;

            }
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                if (result.ReturnCode > 0)
                    TempData["href"] = "/dashboard/index";

                //model = (UserModel)TempData["UserModel"];
                return RedirectToAction("myprofile", "user");
            }
            else
                return RedirectToAction("error", "home");

            //request = userAccess.GetUser(model);
            //return View(request.UserModel);
        }
        [HttpPost]
        public JsonResult UploadProfileImage()
        {
            String UserId = UtilityAccess.Decrypt(Function.ReadCookie("EncryptedUserId"));
            String FolderName = "ProfilePics/" + UserId;
            Int32 result = 0;
            String FilePath = Function.UserUploadFile((Request.Files[0] as HttpPostedFileBase), FolderName);
            if (FilePath == "Minimum file size should be 130*130 Pixel")
            {
                string Result = "Minimum file size should be 130*130 Pixel";
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                result = userAccess.ProfilePicUpdate(Convert.ToInt32(UserId), Function.ReadCookie("EncryptedSessionToken"), FilePath);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UploadCropImage(ImageCropModel model)
        {
            String UserId = UtilityAccess.Decrypt(Function.ReadCookie("EncryptedUserId"));
            String FolderName = "ProfilePics/" + UserId;
            Int32 result = 0;

            String FilePath = Function.UploadCropImage(model.BaseData, FolderName);
            //Function.UserUploadFile((Request.Files[0] as HttpPostedFileBase), FolderName);

            if (FilePath == "Minimum file size should be 130*130 Pixel")
            {
                string Result = "Minimum file size should be 130*130 Pixel";
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                result = userAccess.ProfilePicUpdate(Convert.ToInt32(UserId), Function.ReadCookie("EncryptedSessionToken"), FilePath);
                return Json(result, JsonRequestBehavior.AllowGet);

            }


        }

        public ActionResult table()
        {
            return View();
        }

        [HttpPost]
        public ActionResult _ChangePassword(UserModel model)
        {
            ViewBag.Message = "";
            //try
            //{
            //    //model.UserId = Function.ReadCookie("EncryptedUserId");
            //model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.OldPasswordCompare = Session["Password"].ToString();
            model.OldPasswordCompare1 = UtilityAccess.Decrypt(model.OldPasswordCompare);
            UserResponse data = userAccess.ChangePassword(model);

            if (data != null)
            {
                int ISEmailSend = ChangePasswordAccess.SendPasswordForChangepassword(data.UserModel);
                TempData["ReturnCode"] = data.ReturnCode;
                TempData["ReturnMessage"] = data.ReturnMessage;
            //    TempData["href"] = "/user/profile";
            }

           // return View(model);
            return RedirectToAction("myprofile", "user");
        }
    }
}