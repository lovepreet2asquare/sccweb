﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FamousPerson.Models;

namespace FamousPerson.Controllers
{
    public class CategoryController : BaseController
    {
        ICategory categoryaccess = new CategoryAccess();
        // GET: Category
        public ActionResult Index()
        
        {
            CategoryModel categoryModel = new CategoryModel();

            categoryModel.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            CategoryResponse result = categoryaccess.SelectAll(categoryModel);
            ViewBag.Header = "Category";
            return View(result.Categorymodel);
        }
        [HttpPost]
        public ActionResult Index(CategoryModel model)
        {
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            CategoryResponse result = categoryaccess.SelectAll(model);

            ViewBag.Header = "Category";
            return View(result.Categorymodel);
        }

        // GET: Category/Details/5
        public ActionResult CategoryDetail(string cid)
        {
            CategoryModel model = new CategoryModel();
            CategoryResponse request = new CategoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedCategoryId = cid;
            CategoryResponse result = categoryaccess.SelectById(model);
            ViewBag.PageHeader = "Category Info";
            if (result.Categorymodel != null)
            {

                return View(result.Categorymodel);
            }
            else
                return RedirectToAction("error", "home");
        }

        // GET: Category/Create
        [HttpGet]
        public ActionResult Create(string cid)
        {
            CategoryModel model = new CategoryModel();
            CategoryResponse request = new CategoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedCategoryId = cid;
            CategoryResponse result = categoryaccess.SelectById(model);
            ViewBag.PageHeader = "Category Info";
            if (result.Categorymodel != null)
            {

                return View(result.Categorymodel);
            }
            else
                return RedirectToAction("error", "home");

        }

        // GET: Category/Edit
        [HttpGet]
        public ActionResult Edit(string cid)
        {
            CategoryModel model = new CategoryModel();
            CategoryResponse request = new CategoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedCategoryId = cid;
            CategoryResponse result = categoryaccess.SelectById(model);
            ViewBag.PageHeader = "Category Info";
            if (result.Categorymodel != null)
            {

                return View(result.Categorymodel);
            }
            else
                return RedirectToAction("error", "home");
        }


        [HttpPost]
        public ActionResult CreateOrEdit(CategoryModel model)
        {
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            CategoryResponse result = null;
            if (!String.IsNullOrEmpty(model.ImageBase64))
                model.LogoPath = Function.UploadCropImage(model.ImageBase64, "category");
            result = categoryaccess.AddorEdit(model);
            if (result.Categorymodel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                return RedirectToAction("index", "Category");
            }
            else
            {
                return RedirectToAction("error", "home");
            }
        }

        public ActionResult Delete(string cid)
        {
            CategoryModel model = new  CategoryModel();
            model.EncryptedCategoryId = cid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            CategoryResponse result = categoryaccess.Delete(model);
            //ViewBag.PageHeader = "Delete headline";
            //return View(result._HeadlineModel);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "Category");
        }

    }
}
