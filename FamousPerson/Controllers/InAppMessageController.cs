﻿using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class InAppMessageController : BaseController
    {

        IAppMessage appMessageAccess = new InAppMessageAccess();
        // GET: InAppMessage
        public ActionResult Edit(string mid)
        {
            InAppMessageModel model = new InAppMessageModel();
            model.MessageId = mid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            InAppMessageResponse result = appMessageAccess.DetailView(model);
            result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
            result.InAppMessageModel.PublishDate = result.InAppMessageModel.Status == "Scheduled" ? result.InAppMessageModel.PublishCalDate : "";
            //InAppMessageResponse result = appMessageAccess.AddOrEdit(model);
            if (result != null)
            {

                return View(result.InAppMessageModel);
            }

            return View();
        }
        [HttpPost]
        public ActionResult Edit(InAppMessageModel model)
        {
            InAppMessageResponse result = new InAppMessageResponse();
            //var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
            //if(ModelState.IsValid)
            //{
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            
            result = appMessageAccess.AddOrEdit(model);
            //}
            TempData["ReturnCode"] = result.ReturnCode;
            TempData["ReturnMessage"] = result.ReturnMessage;
            return RedirectToAction("index", "InAppMessage");
        }
        public ActionResult Index()
        {
            InAppMessageModel model = new InAppMessageModel();
            InAppMessageResponse response = new InAppMessageResponse();
            string fid = TempData["ReturnCode"] == null ? "1" : TempData["ReturnCode"].ToString();
            model.isview = fid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.DateTo = UtilityAccess.ToDate(model.DateTo);
            //model.DateFrom = UtilityAccess.FromDate(model.DateFrom);

            InAppMessageResponse result = appMessageAccess.Detail(model);
            if (result.InAppMessageModel != null)
            {
                // result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                //result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                return View(result.InAppMessageModel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult Index(InAppMessageModel model)
        {

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            InAppMessageResponse result = new InAppMessageResponse();
            result.InAppMessageModel = new InAppMessageModel();
           // model.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            //model.DateTo = UtilityAccess.ToDate(model.DateTo);
            result = appMessageAccess.Detail(model);
            if (result != null)
            {

                return View(result.InAppMessageModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        public ActionResult DetailView(string mid)
        {
            InAppMessageModel model = new InAppMessageModel();
            InAppMessageResponse response = new InAppMessageResponse();
            model.MessageId = mid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            InAppMessageResponse result = appMessageAccess.DetailView(model);
            if (result.InAppMessageModel != null)
            {
                result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
                result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
                return View(result.InAppMessageModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult DetailView(string mid, InAppMessageModel model)
        {
            InAppMessageResponse response = new InAppMessageResponse();
            model.MessageId = Convert.ToString(mid);
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");

            //InAppMessageResponse result = appMessageAccess.DetailView(model);
            //if (result.InAppMessageModel != null)
            //{
            //    result.InAppMessageModel.DateFilterSelect = model.DateFilterSelect;
            //    result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            //    result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
            //    return View(result.InAppMessageModel);
            //}
            //else
            return RedirectToAction("create", "inappmessage");
        }
        public ActionResult _Dashboard()
        {
            InAppMessageModel model = new InAppMessageModel();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            InAppMessageResponse result = appMessageAccess.DetailView(model);
            result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
            //InAppMessageResponse result = appMessageAccess.AddOrEdit(model);
            if (result != null)
            {

                return View(result.InAppMessageModel);
            }

            return View();
        }
        public ActionResult Create(string mid)
        {
            InAppMessageModel model = new InAppMessageModel();
            model.MessageId = mid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            InAppMessageResponse result = appMessageAccess.DetailView(model);
            result.InAppMessageModel.DateFrom = UtilityAccess.FromDate(model.DateFrom);
            result.InAppMessageModel.DateTo = UtilityAccess.FromDate(model.DateTo);
            //InAppMessageResponse result = appMessageAccess.AddOrEdit(model);
            if (result != null)
            {

                return View(result.InAppMessageModel);
            }

            return View();

        }
        [HttpPost]
        public ActionResult Create(InAppMessageModel model)
        {
            InAppMessageResponse result = new InAppMessageResponse();
            //var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
            //if(ModelState.IsValid)
            //{
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            result = appMessageAccess.AddOrEdit(model);
            if (result.InAppMessageModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }
            return RedirectToAction("index", "InAppMessage");

        }

        public ActionResult Delete(string mid)
        {
            InAppMessageModel model = new InAppMessageModel();
            model.MessageId = mid;
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            InAppMessageResponse result = appMessageAccess.Delete(model);

            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "inappmessage");
        }
        public ActionResult Archive(string mid)
        {
            InAppMessageModel model = new InAppMessageModel();
            model.MessageId = mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            InAppMessageResponse result = appMessageAccess.Archive(model);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }
            return RedirectToAction("index", "inappmessage");
        }

    }
}