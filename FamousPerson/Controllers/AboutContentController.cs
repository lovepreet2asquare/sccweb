﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamousPerson.Controllers
{
    public class AboutContentController : Controller
    {
        // GET: AboutContent
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PrivacyPolicy()
        {
            return View();
        }
        public ActionResult Download()
        {
            return View();
        }

        public ActionResult TermsAndCondition()
        {
            return View();
        }
        public ActionResult Eula()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }
    }
}