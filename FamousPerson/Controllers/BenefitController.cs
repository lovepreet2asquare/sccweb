﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FamousPerson.Models;
using System.Web.Mvc;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;

namespace FamousPerson.Controllers
{
    public class BenefitController : BaseController
    {
        IMemberBenefitWeb memberBenefitAccess = new MemberBenefitAccessWeb();
        // GET: MemberBenefit
        public ActionResult Index()
        
        
        {
            MemberBenefitModelWeb model = new MemberBenefitModelWeb();

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            MemberBenefitResponseWeb result = memberBenefitAccess.SelectAll(model);

            ViewBag.Header = "Benefit";
            return View(result.MemberBenefit);
        }
        [HttpPost]
        public ActionResult Index(MemberBenefitModelWeb model)
        {
            //MemberBenefitModel model = new MemberBenefitModel();

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            MemberBenefitResponseWeb result = memberBenefitAccess.SelectAll(model);

            ViewBag.Header = "Benefit";
            return View(result.MemberBenefit);
        }
        public ActionResult Create(string mid)
        {
            MemberBenefitModelWeb model = new MemberBenefitModelWeb();
            model.EncryptedMemberBenefitId = mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.MemberBenefitId = 0;
            MemberBenefitResponseWeb result = new MemberBenefitResponseWeb();
            result.MemberBenefit = model;

            ViewBag.PageHeader = "Basic Info";
            return View(result.MemberBenefit);
        }
        public ActionResult Edit(string mid)
        {
            MemberBenefitModelWeb model = new MemberBenefitModelWeb();
            model.EncryptedMemberBenefitId = mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            MemberBenefitResponseWeb result = memberBenefitAccess.Select(model);
            TempData["MemberBenefitModel"] = result.MemberBenefit;
            ViewBag.PageHeader = "Basic Info";
            
            return View(result.MemberBenefit);
        }
        public ActionResult Detail(string mid)
        {
            MemberBenefitModelWeb model = new MemberBenefitModelWeb();
            model.EncryptedMemberBenefitId = mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            MemberBenefitResponseWeb result = memberBenefitAccess.Select(model);
            TempData["MemberBenefitModel"] = result.MemberBenefit;
            ViewBag.PageHeader = "Basic Info";

            return View(result.MemberBenefit);
        }
        [HttpPost]
        public ActionResult CreateorEdit(MemberBenefitModelWeb model)
        {
            Function function = new Function();
            
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            {
                MemberBenefitResponseWeb result = memberBenefitAccess.AddorEdit(model);
                if (result.MemberBenefit != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["MemberBenefitModel"] = result.MemberBenefit;
                    return RedirectToAction("index", "benefit");
                }
            }
            return View(model);
        }
        //[HttpPost]
        public ActionResult Delete(string mid)
        {
            //Function function = new Function();
            MemberBenefitModelWeb model = new MemberBenefitModelWeb();
            model.EncryptedMemberBenefitId = mid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            {
                MemberBenefitResponseWeb result = memberBenefitAccess.Delete(model);
                if (result.MemberBenefit != null)
                {
                    TempData["ReturnCode"] = result.ReturnCode;
                    TempData["ReturnMessage"] = result.ReturnMessage;
                    TempData["MemberBenefitModel"] = result.MemberBenefit;
                    return RedirectToAction("index", "benefit");
                }
            }
            return View(model);
        }

    }
}