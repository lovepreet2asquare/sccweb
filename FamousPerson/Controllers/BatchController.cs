﻿using FPModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FamousPerson.Models;
using FPBAL.Business;
using FPBAL.Interface;

namespace FamousPerson.Controllers
{
    public class BatchController : BaseController
    {
        // GET: Batch
        IBatch batchAccess = new BatchAccess();
        public ActionResult Index()
        {
            BatchModel model = new BatchModel();
            BatchResponse request = new BatchResponse();
            string fid = TempData["ReturnCode"] == null ? "1" : TempData["ReturnCode"].ToString();
            model.IsView = fid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.DateFrom = UtilityAccess.FromDate();
            model.DateTo = UtilityAccess.ToDate();

            BatchResponse result = batchAccess.BatchSelectAll(model);
            if (result._BatchModel != null)
            {
               // result._BatchModel.DateFrom = UtilityAccess.FromDate();
                //result._BatchModel.DateTo = UtilityAccess.FromDate();

                ViewBag.PageHeader = "Batch";
                return View(result._BatchModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpPost]
        public ActionResult Index(BatchModel model)
        {
            BatchResponse request = new BatchResponse();
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            BatchResponse result = batchAccess.BatchSelectAll(model);
            //result.UserModel.EncryptedUserId = model.EncryptedUserId;
            
            model.DateTo = UtilityAccess.ToDate(model.DateTo);

            if (result._BatchModel != null)
            {               

                ViewBag.PageHeader = "Batch";
                return View(result._BatchModel);
            }
            else
                return RedirectToAction("error", "home");
        }


        public ActionResult Create()
        {
            BatchModel model = new BatchModel();
            BatchResponse response = new BatchResponse();
            //model.DayId = Convert.ToInt32(UtilityAccess.Decrypt(oid));
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            BatchResponse result = batchAccess.Select(model);
            if (result != null)
            {
              return View(result._BatchModel);
            }
            return View();
        }
        [HttpPost]
        public ActionResult Create(BatchModel model)
        {
            
            BatchResponse response = new BatchResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.Status = "Created";
            BatchResponse result = batchAccess.SelectBatchTransactions(model);
            if (result != null)
            {
              //  result = batchAccess.BatchAdd(model);
            }
            
            TempData["ReturnCode"] = result.ReturnCode;
            TempData["ReturnMessage"] = result.ReturnMessage;
            return RedirectToAction("index", "batch");
        }

        public ActionResult Edit(String bid)
        {
            BatchModel model = new BatchModel();
            BatchResponse response = new BatchResponse();
            if(!String.IsNullOrEmpty(bid))
                model.BatchId= Convert.ToInt32(UtilityAccess.Decrypt(bid));

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            BatchResponse result = batchAccess.Select(model);
            if (result._BatchModel != null)
            {
                return View(result._BatchModel);
            }
            else

                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult Edit(BatchModel model)
        {

            BatchResponse response = new BatchResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            BatchResponse result = batchAccess.SelectBatchTransactions(model);
            
            TempData["ReturnCode"] = result.ReturnCode;
            TempData["ReturnMessage"] = result.ReturnMessage;
            return RedirectToAction("index", "batch");
        }

        public ActionResult Detail(String bid)
        {
            BatchModel model = new BatchModel();
            BatchResponse response = new BatchResponse();
            if (!String.IsNullOrEmpty(bid))
                model.BatchId = Convert.ToInt32(UtilityAccess.Decrypt(bid));

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            BatchResponse result = batchAccess.Select(model);
            if (result._BatchModel != null)
            {
                return View(result._BatchModel);
            }
            else

                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult Detail(BatchModel model)
        {

            BatchResponse response = new BatchResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            BatchResponse result = batchAccess.FileStatusUpdate(model);

            TempData["ReturnCode"] = result.ReturnCode;
            TempData["ReturnMessage"] = result.ReturnMessage;
            return RedirectToAction("index", "batch");
        }
    }
}