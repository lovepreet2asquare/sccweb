﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FamousPerson.Models;

namespace FamousPerson.Controllers
{
    public class BoardMemberController : BaseController
    {
        IMemberWeb memberaccess = new MemberWebAccess();
        // GET: BoardMemberWeb

        public ActionResult Index()
        
        {
            MemberWebModel model = new MemberWebModel();

            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            MemberResponseWeb result = memberaccess.SelectAll(model);

            ViewBag.Header = "Board Member";
            return View(result.membermodel);
        }
        [HttpPost]
        public ActionResult Index(MemberWebModel model)
        {
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            MemberResponseWeb result = memberaccess.SelectAll(model);

            ViewBag.Header = "Board Member";
            return View(result.membermodel);
        }

        [HttpGet]
        public ActionResult Create(string vid)
        {
            //ModelState.Clear();

            MemberWebModel model = new MemberWebModel();
            //DirectoryResponse request = new DirectoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedMemberId = vid;
            MemberResponseWeb result = memberaccess.SelectById(model);
            ViewBag.PageHeader = "Board Member Info";
            if (result.membermodel != null)
            {

                return View(result.membermodel);
            }
            else
                return RedirectToAction("error", "home");
        }
        [HttpGet]
        public ActionResult Edit(String vid)
        {

            MemberWebModel model = new MemberWebModel();
            //DirectoryResponse request = new DirectoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedMemberId = vid;
            MemberResponseWeb result = memberaccess.SelectById(model);
            ViewBag.PageHeader = "Board Member Info";
            if (result.membermodel != null)
            {

                return View(result.membermodel);
            }
            else
                return RedirectToAction("error", "home");
        }

        [HttpPost]
        public ActionResult CreateOrEdit(MemberWebModel model)
      {

            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            MemberResponseWeb result = null;
            if (!String.IsNullOrEmpty(model.ImageBase64))
                model.LogoPath = Function.UploadCropImage(model.ImageBase64, "Chapter");
            result = memberaccess.AddorEdit(model);
            if (result.membermodel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                return RedirectToAction("index", "BoardMember");
            }
            else
            {
                return RedirectToAction("error", "home");
            }

        }
        public ActionResult Detail(string vid)
        {
            MemberWebModel model = new MemberWebModel();
            //DirectoryResponse request = new DirectoryResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedMemberId = vid;
            MemberResponseWeb result = memberaccess.SelectById(model);
            ViewBag.PageHeader = "Board Member Info";
            if (result.membermodel != null)
            {

                return View(result.membermodel);
            }
            else
                return RedirectToAction("error", "home");
        }

        public ActionResult Delete(string vid)
        {
            MemberWebModel model = new MemberWebModel();
            model.EncryptedMemberId = vid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            MemberResponseWeb result = memberaccess.Delete(model);
            //ViewBag.PageHeader = "Delete headline";
            //return View(result._HeadlineModel);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "BoardMember");
        }
    }
}