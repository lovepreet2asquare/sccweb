﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPModels.Models;
using FPBAL.Business;
using FPBAL.Interface;
using FamousPerson.Models;

namespace FamousPerson.Controllers
{
    public class ReferVendorController : BaseController
    {
        IReferVendor referVendoraccess = new ReferVendorAccess();
        IBusiness businessAccess = new BusinessAccess();
        // GET: ReferVendor
        public ActionResult Index()
        {
            ReferVendorModel vendorModel = new ReferVendorModel();

            vendorModel.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            ReferVendorResponse result = referVendoraccess.SelectAll(vendorModel);
            ViewBag.Header = "Refer Vendor";
            return View(result.Categorymodel);
        }
        public ActionResult CreateOrEdit(BusinessModel model)
        {
            //model.EncryptedBusinessId = Function.ReadCookie("EncryptedBusinessId");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            //model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            model.SessionToken = Function.ReadCookie("EncryptedSessionToken");
            BusinessResponse result = null;
            if (!String.IsNullOrEmpty(model.ImageBase64))
                model.LogoPath = Function.UploadCropImage(model.ImageBase64, "Business");
            result = businessAccess.Edit(model);
            if (result._businessModel != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
                TempData["BusinessModel"] = result._businessModel;
                return RedirectToAction("index", "ReferVendor");
            }
            else
            {
                return RedirectToAction("error", "home");
            }

        }
        [HttpPost]
        public ActionResult Index(ReferVendorModel model)
        {
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");

            ReferVendorResponse result = referVendoraccess.SelectAll(model);

            ViewBag.Header = "Refer Vendor";
            return View(result.Categorymodel);
        }
        public ActionResult Detail(string did)
        {
            BusinessModel model = new BusinessModel();
            BusinessResponse request = new BusinessResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedBusinessId = did;
            BusinessResponse result = businessAccess.SelectById(model);
            ViewBag.PageHeader = "Business Info";
            if (result._businessModel != null)
            {

                return View(result._businessModel);
            }
            else
                return RedirectToAction("error", "home");
        }
        public ActionResult Delete(string lid)
        {
            BusinessModel model = new BusinessModel();
            model.EncryptedBusinessId = lid;
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            BusinessResponse result = businessAccess.Delete(model);
            //ViewBag.PageHeader = "Delete headline";
            //return View(result._HeadlineModel);
            if (result != null)
            {
                TempData["ReturnCode"] = result.ReturnCode;
                TempData["ReturnMessage"] = result.ReturnMessage;
            }

            return RedirectToAction("index", "refervendor");
        }
        [HttpGet]
        public ActionResult Edit(String bid)
        {
            BusinessModel model = new BusinessModel();
            BusinessResponse request = new BusinessResponse();
            model.EncryptedUserId = Function.ReadCookie("EncryptedUserId");
            model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            //model.EncryptedBusinessId = Function.ReadCookie("EncryptedBusinessId");
            model.EncryptedBusinessId = bid;
            BusinessResponse result = businessAccess.SelectByIdforrefervendor(model);
            ViewBag.PageHeader = "Business Info";
            if (result._businessModel != null)
            {

                return View(result._businessModel);
            }
            else
                return RedirectToAction("error", "home");
            //BusinessModel model = new BusinessModel();
            //BusinessResponse request = new BusinessResponse();
            //model.EncryptedUserId = bid;
            //model.EncryptedLoggedId = Function.ReadCookie("EncryptedUserId");
            //model.EncryptedSessionToken = Function.ReadCookie("EncryptedSessionToken");
            //BusinessResponse result = businessAccess.SelectById(model);
            //if (result._businessModel != null)
            //{
            //    TempData["BusinessModel"] = result._businessModel;
            //    return View(result._businessModel);
            //}
            //else
            //    return RedirectToAction("error", "home");
        }
    }
}