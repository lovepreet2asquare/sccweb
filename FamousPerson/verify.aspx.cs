﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using FPBAL.Business;

namespace FamousPerson
{
    public partial class verify : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                GetUserInfo();
        }
        protected void GetUserInfo()
        {
            if (Request.QueryString["q"] != null && Request.QueryString["token"] != null)
            {
                int FollowerId = 0;

                string UserIdEncrypted = Request.QueryString["q"];
                UserIdEncrypted = UserIdEncrypted.Replace(" ", "+");
                string UserIdDecrypted = UtilityAccess.Decrypt(UserIdEncrypted);

                if (int.TryParse(UserIdDecrypted, out FollowerId))
                {

                    DataTable dtblInfo = new DataTable();
                    using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["fpCon"].ConnectionString))
                    {
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("dbo.FP_FollowerEmailInfo", sqlConnection))
                        {
                            try
                            {
                                sqlDataAdapter.SelectCommand.Parameters.Add("@FollowerId", SqlDbType.Int).Value = FollowerId;
                                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                                sqlDataAdapter.Fill(dtblInfo);
                                if (dtblInfo.Rows.Count > 0)
                                {
                                    string strEmail = dtblInfo.Rows[0]["Email"].ToString();
                                    string FirstName = dtblInfo.Rows[0]["FirstName"].ToString();
                                    int IsExpired = Convert.ToInt32(dtblInfo.Rows[0]["IsExpired"]);
                                    bool TokenUsed = Convert.ToBoolean(dtblInfo.Rows[0]["TokenUsed"]);
                                    // generate MD5 by @password+@email+@puid
                                    string strToken = UtilityAccess.CalculateMD5Hash(FirstName + strEmail.ToLower() + FollowerId);
                                    if (strToken == Request.QueryString["token"] && IsExpired > 0)
                                    {

                                        if (!TokenUsed)
                                        {
                                            VerifyUser(FollowerId);
                                            // Valid Link
                                            //  noresult.Visible = false;
                                            pnlMsg.Visible = false;
                                            pnlSuccess.Visible = true;
                                        }
                                        else
                                        {
                                            // Invalid token
                                            // noresult.Visible = true;
                                            pnlMsg.Visible = true;
                                            pnlSuccess.Visible = false;

                                        }
                                    }
                                    else
                                    {
                                        // Invalid token
                                        pnlSuccess.Visible = false;
                                        // noresult.Visible = true;
                                        pnlMsg.Visible = true;
                                        //pnlInvalid.Visible = true;
                                    }

                                }
                                else
                                {
                                    // invalid puid
                                    pnlSuccess.Visible = false;
                                    //  noresult.Visible = true;
                                    pnlMsg.Visible = true;
                                    //pnlInvalid.Visible = true;
                                }

                            }
                            catch (Exception ex)
                            {
                                //ApplicationLogger.LogError(ex, "verify.aspx", "GetUserInfo()");
                                //ExceptionUtility.LogException(ex, "ForgotPassword");
                            }
                            finally
                            {
                                sqlConnection.Close();
                            }
                        }
                    }
                }
                else
                {
                    // invalid puid
                    pnlSuccess.Visible = false;
                    //  noresult.Visible = true;
                    pnlMsg.Visible = true;
                    //pnlInvalid.Visible = true;
                }
            }
        }

        protected void VerifyUser(int FollowerId)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["fpCon"].ConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("dbo.FP_FollowerEmailVerify", sqlConnection))
                {
                    try
                    {

                        sqlCommand.Parameters.Add("@FollowerId", SqlDbType.Int).Value = FollowerId;
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        if (sqlConnection.State == ConnectionState.Closed)
                        {
                            sqlConnection.Open();
                        }
                        sqlCommand.ExecuteNonQuery();


                    }
                    catch (Exception ex)
                    {
                        //ApplicationLogger.LogError(ex, "verify.aspx", "VerifyUser");
                        // ExceptionUtility.LogException(ex, "ForgotPassword");
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}