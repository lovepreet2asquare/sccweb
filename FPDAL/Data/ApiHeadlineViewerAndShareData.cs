﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class ApiHeadlineViewerAndShareData: ConnectionObject
    {
        public static int HeadLineViewerInsert(FP_HeadlineViewerAndShareRequest model)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@FollowerId",model.FollowerId),
                                            new SqlParameter("@HeadlineId",model.HeadlineId)
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_ApiHeadLineViewerInsert", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ReturnMessage = -1;
                ApplicationLogger.LogError(ex, "ApiHeadlineViewerAndShareData", "HeadLineViewer");
                return ReturnMessage;
            }
        }

        public static int HeadLineShareInsert(FP_HeadlineViewerAndShareRequest model)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@FollowerId",model.FollowerId),
                                            new SqlParameter("@HeadlineId",model.HeadlineId)
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_ApiHeadLineShare", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ReturnMessage = -1;
                ApplicationLogger.LogError(ex, "ApiHeadlineViewerAndShareData", "HeadLineShareInsert");
                return ReturnMessage;
            }
        }
    }
}
