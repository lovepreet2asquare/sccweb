﻿using System;
using System.Data;
using System.Data.SqlClient;
using FPModels.Models;
using Infotech.ClassLibrary;

namespace FPDAL.Data
{
    public class EventWebData:ConnectionObject
    {

        public DataSet EventSelectAll(EventWebModel model,out int returnResult)
        {
            
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@DateFrom",model.DateFrom),
                                            new SqlParameter("@DateTo",model.DateTo),
                                             new SqlParameter("@Isview",model.IsView??"1"),
                                            //new SqlParameter("@Status",model.Status=="-1"?"":model.Status),
                                            new SqlParameter("@Status",model.Status=="-1"?"":model.Status),
                                            new SqlParameter("@Search",model.Search??""),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_EventSelectAll", parameters);
                returnResult = Convert.ToInt32(parameters[6].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventData", "EventSelectAll");
                returnResult = -1;
                return ds;
            }
            finally
            {
            }
        }
        public DataSet EventCountSelectAll(EventWebModel model,out int returnResult)
        {
            
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@EventId", model.EventId ),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_EventCountSelectAll", parameters);
                returnResult = Convert.ToInt32(parameters[2].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventData", "EventSelectAll");
                returnResult = -1;
                return ds;
            }
            finally
            {
            }
        }
        public DataSet SelectById(EventWebModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@EventId",Convert.ToInt32(model.EventId)),
                                            new SqlParameter("@UserId",Convert.ToInt32(model.UserId)),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_EventSelectById", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }

        public DataSet AddorEdit(EventWebModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={ 
                                            new SqlParameter("@eventId",model.EventId),
                                            new SqlParameter("@userId",model.UserId),
                                            new SqlParameter("@title",model.Title??string.Empty),
                                            new SqlParameter("@content",model.Content??string.Empty),
                                            new SqlParameter("@eventDate",model.EventDate??string.Empty),
                                            new SqlParameter("@invitationType",model.InvitationType??String.Empty),
                                            new SqlParameter("@publishDate",model.PublishDate),
                                            new SqlParameter("@addLine1",model.AddressLine1??string.Empty),
                                            new SqlParameter("@addLine2",model.AddressLine2??string.Empty),
                                            new SqlParameter("@EventImage",model.LogoPath??string.Empty),
                                            new SqlParameter("@cityName",model.CityName??string.Empty),
                                            new SqlParameter("@stateId",model.StateId),
                                            new SqlParameter("@CountryId",model.CountryId),
                                            new SqlParameter("@zip",model.Zip),
                                            new SqlParameter("@status",model.Status??string.Empty),
                                            //new SqlParameter("@SelectedIndividualId",model.SelectedIndividualId),
                                            //new SqlParameter("@SelectedCountyId",model.SelectedCountyId??string.Empty),
                                            //new SqlParameter("@SelectedLocalId",model.SelectedLocalId??string.Empty),
                                            new SqlParameter("@AudienceType",model.SelectAudience??string.Empty),
                                            //new SqlParameter("@lat",Convert.ToDouble(model.Latitude??default(double))),
                                            //new SqlParameter("@long",Convert.ToDouble(model.Longitude??default(double))),
                                            //new SqlParameter("@AudienceId",model.AudienceId),
                                            new SqlParameter("@AudienceCount",model.ApproxAudience),
                                            new SqlParameter("@ConfirmationMsgResponse",model.Confirmation),
                                            new SqlParameter("@Summary",model.Summary??string.Empty),
                                            new SqlParameter("@EventURL",model.URL??string.Empty),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@Timezone",model.TimeZone),
                                            new SqlParameter("@AgendaId",model.AgendaId),
                                            new SqlParameter("@RejectReason",model.RejectReason),
                                            new SqlParameter("@FileType",model.FileType??string.Empty),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_EventUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[25].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        public void AddAgenda(EventWebModel model, out Int32 ReturnResult)
        {
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@AgendaId",model.AgendaId),
                                           new SqlParameter("@Title",model.AgendaAdd),
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_EventAgendaUpdate", parameters);
                ReturnResult = Convert.ToInt32(parameters[4].Value);

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventData", "AddAgenda");
                ReturnResult = -1;
            }
            finally
            {
            }
        }

        public DataSet PublishNSendNotification(out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@CurrentDate",null),

                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_EventCronJob", parameters);
                ReturnResult = 1;
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventData", "PublishNSendNotification");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }

        public int Delete(EventWebModel model)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@EventId",model.EventId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_EventDelete]", parameters);
                returnResult = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "EventData", "Delete");
            }
            finally
            { }
            return returnResult;
        }

        public int Archive(EventWebModel model)
        {

            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@EventId",model.EventId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_EventArchive", parameters);
                ReturnResult = Convert.ToInt32(obj);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventData", "Archive");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally

            {
            }
        }

    }
}
