﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FPDAL.Data
{

    public class DashboardData : ConnectionObject
    {
        public DataSet LoadData(DashboardSearch model, out Int32 returnResult)
        {
            returnResult =0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                                new SqlParameter("@UserId",model.UserId),
                                                new SqlParameter("@TokenKey",model.TokenKey),
                                                new SqlParameter("@Duration",model.Duration??"7"),
                                                new SqlParameter("@CallType",model.CallType??"Load"),
                                        };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_UserDashboard", parameters);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "DashboardData", "LoadData");
            }

            return ds;
        }
        public static int CreateAndSaveToken(LoginModel request, int userId, string sessionToken)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",userId),
                                            new SqlParameter("@SessionToken",sessionToken),
                                            new SqlParameter("@DeviceToken",request.DeviceToken ?? ""),
                                            new SqlParameter("@DeviceType",request.DeviceType),
                                            //new SqlParameter("@CustomerId",request.CustomerId)
                };
                object obj = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.FP_UserTokenInsert", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "UserData", "CreateAndSaveToken");
            }

            return returnResult;
        }

        public static DataTable ForgotPassword(string EmailId, out int returnResult)
        {
            returnResult = 0;
            // int userId = 0;
            string firstName = string.Empty;
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@Email",EmailId)
                                           };
                DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.GS_UserForgotPassword", parameters);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    returnResult = 1;
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "UserData", "ForgotPassword");
            }

            return dt;
        }
        public static DataTable IsEmailExist(ForgotPasswordModel model)
        {
            DataTable myDataTable = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@Email",model.Email),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {

                myDataTable = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "FP_UserIsEmailExist", parameters).Tables[0];
                return myDataTable;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "USerData", "IsEmailExist");
                return null;
            }
            finally
            {
            }
        }

        public static Int32 ResetPassword(ResetPasswordModel model)
        {
            Int32 myInt = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@Password", model.Password),
                                            new System.Data.SqlClient.SqlParameter("@ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {

                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.FP_UserResetPassword", parameters);
                myInt = Convert.ToInt32(parameters[2].Value);
                return myInt;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "ResetPassword");
                return -1;
            }
            finally
            {
            }
        }
        public static DataSet GetUserList()
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            //new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_ManageUserList", parameters);
                //myInt = Convert.ToInt32(parameters[2].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UserData", "GetUserList");
                return ds;
            }
            finally
            {
            }
        }
    }
}
