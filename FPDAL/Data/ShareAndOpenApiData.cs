﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class ShareAndOpenApiData: ConnectionObject
    {
        public static int ShareCount(FP_ShareAndOpenApiRequest model)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@FollowerId",model.FollowerId),
                                            new SqlParameter("@MessageId",model.MessageId)
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_APIShareMessageCount", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ShareAndOpenApiData", "ShareCount");
                return ReturnMessage;
            }
        }

        public static int OpenMessage(FP_ShareAndOpenApiRequest model)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@FollowerId",model.FollowerId),
                                            new SqlParameter("@MessageId",model.MessageId)
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_APIOpenMessage", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ShareAndOpenApiData", "OpenMessage");
                return ReturnMessage;
            }
        }
    }
}
