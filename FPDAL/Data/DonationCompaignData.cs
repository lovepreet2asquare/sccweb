﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FPDAL.Data
{
    public class DonationCompaignData: ConnectionObject
    {
        public DataSet AddOrEdit(DonationCompaign model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@CampaignId",model.CampaignId),
                                            new SqlParameter("@CampaignTitle", model.CampaignTitle),
                                            new SqlParameter("@CampaignDesc", model.CampaignDesc),
                                            new SqlParameter("@CampaignNumber", model.CampaignNumber),
                                            new SqlParameter("@CampaignUrl", model.CampaignUrl),
                                            new SqlParameter("@StartDate", model.StartDate),
                                            new SqlParameter("@EndDate", model.EndDate),
                                            //new SqlParameter("@CreateDate",model.CreateDate),
                                            new SqlParameter("@Status",model.Status),
                                            new SqlParameter("@IsDefaultCampaign",model.IsDefaultCampaign),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                          
                                    };

            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_DonationCampaignInsertUpdate", parameters);
                ReturnResult = Convert.ToInt32(parameters[11].Value);
              
                return myDataSet;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationCompaignData", "AddOrEdit");
                ReturnResult = -1;
                return null;

            }

        }
        public int Delete(DonationCompaign model)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@CampaignId",model.EncryptedCampaignId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                           };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_CampaignDeleteNew]", parameters);
                returnResult = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "CampaignData", "Delete");
            }
            finally
            {
            }
            return returnResult;
        }
        public DataSet Select(DonationCompaign model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@CampaignId",model.CampaignId ?? 0),
                                           new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_DonationCampaignSelectId", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationCompaignData", "Select");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        public DataSet SelectAll(DonationCompaign model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           
                                          // new SqlParameter("@CampaignTitle",model.CampaignTitle??string.Empty),
                                           new SqlParameter("@search",model.Search??string.Empty),
                                           new SqlParameter("@CampaignNumber",0),
                                           new SqlParameter("@StartDate",model.StartDate??null),
                                           new SqlParameter("@EndDate",model.EndDate??null),
                                           new SqlParameter("@Status",model.Status??string.Empty),
                                           new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_DonationCampaignSelectAll", parameters);
                ReturnResult = Convert.ToInt32(parameters[7].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DonationCompaignData", "SelectAll");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }

    }
}
