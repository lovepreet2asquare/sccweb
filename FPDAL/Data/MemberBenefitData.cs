﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class MemberBenefitData : ConnectionObject
    {
        public static DataSet MemberBenefitSelect(MemberBenefitRequest request, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                        new SqlParameter("@UserId",request.FollowerId),
                                        new SqlParameter("@SessionToken",request.SessionToken)
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppMemberBenefitsSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MemberBenefitData", "MemberBenefitSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }

    }
}
