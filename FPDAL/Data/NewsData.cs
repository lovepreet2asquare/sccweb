﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Data;
using System.Data.SqlClient;

namespace FPDAL.Data
{
    public class NewsData : ConnectionObject
    {
        public DataSet NewsSelectAll(NewsModel model)
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken),
                                            new SqlParameter("@DateFrom",model.DateFrom),
                                            new SqlParameter("@DateTo",model.DateTo),
                                            new SqlParameter("@UserId",model.SelectedUserId),
                                            new SqlParameter("@IsView",model.IsView??"1"),
                                            new SqlParameter("@Status",model.Status=="0"||model.Status=="-1"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_NewsSelectAll", parameters);
                myInt = Convert.ToInt32(parameters[7].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsData", "NewsSelectAll");
                return ds;
            }
            finally
            {
            }
        }

        public DataSet SelectById(NewsModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
           // model.NewsId = 188;
            SqlParameter[] parameters ={
                                            new SqlParameter("@NewsId",model.NewsId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken??"O6U3E0L96COJ9MX0A1FW"),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_NewsSelectById", parameters);
                ReturnResult = Convert.ToInt32(parameters[2].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        public DataSet AddorEdit(NewsModel model, DataTable dt, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@NewsId",model.NewsId),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@Title",model.Title??string.Empty),
                                            new SqlParameter("@Content",model.Content??string.Empty),
                                            new SqlParameter("@FP_NewsImageType",dt),
                                            new SqlParameter("@Status",model.Status),
                                            new SqlParameter("@PublishDate",model.PublishDate),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@Timezone",model.TimeZone),
                                            new SqlParameter("@IsDonate",model.IsDonate),
                                            new SqlParameter("@DonateURL",model.URL),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_NewsUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[11].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsData", "AddorEdit");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }

        public int DeleteNewsImage(ImageListModel model,string SessionToken)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@ImageId",model.ImageId),
                                            new SqlParameter("@SessionToken",SessionToken),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
                object obj = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_NewsImageDelete]", parameters);
                returnResult = Convert.ToInt32(parameters[2].Value);
                
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "NewsData", "DeleteNewsImage");
            }

            return returnResult;
        }

        public int DeleteNews(NewsModel model)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@NewsId",model.NewsId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_NewsDelete]", parameters);
                returnResult = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "NewsData", "DeleteNews");
            }
            finally
            { }
            return returnResult;
        }

        public int Archive(NewsModel model)
        {

            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@NewsId",model.NewsId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_NewsArchive", parameters);
                ReturnResult = Convert.ToInt32(obj);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsData", "Archive");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally

            {
            }
        }

        public DataSet ReadMore(NewsModel model, out Int32 ReturnResult)
        {

            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@NewsId",model.NewsId),
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_ReadNewsById]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsData", "ReadMore");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }

        }
        public DataSet PublishNSendNotification(out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@CurrentDate",null),

                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_NewsCronJob", parameters);
                ReturnResult = 1;
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsData", "PublishNSendNotification");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }

        public Int32 Updatenewnotification(string EncryptedUserId)
        {
            Int32 myInt = -1;
            DataSet ds = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@Isread",1),
                                            new SqlParameter("@UserId",EncryptedUserId),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_NewsNotificationUpsert", parameters);
                if (ds != null && ds.Tables.Count > 0)
                {
                    myInt = Convert.ToInt32(ds.Tables[0].Rows[0]["Column1"]);
                }
                return myInt;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsData", "Updatenewnotification");
                return -1;
            }
            finally
            {
            }
        }
    }
}
