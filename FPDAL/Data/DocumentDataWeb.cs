﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class DocumentDataWeb:ConnectionObject
    {
        public DataSet Add(DocumentModelWeb model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                           new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.EncryptedSessionToken),
                                            new SqlParameter("@DocumentPath",model.DocumentPath),
                                            new SqlParameter("@DocumentName",model.DocumentName),
                                            new SqlParameter("@DocumentTitle",model.DocumentTitle),
                                            new SqlParameter("@ThumbnailPath",model.ThumbnailPath),
                                            new SqlParameter("@DocumentId",model.DocumentId),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_DocumentUpsert]", parameters);
                ReturnResult = Convert.ToInt32(parameters[7].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentData", "Add");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }
        public DataSet SelectAll(DocumentModelWeb model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.EncryptedSessionToken ),
                                            new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_DocumentSelectAll]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentData", "SelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }
        public DataSet LogSelectAll(DocumentModelWeb model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.EncryptedSessionToken ),
                                            new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_DocumentLogSelectAll", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentData", "LogSelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }
        public int Delete(DocumentModelWeb model)
        {
            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@DocumentId",model.DocumentId) ,
                                            new SqlParameter("@SessionToken",model.EncryptedSessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_DocumentDelete]", parameters);
                ReturnResult = Convert.ToInt32(obj);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentData", "Delete");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally

            {
            }
        }
    }
}
