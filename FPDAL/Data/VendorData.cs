﻿
using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class VendorData : ConnectionObject
    {
        public static DataSet DirectorySelectBySearch(DirectorySearchRequest request, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = -2;
            SqlParameter[] parameters ={
                                        new SqlParameter("@FollowerId",request.FollowerId),
                                        new SqlParameter("@SessionToken",request.SessionToken),
                                        new SqlParameter("@Search",request.Search??string.Empty),
                                        new SqlParameter("@StartIndex",request.StartIndex??string.Empty),
                                        new SqlParameter("@EndIndex",request.EndIndex??string.Empty),

                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppDirectorySelectByName", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryData", "DirectorySelectBySearch");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }

        public static DataSet DirectorySelect(DirectoryRequest request, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = -2;
            SqlParameter[] parameters ={
                                        new SqlParameter("@FollowerId",request.FollowerId),
                                        new SqlParameter("@SessionToken",request.SessionToken),
                                        new SqlParameter("@StartIndex",request.StartIndex??string.Empty),
                                        new SqlParameter("@EndIndex",request.EndIndex??string.Empty),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppDirectorySelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryData", "DirectorySelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }

        public static DataSet GetVendorsInfo(VendorRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppVendorSelect", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "VendorData", "GetVendorsInfo");
            }

            return ds;
        }

        public static DataSet GetBoardMemberInfo(VendorRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_SelectBoardMember", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "VendorData", "GetVendorsInfo");
            }

            return ds;
        }
        public static DataSet GetSSCChapterList(VendorRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppSelectSscChapter", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "VendorData", "GetVendorsInfo");
            }

            return ds;
        }
        public static DataSet ReferVendorInsert(ReferVendorInsertRequest request)
        {
            //int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@BusinessName",request.BusinessName),
                                            new SqlParameter("@BusinessWebsite",request.BusinessWebsite),
                                            new SqlParameter("@Category",request.CategoryId),
                                            new SqlParameter("@EstablishedYear",request.EstablishedYear),
                                            new SqlParameter("@PersonName",request.PersonName),
                                            new SqlParameter("@BusinessServiceType",request.BusinessServiceType),
                                            new SqlParameter("@BusinessDescription",request.BusinessDescription),
                                            new SqlParameter("@Country",request.CountryId),
                                            new SqlParameter("@Address1",request.Address1),
                                            new SqlParameter("@Address2",request.Address2),
                                            new SqlParameter("@City",request.City),
                                            new SqlParameter("@State",request.StateId),
                                            new SqlParameter("@ZipCode",request.ZipCode),
                                            new SqlParameter("@BusinessEmail",request.BusinessEmail),
                                            new SqlParameter("@BusinessPhone",request.BusinessPhone),
                                           

                };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppReferVendorInsert", parameters);

            }
            catch (Exception ex)
            {
                //returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "SendFriendInvitations");
            }

            return ds;
        }
        public static DataSet VendorSignupInsert(VendorSignupInsertRequest request)
        {
            //int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@BusinessName",request.BusinessName),
                                            new SqlParameter("@BusinessWebsite",request.BusinessWebsite),
                                            new SqlParameter("@Category",request.CategoryId),
                                            new SqlParameter("@EstablishedYear",request.EstablishedYear),
                                            new SqlParameter("@BusinessServiceType",request.BusinessServiceType),
                                            new SqlParameter("@BusinessDescription",request.BusinessDescription),
                                            new SqlParameter("@Country",request.CountryId),
                                            new SqlParameter("@Address1",request.Address1),
                                            new SqlParameter("@Address2",request.Address2),
                                            new SqlParameter("@City",request.City),
                                            new SqlParameter("@State",request.StateId),
                                            new SqlParameter("@ZipCode",request.ZipCode),
                                            new SqlParameter("@BusinessEmail",request.BusinessEmail),
                                            new SqlParameter("@BusinessPhone",request.BusinessPhone),


                };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppVendorSignupInsert", parameters);

            }
            catch (Exception ex)
            {
                //returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "SendFriendInvitations");
            }

            return ds;
        }

        public static DataSet offerLikeDislikeInsert(OfferLikeDisliketRequest request)
        {
            //int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@DealId",request.DealId),
                                            new SqlParameter("@IsLike",request.IsLike),
                                            


                };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_Appofferlikedislikeinsert", parameters);

            }
            catch (Exception ex)
            {
                //returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "SendFriendInvitations");
            }

            return ds;
        }

        public static DataSet GetPreferredVendorCategories(VendorRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_AppSelectPreferredVendorCategories", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "VendorData", "GetVendorsInfo");
            }

            return ds;
        }
        public static DataSet GetPackagesList(VendorRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppSelectPackages", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "VendorData", "GetVendorsInfo");
            }

            return ds;
        }
        public static DataSet GetofferandDealdetail(APIDealDetailRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@ServiceId",request.ServiceId),
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_AppSelectOfferAndDealsDetail", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "VendorData", "GetVendorsInfo");
            }

            return ds;
        }

        public static DataSet GetofferanddealsList(PreferredVendorRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@CategoryId",request.CategoryId),
                                              new SqlParameter("@Latitude",request.Latitude),
                                            new SqlParameter("@Longitude",request.Longitude),
                                            new SqlParameter("@Radius",request.Radius),
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_AppSelectOfferAndDeals", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "VendorData", "GetVendorsInfo");
            }

            return ds;
        }
        public static DataSet GetSccPreferredVendorsList(PreferredVendorRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@CategoryId",request.CategoryId),
                                            new SqlParameter("@Latitude",request.Latitude),
                                            new SqlParameter("@Longitude",request.Longitude),
                                            new SqlParameter("@Radius",request.Radius),
                                            
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_AppSelectSCCPreferredVendor", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "VendorData", "GetVendorsInfo");
            }

            return ds;
        }
        public static DataSet GetPreferredVendorsList(PreferredVendorRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@CategoryId",request.CategoryId),
                                            new SqlParameter("@Latitude",request.Latitude),
                                            new SqlParameter("@Longitude",request.Longitude),
                                            new SqlParameter("@Radius",request.Radius),
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_AppSelectPreferredVendor", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "VendorData", "GetVendorsInfo");
            }

            return ds;
        }

    }
}
