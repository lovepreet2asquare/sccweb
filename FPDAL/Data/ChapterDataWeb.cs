﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Infotech.ClassLibrary;
using FPModels.Models;

namespace FPDAL.Data
{
    public class ChapterDataWeb : ConnectionObject
    {
        public DataSet SelectAll(ChapterModelWeb chapterModel)
        {
            //int myint = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters = {
                new SqlParameter("@SessionToken",chapterModel.SessionToken),
                new SqlParameter("@Search",String.IsNullOrEmpty(chapterModel.Search)?string.Empty:chapterModel.Search)
            };
            try
            {
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_SscChapterSelectAll", parameters);
               // myint = Convert.ToInt32(parameters[1].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChapterData", "SelectAll");
                return ds;
            }
            finally
            {

            }
        }

        public DataSet SelectById(ChapterModelWeb model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@ChapterId",model.ChapterId),
                                            new SqlParameter("@loggeduserid",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_SscChapterSelectById", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChapterData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }


        public DataSet AddorEdit(ChapterModelWeb model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@loggeduserid",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@ChapterId",model.ChapterId),
                                            new SqlParameter("@Title",model.Title),
                                            new SqlParameter("@Location",model.Location),
                                            new SqlParameter("@ImagePath",model.LogoPath),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_SscChapterUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[6].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChapterData", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }

        public int Delete(ChapterModelWeb model)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@ChapterId",model.ChapterId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_SscChapterDelete]", parameters);
                returnResult = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "ChapterData", "Delete");
            }
            finally
            { }
            return returnResult;
        }
    }
}
