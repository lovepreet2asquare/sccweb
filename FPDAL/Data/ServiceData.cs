﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Infotech.ClassLibrary;
using FPModels.Models;

namespace FPDAL.Data
{
    public class ServiceData : ConnectionObject
    {
        public DataSet SelectAll(ServiceModel model)
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@BusinessId",model.BusinessId),
                                            //new SqlParameter("@LoginUserId",model.UserId),
                                            new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_ServiceSelectAll]", parameters);
                myInt = Convert.ToInt32(parameters[4].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ServiceData", "ServiceSelectAll");
                return ds;
            }
            finally
            {
            }
        }

        public DataSet AddorEdit(ServiceModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@ServiceId",model.ServiceId),
                                            new SqlParameter("@BusinessId",model.BusinessId),
                                            new SqlParameter("@ServiceType",model.ServiceType),
                                            new SqlParameter("@PeriodFrom",model.PeriodFrom),
                                            new SqlParameter("@PeriodTo",model.PeriodTo),
                                            new SqlParameter("@OffersTitle",model.OffersTitle),
                                            new SqlParameter("@OffersTerms",model.OffersTerms),
                                            new SqlParameter("@PaymentMethod",model.PaymentMethod),
                                            new SqlParameter("@PaymentNotes",model.PaymentNotes),
                                            new SqlParameter("@Status",model.Status),
                                            new SqlParameter("@DealsAmounts",model.DealsAmounts),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_ServiceInsertUpdate]", parameters);
                ReturnResult = Convert.ToInt32(parameters[13].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ServiceData", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }

        public DataSet SelectById(ServiceModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@UserId",model.UserId) ,
                                            new SqlParameter("@ServiceId",model.ServiceId) ,
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_ServiceSelectById]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ServiceData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally


            {
            }
        }
        public DataSet SelectDetailById(ServiceModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@UserId",model.UserId) ,
                                            new SqlParameter("@ServiceId",model.ServiceId) ,
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_ServiceDetailSelectById]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ServiceData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally


            {
            }
        }

        public DataSet ServiceTypeSelectAll(ServiceModel model)
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@UserId", model.UserId ),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_ServiceTypeSelectALl]", parameters);
                myInt = Convert.ToInt32(parameters[1].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ServiceData", "ServiceTypeSelectAll");
                return ds;
            }
            finally
            {
            }
        }

        public int Delete(ServiceModel model)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@ServiceId",model.ServiceId),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),

                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_ServiceDelete]", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "ServiceData", "Delete");
            }

            return returnResult;
        }
    }
}
