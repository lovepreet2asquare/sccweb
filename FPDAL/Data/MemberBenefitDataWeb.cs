﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Infotech.ClassLibrary;
using FPModels.Models;

namespace FPDAL.Data
{
    public class MemberBenefitDataWeb : ConnectionObject
    {
        public DataSet SelectAll(MemberBenefitModelWeb model)
        {
            Int32 myInt = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.SearchText)?string.Empty:model.SearchText),
                                            new SqlParameter("@ReturnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_MemberBenefitsSelectAllWeb", parameters);
                myInt = Convert.ToInt32(parameters[3].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MemberBenefitData", "SelectAll");
                return ds;
            }
            finally
            {
            }
        }

        public DataSet SelectById(MemberBenefitModelWeb model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@MemberBenefitId",model.MemberBenefitId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_MemberBenefitsSelectByIdWeb", parameters);
                ReturnResult = Convert.ToInt32(parameters[2].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MemberBenefitData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public DataSet DetailById(MemberBenefitModelWeb model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@MemberBenefitId",model.MemberBenefitId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_MemberBenefitsDetailByIdWeb", parameters);
                ReturnResult = Convert.ToInt32(parameters[2].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MemberBenefitData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public DataSet AddorEdit(MemberBenefitModelWeb model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@MemberBenefitId",model.MemberBenefitId),
                                            new SqlParameter("@Title",model.Title??string.Empty),
                                            new SqlParameter("@AddedBy",model.AddedBy),
                                            new SqlParameter("@AddedByDate",model.AddedByDate),
                                            new SqlParameter("@Description",model.Descriptiion),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_MemberBenefitsUpsertWeb", parameters);
                ReturnResult = Convert.ToInt32(parameters[7].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MemberBenefitData", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally
            {
            }
        }

        public int Delete(MemberBenefitModelWeb model)
        {
            
            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@MemberBenefitId",model.MemberBenefitId)

                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_MemberBenefitsDeleteWeb", parameters);
                if (obj != null)
                    ReturnResult = Convert.ToInt32(obj);

                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MemberBenefitData", "Delete");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally
            {
            }
        }

    }
}
