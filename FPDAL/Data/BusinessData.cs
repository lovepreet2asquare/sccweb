﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;
using Infotech.ClassLibrary;
using System.Data;
using System.Data.SqlClient;

namespace FPDAL.Data
{
    public class BusinessData:ConnectionObject
    {
        public DataSet AddorEdit(BusinessModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@BusinessId",model.BusinessId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@BusinessName",model.BusinessName),
                                            new SqlParameter("@BusinessLogo",model.LogoPath),
                                            //new SqlParameter("@CategoryName",model.CategoryName),
                                            new SqlParameter("@BusinessType",model.BusinessType),
                                            new SqlParameter("@BusinessDescription",model.BusinessDescription),
                                            new SqlParameter("@Company",model.Company),
                                            new SqlParameter("@Address2",model.Address2),
                                            new SqlParameter("@Address1",model.Address1),
                                            new SqlParameter("@City",model.City),
                                            new SqlParameter("@State",model.StateId),
                                            new SqlParameter("@CountryId",model.CountryId),
                                            new SqlParameter("@ZipCode",model.ZipCode),
                                            new SqlParameter("@BusinessPhone",model.BusinessPhone??""),
                                            new SqlParameter("@BusinessEmail",model.BusinessEmail??""),
                                            new SqlParameter("@BusinessWebsite",model.BusinessWebsite),
                                            new SqlParameter("@EstablishedYear",model.EstablishedYear),
                                            new SqlParameter("@Name",model.Name??""),
                                            new SqlParameter("@Title ",model.Title??""),
                                            new SqlParameter("@Phone",model.Phone??""),
                                            new SqlParameter("@Email",model.Email??""),
                                            new SqlParameter("@ReferredBy",model.ReferredBy??""),
                                            new SqlParameter("@Notes",model.Notes??""),
                                            new SqlParameter("@Status",model.StatusId),
                                            new SqlParameter("@CategoryId",model.CategoryId),
                                            //new SqlParameter("@TeleIsdCode",model.TeleISDCode),
                                            //new SqlParameter("@ISDCode",model.ISDCode),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_BusinessInsertandUpdate]", parameters);
                ReturnResult = Convert.ToInt32(parameters[25].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BusinessData", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }

        public DataSet Edit(BusinessModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@BusinessId",model.BusinessId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@BusinessName",model.BusinessName),
                                            new SqlParameter("@BusinessLogo",model.LogoPath),
                                            //new SqlParameter("@CategoryName",model.CategoryName),
                                            new SqlParameter("@BusinessType",model.BusinessType),
                                            new SqlParameter("@BusinessDescription",model.BusinessDescription),
                                            new SqlParameter("@Company",model.Company),
                                            new SqlParameter("@Address2",model.Address2),
                                            new SqlParameter("@Address1",model.Address1),
                                            new SqlParameter("@City",model.City),
                                            new SqlParameter("@State",model.StateId),
                                            new SqlParameter("@ZipCode",model.ZipCode),
                                            new SqlParameter("@BusinessPhone",model.BusinessPhone??""),
                                            new SqlParameter("@BusinessEmail",model.BusinessEmail??""),
                                            new SqlParameter("@BusinessWebsite",model.BusinessWebsite),
                                            new SqlParameter("@EstablishedYear",model.EstablishedYear),
                                            new SqlParameter("@Name",model.Name??""),
                                            new SqlParameter("@Title ",model.Title??""),
                                            new SqlParameter("@Phone",model.Phone??""),
                                            new SqlParameter("@Email",model.Email??""),
                                            new SqlParameter("@ReferredBy",model.ReferredBy??""),
                                            new SqlParameter("@Notes",model.Notes??""),
                                            new SqlParameter("@Status",model.StatusId),
                                            new SqlParameter("@CategoryId",model.CategoryId),
                                            //new SqlParameter("@TeleIsdCode",model.TeleISDCode),
                                            //new SqlParameter("@ISDCode",model.ISDCode),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_BusinessUpdate]", parameters);
                ReturnResult = Convert.ToInt32(parameters[24].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BusinessData", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }
        public DataSet SelectById(BusinessModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@Userid",model.UserId) ,
                                            new SqlParameter("@BusinessId",model.BusinessId) ,
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_BusinessSelectById]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BusinessData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally


            {
            }
        }

        public DataSet SelectAll(BusinessModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@UserId", model.UserId ),
                                            new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_BusinessSelectAll]", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BusinessData", "BusinessSelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }
        public int BusinessLatLngUpdate(BusinessLatLngModel model)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@BusinessId",model.BusinessId),
                                            new SqlParameter("@Latitude",model.Latitude),
                                            new SqlParameter("@Longitude",model.Longitude),

                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_BusinessLatLngUpdate]", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "BusinessData", "Delete");
            }

            return returnResult;
        }
        public int Delete(BusinessModel model)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@BusinessId",model.BusinessId),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),

                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_BusinessDelete]", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "BusinessData", "Delete");
            }

            return returnResult;
        }

    }
}
