﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class UtilityData : ConnectionObject
    {

        #region Common_Lists
        public DataSet StatesByCountry(Int32 CountryId)
        {
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                           new SqlParameter("@CountryId",CountryId),
                                           new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_StateList]", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "UtilityData", "StatesByCountry");
                return null;
            }
            finally
            {
            }
        }
        #endregion Common_Lists
    }
}
