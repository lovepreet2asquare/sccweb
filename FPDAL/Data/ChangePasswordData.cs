﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class ChangePasswordData : ConnectionObject
    {
        public static DataSet ChangePassword(ChangePasswordModel model)
        {
            DataSet myDataSet = null;
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId??"23"),
                                            new SqlParameter("@Password",model.Password),
                                            new SqlParameter("@SessionToken",model.SessionToken??"CMGROAVH0J2Y1KXKYQOI")
                                        };



                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_ChangePassword", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChangePasswordData", "ChangePassword");
                return null;
            }
            finally
            {
            }
        }

        public static DataSet ChangePassword(UserModel model)
        {
            DataSet myDataSet = null;
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.ChangePassworMMOdel.UserId??"23"),
                                            new SqlParameter("@Password",model.ChangePassworMMOdel.Password),
                                            new SqlParameter("@OldPassWord",model.ChangePassworMMOdel.OldPassWord),
                                            new SqlParameter("@SessionToken",model.ChangePassworMMOdel.SessionToken??"CMGROAVH0J2Y1KXKYQOI")
                                        };



                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_ChangePassword", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChangePasswordData", "ChangePassword");
                return null;
            }
            finally
            {
            }
        }
        public static Int32 ReadUnRead(string Userid)
        {
            Int32 myInt = -1;
            DataSet ds = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",Userid),
                                            new System.Data.SqlClient.SqlParameter("ReturnValue", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, System.Data.DataRowVersion.Default, null),
                                        };
            try
            {
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_NotificationReadAndUnread", parameters);
                if (ds != null && ds.Tables.Count > 0)
                {
                    myInt = Convert.ToInt32(ds.Tables[0].Rows[0]["NotificationCount"]);
                }
                return myInt;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChangePasswordData", "ReadUnRead");
                return -1;
            }
            finally
            {
            }
        }
    }
}
