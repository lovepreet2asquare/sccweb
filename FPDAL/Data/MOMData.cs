﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class MOMData : ConnectionObject
    {
        public static DataSet MOMSelect(MOMRequest request, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                        new SqlParameter("@UserId",request.FollowerId),
                                        new SqlParameter("@SessionToken",request.SessionToken)
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppMOMSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MOMData", "MOMSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }

        public static int MOMLogInsert(MOMLogRequest request)
        {
            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                        new SqlParameter("@UserId",request.FollowerId),
                                        new SqlParameter("@SessionToken",request.SessionToken),
                                        new SqlParameter("@MOMId",request.MOMId)
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_MOMLogbookInsert", parameters);
                if (obj != null)
                {
                    ReturnResult = Convert.ToInt32(obj);
                }
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MOMData", "MOMSelect");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally
            {
            }
        }
    }
}
