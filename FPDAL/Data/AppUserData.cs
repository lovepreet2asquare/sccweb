﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Infotech.ClassLibrary;
using System.Data;
using FPModels.Models;
using static FPModels.Models.UserAPIModel;

namespace FPDAL.Data
{
    public class AppUserData:ConnectionObject
    {
        public static int SettingUpdate(SettingRequest model)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.FollowerId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@IsEventNotify",model.IsEventNotify),
                                            new SqlParameter("@IsnewsNotify",model.IsNewsNotify),
                                            new SqlParameter("@IsMobilePrivacy",model.IsMobilePrivacy),
                                            new SqlParameter("@IsEmailPrivacy",model.IsEmailPrivacy)
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppUserSettingUpdate", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ReturnMessage = -1;
                ApplicationLogger.LogError(ex, "NotificationApiData", "UpdateNotification");
                return ReturnMessage;
            }
        }
        public static int ProfileUpdate(UserAPIModel request, int followerId, string sessionToken)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",followerId),
                                            new SqlParameter("@SessionToken",sessionToken),
                                            new SqlParameter("@FirstName",request.FirstName),
                                            new SqlParameter("@MI",request.MI),
                                            new SqlParameter("@LastName",request.LastName),
                                            new SqlParameter("@Email",request.Email),
                                            new SqlParameter("@MobileNumber",request.MobileNumber),
                                            new SqlParameter("@ISDCode",request.ISDCode),
                                            new SqlParameter("@BirthMonth",request.BirthMonth),
                                            new SqlParameter("@BirthDayName",request.BirthDayName),
                                            new SqlParameter("@AddressId",request.AddressId),
                                            new SqlParameter("@CountryId",request.CountryId),
                                            new SqlParameter("@StateId",request.StateId),
                                            new SqlParameter("@CityName",request.CityName),
                                            new SqlParameter("@AddressLine1",request.AddressLine1),
                                            new SqlParameter("@AddressLine2",request.AddressLine2),
                                            new SqlParameter("@Zipcode", request.Zipcode),
                                            new SqlParameter("@Accupation",request.Accupation),
                                            new SqlParameter("@Latitude",request.Latitude??"0"),
                                            new SqlParameter("@Longitude",request.Longitude??"0"),
                                            new SqlParameter("@Employer",request.Employer),
                                            new SqlParameter("@ProfilePic",request.ProfilePic??"")

                };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerUpdate", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);

                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "ProfileUpdate");
            }

            return returnResult;
        }

        public static int ProfileUpdateNew(APIProfileupdateModel request, int followerId, string sessionToken)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",followerId),
                                            new SqlParameter("@SessionToken",sessionToken),
                                            new SqlParameter("@FirstName",request.FirstName),
                                            new SqlParameter("@MI",request.MI),
                                            new SqlParameter("@Email",request.Email),
                                            new SqlParameter("@LastName",request.LastName),
                                            new SqlParameter("@MobileNumber",request.MobileNumber),
                                            new SqlParameter("@ISDCode",request.ISDCode),
                                            new SqlParameter("@AddressId",request.AddressId),
                                            new SqlParameter("@CountryId",request.CountryId),
                                            new SqlParameter("@StateId",request.StateId),
                                            new SqlParameter("@CityName",request.CityName),
                                            new SqlParameter("@AddressLine1",request.AddressLine1),
                                            new SqlParameter("@AddressLine2",request.AddressLine2),
                                            new SqlParameter("@Zipcode", request.Zipcode),
                                            new SqlParameter("@Accupation",request.Accupation),
                                            new SqlParameter("@Employer",request.Employer),
                                      
                };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerUpdate1", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);

                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "ProfileUpdate");
            }

            return returnResult;
        }

        public static DataSet SignUp(SignUpAPIModel request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FirstName",request.FirstName ?? string.Empty),
                                            new SqlParameter("@MI",request.MI ?? string.Empty),
                                            new SqlParameter("@LastName",request.LastName ?? string.Empty),
                                            new SqlParameter("@Email",request.Email ?? string.Empty),
                                            new SqlParameter("@Password", request.Password ?? string.Empty),
                                            new SqlParameter("@DeviceToken",request.DeviceToken ?? string.Empty),
                                            new SqlParameter("@DeviceType",request.DeviceType ?? string.Empty),
                                            new SqlParameter("@DeviceId",request.DeviceId ?? string.Empty),
                                            new SqlParameter("@MobileNumber",request.MobileNumber ?? string.Empty),
                                            new SqlParameter("@ISDCode",request.ISDCode ?? string.Empty),
                                            new SqlParameter("@IsSkip",request.IsSkip)

                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.App_UserSignUp", parameters);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "SignUp");
            }

            return ds;
        }
        public static DataSet Login(LoginRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@Email",request.Email)
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerLogin", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "Login");
            }

            return ds;
        }
        public static int CreateAndSaveToken(LoginRequest request, int followerId, string sessionToken)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                //request.DeviceToken = "1";
                //request.DeviceType = "1";
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",followerId),
                                            new SqlParameter("@SessionToken",sessionToken),
                                            new SqlParameter("@DeviceToken",request.DeviceToken ?? ""),
                                            new SqlParameter("@DeviceType",request.DeviceType),
                                            new SqlParameter("@DeviceId",request.DeviceId ?? "")
                };
                object obj = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerTokenInsert", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "UserData", "CreateAndSaveToken");
            }

            return returnResult;
        }
        public static DataTable ForgotPassword(string EmailId, out int returnResult)
        {
            returnResult = 0;
            // int userId = 0;

            string firstName = string.Empty;

            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@Email",EmailId)

                                        };
                DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerForgotPassword", parameters);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    returnResult = 1;
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "UserData", "ForgotPassword");
            }

            return dt;
        }
        public static int VerifyEmail(string email)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@Email",email)
                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerVerifyEmail", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "ReferralCodeUpdate");
            }

            return returnResult;
        }
       // public static int ProfileUpdate(UserAPIModel request, int followerId, string sessionToken)
        //{
        //    int returnResult = 0;
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlParameter[] parameters ={

        //                                    new SqlParameter("@FollowerId",followerId),
        //                                    new SqlParameter("@SessionToken",sessionToken),
        //                                    new SqlParameter("@FirstName",request.FirstName),
        //                                    new SqlParameter("@MI",request.MI),
        //                                    new SqlParameter("@LastName",request.LastName),
        //                                    new SqlParameter("@Email",request.Email),
        //                                    new SqlParameter("@MobileNumber",request.MobileNumber),
        //                                    new SqlParameter("@ISDCode",request.ISDCode),
        //                                    new SqlParameter("@BirthMonth",request.BirthMonth),
        //                                    new SqlParameter("@BirthDayName",request.BirthDayName),
        //                                    new SqlParameter("@AddressId",request.AddressId),
        //                                    new SqlParameter("@CountryId",request.CountryId),
        //                                    new SqlParameter("@StateId",request.StateId),
        //                                    new SqlParameter("@CityName",request.CityName),
        //                                    new SqlParameter("@AddressLine1",request.AddressLine1),
        //                                    new SqlParameter("@AddressLine2",request.AddressLine2),
        //                                    new SqlParameter("@Zipcode", request.Zipcode),
        //                                    new SqlParameter("@Accupation",request.Accupation),
        //                                    new SqlParameter("@Latitude",request.Latitude??"0"),
        //                                    new SqlParameter("@Longitude",request.Longitude??"0"),
        //                                    new SqlParameter("@Employer",request.Employer),
        //                                    new SqlParameter("@ProfilePic",request.ProfilePic??"")

        //        };
        //        object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerUpdate", parameters);
        //        if (obj != null)
        //        {
        //            returnResult = Convert.ToInt32(obj);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        returnResult = -1;
        //        ApplicationLogger.LogError(ex, "AppUserData", "ProfileUpdate");
        //    }

        //    return returnResult;
        //}
        public static int ProfileUpdate(UserAPIModelNew request, int UserId, string sessionToken, out DataSet ds)
        {
            int returnResult = 0;
            ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",UserId),
                                            new SqlParameter("@SessionToken",sessionToken),
                                            //new SqlParameter("@FirstName",request.FirstName),
                                            //new SqlParameter("@MI",request.MI),
                                            //new SqlParameter("@LastName",request.LastName),
                                            //new SqlParameter("@Email",request.Email),
                                            new SqlParameter("@Password",request.Password),
                                            new SqlParameter("@MobileNumber",request.MobileNumber),
                                            new SqlParameter("@ISDCode",request.ISDCode),
                                            new SqlParameter("@NickName",request.NickName),
                                          //  new SqlParameter("@BirthMonth",request.BirthMonth),
                                          //  new SqlParameter("@BirthDayName",request.BirthDayName),
                                         //   new SqlParameter("@AddressId",request.AddressId),
                                         //   new SqlParameter("@CountryId",request.CountryId),
                                         //   new SqlParameter("@StateId",request.StateId),
                                         //   new SqlParameter("@CityName",request.CityName??""),
                                         //   new SqlParameter("@AddressLine1",request.AddressLine1??""),
                                         //   new SqlParameter("@AddressLine2",request.AddressLine2??""),
                                         //   new SqlParameter("@Zipcode", request.Zipcode??""),
                                         ////   new SqlParameter("@Accupation",request.Accupation),
                                         //   new SqlParameter("@Latitude",request.Latitude??"0"),
                                         //   new SqlParameter("@Longitude",request.Longitude??"0")
                                        //    new SqlParameter("@Employer",request.Employer),
                                            //new SqlParameter("@ProfilePic",request.ProfilePic??"")

                };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerUpdateNew", parameters);
                //if (obj != null)
                //{
                //    returnResult = Convert.ToInt32(obj);

                //}
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "UserData", "ProfileUpdate");
            }

            return returnResult;
        }

        public static int ProfilePicUpdate(ProfilePicAPIModel request)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken??""),
                                            new SqlParameter("@ProfilePic",request.ProfilePic??"")

                };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerProfilePicUpdate", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);

                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "ProfileUpdate");
            }

            return returnResult;
        }
        public static DataSet GetFollowerInfo(LogoutRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken)
                                        };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerSelect", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "GetFollowerInfo");
            }

            return ds;
        }
        public static DataSet GetCommonData(out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@ID",0)
                                        };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FL_CommonDataSelect", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "GetCommonData");
            }

            return ds;
        }
        public static int DeleteAccount(AccountDeleteRequest request)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@IsDelete",request.IsDelete)

                };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerDeleteAccount", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);

                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "DeleteAccount");
            }

            return returnResult;
        }
        public static int Logout(LogoutRequest request)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),

                };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerLogout", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);

                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "Logout");
            }

            return returnResult;
        }
        public static int ValidateSession(FPRequests request)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),

                };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_ValidateSession", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);

                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "ValidateSession");
            }

            return returnResult;
        }
        public static int ValidateEmail(FPRequests request)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),

                };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_ValidateEmail", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);

                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "ValidateEmail");
            }

            return returnResult;
        }
        public static DataSet SendFriendInvitations(FPInviteRequests request)
        {
            //int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@FollowerId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@ContactList",request.ContactList)

                };
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_InviteFriendInsert", parameters);
                
            }
            catch (Exception ex)
            {
                //returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "SendFriendInvitations");
            }

            return ds;
        }

        public static int TimezoneInsert(string Id, string StandardName, string DisplayName)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@TimeZoneId",Id),
                                            new SqlParameter("@StandardName",StandardName),
                                            new SqlParameter("@DisplayName",DisplayName)

                };
                object obj = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.FP_SystemTimeZonesInsert", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);

                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "AppUserData", "DeleteAccount");
            }

            return returnResult;
        }


        public static DataSet GetCardExpiryFollowers()
        {
            
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@CurrentDate",null)
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_CardExpiryCronJob", parameters);
                
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "AppUserData", "GetCardExpiryFollowers");
            }

            return ds;
        }

    }
}
