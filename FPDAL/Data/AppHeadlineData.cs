﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Infotech.ClassLibrary;
using System.Data;
using FPModels.Models;



namespace FPDAL.Data
{
    public class AppHeadlineData : ConnectionObject
    {
        public static DataSet HeadlinesSelect(HeadlinesRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken", request.SessionToken),
                                            new SqlParameter("@StartIndex",request.StartIndex),
                                            new SqlParameter("@EndIndex",request.EndIndex)

                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppHeadlineSelect", parameters);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "HeadlineData", "HeadlinesSelect");
            }

            return ds;
        }

        public static DataSet FavHeadlinesSelect(HeadlinesRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken", request.SessionToken),
                                            new SqlParameter("@StartIndex",request.StartIndex),
                                            new SqlParameter("@EndIndex",request.EndIndex)

                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppFavHeadlineSelect", parameters);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "HeadlineData", "FavHeadlinesSelect");
            }

            return ds;
        }

        public static DataSet HeadlineDetailSelect(HeadlineDetailRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken", request.SessionToken),
                                            new SqlParameter("@HeadlineId",request.HeadlineId)

                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppHeadlineDetail", parameters);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "HeadlineData", "HeadlineDetailSelect");
            }

            return ds;
        }

        public static int HeadlineFavUnFav(HeadlineFavRequest request)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@HeadlineId",request.HeadlineId),
                                            new SqlParameter("@IsFav",request.IsFav)
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppHeadlineFavUnFav", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ReturnMessage = -1;
                ApplicationLogger.LogError(ex, "HeadlineData", "HeadlineFavUnFav");
                return ReturnMessage;
            }
        }

        public static int HeadLineShareInsert(HeadlineDetailRequest request)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@HeadlineId",request.HeadlineId)
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppHeadLineShareInsert", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ReturnMessage = -1;
                ApplicationLogger.LogError(ex, "HeadlineData", "HeadLineShareInsert");
                return ReturnMessage;
            }
        }
    }
}
