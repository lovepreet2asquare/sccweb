﻿
using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class PreferredVendorData : ConnectionObject
    {
        public static DataSet GetPreferredVendorCategories(VendorRequest request, out int returnResult)
        {
            returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                        };

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_AppSelectPreferredVendorCategories", parameters);
                returnResult = 1;
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "VendorData", "GetVendorsInfo");
            }

            return ds;
        }
    }
}
