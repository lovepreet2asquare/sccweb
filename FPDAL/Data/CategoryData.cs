﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Infotech.ClassLibrary;
using FPModels.Models;

namespace FPDAL.Data
{
   public class CategoryData : ConnectionObject
    {
        public DataSet SelectAll(CategoryModel categoryModel)
        {
            int myint = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters = {
                new SqlParameter("@SessionToken",categoryModel.SessionToken),
                new SqlParameter("@UserId",categoryModel.UserId),
                new SqlParameter("@Search",String.IsNullOrEmpty(categoryModel.Search)?string.Empty:categoryModel.Search)
            };
            try
            {
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_CategorySelectAll]", parameters);
                myint = Convert.ToInt32(parameters[1].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "categorydata", "SelectAll");
                return ds;
            }
            finally
            {

            }
        }

        public DataSet SelectById(CategoryModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@CategoryId",model.CategoryId),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken)
                                            //new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_CategorySelectById]", parameters);
                ReturnResult = Convert.ToInt32(parameters[1].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "categorydata", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }

        public DataSet AddorEdit(CategoryModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@CategoryId",model.CategoryId),
                                            new SqlParameter("@CategoryName",model.CategoryName),
                                            //new SqlParameter("@OfferDescription",model.OfferDescription??string.Empty),
                                            //new SqlParameter("@Websitelink",model.Websitelink),
                                            //new SqlParameter("@status",model.StatusId??"0"),
                                            new SqlParameter("@ImagePath",model.LogoPath??string.Empty),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_CategoryInsertUpdate]", parameters);
                ReturnResult = Convert.ToInt32(parameters[5].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "categorydata", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }

        public int Delete(CategoryModel model)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@CategoryId",model.CategoryId),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_CategoryDelete]", parameters);
                returnResult = Convert.ToInt32(parameters[1].Value);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "categorydata", "Delete");
            }
            finally
            { }
            return returnResult;
        }
    }
}
