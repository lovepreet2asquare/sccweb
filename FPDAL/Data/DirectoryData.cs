﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class DirectoryData:ConnectionObject
    {
        public DataSet SelectAll(DirectoryModel model)
        {
            DataSet myDataSet = null;
            //ReturnResult = 0;
            Int32 myInt = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            //new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            //new SqlParameter("@StartIndex",model.StartIndex??"1"),
                                            //new SqlParameter("@EndIndex",model.EndIndex??"5"),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.KW_DirectorySelectAll", parameters);
                myInt = Convert.ToInt32(parameters[2].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryData", "DirectorySelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }
        public DataSet SelectById(DirectoryModel model)
        {
            DataSet myDataSet = null;
            //ReturnResult = 0;
            Int32 myint = 0;
            SqlParameter[] parameters ={
                                           // new SqlParameter("@UserId",model.UserId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@FollowerId",model.FollowerId)
                                            //new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.KW_DirectorySelectById", parameters);
                myint = Convert.ToInt32(parameters[1].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DirectoryData", "SelectById");
                //myint = -1;
                return null;
            }
            finally

            {
            }
        }
        public DataSet AddorEdit(DirectoryModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            //new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@FollowerId",model.FollowerId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@FirstName",model.FirstName),
                                            new SqlParameter("@MI",model.MI),
                                            new SqlParameter("@LastName",model.LastName),
                                            new SqlParameter("@Title",model.Title),
                                            new SqlParameter("@MemberTypeId",model.MemberTypeId),
                                            new SqlParameter("@AddressLine1",model.AddressLine1),
                                            new SqlParameter("@AddressLine2",model.AddressLine2),
                                            new SqlParameter("@StateId",model.StateId),
                                            new SqlParameter("@City",model.CityName),
                                            new SqlParameter("@Zipcode",model.Zip),
                                            //new SqlParameter("@CountryId",model.CountryId),
                                            new SqlParameter("@CountryId","231"),
                                            new SqlParameter("@Email",model.Email),
                                            new SqlParameter("@MobileNumber",model.Mobile),
                                            new SqlParameter("@ISDCode",model.ISDCode),
                                            //new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[FP_DirectoryUpsert]", parameters);
                //ReturnResult = Convert.ToInt32(parameters[5].Value);
                ReturnResult =1;
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "Directorydata", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }

        public int Delete(DirectoryModel model)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@LoggedUserId",model.LoggedUserId),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),

                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_DirectoryDelete]", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "DirectoryData", "Delete");
            }

            return returnResult;
        }
    }
}
