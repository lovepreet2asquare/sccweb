﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class EventApiData: ConnectionObject
    {
        public static DataSet EventDetailSelect(EventDetailRequest request, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                        new SqlParameter("@UserId",request.FollowerId),
                                        new SqlParameter("@SessionToken",request.SessionToken),
                                        new SqlParameter("@EventId",request.EventId)
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppEventDetailSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventData", "EventDetailSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public static int EventAttendeeInsert(EventAttendeeRequest request, out string ConfirmationMsgResponse, out string firstName, out string email)
        {
            email = string.Empty;
            firstName = string.Empty;
            ConfirmationMsgResponse = string.Empty;
            int ReturnMessage = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",request.FollowerId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@EventId",request.EventId),
                                            new SqlParameter("@IsAccepted",request.IsAccepted),
                                            new SqlParameter("@AttendeeCount",request.AttendeeCount)
                                            };

            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppEventAttendeesInsert", parameters);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ConfirmationMsgResponse = Convert.ToString(row["ConfirmationMsgResponse"]);
                        firstName = Convert.ToString(row["FirstName"]);
                        email = Convert.ToString(row["Email"]);
                        ReturnMessage = Convert.ToInt32(row["IsValid"]);

                    }

                }
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ReturnMessage = -1;
                ApplicationLogger.LogError(ex, "EventData", "EventAttendeeInsert");
                return ReturnMessage;
            }
        }

        public static DataSet EventSelect(EventAPIRequest request, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                        new SqlParameter("@UserId",request.FollowerId),
                                        new SqlParameter("@SessionToken",request.SessionToken)
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppEventSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventData", "EventSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public static DataSet AboutSelect(EpAboutRequest model, Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                        new SqlParameter("@SessionToken",model.SessionToken) ,
                                         new SqlParameter("@FollowerId",model.FollowerId)
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_AboutSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiData", "EventSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }

        public static DataSet OfficeLocationSelect(EpAboutRequest model, Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                        new SqlParameter("@SessionToken",model.SessionToken) ,
                                        new SqlParameter("@FollowerId",model.FollowerId)
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_OfficeLocationList", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventApiData", "EventSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
    }
}
