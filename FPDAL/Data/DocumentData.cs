﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class DocumentData : ConnectionObject
    {
        public static DataSet DocumentSelect(DocumentRequest request, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                        new SqlParameter("@UserId",request.FollowerId),
                                        new SqlParameter("@SessionToken",request.SessionToken)
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppDocumentSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentData", "DocumentSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public static int DocumentLogInsert(RestrictedDocLogRequest request)
        {
            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                        new SqlParameter("@UserId",request.FollowerId),
                                        new SqlParameter("@SessionToken",request.SessionToken),
                                        new SqlParameter("@DocumentId",request.DocumentId)
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_DocumentLogbookInsert", parameters);
                if (obj != null)
                {
                    ReturnResult = Convert.ToInt32(obj);
                }
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "DocumentData", "DocumentLogInsert");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally
            {
            }
        }

    }
}
