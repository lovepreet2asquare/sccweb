﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class BatchData : ConnectionObject
    {

        public DataSet BatchSelectAll(BatchModel model, out int returnResult)
        {

            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@DateFrom",model.DateFrom),
                                            new SqlParameter("@DateTo",model.DateTo),
                                             new SqlParameter("@Isview",model.IsView??"1"),
                                               new SqlParameter("@LoginUserId",model.UserId),
                                            //new SqlParameter("@Status",model.Status=="-1"?"":model.Status),
                                            new SqlParameter("@PurposeId",model.PurposeId??"-1"),
                                            new SqlParameter("@Search",model.Search??""),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_BatchSelectAll", parameters);
                returnResult = Convert.ToInt32(parameters[7].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BatchData", "BatchSelectAll");
                returnResult = -1;
                return ds;
            }
            finally
            {
            }
        }


        public DataSet BatchSelectTransactions(BatchModel model, out int returnResult)
        {

            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@DateFrom",model.DateFrom),
                                            new SqlParameter("@DateTo",model.DateTo),
                                            new SqlParameter("@LoginUserId",model.UserId),
                                            new SqlParameter("@TransactionType",model.TransactionType),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_BatchSelectTransactions", parameters);
                returnResult = Convert.ToInt32(parameters[5].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BatchData", "BatchSelectTransactions");
                returnResult = -1;
                return ds;
            }
            finally
            {
            }
        }
        public Int32 AddOrEdit(BatchModel model, out Int32 ReturnResult)
        {
            int myInt = -1;
            SqlParameter[] parameters ={
                                            new SqlParameter("@BatchId",model.BatchId),
                                            new SqlParameter("@PurposeId",model.PurposeId),
                                            new SqlParameter("@PurposeText",model.PurposeText),
                                            new SqlParameter("@FileTypeId",model.FileTypeId),
                                            new SqlParameter("@DateFrom",model.DateFrom),
                                            new SqlParameter("@DateTo",model.DateTo),
                                            new SqlParameter("@TransactionType",model.TransactionType),
                                            new SqlParameter("@Status",model.Status),
                                            new SqlParameter("@CreateDate",model.CreateDate),
                                            new SqlParameter("@FilePath",model.FilePath),
                                            new SqlParameter("@NoOfPayment",model.NoOfPayment),
                                            new SqlParameter("@NoOfRefund",model.NoOfRefund),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),

        };
            try
            {
                myInt = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.FP_TransactionBatchesUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[14].Value);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BatchData", "AddOrEdit");
                ReturnResult = -1;
                return myInt;

            }

        }
        public Int32 FileStatusUpdate(BatchModel model, out Int32 ReturnResult)
        {
            int myInt = -1;
            SqlParameter[] parameters ={
                                            new SqlParameter("@BatchId",model.BatchId),                                            
                                            new SqlParameter("@Status",model.Status),                                            
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),

        };
            try
            {
                myInt = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.FP_TransactionBatchesStatusUpdate", parameters);
                ReturnResult = Convert.ToInt32(parameters[4].Value);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BatchData", "FileStatusUpdate");
                ReturnResult = -1;
                return myInt;

            }

        }
        public DataSet Select(BatchModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                           new SqlParameter("@BatchId",model.BatchId) ,
                                           new SqlParameter("@LoginUserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_BatchSelectDropdowns", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BatchData", "Detail");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
    }
}
