﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class PresidentMessageWebData : ConnectionObject
    {
        public Int32 AddOrEdit(PresidentMessageWebModel model, out Int32 ReturnResult)
        {
            int myInt = -1;

            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@Description",model.Description),
                                            new SqlParameter("@ImagePath",model.ImagePath),
                                            new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),

        };
            try
            {
                myInt = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_PresidentMessageUpsert]", parameters));
                ReturnResult = Convert.ToInt32(parameters[4].Value);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PresidentMessageData", "Create");
                ReturnResult = -1;
                return myInt;

            }

        }

        public DataSet Select(PresidentMessageWebModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            //model.AboutId = 75;

            SqlParameter[] parameters ={
                                           new SqlParameter("@UserId",model.UserId) ,
                                           new SqlParameter("@SessionToken",model.SessionToken) ,
                                           new SqlParameter("@returnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                       };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_PresidentMessageDetail", parameters);
                ReturnResult = Convert.ToInt32(parameters[2].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PresidentMessageData", "Detail");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }

        public int Delete(PresidentMessageWebModel model)
        {

            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@PresidentMessage",model.PresidentMessageId),
                                            new SqlParameter("@SessionToken",model.SessionToken)
                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_PresidentMessageDelete]", parameters);
                if (obj != null)
                {
                    returnResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "PresidentMessageData", "Delete");
            }

            return returnResult;
        }
    }
}
