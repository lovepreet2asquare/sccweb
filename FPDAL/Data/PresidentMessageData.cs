﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class PresidentMessageData : ConnectionObject
    {
        public static DataSet PresidentMessageSelect(NJRequest model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                        new SqlParameter("@SessionToken",model.SessionToken) ,
                                         new SqlParameter("@FollowerId",model.FollowerId)
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppPresidentMessageSelect", parameters);
                ReturnResult = 1;
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "PresidentMessageData", "PresidentMessageSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
    }
}
