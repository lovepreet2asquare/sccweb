﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infotech.ClassLibrary;
using FPModels.Models;
using System.Data;
using System.Data.SqlClient;


namespace FPDAL.Data
{
    public class Dealdata : ConnectionObject
    {

        public DataSet AddorEdit(Dealmodel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                    new SqlParameter("@SessionToken",model.SessionToken),
                    new SqlParameter("@DealsId",model.DealsId),
                    new SqlParameter("@ServiceId",model.ServiceId),
                    new SqlParameter("@DealTitle",model.DealTitle),
                    new SqlParameter("@DealCode",model.DealCode),
                    new SqlParameter("@UserId",model.UserId),
                    new SqlParameter("@DealStatus",model.DealStatus),
                    new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),

                     };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.KW_DealsInsertUpdate", parameters);
                ReturnResult = Convert.ToInt32(parameters[7].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "Dealdata", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }
        public DataSet SelectById(Dealmodel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                     new SqlParameter("@SessionToken",model.SessionToken),
                    new SqlParameter("@UserId",model.UserId),
                    new SqlParameter("@DealsId",model.DealsId),
                    new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                    };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.KW_DealsSelectById", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "dealdata", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
        public int Delete(Dealmodel model)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                new SqlParameter("@DealId",model.DealsId),
                                new SqlParameter("@UserId",model.UserId),
                                new SqlParameter("@SessionToken",model.SessionToken),
                                //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "[dbo].[KW_dealDelete]", parameters);
                returnResult = Convert.ToInt32(parameters[1].Value);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "categorydata", "Delete");
            }
            finally
            { }
            return returnResult;
        }

    }
}
