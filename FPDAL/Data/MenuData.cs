﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class MenuData : ConnectionObject
    {
        ApplicationLogger logger = new ApplicationLogger();
        /// <summary>
        /// This function is used to get all menus from saavor database as per logged user. 
        /// It is calling store procedure "dbo.GS_MenusByUser" which is written in saavor database.
        /// </summary>
        /// <param name="UserId">Passing logged user id.</param>
        /// /// <param name="Url">Pass currenct url/action to database to verify page access actions e.g. Add/Edit/Delete/Etc. </param>
        /// <returns>Returns dataset with the Menus to MenuModel class.</returns>
        /// <exception cref="FamousPerson.Model.ExceptionModel">
        /// If any exception occured, it store into exception datatable.
        /// </exception>
        public static DataSet MenusByUser(String Url, Int32 LoggedId, String MenuId)//,string sessionToken)
        {
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",LoggedId),                                            
                                            new SqlParameter("@MenuId",MenuId),
                                            new SqlParameter("@Url", Url),
                                            //new SqlParameter("@sessionToken",sessionToken)
                                            new SqlParameter("ReturnValue", SqlDbType.Int, 4, ParameterDirection.ReturnValue, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_MenusByUser", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MenuData", "MenusByUser");
                return null;
            }
            finally
            {
            }
        }
    }
}
