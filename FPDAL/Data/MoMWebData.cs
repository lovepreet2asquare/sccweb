﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Infotech.ClassLibrary;
using FPModels.Models;

namespace FPDAL.Data
{
    public class MoMWebData : ConnectionObject
    {
        public DataSet SelectAll(MoMWebModel model, out int myInt)
        {
            myInt = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@MonthsName",model.Month??"-1"),
                                            new SqlParameter("@YearName",model.Year==0?-1:model.Year),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_MOMSelectAll", parameters);
                myInt = Convert.ToInt32(parameters[5].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MoMData", "SelectAll");
                return ds;
            }
            finally
            {
            }
        }
        public int ValidateMoM(string month, string year)
        {

            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@Month",month) ,
                                            new SqlParameter("@Year",year)
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_MOMValidate", parameters);
                ReturnResult = Convert.ToInt32(obj);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MoMData", "ValidateMoM");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally
            {
            }
        }
        public DataSet SelectById(MoMWebModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId) ,
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@MoMId",model.MoMId) ,
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_MOMSelectById", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MoMData", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public DataSet AddorEdit(MoMWebModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@MoMId",model.MoMId),
                                            new SqlParameter("@DocumentName",model.DocumentName??string.Empty),
                                            new SqlParameter("@DocumentPath",model.DocumentPath),
                                            new SqlParameter("@ThumbnailPath",model.ThumbnailPath),
                                            new SqlParameter("@MonthsName",model.Month),
                                            new SqlParameter("@YearName",model.Year),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_MoMUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[8].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MoMData", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally
            {
            }
        }

        public int Delete(MoMWebModel model)
        {

            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@MoMId",model.MoMId),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),

                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_MOMDelete", parameters);
                if (obj != null)
                    ReturnResult = Convert.ToInt32(obj);

                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "MoMData", "Delete");
                ReturnResult = -1;
                return ReturnResult;
            }
            finally
            {
            }
        }
        public DataSet LogSelectAll(MoMWebModel model, out Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.EncryptedSessionToken ),
                                            //new SqlParameter("@Status",model.Status=="0"||string.IsNullOrEmpty(model.Status)?string.Empty:model.Status),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_MOMLogSelectAll]", parameters);
                ReturnResult = Convert.ToInt32(parameters[2].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "RestriectedDocumentData", "DocumentModel SelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }
    }
}
