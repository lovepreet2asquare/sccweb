﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
  public class NewsAPIData: ConnectionObject
    {
        public static int NewsFavUnFav(NewsFavRequest request)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",request.UserId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@NewsId",request.NewsId),
                                            new SqlParameter("@IsFav",request.IsFav)
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppNewsFavUnFav", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ReturnMessage = -1;
                ApplicationLogger.LogError(ex, "HeadlineData", "NewsFavUnFav");
                return ReturnMessage;
            }
        }

        public static int NewsShareInsert(NewsDetailRequest request)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={
                                            new SqlParameter("@UserId",request.UserId),
                                            new SqlParameter("@SessionToken",request.SessionToken),
                                            new SqlParameter("@NewsId",request.NewsId)
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppNewsShareInsert", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ReturnMessage = -1;
                ApplicationLogger.LogError(ex, "NewsData", "NewsShareInsert");
                return ReturnMessage;
            }
        }
        public static DataSet FollowerNewsList(FollowerNewsRequest model, Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                       
                                        new SqlParameter("@SessionToken",model.SessionToken) ,
                                         new SqlParameter("@FollowerId",model.FollowerId) ,
                                        new SqlParameter("@StartIndex",model.StartIndex) ,
                                        new SqlParameter("@EndIndex",model.EndIndex),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerNewsList", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsAPIData", "FollowerNewsList");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public static DataSet FollowerNewsDetail(FolloerNewsDetailRequest request, Int32 ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                        new SqlParameter("@SessionToken",request.SessionToken) ,
                                         new SqlParameter("@FollowerId",request.FollowerId) ,
                                          new SqlParameter("@NewsId",request.NewsId) ,
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerNewsDetail", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NewsAPIData", "FollowerNewsDetail");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }
    }
}

