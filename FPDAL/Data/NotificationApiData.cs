﻿using FPModels.Models;
using Infotech.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDAL.Data
{
    public class NotificationApiData: ConnectionObject
    {
        public static DataSet GetNotificationSelect(NJRequest request, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={
                                        new SqlParameter("@UserId",request.FollowerId),
                                        new SqlParameter("@SessionToken",request.SessionToken)
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppNotificationSelect", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationData", "NotificationSelect");
                ReturnResult = -1;
                return null;
            }
            finally
            {
            }
        }
        public static int NotificationRead(NotificationRequest request)
        {

            int ReturnResult = 0;
            SqlParameter[] parameters ={
                                        new SqlParameter("@UserId",request.FollowerId),
                                        new SqlParameter("@SessionToken",request.SessionToken),
                                        new SqlParameter("@NotificationIds",request.NotificationIds)
                                      };
            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_AppNotificationRead", parameters);

                ReturnResult = Convert.ToInt32(obj);
                return ReturnResult;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "EventData", "EventSelect");
                //ReturnResult = -1;
                return -1;
            }
            finally
            {
            }
        }
        public static int UpdateNotification(UpdateSettingRequest model)
        {
            int ReturnMessage = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@FollowerId",model.FollowerId),
                                            new SqlParameter("@IsHeadLineNotify",model.IsHeadLineNotify),
                                            new SqlParameter("@IsEventNotify",model.IsEventNotify),
                                            new SqlParameter("@IsnewsNotify",model.IsnewsNotify)
                                            };

            try
            {
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.FP_FollowerSettingUpdate", parameters);
                if (obj != null)
                    ReturnMessage = Convert.ToInt32(obj);
                return ReturnMessage;

            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "NotificationApiData", "UpdateNotification");
                return ReturnMessage;
            }
        }
    }
}
