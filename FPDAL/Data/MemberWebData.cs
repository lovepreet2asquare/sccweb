﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Infotech.ClassLibrary;
using FPModels.Models;

namespace FPDAL.Data
{
    public class MemberWebData : ConnectionObject
    {
        public DataSet SelectAll(MemberWebModel memberModel)
        {
            //int myint = 0;
            DataSet ds = new DataSet();
            SqlParameter[] parameters = {
                new SqlParameter("@SessionToken",memberModel.SessionToken),
                new SqlParameter("@Search",String.IsNullOrEmpty(memberModel.Search)?string.Empty:memberModel.Search)
            };
            try
            {
                ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_SccMemberSelectAll", parameters);
               // myint = Convert.ToInt32(parameters[1].Value);
                return ds;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ChapterData", "SelectAll");
                return ds;
            }
            finally
            {

            }
        }

        public DataSet SelectById(MemberWebModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@MemberId",model.MemberId),
                                            new SqlParameter("@loggeduserid",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_SccMemberSelectById", parameters);
                ReturnResult = Convert.ToInt32(parameters[3].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BoardMember", "SelectById");
                ReturnResult = -1;
                return null;
            }
            finally

            {
            }
        }


        public DataSet AddorEdit(MemberWebModel model, out int ReturnResult)
        {
            DataSet myDataSet = null;
            ReturnResult = 0;
            SqlParameter[] parameters ={

                                            new SqlParameter("@loggeduserid",model.UserId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            new SqlParameter("@MemberId",model.MemberId),
                                            new SqlParameter("@Title",model.Title),
                                            new SqlParameter("@Name",model.Name),
                                            new SqlParameter("@ImagePath",model.LogoPath),
                                            new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                      };
            try
            {
                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.NJ_SccMemberUpsert", parameters);
                ReturnResult = Convert.ToInt32(parameters[6].Value);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "BoardMemberData", "AddorEdit");
                ReturnResult = -1;
                return myDataSet;
            }
            finally

            {
            }
        }

        public int Delete(MemberWebModel model)
        {
            int returnResult = 0;
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters ={
                                            new SqlParameter("@MemberId",model.MemberId),
                                            new SqlParameter("@SessionToken",model.SessionToken),
                                            //new SqlParameter("@returnResult", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
                object obj = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.[NJ_SccMemberDelete]", parameters);
                returnResult = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                returnResult = -1;
                ApplicationLogger.LogError(ex, "BoardMemberData", "Delete");
            }
            finally
            { }
            return returnResult;
        }
    }
}
