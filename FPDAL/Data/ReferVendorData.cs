﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPModels.Models;
using Infotech.ClassLibrary;
using System.Data;
using System.Data.SqlClient;

namespace FPDAL.Data
{
    public class ReferVendorData : ConnectionObject
    {
        public DataSet signupSelectAll(ReferVendorModel model)
        {
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@UserId", model.UserId ),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            //new SqlParameter("@StartIndex",model.StartIndex??"1"),
                                            //new SqlParameter("@EndIndex",model.EndIndex??"5"),
                                            //new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_signupVendorSelectAll]", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ReferVendorData", "ReferVendorSelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }
        public DataSet SelectAll(ReferVendorModel model)
        {
            DataSet myDataSet = null;
            SqlParameter[] parameters ={
                                            new SqlParameter("@SessionToken", model.SessionToken ),
                                            new SqlParameter("@UserId", model.UserId ),
                                            new SqlParameter("@Search",String.IsNullOrEmpty(model.Search)?string.Empty:model.Search),
                                            //new SqlParameter("@StartIndex",model.StartIndex??"1"),
                                            //new SqlParameter("@EndIndex",model.EndIndex??"5"),
                                            //new SqlParameter("@ReturnValue", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, System.String.Empty, DataRowVersion.Default, null),
                                        };
            try
            {

                myDataSet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.[KW_ReferVendorSelectAll]", parameters);
                return myDataSet;
            }
            catch (Exception ex)
            {
                ApplicationLogger.LogError(ex, "ReferVendorData", "ReferVendorSelectAll");
                return myDataSet;
            }
            finally
            {
            }
        }
    }
}
