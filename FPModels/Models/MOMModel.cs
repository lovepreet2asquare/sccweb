﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{

    public class MOMResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<MOMModel> MOMList { get; set; }
    }
    public class MOMRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }

    }
    public class MOMLogRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string MOMId { get; set; }

    }
    public class MOMModel
    {

        public int Year { get; set; }
        public List<MOMDetailModel> MOMDetail { get; set; }

    }
    public class MOMDetailModel
    {
        public string MOMId { get; set; }
        public string Month { get; set; }
        public string DocumentPath { get; set; }
        public string ThumbnailPath { get; set; }
        //public string DocumentName { get; set; }

    }

}
