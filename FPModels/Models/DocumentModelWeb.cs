﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class DocumentResponseWeb
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public DocumentModelWeb _documentModel { get; set; }
    }
    public class DocumentModelWeb
    {
        public string EncryptedSessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public string EncryptedDocumentId { get; set; }
        public int DocumentId { get; set; }
        public int UserId { get; set; }
        public string UniqueId { get; set; }
        public string FileName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MI { get; set; }
        public string CreateDate { get; set; }
        public string ISDCode { get; set; }
        public string Mobile { get; set; }
        public string DocumentName { get; set; }
        public string DocumentTitle { get; set; }
        public string ThumbnailPath { get; set; }
        public string DocumentPath { get; set; }
        public string Status { get; set; }
        public string Search { get; set; }
        public string NoFile { get; set; }
        [Required(ErrorMessage = "Please select Document")]
        public HttpPostedFileBase HttpPostedFile { get; set; }
        public List<DocumentModelWeb> _DocumentList { get; set; }
        public List<DocumentModelWeb> _LogBookList { get; set; }
        public List<SelectListItem> _StatusList { get; set; }

    }
}
