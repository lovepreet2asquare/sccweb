﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
     public class MemberWebModel
    {
        public int MemberId { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string LogoPath { get; set; }
        public string ImageBase64 { get; set; }
        public string SessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public int UserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedMemberId { get; set; }
        public string Search { get; set; }
        public string CreateDate { get; set; }
        public List<MemberWebModel> memberModelList { get; set; }

     }
    public class MemberResponseWeb
    {
        public int ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public MemberWebModel membermodel { get; set; }
    }
}
