﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class APIPreferredVendorCategoryResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<APIPreferredVendorCategoryModel> PreferredVendorList { get; set; }

    } 
    public class APIPreferredVendorResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<APIPreferredVendorModel> PreferredVendorList { get; set; }

    }
    public class APIOfferAndDealResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<APIOfferAndDealModel> OfferList { get; set; }

    } 
    public class APIOfferAndDealDetailResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public  APIOfferAndDealDetailModel OfferDetail { get; set; }

    }
    public class APIPreferredVendorCategoryModel
    {
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ImagePath { get; set; }
    }
    public class APIOfferAndDealDetailModel
    {
        public string BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string BusinessType { get; set; }
        public string BusinessDescription { get; set; }
        public string BusinessLogo { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CountryName { get; set; }
        public string ZipCode { get; set; }
        public string BusinessServiceType { get; set; }
        public string BusinessPhone { get; set; }
        //public string BusinessEmail { get; set; }
        public string BusinessWebsite { get; set; }
        public string OfferTitle { get; set; }
        public string OffersTerms { get; set; }
        public string ExpiryDate { get; set; }
        public string ServiceId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public List<APIDealsModel> Deals { get; set; }
    }

    public class APIDealsModel
    {
        public string DealsId { get; set; }
        public string DealTitle { get; set; }
        public string DealCode { get; set; }
        public int IsLike { get; set; }
    }
    public class APIOfferAndDealModel
    {
        public string BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string BusinessType { get; set; }
        public string BusinessDescription { get; set; }
        public string BusinessLogo { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CountryName { get; set; }
        public string ZipCode { get; set; }
        public string BusinessServiceType { get; set; }
        public string BusinessPhone { get; set; }
        //public string BusinessEmail { get; set; }
        public string BusinessWebsite { get; set; }
        public string OfferTitle { get; set; }
        public string ExpiryDate { get; set; }
        public string ServiceId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
    public class APIPreferredVendorModel
    {
        public string BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string BusinessType { get; set; }
        public string BusinessDescription { get; set; }
        public string BusinessLogo { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string BusinessServiceType { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessEmail { get; set; }
        public string BusinessWebsite { get; set; }
        public string CountryName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
    public class PreferredVendorModel
    {
    }
}
