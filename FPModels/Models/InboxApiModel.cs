﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class InboxApiModel
    {
        public int MessageId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string PublishedDate { get; set; }
        public string PublishedTime { get; set; }
        public bool IsDonate { get; set; } 
        public bool IsOpen { get; set; }
        public bool IsUpdated { get; set; }
        public string DonateURL { get; set; }
        

    }

    public class InboxApiResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<InboxApiModel> InboxApiModel { get; set; }
    }
    public class InboxDetailApiResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public InboxApiModel MessageDetail { get; set; }
    }

    public class FPInboxApiRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
    }
    public class InAppMessageDetailApiRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string MessageId { get; set; }
        
    }
}
