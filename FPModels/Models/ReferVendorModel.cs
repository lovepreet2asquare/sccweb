﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class ReferVendorModel
    {
        public int BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string BusinessWebsite { get; set; }
        public string SessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public int UserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedBusinessId { get; set; }
        public string Category { get; set; }
        public int EstablishedYear { get; set; }
        public string PersonName { get; set; }
        public string ServiceType { get; set; }
        public string BusinessDescription { get; set; }
        public string Country { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string BusinessEmail { get; set; }
        public string BusinessPhone { get; set; }

        public string Search { get; set; }
        public List<ReferVendorModel> referModelsList { get; set; }
    }

        public class ReferVendorResponse
        {
        public int ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public ReferVendorModel Categorymodel { get; set; }
        }
}
