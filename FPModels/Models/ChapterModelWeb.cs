﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
     public class ChapterModelWeb
     {
        public int ChapterId { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
        public string LogoPath { get; set; }
        public string ImageBase64 { get; set; }
        public string SessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public int UserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedChapterId { get; set; }
        public string Search { get; set; }
        public string CreateDate { get; set; }
        public List<ChapterModelWeb> ChapterModelsList { get; set; }

     }
    public class ChapterResponseWeb
    {
        public int ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public ChapterModelWeb Chaptermodel { get; set; }
    }
}
