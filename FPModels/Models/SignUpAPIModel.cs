﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class SettingRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
   
        public bool IsNewsNotify { get; set; }
        public bool IsEventNotify { get; set; }
        public bool IsMobilePrivacy { get; set; }
        public bool IsEmailPrivacy { get; set; }
    }
    public class SignUpAPIModel
    {
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public string DeviceId { get; set; }
        public bool IsSkip { get; set; }
        public string MobileNumber { get; set; }
        public string ISDCode { get; set; }
        public string ReferralCode { get; set; }

    }
}
