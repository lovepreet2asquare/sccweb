﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class MoMResponse
    {
        public int ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; }
        public MoMWebModel MinutesOfMeeting { get; set; }
    }
    public class MoMValidateResponse
    {
        public int ReturnCode { get; set; }
        public MoMValidateResponse Response { get; set; }

    }
    public class MoMWebModel
    {
        public HttpPostedFileBase file { get; set; }
        public string SessionToken { get; set; }
        public int UserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public int MoMId { get; set; }
        public string EncryptedMoMId { get; set; }
        [Required(ErrorMessage = "Please select month")]
        //[Required(ErrorMessage = "{0} is required.")]
        public string Month { get; set; }
        [Required(ErrorMessage = "Please select year")]
        public int Year { get; set; }

        public string DocumentName { get; set; }
        [Required(ErrorMessage = "Please select file")]
        public string DocumentPath { get; set; }
        public string ThumbnailPath { get; set; }
        public string Size { get; set; }

        public int UniqueId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MI { get; set; }
        public string CreateDate { get; set; }
        public string ISDCode { get; set; }
        public string Mobile { get; set; }

        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }

        public string Search { get; set; }

        public List<MoMWebModel> MinutesOfMeetings { get; set; }
    }
}
