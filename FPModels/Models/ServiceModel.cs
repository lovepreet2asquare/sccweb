﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class ServiceModel
    {
        public int BusinessId { get; set; }
        public string EncryptedBusinessId { get; set; }
        public int ServiceId { get; set; }
        public string ServiceType { get; set; }
        public string EncryptedServiceId { get; set; }
        public string EncryptedUserId { get; set; }
        public int UserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string SessionToken { get; set; }
        public string Status { get; set; }
        public string Search { get; set; }
        public int StatusId { get; set; }
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }
        public string Apps { get; set; }
        public string AppsId { get; set; }
        public string Likes { get; set; }
        public string Dislike { get; set; }
        public string Favorite { get; set; }
        public string Clicks { get; set; }
        public string Impression { get; set; }
        public string ServiceLink { get; set; }
        public string OffersTitle { get; set; }
        public string OffersTerms { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentMethodId { get; set; }
        public string PaymentNotes { get; set; }
        public string CreateDate { get; set; }
        public string CategoryName { get; set; }
        public decimal DealsAmounts { get; set; }

        public List<SelectListItem> _StatusList { get; set; }
        public List<SelectListItem> _PaymentMethodList { get; set; }
        public List<ServiceModel> _ServiceList { get; set; }

        public List<SelectListItem> ServiceList { get; set; }
        public List<SelectListItem> AppList { get; set; }
        public List<DealsModel> DealsList { get; set; }
        
    }
    public class DealsModel 
    {
        public string DealId { get; set; }
        public string DealCode { get; set; }
        public string DealTitle { get; set; }
        public string DealStatus { get; set; }
    }

    public class ServiceResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public ServiceModel _ServiceModel { get; set; }
    }
}
