﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class HeadlineAPIModel
    {
        public string HeadlineId { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public string Location { get; set; }
        public string PublishDate { get; set; }
        public string PublishTime { get; set; }
        public bool IsFavorite { get; set; }
        public string Content { get; set; }
        public string ShareLink { get; set; }
        public bool IsUpdated { get; set; }
        public bool IsDonate { get; set; }

        public string DonateURL { get; set; }
    }
    public class DocumentModel
    {
        public string FolderName { get; set; }
        public List<DocumentDetailModel> _DocumentList { get; set; }
    }
    public class DocumentDetailModel
    {
        public string DocumentId { get; set; }
        public string DocumentPath { get; set; }
        public string ThumbnailPath { get; set; }
        public string DocumentName { get; set; }

    }
    public class FPHeadlineAPIResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<HeadlineAPIModel> HeadlineList { get; set; }
    }
    public class NJResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage

    }
    public class DocumentResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<DocumentModel> Documents { get; set; }
    }
    public class FPHeadlineDetailAPIResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public HeadlineAPIModel HeadlineDetail { get; set; }
    }
    public class HeadlinesRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string StartIndex { get; set; }

        public string EndIndex { get; set; }
    }
    public class HeadlineDetailRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string HeadlineId { get; set; }
    }
    public class HeadlineFavRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string HeadlineId { get; set; }
        public bool IsFav { get; set; }
    }
    public class HeadlineModelAPI
    {
        public string EncryptedHeadlineId { get; set; }
        public bool IsFavorite { get; set; }
        public string HeadlineId { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public string PublishDate { get; set; }
        public string PublishTime { get; set; }
        public string Content { get; set; }
        public string ShareLink { get; set; }
        public bool IsUpdated { get; set; }
        public string Location { get; set; }
        public string County { get; set; }
    }
    public class HeadlineDetailModel
    {
        public bool IsFavorite { get; set; }
        public string HeadlineId { get; set; }
        public string Title { get; set; }
        public string EncryptedHeadlineId { get; set; }
        public string PublishDate { get; set; }
        public string PublishTime { get; set; }
        public string Content { get; set; }
        public string ShareLink { get; set; }
        public bool IsUpdated { get; set; }
        public string Location { get; set; }
        public string County { get; set; }
        public List<HeadlineImage> Images { get; set; }
        public List<HeadlineDocumentModel> HeadlineDocument { get; set; }
    }

    public class HeadlinesResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<HeadlineModelAPI> News { get; set; }
    }
    public class HeadlineDocumentModel
    {
        public string DocumentId { get; set; }
        public string DocumentPath { get; set; }
        public string ThumbnailPath { get; set; }
        public string DocumentName { get; set; }
    }
    public class HeadlineAPIResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public HeadlineDetailModel HeadlineDetail { get; set; }
    }
    public class HeadlineImage
    {
        public string ImagePath { get; set; }
    }
}
