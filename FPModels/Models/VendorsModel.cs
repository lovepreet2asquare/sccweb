﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class VendorsModel
    {
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public string OfferDescription { get; set; }
        public string Websitelink { get; set; }
        public string Status { get; set; }
        public string Location { get; set; }
        public string Category { get; set; }
        public string StatusId { get; set; }
        public string LogoPath { get; set; }
        public string ImageBase64 { get; set; }
        public string SessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public int UserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedVendorId { get; set; }
        public string Search { get; set; }
        public List<VendorsModel> vendorsModelsList { get; set; }
        public List<SelectListItem> _StatusList { get; set; }


    }

    public class VendorsResponse
    {
        public int ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public VendorsModel Vendorsmodel { get; set; }
    }
}
