﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class SettingApiModel
    {
        public int FollowerId { get; set; }
        public string SessionToken { get; set; }
        public bool IsHeadLineNotify { get; set; }
        public bool IsnewsNotify { get; set; }
        public bool IsEventNotify { get; set; }
    }
    public class NJRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
    }
    public class NotificationRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string NotificationIds { get; set; }

    }
    public class UpdateSettingRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public bool IsHeadLineNotify { get; set; }
        public bool IsnewsNotify { get; set; }
        public bool IsEventNotify { get; set; }
    }
}
