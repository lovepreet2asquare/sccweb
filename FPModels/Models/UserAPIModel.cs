﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class APIUserModel
    {
        public string OfficerId { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string ISDCode { get; set; }
        public string Title { get; set; }
        public string MemberType { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public string TourSwapLink { get; set; }
        public string GrievancesLink { get; set; }
        public string FormsLink { get; set; }
        public string VendorLink { get; set; }
        public string DealsLink { get; set; }
        public string StoreLink { get; set; }
        public bool IsHeadlineNotify { get; set; }
        public bool IsNewsNotify { get; set; }
        public bool IsEventNotify { get; set; }
        public bool IsVerified { get; set; }
        public bool IsMobilePrivacy { get; set; }
        public bool IsSkip { get; set; }
        public bool IsEmailPrivacy { get; set; }
        public string NickName { get; set; }


        public string ProfilePic { get; set; }

        public int MessageCount { get; set; }
        //public string CountryName { get; set; }
        //public string StateName { get; set; }
    }
    public class UserAPIModelNew
    {
        public string Password { get; set; }
        //public string FirstName { get; set; }
        //public string MI { get; set; }
        //public string LastName { get; set; }
        //public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string ISDCode { get; set; }
        //public string Title { get; set; }
        //public string MemberType { get; set; }
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        //public bool IsHeadlineNotify { get; set; }
        //public bool IsNewsNotify { get; set; }
        //public bool IsEventNotify { get; set; }
        //public bool IsVerified { get; set; }
        public string NickName { get; set; }
        //public bool IsEmailVerified { get; set; }
        //public string BirthMonth { get; set; }
        //public string BirthDayName { get; set; }
        //public string Accupation { get; set; }
        //public string Employer { get; set; }
        //public int AddressId { get; set; }
        //public string Latitude { get; set; }
        //public string Longitude { get; set; }
        //public int CountryId { get; set; }
        //public int StateId { get; set; }
        //public string CityName { get; set; }
        //public string AddressLine1 { get; set; }
        //public string AddressLine2 { get; set; }
        //public string Zipcode { get; set; }

        //public string ProfilePic { get; set; }

        //public int MessageCount { get; set; }
        //public string CountryName { get; set; }
        //public string StateName { get; set; }
    }
    public class UserAPIModel
    {

        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string MemberType { get; set; }
        public string ReferrelCode { get; set; }
        public string NickName { get; set; }
        public string SCCID { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string CityName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Zipcode { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedFollowerId { get; set; }
        public bool IsHeadlineNotify { get; set; }
        public bool IsNewsNotify { get; set; }
        public bool IsEventNotify { get; set; }
        public bool IsSkip { get; set; }
        public bool IsFree { get; set; }
        public string ISDCode { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool IsMobilePrivacy { get; set; }
        public bool IsEmailPrivacy { get; set; }
        public string BirthMonth { get; set; }
        public string BirthDayName { get; set; }
        public string Accupation { get; set; }
        public string Employer { get; set; }
        public int AddressId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string ProfilePic { get; set; }

        public int MessageCount { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
    }
    public class APIProfileupdateModel
    {
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string CityName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Zipcode { get; set; }
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        public string ISDCode { get; set; }
        public string Accupation { get; set; }
        public string Employer { get; set; }
        public int AddressId { get; set; }
       
    }
    public class ProfilePicAPIModel
    {
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        public string ProfilePic { get; set; }

    }
}
