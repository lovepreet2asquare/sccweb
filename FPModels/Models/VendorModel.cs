﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class VendorModel
    {
        public string VendorId { get; set; }
        public string VendorName { get; set; }
        public string Description { get; set; }
        public string Websitelink { get; set; }
        public string ImagePath { get; set; }
        public string Category { get; set; }
        public string Location { get; set; }

    }
    public class BoardMemberModel
    {
        public string MemberId { get; set; }
        public string MemberName { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
       

    }
    public class SSCChapterModel
    {
        public string ChapterId { get; set; }
        public string ImagePath { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
     
    } 
    public class PackageModel
    {
        public string PackageId { get; set; }
        public string PackageName { get; set; }
        public string Price { get; set; }
       

    }
    public class BoardMemberResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<BoardMemberModel> Members { get; set; }

    } 
    public class SSCChapterResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<SSCChapterModel> ChapterList { get; set; }

    }
    public class DirectoryAPIModel
    {
        public string FollowerId { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string ProfilePic { get; set; }
        public string StateName { get; set; }
        public string MobileNumber { get; set; }
        public string ISDCode { get; set; }
        public string NickName { get; set; }
        public bool IsVerified { get; set; }
        public string MemberType { get; set; }
        public bool IsMobilePrivacy { get; set; }
        public bool IsEmailPrivacy { get; set; }
    }
    public class DirectorySearchRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string Search { get; set; }
        public string StartIndex { get; set; }
        public string EndIndex { get; set; }


    }

    public class DirectoryRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string StartIndex { get; set; }
        public string EndIndex { get; set; }

    }
    public class DirectoryAPIResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<DirectoryAPIModel> Directories { get; set; }
    
    }
    public class UserPackageResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<PackageModel> PackageList { get; set; }

    }
    public class VendorResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<VendorModel> Vendors { get; set; }

    }
    public class VendorRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
    }
    public class PreferredVendorRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string CategoryId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Radius { get; set; }
      
    }
    public class APIDealDetailRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string ServiceId { get; set; }
    }
    public class OfferLikeDisliketRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string DealId { get; set; }
        public string IsLike { get; set; }
       
    }
    public class VendorSignupInsertRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string BusinessName { get; set; }
        public string BusinessWebsite { get; set; }
        public string CategoryId { get; set; }
        public string EstablishedYear { get; set; }
     //   public string PersonName { get; set; }
        public string BusinessServiceType { get; set; }
        public string BusinessDescription { get; set; }
        public string CountryId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateId { get; set; }
        public string ZipCode { get; set; }
        public string BusinessEmail { get; set; }
        public string BusinessPhone { get; set; }
    }
    public class ReferVendorInsertRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string BusinessName { get; set; }
        public string BusinessWebsite { get; set; }
        public string CategoryId { get; set; }
        public string EstablishedYear { get; set; }
        public string PersonName { get; set; }
        public string BusinessServiceType { get; set; }
        public string BusinessDescription { get; set; }
        public string CountryId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateId { get; set; }
        public string ZipCode { get; set; }
        public string BusinessEmail { get; set; }
        public string BusinessPhone { get; set; }
    }
}
