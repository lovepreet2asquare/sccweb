﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class PresidentMessageNewResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public PresidentMessageModel presidentMessageModel { get; set; }
    }
    public class PresidentMessageModel
    {
        public string ImagePath { get; set; }
        public string Content { get; set; }
    }
}
