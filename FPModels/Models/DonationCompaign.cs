﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class DonationCampaignResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public DonationCompaign CampaignModel { get; set; } // Object
    }
    public class DonationCompaign
    {
        public Nullable<int> CampaignId { get; set; }
        public string EncryptedCampaignId { get; set; }

        [Required(ErrorMessage = "Please enter the title")]
        public string CampaignTitle { get; set; }
        //[Required(ErrorMessage = "Please enter the title")]
        public string CampaignDesc { get; set; }
        //[Required(ErrorMessage = "Please enter the title")]
        public int CampaignNumber { get; set; }
        //[Required(ErrorMessage = "Please enter the title")]
        public string CampaignUrl { get; set; }
        public string CampaignWebUrl { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<SelectListItem> _StatusList { get; set; }
        public string Status { get; set; }

        public bool IsDefaultCampaign { get; set; }
        public int UserId { get; set; }
        public string EncryptedUserId { get; set; }
        public string SessionToken { get; set; }
        public string EncryptedSessionToken { get; set; }

        public string CreateDate { get; set; }
        public string ModifyDate { get; set; }
        public string Search { get; set; }

        public int TransactionCount { get; set; }
        public decimal TotalTransactionAmount { get; set; }
        public decimal TotalRefundAmount { get; set; }

        public List<DonationCompaign> Campaignlist { get; set; }
        public List<SelectListItem> StatusAIList { get; set; }

    }
}
