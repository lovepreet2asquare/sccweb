﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Web;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class BusinessResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public BusinessModel _businessModel { get; set; }
        //public InviteModel _inviteModel { get; set; }
    }

    public class BusinessLatLngModel 
    {
        public int BusinessId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
   

    }
    public class BusinessModel
    {
        public int BusinessId { get; set; }
        public string EncryptedBusinessId { get; set; }
        [Required(ErrorMessage = "please enter business name")]
        //[RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Only alphabets are allowed")]
        [RegularExpression(@"^[a-zA-Z0-9\-_.' ]*$", ErrorMessage = "Only alphanumeric and -, _ , . ,' are allowed")]
        public string BusinessName { get; set; }
        //[Required(ErrorMessage = "please enter Local")]
        public string BusinessType { get; set; }
        public string CategoryName { get; set; }
        public string CategoryId { get; set; }
        public string BusinessDescription { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        [Required(ErrorMessage = "Please select state")]
        public int StateId { get; set; }
        public string StateName { get; set; }
        [Required(ErrorMessage = "Please select Country")]
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        [Required(ErrorMessage = "Please enter city")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please enter ZIP")]
        public string ZipCode { get; set; }
        [MinLength(14, ErrorMessage = "Minimum 10 digits are required")]
        public string BusinessPhone { get; set; }
       // [Required(ErrorMessage = "please enter email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter valid email")]
        public string BusinessEmail { get; set; }
        public string BusinessWebsite { get; set; }
        public int EstablishedYear { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        [MinLength(14, ErrorMessage = "Minimum 10 digits are required")]
        public string Phone { get; set; }
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter valid email")]
        public string Email { get; set; }
        public string ReferredBy { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
        public string AppName { get; set; }
        public int Services { get; set; }
        public String LogoPath { get; set; }
        public string ImageBase64 { get; set; }

        [Required(ErrorMessage = "Please select Status")]
        public string StatusId { get; set; }


        public int PageCount { get; set; }
        public int TotalRecords { get; set; }
        public int TotalCountlist { get; set; }
        public int CurrentPageIndex { get; set; }
        public int CurrentPageIndex1 { get; set; }
        

        public string EncryptedSessionToken { get; set; }
        public string EncryptedUserId { get; set; }

        public string EncryptedLoggedId { get; set; }
        public string SessionToken { get; set; }
        //[Required(ErrorMessage = "Required")]
        public string ISDCode { get; set; }
        public string TeleISDCode { get; set; }

 
        public int UserId { get; set; }
        public int LoggedUserId { get; set; }
        public string Search { get; set; }
        public string CreateDate { get; set; }
        public string ModifyDate { get; set; }
       
        public string StartIndex { get; set; }
        public string EndIndex { get; set; }
        public List<SelectListItem> _ISDCodeList { get; set; }
      
        public List<SelectListItem> _StatusList { get; set; }
        //public List<SelectListItem> _MemberTypeList { get; set; }
        public List<SelectListItem> _CategoryList { get; set; }
        //public List<SelectListItem> _TitleList { get; set; }
        public List<SelectListItem> _StateList { get; set; }
        public List<SelectListItem> _CountryList { get; set; }
        public List<BusinessModel> _businessList { get; set; }
       // public List<BusinessModel> _LogBookList { get; set; }
       // public HttpPostedFileBase HttpPostedFile { get; set; }


        public DataTable dtImportExcel { get; set; }
        public String FilePath { get; set; }
        public int FileNotAdded { get; set; }
        public int FileId { get; set; }
        public int DuplicateCount { get; set; }
        public int InsertedCount { get; set; }
        public int UpdatedCount { get; set; }

        public int TotalCount { get; set; }
        public int PendingCount { get; set; }

        public string ErrorMessage { get; set; }
        public bool InsertOnly { get; set; }
    }
}
