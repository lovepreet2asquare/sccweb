﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class EventDetailRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string EventId { get; set; }

    }
    public class EventAttendeeRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string EventId { get; set; }
        public bool IsAccepted { get; set; }
        public int AttendeeCount { get; set; }
    }
    public class EventAPIRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }

    }
    public class EventDetailResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public EventAPIModel EventDetail { get; set; }
    }
    public class EventAPIModel
    {
        public string EventId { get; set; }
        public string Title { get; set; }
        public string EventAgenda { get; set; }
        public string Summary { get; set; }
        public string EventDate { get; set; }
        public string EventURL { get; set; }
        public string EventImage { get; set; }
        public string FileType { get; set; }
        public string InvitationType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }

        public string StateName { get; set; }
        public string CityName { get; set; }
        public string Zip { get; set; }
        public bool IsAttending { get; set; }
        public bool IsSubmited { get; set; }
        public int AttendeeCount { get; set; }
        public string EventTime { get; set; }
        public int EventDayLeft { get; set; }
        public string Fundraiser { get; set; }



    }
    public class EventApiModel
    {
       public int EventId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Location { get; set; }
        public string EventDate { get; set; }
        public string EventTime { get; set; }
        public string InvitationType { get; set; }
        public string EventDayLeft { get; set; }
    }

    public class FPEventRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        //public string UserId { get; set; }
        public string RequestDate { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
    public class EventAPIResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<EventAPIModel> Events { get; set; }
    }
    public class FPEventResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
       public List<EventApiModel> eventApiModel { get; set; }
    }

    public class AboutApiModel
    {
        //public Int32 AboutId { get; set; }
        public string ImagePath { get; set; }
        public string Content { get; set; }
        public Int32 UserId { get; set; }
    }
    public class EPAboutResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public AboutApiModel aboutApiModel { get; set; }
    }

    public class EpAboutRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
    }
    public class OfficeLocationModel
    {
        public Int32 OfficeId { get; set; }
        public string Title { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public int ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string CreateDate { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public List<OfficeLocationDayModel> officeLocationDayList { get; set; }
    }
    public class OfficeLocationDayModel
    {
        //public Int32 OfficeId { get; set; }
        public string DayTitle { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
    }
    public class FPOfficeLocationResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public List<OfficeLocationModel> officeLocationModel { get; set; }
    }
}
