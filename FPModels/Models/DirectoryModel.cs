﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class DirectoryResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public DirectoryModel _directoryModel { get; set; }
        public InviteModel _inviteModel { get; set; }
    }
    public class DirectoryModel
    {
        [Required(ErrorMessage = "please enter first name")]
        //[RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Only alphabets are allowed")]
        [RegularExpression(@"^[a-zA-Z0-9\-_.' ]*$", ErrorMessage = "Only alphanumeric and -, _ , . ,' are allowed")]
        public string FirstName { get; set; }
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Only alphabets are allowed")]
        public string MI { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9\-_.' ]*$", ErrorMessage = "Only alphanumeric and -, _ , . ,' are allowed")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "please enter email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter valid email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter Title")]
        [RegularExpression(@"^[a-zA-Z ]*$", ErrorMessage = "Only alphabets are allowed")]
        public string Title { get; set; }
        public string MemberType { get; set; }
        [MinLength(14, ErrorMessage = "Minimum 10 digits are required")]
        public string Mobile { get; set; }
        [Required(ErrorMessage = "Please select country")]
        public string County { get; set; }
        public int CountryId { get; set; }
        
        [Required(ErrorMessage = "Please select state")]
        public string State { get; set; }
        public int StateId { get; set; }
        [Required(ErrorMessage = "Please enter city")]
        public string CityName { get; set; }
        //[Required(ErrorMessage = "Please enter Address Line1")]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }

        [Required(ErrorMessage = "Please enter ZIP")]
        public string Zip { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public string SessionToken { get; set; }
        //[Required(ErrorMessage = "Required")]
        public string ISDCode { get; set; }
        public string CreateDate { get; set; }
        public string ModifyDate { get; set; }
        public int FollowerId { get; set; }
        public string EncryptedFollowerId { get; set; }
        public int UserId { get; set; }
        public int LoggedUserId { get; set; }
        public List<DirectoryModel> directoryModelsList { get; set; }



        public string Local { get; set; }

        
        
        public int PageCount { get; set; }
        public int TotalRecords { get; set; }
        public int TotalCountlist { get; set; }
        public int CurrentPageIndex { get; set; }
        public int CurrentPageIndex1 { get; set; }

        //[RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Only alphabets are allowed")]
        

        

        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter valid email")]
        public string OldEmail { get; set; }
        //[Required(ErrorMessage = "Please enter mobile number")]
        
        [MinLength(14, ErrorMessage = "Minimum 10 digits are required")]
        public string Telephone { get; set; }

        
        public string District { get; set; }
        [Required(ErrorMessage = "Please select Time Zone")]
        public string TimeZone { get; set; }
        public int TimeOffSet { get; set; }

        
        
        public string OldPasswordCompare { get; set; }
        public string OldPasswordCompare1 { get; set; }
        
        public string ProfilePic { get; set; }
     

        public string FileName { get; set; }
        public string NewAdded { get; set; }
        public string Updated { get; set; }
        public string Duplicate { get; set; }

       public string EncryptedLoggedId { get; set; }
        
        public string TeleISDCode { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [StringLength(40, ErrorMessage = "Max 40 characters")]
        [DisplayName("Password :")]
        public string Password { get; set; }
        public string OldPassword { get; set; }
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [StringLength(40, ErrorMessage = "Max 40 characters")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password")]
        [DisplayName("Confirm Password :")]
        public string ConfirmPassword { get; set; }

        public string CurrentPassword { get; set; }
       
        public string Status{ get; set; }
        public bool IsVerified{ get; set; }
        public string Search{ get; set; }
        public string Username { get; set; }

        
        public int TitleId { get; set; }
        [Required(ErrorMessage = "Please select MemberType")]
        public int MemberTypeId { get; set; }
        [Required(ErrorMessage = "Please select Status")]
        public string StatusId { get; set; }
        public String LogoPath { get; set; }
        public string ImageBase64 { get; set; }
        [Required(ErrorMessage = "Please enter UniqueID")]
        public string UniqueId { get; set; }
        public string ConDis { get; set; }
        public string BirthDay{ get; set; }
        public string DL{ get; set; }
        public string StartIndex { get; set; }
        public string EndIndex { get; set; }
        public List<SelectListItem> _ISDCodeList { get; set; }
        public List<SelectListItem> _CountryList { get; set; }
        public List<SelectListItem> _StatusList { get; set; }
        public List<SelectListItem> _MemberTypeList { get; set; }
        public List<SelectListItem> _TitleList { get; set; }
        public List<SelectListItem> _StateList { get; set; }
        public List<DirectoryModel> _DirectoryList { get; set; }
        public List<DirectoryModel> _LogBookList { get; set; }
        public HttpPostedFileBase HttpPostedFile { get; set; }


         public DataTable dtImportExcel { get; set; }
        public String FilePath { get; set; }
        public int FileNotAdded { get; set; }
        public int FileId { get; set; }
        public int DuplicateCount { get; set; }
        public int InsertedCount { get; set; }
        public int UpdatedCount { get; set; }

        public int TotalCount { get; set; }
        public int PendingCount { get; set; }

        public string ErrorMessage { get; set; }
        public bool InsertOnly { get; set; }
    }
    //public class InviteModel
    //{
    //    public int UserId { get; set; }
    //    public string FirstName { get; set; }
    //    public string Email { get; set; }
    //    public List<InviteModel> _InviteEmail { get; set; }
    //    public List<InviteModel> _InviteData { get; set; }
    //}
}
