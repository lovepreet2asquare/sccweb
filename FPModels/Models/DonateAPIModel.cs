﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{


    public class BuyPackageAPIModel
    {
        public string PackageTypeID { get; set; }
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        public string TransactionId { get; set; }
        public string PaymentMethodId { get; set; }
        public decimal Amount { get; set; }
        public int RecurringTypeId { get; set; }
        public string TransactionDate { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorText { get; set; }
        public string IsRecurring { get; set; }
        public string CurrentDateTime { get; set; }
       
    } 
    public class BuyPackageAPINewModel
    {
        public string PackageTypeID { get; set; }
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        public string PaymentMethodId { get; set; }
        public decimal Amount { get; set; }
       
      
       
    }
    public class DonateAPIModel
    {
        public string CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        public string TransactionId { get; set; }
        [Required(ErrorMessage = "Please select the payment method")]
        public string PaymentMethodId { get; set; }
        [Required(ErrorMessage = "Please enter the amount")]
        [Range(1, 11200, ErrorMessage = "Amount must be between $1 to $11200")]
        public decimal Amount { get; set; }
        public string SpouseName { get; set; }
        public int DonationTypeId { get; set; }
        public int RecurringTypeId { get; set; }
        public string TransactionDate { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorText { get; set; }
        public string IsRecurring { get; set; }
        [Required(ErrorMessage = "Please enter the  First name")]
        public string Donarfirstname { get; set; }
        [Required(ErrorMessage = "Please enter the  last name")]
        public string Donarlastname { get; set; }
        [Required(ErrorMessage = "Please enter the occupation")]
        public string Accupation { get; set; }
        [Required(ErrorMessage = "Please enter the employer")]
        public string Employer { get; set; }
        public string IsHostingEvent { get; set; }
        [Display(Name = "I am interested in hosting an event for SCC.")]
        public bool IsHostingEvent1 { get; set; }
        [Display(Name = "I want to volunteer with SCC.")]
        public bool IsVolunteer1 { get; set; }
        public string IsVolunteer { get; set; }
        public string CurrentDateTime { get; set; }
        public List<SelectListItem> _PaymentMethodList { get; set; }
        public List<SelectListItem> _ISMonthlyList { get; set; }
        public List<SelectListItem> _RecurringList { get; set; }
        public List<DonateAPIModel> _DonarDetailModel { get; set; }
        public List<DonationListModel> _DonationTypeList { get; set; }
    }
    public class DonationListModel
    {
        public string DonationTypeId { get; set; }
        public string DonationType { get; set; }
        public string Amount { get; set; }
        public string DonationText { get; set; }
    }
        public class TransactionCronjobModel
    {
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public int FollowerId { get; set; }
        public int PaymentId { get; set; }
        public string Status { get; set; }
        public decimal Amount { get; set; }

        public string ErrorCode { get; set; }
        public string ErrorText { get; set; }
        public string TransactionId { get; set; }
    }
    public class FailTransactionCronjobModel
    {
        public string PaymentMethodId { get; set; }
        public int FollowerId { get; set; }
        public int PaymentId { get; set; }
        public string Status { get; set; }
        
        public string ErrorCode { get; set; }
        public string ErrorText { get; set; }
        public string TransactionId { get; set; }
    }
}
