﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FPModels.Models.UserAPIModel;

namespace FPModels.Models
{
    public class FPResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
    }
    public class SignUpRespone
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public UserAPIModel UserDetail { get; set; }


    }
    public class ProfileUpdateAPIRespone
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public bool IsEmailVerified { get; set; }

    }

    public class ProfileUpdateAPIResponeNew
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public APIUserModel UserDetail { get; set; }

    }

    public class FPFollowerNewsResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage

        public List<NewsApiModel> newsApiModel { get; set; }
    }
    public class FPFollowerNewsDetailResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public NewsApiDetailModel newsApiDetailModel { get; set; }

    }

    public class CommonDataRespone
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public string donationURL { get; set; } // error message/any return messaage
        public List<APICountry> Countries { get; set; }
        public List<APIState> States { get; set; }
        public List<APIDonationType> DonationTypes { get; set; }

        public List<APIRecurringType> RecurringTypes { get; set; }

        public string DonateMessage1 { get; set; } // error message/any return messaage
        public string DonateMessage2 { get; set; } // error message/any return messaage


    }
    public class FPInvolvedResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
    }

}
