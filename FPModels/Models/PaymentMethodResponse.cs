﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class PaymentMethodResponse
    {
        public string ReturnCode { get; set; } //-1/0/1
                                               
        public PaymentMethodInfo PaymentMethodInfo { get; set; } // return obj

        public string ReturnMessage { get; set; } // error message/any return messaage

    }
    public class PaymentMethodsResponse
    {
        public string ReturnCode { get; set; } //-1/0/1

        public List<PaymentMethodInfo> PaymentMethods { get; set; } // return obj

        public string ReturnMessage { get; set; } // error message/any return messaage

    }
    public class DonarDetailResponse
    {
        public string ReturnCode { get; set; } //-1/0/1

        public DonateAPIModel DonarDetailModel { get; set; } // return obj

        public string ReturnMessage { get; set; } // error message/any return messaage

    }

    public class DonationDetailResponse
    {
        public string ReturnCode { get; set; } //-1/0/1

        public CustProfileInfoNew DonationDetailModel { get; set; } // return obj

        public string ReturnMessage { get; set; } // error message/any return messaage

    }

    public class PaymentMethodInfo
    {
        
        public string PaymentMethodId { get; set; }

        public string CardNumber { get; set; }
        public string CardType { get; set; }

        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentProfileId { get; set; }

        public bool IsPrimary { get; set; }
    }
}
