﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class MemberBenefitResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public List<MemberBenefitModel> MemberBenefits { get; set; }
    }
    public class MemberBenefitRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }

    }
    public class MemberBenefitModel
    {
        public string EncryptedMemberBenefitId { get; set; }
        public string MemberBenefitId { get; set; }
        public string Title { get; set; }
        public string Descriptiion { get; set; }
        public string AddedBy { get; set; }
        public string AddedByDate { get; set; }
        public string Sharelink { get; set; }

    }
}
