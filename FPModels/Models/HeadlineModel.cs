﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Web;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class HeadlineModel
    {
        public int TimeZone { get; set; }
        public int HeadLineId { get; set; }
        public string isView { get; set; }
        public string EncryptedHeadLineId { get; set; }
        public int Edit { get; set; }

        [Required(ErrorMessage = "Please enter Title")]
        public string Title { get; set; }
        public string Location { get; set; }
        public int UserTitle{ get; set; }
        public string County { get; set; }
        [Required(ErrorMessage = "Write here")]
        public string Content { get; set; }
       // [Required(ErrorMessage = "Please select image")]
        public  string ImagePath { get; set; }
        public string ImageBase64 { get; set; }
        public  string Path { get; set; }
        public string ImagePath1 { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }

        public string PublishDate { get; set; }

        [Required(ErrorMessage = "Please select file.")]
        [Display(Name = "Browse File")]
        public HttpPostedFileBase[] files { get; set; }
        
        public string PublishTime { get; set; }
        public string PublishCalDate { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string SessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public int UserId { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string Search { get; set; }
        public int ViewCount { get; set; }
        public int ShareCount { get; set; }
        

        [Required(ErrorMessage = "Please select")]
        public string Status { get; set; }
        public int Create{ get; set; }
        public string RejectReason { get; set; }
        public string Draft { get; set; }
        public string Fileds { get; set; }
        public int SelectedUserId { get; set; }
        public int SelectedStatusId { get; set; }
        public List<SelectListItem> _UserList { get; set; }
        public List<SelectListItem> _StatusList { get; set; }
        public List<SelectListItem> _StatusListforAdmin { get; set; }
        public List<ImageListModel> _HImagePathList { get; set; }
        public List<HeadlineModel> _HeadlineList { get; set; }
        public List<HeadlineDocumentWebModel> _DocumentList { get; set; }
        public string Size { get; set; }

        public string IsDonate { get; set; }
        public string URL { get; set; }
    }
    public class HeadlineDocumentWebModel
    {
        public int DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string ThumbnailPath { get; set; }
        public string DocumentPath { get; set; }
        public HttpPostedFileBase HttpPostedFileBase { get; set; }
    }

    public class HeadlineResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public HeadlineModel _HeadlineModel { get; set; }
    }
}
