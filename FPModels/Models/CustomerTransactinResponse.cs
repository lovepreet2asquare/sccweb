﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class CustomerTransactinResponse
    {
        public string ReturnCode { get; set; } //-1/0/1
        public string TransactionId { get; set; } // return obj
        public string ReturnMessage { get; set; } // error message/any return messaage

    }
    public class CronJobTransactinResponse
    {
        public string ErrorCode { get; set; } 
        public string ErrorText { get; set; } 
        public string TransactionId { get; set; } // return obj
        public string Status { get; set; } 

    }
}
