﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class PresidentMessageResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public PresidentMessageWebModel PresidentMessageModel { get; set; } // Object
    }

    public class PresidentMessageWebModel
    {
        public string EncryptedUserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string SessionToken { get; set; }
        public int UserId { get; set; }
        public int PresidentMessageId { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public string ImageBase64 { get; set; }
        public string CreateDate { get; set; }
        public string ModifyDate { get; set; }
        public string CreateTime { get; set; }
        public string ModifyTime { get; set; }
    }
}
