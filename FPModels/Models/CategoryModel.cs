﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class CategoryModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CreateDate { get; set; }
        public string ModifyDate { get; set; }
       
        public string LogoPath { get; set; }
        public string ImageBase64 { get; set; }
        public string SessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public int UserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedCategoryId { get; set; }
        public string Search { get; set; }
        public List<CategoryModel> categoryModelsList { get; set; }
        public List<SelectListItem> _StatusList { get; set; }
    }

    public class CategoryResponse
    {
        public int ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public CategoryModel Categorymodel { get; set; }
    }
}
