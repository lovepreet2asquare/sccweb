﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class Dealmodel
    {

        public int DealsId { get; set; }
        public int ServiceId { get; set; }
        public string DealTitle { get; set; }
        public string DealCode { get; set; }
        public string DealStatus { get; set; }
        public int UserId { get; set; }
        public string StatusId { get; set; }
        public string EncryptedServiceId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedVendorId { get; set; }
        public string EncryptedUserId { get; set; }
        public List<Dealmodel> dealModelsList { get; set; }
        public List<SelectListItem> _StatusList { get; set; }

        public string SessionToken { get; set; }
        public string EncryptedDealID { get; set; }
    }
    public class Dealresponse
    {
        public int ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public Dealmodel dealmodel { get; set; }
    }
}