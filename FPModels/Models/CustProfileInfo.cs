﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class CustProfileInfoNew
    {
        public string CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }

        public string CurrentDateTime { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorText { get; set; }
        public string RefId { get; set; }
        //[Required(ErrorMessage = "Please enter the occupation")]
        public string Accupation { get; set; }
        //[Required(ErrorMessage = "Please enter the employer")]
        public string Employer { get; set; }
        public string PaymentMethodId { get; set; }
        public string TransactionId { get; set; }
        public string IsRecurring { get; set; }
        [Required(ErrorMessage = "Please enter the amount")]
        [Range(1, 11200, ErrorMessage = "Amount must be between $1 to $11200")]
        public decimal Amount { get; set; }
        [Required(ErrorMessage = "Please enter the  First name")]
        public string Donarfirstname { get; set; }
        [Required(ErrorMessage = "Please enter the  last name")]
        public string Donarlastname { get; set; }
        public string SpouseName { get; set; }
        public int DonationTypeId { get; set; }
        public int RecurringTypeId { get; set; }
        [Required(ErrorMessage = "Please enter the Card Number")]
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string ResultCode { get; set; }
        public string MessageCode { get; set; }
        public string MessageText { get; set; }
        [Required(ErrorMessage = "Please enter the expiry month")]
        public string ExpiryMonth { get; set; }
        [Required(ErrorMessage = "Please enter the expiry year")]
        public string ExpiryYear { get; set; }
        [Required(ErrorMessage = "Please enter email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter valid email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter the cvv")]
        public string CardSecurity { get; set; }
        [Required(ErrorMessage = "Please enter the Card Holder Name")]
        public string CardHolderName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        [Required(ErrorMessage = "Please enter the address")]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string SuccessMessage { get; set; }
        [Required(ErrorMessage = "Please enter the city")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please enter the state")]
        public string State { get; set; }
        public string Country { get; set; }
        [Required(ErrorMessage = "Please select Country")]
        public int CountryId { get; set; }
        public string IsHostingEvent { get; set; }
        [Display(Name = "I am interested in hosting an event for SCC.")]
        public bool IsHostingEvent1 { get; set; }
        [Display(Name = "I want to volunteer with SCC.")]
        public bool IsVolunteer1 { get; set; }
        public string IsVolunteer { get; set; }
        [Required(ErrorMessage = "Please select State")]
        public int StateId { get; set; }
        [Required(ErrorMessage = "Please enter zipcode")]
        public string Zipcode { get; set; }
        public List<SelectListItem> _iSMonthlyList { get; set; }
        public List<SelectListItem> _MonthList { get; set; }
        public List<SelectListItem> _YearList { get; set; }
        public List<SelectListItem> _recurringList { get; set; }
        public List<DonationListModel> _donationTypeList { get; set; }
        public List<SelectListItem> CountryList { get; set; }
        public List<SelectListItem> _StateList { get; set; }
    }
   public class CustProfileInfo
    {
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        public string RefId { get; set; }

        public string PaymentMethodId { get; set; }

        public string CardNumber { get; set; }

        public string CardType { get; set; }
        public string ResultCode { get; set; }
        public string MessageCode { get; set; }
        public string MessageText { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }

        public string Email { get; set; }
        public string CardSecurity { get; set; }
        public string CardHolderName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string Zipcode { get; set; }
    }
    public class CustomerProfileResponse
    {
        public string ReturnCode { get; set; } //-1/0/1
        public CustProfileInfo CustomerProfile { get; set; } // return obj
        public string ReturnMessage { get; set; } // error message/any return messaage

    }
    public class CustomerProfileResponseNew
    {
        public string ReturnCode { get; set; } //-1/0/1
        public CustProfileInfoNew CustomerProfile { get; set; } // return obj
        public string ReturnMessage { get; set; } // error message/any return messaage

    }
    public class DeletePaymentMethodRequest
    {
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        public string PaymentMethodId { get; set; }
    }
}
