﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class InAppMessageResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public InAppMessageModel InAppMessageModel { get; set; } // Object
    }
    public class InAppMessageModel
    {
        public int TimeZone { get; set; }
        [Required(ErrorMessage = "Please enter the title")]
        // [MaxLength(50, ErrorMessage = "Maximum 50 words")]
        public string Title { get; set; }
        [Required(ErrorMessage = "  Please enter agenda")]
        // [MaxLength(1000,ErrorMessage = "Maximum 1000 words")]
        public string Content { get; set; }
        [Required(ErrorMessage = "Please select one option")]
        public string Time { get; set; }
        //[Required(ErrorMessage = "Please select one option")]
        public string IsDonate { get; set; }
        public string URL { get; set; }
        public string Button { get; set; }
        //[Required(ErrorMessage = "Please select radius")]
        public int? SelectedRadius { get; set; }
        public string isview { get; set; }
        public List<SelectListItem> Radius { get; set; }
        public string PublishCalDate { get; set; }
        public string Search { get; set; }
        public bool IsDonateNo { get; set; }
        public bool IsDonateYes { get; set; }
        [Required(ErrorMessage = "Please select one option")]
        public string Status { get; set; }
        public string SearchStatus { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public Int32 Shares { get; set; }
        public Int32 View { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public string SessionToken { get; set; }
        public Int32 UserId { get; set; }
        public string MessageId { get; set; }
        public string CreateTime { get; set; }
        public string CreateDate { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        //[Required(ErrorMessage = "Please select city")]
        public string CityName { get; set; }
        public string ModifyDate { get; set; }
        //[Required(ErrorMessage = "Please select Date")]
        public string PublishDate { get; set; }
        public string DateFilterText { get; set; }
        public string DateFilterSelect { get; set; }
        public List<SelectListItem> _DateFilterList { get; set; }
        public List<InAppMessageModel> _messagelist { get; set; }
        public string DateTo { get; set; }
        public string DateFrom { get; set; }

        // [Required(ErrorMessage = "Please select country")]
        public int CountryId { get; set; }
        public List<SelectListItem> _CountryList { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }

        // [Required(ErrorMessage = "Please select state")]
        public int StateId { get; set; }

        // [Required(ErrorMessage = "Please enter address line 1")]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }

        // [Required(ErrorMessage = "Please enter zipcode")]
        public string Zip { get; set; }

        public List<SelectListItem> _StateList { get; set; }

    }
}
