﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class DashboardResponse
    {
        public int ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage

        public DashboardModel DashboardModel { get; set; }
    }

    public class DashboardSearch
    {
        public String UserId { get; set; }
        public String TokenKey { get; set; }
        public String Duration { get; set; }
        public String CallType { get; set; }
    }
    public class DashboardModel
    {
        public DbTrendModel TrendModel { get; set; }
        public DbActivityModel ActivityModel { get; set; }
        public DbHeadlineModel HeadlineModel { get; set; }
        public List<DbTrendArticleModel> _TrendArticle { get; set; }
        public DbVolunteerModel VolunteerModel { get; set; }
        public DbDonationModel DonationModel { get; set; }
        public DbAppSummaryModel AppSummaryModel { get; set; }
        public DbFollowerModel FollowerModel { get; set; }
        public DbPageAccessModel PageAccessModel { get; set; }
    }
    public class GraphModel
    {
        public String Lebel { get; set; }
        public Decimal Value { get; set; }
        public Decimal Value1 { get; set; }
        public Decimal Value2 { get; set; }
    }

    public class DbTrendModel
    {
        public Int32 AppDownload { get; set; }
        public Int32 Share { get; set; }
        public Decimal Donation { get; set; }
        public String JsonTrendGraph { get; set; }
    public List<GraphModel> _TrendGraph { get; set; }
    }
    public class DbActivityModel
    {
        public Int32 NewDownload { get; set; }
        public Int32 ActiveUser { get; set; }
        public Int32 ExclusiveHeadline { get; set; }
        public Int32 News { get; set; }
    }
    public class DbHeadlineModel
    {
        public String PostTime { get; set; }
        public String HeadlineId { get; set; }
        public String Title { get; set; }
        public String Content { get; set; }

        public String ImagePath { get; set; }
        public String UserName { get; set; }
        public String UserTitle { get; set; }
        public String ProfilePic { get; set; }
        public Int32 Share { get; set; }
    }
    public class DbTrendArticleModel
    {
        public String NewsId { get; set; }
        public String Title { get; set; }
        public String Content { get; set; }
        public String PuplishDate { get; set; }
        public int ReturnCode { get; set; }
        public Int32 Share { get; set; }
        public string FilePath { get; set; }
        public Int32 Viewer { get; set; }
        public String UserName { get; set; }
        public String DurationType { get; set; }
    }
    public class DbVolunteerModel
    {
        public String Volunteer { get; set; }
        public List<GraphModel> _VolunteerGraph { get; set; }
    }
    public class DbDonationModel
    {
        public String Donation { get; set; }
        public List<GraphModel> _DonationGraph { get; set; }
    }
    public class DbAppSummaryModel
    {
        public String NewSent { get; set; }
        public String Opened { get; set; }
        public String Unopened { get; set; }
        public String Share { get; set; }
        public Decimal SuccessRate { get; set; }
    }
    public class DbFollowerModel
    {
        public String TotalFollower { get; set; }
        public String ActiveFollower { get; set; }
        public String InactiveFollower { get; set; }
        public Int32 IOS { get; set; }
        public String IOSText { get; set; }
        public Int32 Android { get; set; }
        public String AndroidText { get; set; }
    }

    public class DbPageAccessModel
    {
        public Boolean Headline { get; set; }
        public Boolean involvement { get; set; }
        public Boolean Donation { get; set; }
       
    }
}
