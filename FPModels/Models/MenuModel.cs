﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class MenuModel
    {
        public Int32 MenuId { get; set; }
        public Int32 ParentId { get; set; }
        public String Text { get; set; }
        public String Url { get; set; }
        public Int32 Level { get; set; }
        public Int32 SortOrder { get; set; }
        public Boolean NewWindow { get; set; }
        public Boolean Active { get; set; }
        public String PrefixIcon { get; set; }
        public Boolean IsTop { get; set; }


        public string Notifycount { get; set; }
        // public List<SelectListItem> _Status { get; set; }
        public List<MenuModel> _TopMenus { get; set; }
        public List<MenuModel> _LeftMenus { get; set; }
       public List<NotificationModel> _NotificationList { get; set; }
        public List<PermissionModel> _PermissionList { get; set; }
        public List<MenuCountModel> _menuCountList { get; set; }
        public NotificatiuonCount CountNotification { get; set; }
        public Int32 UserId { get; set; }
        public bool Access { get; set; } //View
        public bool Create { get; set; } //Insert
        public bool Edit { get; set; }  //Update
        public bool Delete { get; set; } //Delete
    }
    public class MenuCountModel
    {
        public int HeadlineCount { get; set; }
        public int NewsCount { get; set; }
        public int EventCount { get; set; }
        public int MessageCount { get; set; }
        public int officeCount { get; set; }

    }
    public class NotificatiuonCount
    {
        public int NotificationCount { get; set; }
    }
}
