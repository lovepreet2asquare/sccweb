﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FPModels.Models
{
    public class AboutUsResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public AboutUsModel AboutUsModel { get; set; } // Object
    }
    public class AboutUsModel
    {
        public string EncryptedUserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string SessionToken { get; set; }
        public int UserId { get; set; }
        public string AboutId { get; set; }

        public string EncryptedAboutId { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        [Required(ErrorMessage= "Please enter content")]
       // [MaxLength(5000, ErrorMessage = "Maximum 5000 words")]
        public string Content { get; set; }
        //[Required(ErrorMessage = "Please select image")]
        public string ImagePath { get; set; }
        
        public string ImageBase64 { get; set; }
        public HttpPostedFile PostedFile { get; set; }
        public string FilePath { get; set; }
        public string CreateDate { get; set; }
        public string ModifyDate { get; set; }
        public string CreateTime{ get; set; }
        public string ModifyTime{ get; set; }
        public bool Status{ get; set; }

    }
}
