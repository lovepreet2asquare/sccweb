﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class ContactResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public ContactModel ContactModel { get; set; } // Object
    }
    public class ContactModel
    {
        public string EncryptedUserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string SessionToken { get; set; }
        public int UserId { get; set; }
        public string Search { get; set; }
        public string ContactId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ISDCode { get; set; }
        public string Mobile { get; set; }
        public string Message { get; set; }
        public bool Response { get; set; }
        public string Status { get; set; }
        public List<SelectListItem> _StatusList { get; set; }
        public List<ContactModel> _Contactlist { get; set; }
    }
}
