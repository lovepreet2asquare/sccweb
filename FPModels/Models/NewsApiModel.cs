﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
   public class NewsApiModel
    {
        public int NewsId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string PublishDate { get; set; }
        public string ImagePath { get; set; }
        public string PublishTime { get; set; }
        public string EncryptedNewsId { get; set; }
        public bool IsFavorite { get; set; }
        public string ShareLink { get; set; }
        public bool IsUpdated { get; set; }
        public string Location { get; set; }
      }
    public class NewsApiDetailModel
    {
        public int NewsId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string PublishDate { get; set; }
        public string ImagePath { get; set; }
        public string PublishTime { get; set; }
        public bool IsUpdated { get; set; }
        public bool IsDonate { get; set; }
        public string ShareLink { get; set; }
        public string DonateURL { get; set; }
        

        public List<NewsImage> newsImageList { get; set; }
    }
    public class NewsImage
    {
        public string ImagePath { get; set; }
    }
    public class NewsApiDeatilResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public List<NewsApiDetailModel> _NewsApiDetailModel { get; set; }
    }
    public class NewsApiResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; }
        public List<NewsApiModel> _newsApiList { get; set; }
    }
}
