﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class NewsViewerApiModel
    {
        public Int32 FollowerId { get; set; }
        public Int32 NewsId { get; set; }
        public Int32 ShareCount { get; set; }
    }

    public class FPNewsViewerRequest
    {
        public string SessionToken { get; set; }
        public string FollowerId { get; set; }
        public string NewsId { get; set; }
    }
}
