﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class ShareAndOpenApiModel
    {
        public bool IsOpen { get; set; }
        public string ShareCount { get; set; }
    }

    public class FP_ShareAndOpenApiRequest
    {
        public string FollowerId { get; set; }
        public string SessionToken { get; set; }
        public string MessageId { get; set; }
    }

    public class FP_ShareAndOpenApiResponse
    {
        public string ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
    }
}
