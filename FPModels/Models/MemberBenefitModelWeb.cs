﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class MemberBenefitResponseWeb
    {
        public int ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public string ReturnMessage { get; set; } // error message/any return messaage
        public MemberBenefitModelWeb MemberBenefit { get; set; } // Object
    }
    public class MemberBenefitModelWeb
    {
        public string SessionToken { get; set; }
        public int UserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string EncryptedUserId { get; set; }
        public int MemberBenefitId { get; set; }
        public string EncryptedMemberBenefitId { get; set; }
        [Required(ErrorMessage = "Please enter Title")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Please enter Description")]
        public string Descriptiion { get; set; }
        [Required(ErrorMessage = "Please enter your Name")]
        public string AddedBy { get; set; }
        [Required(ErrorMessage ="Please enter Date")]
        public string AddedByDate { get; set; }
        public string SearchText { get; set; }

        public List<MemberBenefitModelWeb> MemberBenefits { get; set; }

    }
}
