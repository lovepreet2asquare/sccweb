﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPModels.Models
{
    public class CampaignResponse
    {
        public Int32 ReturnCode { get; set; } //-1:Error/0:missing or validation /1:success
        public String ReturnMessage { get; set; } // error message/any return messaage
        public CampaignModel CampaignModel { get; set; } // Object
    }
    public class CampaignModel
    {
        [Required(ErrorMessage = "Please enter the title")]
        public string Title { get; set; }

        [Required()]
        //[RegularExpression("^[0-9]*$", ErrorMessage = "Only numbers")]
        public string ISDCode { get; set; }
        public List<SelectListItem> _ISDCodeList { get; set; }


        //[DataType(DataType.PhoneNumber)]
        [MinLength(14,ErrorMessage ="minimum 10 digits are required")]
        [Required(ErrorMessage = "Please enter Phone")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "null")]
        public string Phone { get; set; }


        //[Required(ErrorMessage = "Please enter fax")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "null")]
        public string Fax { get; set; }
        public string OfficeId { get; set; }
        public int DayId { get; set; }
        public string DayTitle { get; set; }
        //[Required(ErrorMessage ="Please select one option")]
        public bool IsChecked{ get; set; }
        public int OfficeTimeId { get; set; }
        [Required(ErrorMessage = "Please select country")]
        public int CountryId { get; set; }
        [Required(ErrorMessage = "Please select country")]
        public string CountryName { get; set; }
        public int CityId { get; set; }
        [Required(ErrorMessage = "Please enter city")]
        public string CityName { get; set; }
        [Required(ErrorMessage = "Please select State")]
        public int StateId { get; set; }
        public string StateName { get; set; }
        [Required(ErrorMessage = "Please enter address line 1")]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Address { get; set; }
        [Required(ErrorMessage= "Please enter ZIP")]
        public string ZIP { get; set; }
        public string EncryptedUserId { get; set; }
        public string EncryptedSessionToken { get; set; }
        public string SessionToken { get; set; }
        public int UserId { get; set; }
        public string CreateDate{ get; set; }
        public bool Status{ get; set; }
        public string DateFrom{ get; set; }
        public string DateFilterText { get; set; }
        public string DateTo{ get; set; }
        public string TimeFrom{ get; set; }
        public string TimeTo{ get; set; }
        public string Search{ get; set; }
        public string Class { get; set; }
        public string ModifyDate { get; set; }

        public List<CampaignModel> _CampaignTable { get; set; }
        public List<CampaignModel> _campaignlist { get; set; }
        public List<CampaignModel> _campaignTimelist { get; set; }
        public List<CampaignModel> DayList { get; set; }
        public List<SelectListItem> CountryList { get; set; }
        public List<SelectListItem> _StateList { get; set; }

    }
  
}
