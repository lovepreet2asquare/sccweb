﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPModels.Models
{
    public class CommonDataInfo
    {
    }
    public class APICountry
    {
        public string ISDCode { get; set; }
        public string CountryName { get; set; }
        public int CountryId { get; set; }
        public List<APIState> States { get; set; }
    }
    public class APIState
    {
        public string StateName { get; set; }
        public int StateId { get; set; }
    }
    public class APIRecurringType
    {
        public string RecurringType { get; set; }
        public int RecurringTypeId { get; set; }
    }
    public class APIDonationType
    {
        public string DonationType { get; set; }
        public int DonationTypeId { get; set; }
        public decimal Amount { get; set; }
    }

}
